# -*- coding: utf-8 -*-

from django.conf.urls import include
from django.urls import reverse_lazy, re_path
from django.views.generic import RedirectView
from umap import views as ls_views
from rest_framework.routers import DefaultRouter
from trapper.apps.geomap.views import api as api_views
from trapper.apps.geomap.views import deployment as deployment_views
from trapper.apps.geomap.views import location as location_views
from trapper.apps.geomap.views import map as map_views

app_name = "geomap"

router = DefaultRouter(trailing_slash=False)
router.register(r"locations", api_views.LocationViewSet, basename="api-location")

router.register(r"deployments", api_views.DeploymentViewSet, basename="api-deployment")

router.register(r"maps", api_views.MapViewSet, basename="api-map")

urlpatterns = [
    re_path(
        r"^api/locations/export/$",
        api_views.LocationTableView.as_view(),
        name="api-location-export",
    ),
    re_path(
        r"^api/deployments/export/$",
        api_views.DeploymentTableView.as_view(),
        name="api-deployment-export",
    ),
    re_path(
        r"^api/locations/geojson/$",
        api_views.LocationGeoViewSet.as_view(),
        name="api-location-geojson",
    ),
    re_path(r"^api/", include(router.urls)),
]

urlpatterns += [
    re_path(
        r"^location/bulk-update/$",
        location_views.view_location_bulk_update,
        name="location_bulk_update",
    ),
    re_path(
        r"^location/create/$",
        RedirectView.as_view(url=u"/geomap/map/?action=create"),
        name="location_create",
    ),
    re_path(
        r"^location/(?P<pk>\d+)/delete/$",
        location_views.view_location_delete,
        name="location_delete",
    ),
    re_path(
        r"^location/delete/$",
        location_views.view_location_delete,
        name="location_delete_multiple",
    ),
    re_path(
        r"^location/list/$", location_views.view_location_list, name="location_list"
    ),
    re_path(
        r"^location/import/$",
        location_views.view_location_import,
        name="location_import",
    ),
    re_path(
        r"^location/filterform/$",
        location_views.view_location_filter_form,
        name="locations_filterform",
    ),
    re_path(
        r"^location/createform/$",
        location_views.view_location_create_form,
        name="locations_createform",
    ),
    re_path(
        r"^location/(?P<pk>\d+)/editform/$",
        location_views.view_location_edit_filter_form,
        name="locations_editform",
    ),
    re_path(
        r"^location/collections/$",
        location_views.view_location_collections,
        name="location_collections",
    ),
]

urlpatterns += [
    re_path(
        r"^map/permissions/update/(?P<map_id>[\d]+)/$",
        map_views.view_update_map_permissions,
        name="map_permissions",
    ),
    re_path(r"^map/(?P<slug>[-_\w]+)_(?P<pk>\d+)$", map_views.view_map, name="map"),
    re_path(r"^map/list/$", map_views.view_map_list, name="map_list"),
    re_path(r"^map/(?P<pk>\d+)/delete/$", map_views.view_map_delete, name="map_delete"),
    re_path(r"^map/delete/$", map_views.view_map_delete, name="map_delete_multiple"),
    re_path(r"^map/$", map_views.view_new_map, name="map_view"),
    re_path(
        r"permissions/update/(?P<map_id>\d+)/$",
        map_views.view_update_map_permissions,
        name="map_permissions",
    ),
    re_path(r"^map/create/$", map_views.view_map_create, name="map_create"),
]

urlpatterns += [
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/settings/$",
        ls_views.MapUpdate.as_view(),
        name="map_update",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/permissions/$",
        ls_views.UpdateMapPermissions.as_view(),
        name="map_update_permissions",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/delete/$",
        ls_views.MapDelete.as_view(),
        name="map_delete",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/clone/$",
        ls_views.MapClone.as_view(),
        name="map_clone",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/datalayer/create/$",
        ls_views.DataLayerCreate.as_view(),
        name="datalayer_create",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/datalayer/update/(?P<pk>\d+)/$",
        ls_views.DataLayerUpdate.as_view(),
        name="datalayer_update",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/datalayer/delete/(?P<pk>\d+)/$",
        ls_views.DataLayerDelete.as_view(),
        name="datalayer_delete",
    ),
]

urlpatterns += [
    # Deployments
    re_path(
        r"^deployment/$",
        RedirectView.as_view(url=reverse_lazy("geomap:deployment_list")),
        name="deployment_index",
    ),
    re_path(
        r"^deployment/list/$",
        deployment_views.view_deployment_list,
        name="deployment_list",
    ),
    re_path(
        r"^deployment/detail/(?P<pk>\d+)/$",
        deployment_views.view_deployment_detail,
        name="deployment_detail",
    ),
    re_path(
        r"^deployment/delete/(?P<pk>\d+)/$",
        deployment_views.view_deployment_delete,
        name="deployment_delete",
    ),
    re_path(
        r"^deployment/delete/$",
        deployment_views.view_deployment_delete,
        name="deployment_delete_multiple",
    ),
    re_path(
        r"^deployment/create/$",
        deployment_views.view_deployment_create,
        name="deployment_create",
    ),
    re_path(
        r"^deployment/update/(?P<pk>\d+)/$",
        deployment_views.view_deployment_update,
        name="deployment_update",
    ),
    re_path(
        r"^deployment/bulk-update/$",
        deployment_views.view_deployment_bulk_update,
        name="deployment_bulk_update",
    ),
    re_path(
        r"^deployment/bulk-create/$",
        deployment_views.view_deployment_bulk_create,
        name="deployment_bulk_create",
    ),
    re_path(
        r"deployment/import/$",
        deployment_views.view_deployment_import,
        name="deployment_import",
    ),
]
