# -*- coding: utf-8 -*-
"""Extensions for views used in various applications"""

from braces.views import JSONResponseMixin, UserPassesTestMixin
from braces.views._access import AccessMixin
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.views import redirect_to_login
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.db.models.deletion import ProtectedError
from django.shortcuts import get_object_or_404, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import View, FormView, DetailView, UpdateView
from taggit.models import Tag

from trapper.apps.common.tools import is_request_ajax
from trapper.apps.common.tools import parse_pks


class LoginRequiredMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.

    .. note::
        This should be the left-most mixin of a view, except when
        combined with CsrfExemptMixin - which in that case should
        be the left-most mixin.
    """

    raise_exception_handler = None

    def dispatch(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            if self.raise_exception:
                handler = self.raise_exception_handler
                if callable(handler):
                    return handler(request, *args, **kwargs)
                else:
                    raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )

        return super().dispatch(request, *args, **kwargs)


class BaseListFilterDataView(JSONResponseMixin, View):
    """Base class for returning list of values that should be used
    to render form filters. Using this class removes need to
    mix angular code with django code in filters"""

    def get_filters_data(self):
        """Method used to return list of filter names and their values

        .. warning::
            To make this class usuable this method has to be overwritten"""
        return {}

    def get(self, request, *args, **kwargs):
        """Return json filter names and values that should be defined
        in :func:`get_filters_data` in inherited classes"""
        context = self.get_filters_data()
        return self.render_json_response(context_dict=context)


class HashedDetailView(DetailView):
    """Base detail view that operate on string hash instead of id/pk
    to select model instances

    By default used field is `hashcode`
    """

    hashcode_url_kwarg = "hashcode"

    def get_object(self, queryset=None):
        """Return object from given model not by `pk` but using
        configurable `hashcode_url_kwarg` which by default is `hashcode`"""
        hashcode = self.kwargs.get(self.hashcode_url_kwarg, None)
        params = {self.hashcode_url_kwarg: hashcode}
        return get_object_or_404(self.model, **params)


class BaseDeleteView(LoginRequiredMixin, View, JSONResponseMixin):
    """Base view used for single or multiple object deletion
    Handle GET method (for single deletion) or POST (for single or multiple
    deletion)
    """

    http_method_names = ["get", "post"]
    raise_exception = True
    model = None
    model_name = None
    item = None
    protected_msg_tmpl = ""
    item_name_field = "name"
    redirect_url = None

    def get_redirect_url(self):
        """Determine where to redirect"""
        return redirect(reverse(self.redirect_url))

    def get_model(self):
        return self.model

    def can_delete(self, item, user=None):
        """Delete access checker"""
        return item.can_delete(user=user)

    def delete_item(self, item):
        item.delete()

    def bulk_delete(self, queryset):
        queryset.delete()

    def filter_editable(self, queryset, user):
        if user.is_staff:
            return queryset
        if getattr(self.model, "managers", None):
            return queryset.filter(Q(owner=user) | Q(managers=user))
        else:
            return queryset.filter(owner=user)

    def get(self, request, *args, **kwargs):
        """Delete objects based on GET request
        Object can be deleted only when can_delete returns True.

        This method is used to delete single object. PK of object is passed
        via GET argument (as part of url)
        """

        model = self.get_model()
        model_name = self.model_name or self.model._meta.model_name
        user = request.user
        item = get_object_or_404(model, pk=kwargs.get("pk", None))
        self.item = item
        if self.can_delete(item=item, user=user):
            try:
                self.delete_item(item)
            except ProtectedError:
                messages.add_message(
                    self.request,
                    messages.ERROR,
                    self.protected_msg_tmpl.format(
                        name=getattr(item, self.item_name_field, "")
                    ),
                )
            else:
                messages.add_message(
                    self.request,
                    messages.SUCCESS,
                    _(f"The {model_name} has been successfully deleted."),
                )
            response = self.get_redirect_url()
        else:
            messages.add_message(
                self.request,
                messages.ERROR,
                _(f"You are not allowed to delete this {model_name}."),
            )
            if self.raise_exception:
                raise PermissionDenied
            response = self.get_redirect_url()
        return response

    def post(self, request, *args, **kwargs):
        """Delete multiple objects based on POST request. Object can be deleted
        only when request user has permission to do that.
        """
        user = request.user
        data = request.POST.get("pks", None)
        model = self.get_model()
        if data:
            values = parse_pks(pks=data)
            status = True
            candidates = model.objects.filter(pk__in=values)
            candidates = self.filter_editable(candidates, user)
            total = len(candidates)
            if not total:
                status = False
                msg = _(
                    "No items to delete (most probably you have no permission to do that)."
                )
            else:
                try:
                    self.bulk_delete(candidates)
                    msg = _(f"{total} record(s) have been successfully deleted.")
                except ProtectedError:
                    status = False
                    msg = _(
                        "Some of selected items can not be deleted because "
                        "they are referenced through a protected foreign key. "
                        "Unselect them and re-run the action."
                    )
        else:
            status = False
            msg = _("Invalid request")
        context = {"status": status, "msg": msg}
        return self.render_json_response(context)


class BaseUpdateView(
    LoginRequiredMixin, UserPassesTestMixin, UpdateView, JSONResponseMixin
):
    """
    Base update view.
    """

    template_name = None
    template_name_modal = None
    model = None
    raise_exception = True
    form_class = None
    form_class_modal = None
    item_name_field = "name"

    def test_func(self, user):
        """
        Update is available only for users that have enough permissions.
        """
        return self.get_object().can_update(user)

    def get_template_names(self):
        if is_request_ajax(self.request):
            templates = [self.template_name_modal]
        else:
            templates = [self.template_name]
        return templates

    def get_form_class(self):
        if is_request_ajax(self.request):
            if not self.form_class_modal:
                self.form_class_modal = self.form_class
            form_class = self.form_class_modal
        else:
            form_class = self.form_class
        return form_class

    def form_valid(self, form):
        success_msg = format_lazy(
            _("{model_name} <strong>{name}</strong> has been successfully updated."),
            model_name=self.model._meta.model_name.capitalize(),
            name=getattr(form.instance, self.item_name_field),
        )
        if is_request_ajax(self.request):
            form.save()
            context = {"success": True, "msg": success_msg}
            return self.render_json_response(context_dict=context)
        else:
            messages.add_message(
                self.request,
                messages.SUCCESS,
                success_msg,
            )
            return super().form_valid(form)

    def form_invalid(self, form):
        if is_request_ajax(self.request):
            context = {
                "success": False,
                "msg": _("Your form contains errors."),
                "form_html": render_to_string(
                    self.template_name_modal, {"form": form}, request=self.request
                ),
            }
            return self.render_json_response(context_dict=context)
        else:
            messages.add_message(
                self.request, messages.ERROR, _("Your form contains errors.")
            )
            return super().form_invalid(form)


class BaseBulkUpdateView(LoginRequiredMixin, FormView, JSONResponseMixin):
    """
    Base bulk udpate view.
    """

    template_name = None
    form_class = None
    raise_exception = True
    tags_field = None
    cleaned_data = None

    def update_extra_m2m_fields(self, records, m2m_data):
        """
        Override this method to add logic for extra m2m fields that should be updated.
        """

    def update_db_indexes(self, records):
        """
        Override this method to add logic for updating db indexes.
        """

    def form_valid(self, form):
        """"""
        self.cleaned_data = form.cleaned_data
        form.cleaned_data.pop("records_pks", None)
        records = form.cleaned_data.pop("records", None)
        if not records:
            msg = _(
                "Nothing to process (most probably you have no permission to run this action on "
                "the selected records)."
            )
            context = {"success": False, "msg": msg}
        else:
            # get model
            model = form.Meta.model
            model_name = model._meta.model_name
            content_type = ContentType.objects.get_for_model(model)
            # get m2m fields of given model
            m2m_fields = [
                k.name
                for k in model._meta.get_fields()
                if k.many_to_many and not k.auto_created
            ]
            basic_data = {}
            m2m_data = {}
            custom_updaters_data = {}

            # check if tags field and data available
            tags2add = form.cleaned_data.pop("tags2add", None)
            tags2remove = form.cleaned_data.pop("tags2remove", None)

            for field_name, updater in form.custom_updaters.items():
                field_data = form.cleaned_data.pop(field_name, None)
                if field_data:
                    custom_updaters_data[field_name] = field_data

            if self.tags_field and (tags2add or tags2remove):
                tags_through_model = getattr(model, self.tags_field).through
                if tags2add:
                    tags_through_to_create = []
                    tags = []
                    # get_or_create `Tag` objects for provided tag names
                    for tag in tags2add:
                        obj = Tag.objects.get_or_create(name=tag)
                        tags.append(obj[0])

            # split posted data into 2 dicts
            for field in form.cleaned_data:
                if field in m2m_fields:
                    m2m_data[field] = form.cleaned_data[field]
                else:
                    model_field = model._meta.get_field(field)
                    if model_field.get_internal_type() == "ForeignKey":
                        if form.cleaned_data[field]:
                            basic_data[field + "_id"] = form.cleaned_data[field].pk
                        else:
                            basic_data[field + "_id"] = None
                    else:
                        basic_data[field] = form.cleaned_data[field]

            to_update = []
            managers = m2m_data.pop("managers", None)
            if managers:
                managers_through_model = getattr(model, "managers").through
                managers_to_update = []
            records_pks = []
            updated_fields = set(basic_data.keys())
            for obj in records:
                records_pks.append(obj.pk)
                if basic_data or custom_updaters_data:
                    for field in basic_data:
                        setattr(obj, field, basic_data[field])

                    for field_name, field_data in custom_updaters_data.items():
                        updater = form.custom_updaters[field_name]
                        updated_fields.update(updater(obj, field_data))

                    to_update.append(obj)

                if managers:
                    for m in managers:
                        managers_through_obj = managers_through_model(user=m)
                        setattr(managers_through_obj, model_name, obj)
                        managers_to_update.append(managers_through_obj)
                if self.tags_field and tags2add:
                    for tag in tags:
                        tags_through_obj = tags_through_model(
                            tag=tag, object_id=obj.pk, content_type=content_type
                        )
                        tags_through_to_create.append(tags_through_obj)

            if basic_data or custom_updaters_data:
                model.objects.bulk_update(
                    to_update, updated_fields, batch_size=settings.BULK_BATCH_SIZE
                )

            if managers:
                managers_through_model.objects.filter(
                    **{model_name + "__in": records}
                ).delete()
                managers_through_model.objects.bulk_create(
                    managers_to_update, batch_size=settings.BULK_BATCH_SIZE
                )
            if self.tags_field and tags2remove:
                # remove specified tags (actually we only remove
                # the objects of the "through" model i.e. `TaggedItem`)
                tags_through_model.objects.filter(
                    tag__name__in=tags2remove, object_id__in=records_pks
                ).delete()
            if self.tags_field and tags2add:
                # delete already existing TaggedItems to avoid duplicated records
                tags_through_model.objects.filter(
                    tag__in=tags, object_id__in=records_pks
                ).delete()
                # create new TaggedItems
                tags_through_model.objects.bulk_create(
                    tags_through_to_create, batch_size=settings.BULK_BATCH_SIZE
                )
            # now bulk update extra m2m fields
            self.update_extra_m2m_fields(records, m2m_data)
            # update db indexes
            self.update_db_indexes(records)
            msg = _(f"You have successfully updated {len(records)} records.")
            context = {"success": True, "msg": msg}
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessful operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(
                self.template_name, {"form": form}, request=self.request
            ),
        }
        return self.render_json_response(context_dict=context)
