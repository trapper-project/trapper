# Generated by Django 2.2.17 on 2021-08-05 13:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0014_userremotetask_stage"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="userremotetask",
            name="stage",
        ),
    ]
