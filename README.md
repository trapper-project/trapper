<p align="center">
  <img src="https://gitlab.com/trapper-project/trapper/-/raw/master/trapper/trapper-project/trapper/apps/common/static/images/logo/logo_text.png">
</p>

<div align="center"> 
<font size="6"> AI-driven data management platform for camera trapping </font>
<br>
<hr> 
<img src="https://img.shields.io/badge/Version-Trapper 2.0 BETA-darkgreen"/>
<img src="https://img.shields.io/badge/Python-3.8-blue"/>
<a href="https://demo.trapper-project.org/"><img src="https://img.shields.io/badge/Trapper-Demo-green" /></a>
<a href="https://trapper-project.readthedocs.io/en/latest/"><img src="https://img.shields.io/badge/Trapper-Documentation-yellow" /></a>
<a href="https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12571"><img src="https://img.shields.io/badge/Trapper-Paper-blue.svg" /></a>
<a href="https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A"><img src="https://img.shields.io/badge/Trapper-Slack-orange" /></a>
<a href="https://gitlab.com/trapper-project/trapper/-/blob/master/LICENSE"><img src="https://img.shields.io/badge/Licence-GPLv3-pink" /></a>
<br><br>
</div>

## Table of Contents

[Overview](https://gitlab.com/oscf/trapper-cs#-overview) |
[TRAPPER in a nutshell](https://gitlab.com/oscf/trapper-cs#%EF%B8%8F-trapper-in-a-nutshell) | 
[TRAPPER ecosystem](https://gitlab.com/trapper-project/trapper#%EF%B8%8F-trapper-ecosystem) | 
[Installation](https://gitlab.com/trapper-project/trapper#-installation) | 
[Demo](https://gitlab.com/trapper-project/trapper#-demo) |
[Documentation](https://gitlab.com/trapper-project/trapper#-documentation) | 
[Who is using TRAPPER?](https://gitlab.com/trapper-project/trapper#-who-is-using-trapper) | 
[Funders and Partners](https://gitlab.com/trapper-project/trapper#-funders-and-partners) | 
[Support](https://gitlab.com/trapper-project/trapper#-support) | 
[License](https://gitlab.com/trapper-project/trapper#-license)

## 🦬 Overview

TRAPPER is an open-source, [Python](<https://www.python.org/>), [Django](<https://www.djangoproject.com/>) and [Docker](https://www.docker.com/)-based web application designed to manage [camera trapping](https://en.wikipedia.org/wiki/Camera_trap) projects. Motion-triggered camera trapping is increasingly becoming an essential tool in ecological research. Given the nature of the collected data (multimedia files), even relatively small camera-trapping projects can generate large and complex datasets. Organizing these extensive collections of multimedia files and efficiently querying specific subsets of data, particularly in a spatio-temporal context, often presents a significant challenge. Without an appropriate software solution, this can evolve into a serious data management issue, leading to delays and long-term data inaccessibility.

We propose a novel approach that, unlike available software solutions, is a fully open-source web application using spatially enabled data capable of handling various media types, including both pictures and videos. TRAPPER supports collaborative work on a project and facilitates data sharing among system users. Leveraging state-of-the-art and well-recognized open-source software components and the modern, general-purpose programming language Python, we have designed a flexible software framework for data management in camera trapping studies.

**TRAPPER 2.0 has been released as an open BETA version. The project is currently in an active development phase, and you can expect regular updates and releases for Citizen Science, Trapper AI, and Trapper Expert itself.**

## 🛠️ TRAPPER in a nutshell

- Open-source and free for use in research, academia, or wildlife conservation projects (GPLv3).
- Offers a spatially-enabled database backend.
- Capable of handling both pictures and videos.
- Features a flexible model for AI-based and expert-based classifications.
- Promotes the [Camtrap DP standard](https://camtrap-dp.tdwg.org/) and encourages data re-use.
- Supports collaborative work on a project.
- Provides Jupyter Notebooks and an API.
- Can be installed locally, on a Virtual Private Server (VPS), or in the cloud (e.g., Azure, AWS with S3 support).

## ⚙️ TRAPPER ecosystem

**Trapper Expert (this repository)** - serves as the core web application (TRAPPER), encompassing all essential functionalities of the system. It features an Angular-based and Bootstrap frontend, coupled with a Django-based backend. The application utilizes a PostgreSQL database with PostGIS extension for spatial data handling, and employs RabbitMQ as a message broker with Celery workers for asynchronous task processing.

**Trapper Citizen Science** [🔗](https://gitlab.com/trapper-project/trapper-frontend) - a smart and innovative frontend application (UI) for TRAPPER, specifically designed for Citizen Science projects.

**Trapper AI** [🔗](https://gitlab.com/trapper-project/trapper-ai) [🔗](https://gitlab.com/trapper-project/trapper-ai-worker) - a module responsible for the integration and configuration of AI models for object detection and species classification. This component is organized using the **Trapper AI Manager** web-based component and **Trapper AI Worker**. Workers can utilize GPUs and can be scaled or launched on multiple machines.

**Trapper Jupyter Hub** - a module responsible for sharing Jupyter Hub and Jupyter Notebooks internally in TRAPPER for users who need to explore and analyze data using Python or R scientific packages;

**Trapper Client** [🔗](https://gitlab.com/oscf/trapper-client) - a GUI application written in Python designed to assist in creating camera trapping data packages (pictures and videos) that are compatible with the TRAPPER system. This approach is recommended when uploading large volumes of data (many GBs) to the TRAPPER web application.


## 📥 Installation

If you want to install Trapper Expert with the basic functionalities of the system, then execute each of the steps below.

1. You need Docker Engine and Docker Compose to run TRAPPER:
   
   * Installing [Docker Engine](https://docs.docker.com/install/)
   * Installing [Docker Compose](https://docs.docker.com/compose/)

2. Clone the source code of TRAPPER to your local repository:

   ```bash                 
   $ git clone https://gitlab.com/trapper-project/trapper.git
   ```
       
3. To get the most up-to-date version of TRAPPER switch to a branch ``development``:
   
   ```bash
   $ git checkout development
   ```

4. Copy the ``trapper.env`` file to the ``.env`` file, you can do it using the command:
   
   ```bash
   $ cp trapper.env .env
   ```
 
5. Adjust the variables in the ``.env`` file if you use a non-standard installation, e.g. the external postgresql database. See the full documentation to learn more about all possible configuration parameters.

6. Run TRAPPER with the following command:

   ```bash
   # Production	
   $ ./start.sh -pb prod
   ```

   ```bash
   # Development
   $ ./start.sh -pb dev
   ```

   The first run may take a while, because the docker images have to be downloaded and built. To learn more about all possible running options simply type ``./start.sh``. If you'd like to override docker-compose parameters for development purpose, you can do it by creating a ``docker-compose.override.yml`` file.
 
7. Enter the ``trapper`` docker container and create the ``superuser``:
   
   ```bash
   $ docker exec -it trapper bash
   $ python3 /app/trapper/trapper-project/manage.py createsuperuser
   ```

8. To turn off the application execute the command:
   
   ```bash
   $ ./start.sh prod stop
   ```

If you need to extend Trapper Expert with **Trapper Citizen Science** or **Trapper AI**, then you need to install each of the module using seperate code repository and related instructions.

## 🌐 Demo
[Trapper Expert ](https://demo.trapper-project.org) demo instance does not include **Trapper Citizen Science** or **Trapper AI** yet.

## 📝 Documentation

[TRAPPER](https://trapper-project.readthedocs.io) documentation.

If you are using TRAPPER, please cite our work:

> Bubnicki, J.W., Churski, M. and Kuijper, D.P.J. (2016), trapper: an open source web-based application to manage camera trapping projects. Methods Ecol Evol, 7: 1209-1216. https://doi.org/10.1111/2041-210X.12571

For more news about TRAPPER please visit the [Open Science Conservation Fund (OSCF) website](https://os-conservation.org) and [OSCF LinkedIn profile](https://www.linkedin.com/company/os-conservation/).

## 🏢 Who is using TRAPPER?
* Mammal Research Institute Polish Academy of Sciences;
* Karkonosze National Park;
* Swedish University of Agricultural Sciences;
* Svenska Jägareförbundet;
* Meles Wildbiologie;
* University of Freiburg Wildlife Ecology and Management;
* Bavarian Forest National Park;
* Georg-August-Universität Göttingen;
* KORA - Carnivore Ecology and Wildlife Management;
* and many more individual scientists and ecologies;

## 💲 Funders and Partners
<p align="center">
  <img src="TRAPPER-funds.png">
</p>
<p align="center">
  <img src="TRAPPER-partners.png">
</p>

## 🤝 Support

Feel free to add a new issue with a respective title and description on the [TRAPPER issue tracker](https://gitlab.com/trapper-project/trapper/-/issues). If you already found a solution to your problem, we would be happy to review your pull request.

If you prefer direct contact, please let us know: `contact@os-conservation.org`

We also have [TRAPPER Mailing List](https://groups.google.com/d/forum/trapper-project) and [TRAPPER Slack](https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A).

## 📜 License

Read more in [TRAPPER License](https://gitlab.com/trapper-project/trapper/-/blob/master/LICENSE).