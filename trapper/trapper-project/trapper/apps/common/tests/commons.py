from rest_framework.test import APITestCase

from trapper.apps.accounts.tests.factories.user import UserFactory


class BaseAPITestCase(APITestCase):
    maxDiff = None

    def setUp(self) -> None:
        super(BaseAPITestCase, self).setUp()
        self.user = UserFactory()
        self.client.force_authenticate(self.user)
