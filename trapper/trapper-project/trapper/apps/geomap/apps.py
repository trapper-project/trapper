from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GeomapConfig(AppConfig):
    name = "trapper.apps.geomap"
    verbose_name = _("Geomap")
