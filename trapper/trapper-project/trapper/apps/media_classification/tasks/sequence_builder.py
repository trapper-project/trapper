import datetime

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from trapper.apps.common.tools import datetime_aware
from trapper.apps.media_classification.models import (
    Classification,
    Sequence,
    SequenceResourceM2M,
)


class SequencesBuilder:
    """
    TODO: docstrings
    """

    def __init__(self, data, user):
        self.user = user
        self.data = data
        self.time_interval = data["time_interval"]
        self.total = None
        self.processed_collections = 0
        self.log = []

    def group_resources(self, resources, delta):
        groups = []
        group = []
        res_len = len(resources)
        for i in range(res_len - 1):
            diff = resources[i + 1].date_recorded - resources[i].date_recorded
            if diff <= delta:
                group.append(resources[i])
                if i + 2 == res_len:
                    group.append(resources[i + 1])
                    groups.append(group)
            else:
                group.append(resources[i])
                groups.append(group)
                group = []
                # the last resource that is not part of a previous sequence
                if i == res_len - 2:
                    groups.append([resources[i + 1]])
        return groups

    def create_sequences(self, cp_collection, groups):
        description = _(
            f"Built automatically. The time interval was set to {self.time_interval} minutes."
        )
        classifications = Classification.objects.filter(collection=cp_collection)
        classifications_dict = {k.resource_id: k for k in classifications}
        classification_objects = []
        sequence_objects = []
        sequence_resource_m2m_objects = []
        for i, group in enumerate(groups):
            sequence = Sequence(
                sequence_id=i + 1,
                collection=cp_collection,
                created_by=self.user,
                created_at=datetime_aware(),
                description=description,
            )
            sequence_objects.append(sequence)
        sequences = Sequence.objects.bulk_create(
            sequence_objects, batch_size=settings.BULK_BATCH_SIZE
        )
        for i, group in enumerate(groups):
            sequence = sequences[i]
            for resource in group:
                obj = SequenceResourceM2M(sequence=sequence, resource=resource)
                sequence_resource_m2m_objects.append(obj)
                classification = classifications_dict[resource.pk]
                classification.sequence = sequence
                classification_objects.append(classification)
        SequenceResourceM2M.objects.bulk_create(
            sequence_resource_m2m_objects, batch_size=settings.BULK_BATCH_SIZE
        )
        Classification.objects.bulk_update(
            classification_objects,
            fields=["sequence"],
            batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
        )

    def process_data(self):
        delta = datetime.timedelta(minutes=self.time_interval)
        deployment_agg = self.data.get("deployments")
        cp_collections = self.data.get("project_collections")
        self.total = len(cp_collections)
        for cp_collection in cp_collections:
            # Remove existing Sequence objects for current collection
            Sequence.objects.filter(collection=cp_collection).delete()
            resources = cp_collection.get_resources()
            if deployment_agg:
                groups = []
                deployments = (
                    resources.values_list("deployment_id", flat=True)
                    .order_by()
                    .distinct()
                )
                for d in deployments:
                    resources_d = resources.filter(deployment_id=d).order_by(
                        "date_recorded"
                    )
                    groups.extend(self.group_resources(resources_d, delta))
                self.create_sequences(cp_collection, groups)
            else:
                groups = self.group_resources(
                    resources.order_by("date_recorded"), delta
                )
                self.create_sequences(cp_collection, groups)
            self.processed_collections += 1
        if self.processed_collections == 0:
            self.log.insert(
                0,
                "The celery task is finished. Unfortunately none of the specified "
                "classification project collections could be processed. See "
                "the errors below.",
            )
        else:
            self.log.insert(
                0,
                "You have successfully built sequences for {cols} "
                "out of {total} classification project collections.".format(
                    cols=self.processed_collections, total=self.total
                ),
            )
        return self.log

    def run_with_logger(self):
        log = self.process_data()
        log = "<br>".join(log)
        return log
