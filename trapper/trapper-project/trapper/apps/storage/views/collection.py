# -*- coding: utf-8 -*-
"""
Views used to handle logic related to collection management in storage
application
"""
from braces.views import UserPassesTestMixin, JSONResponseMixin
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.shortcuts import get_object_or_404, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.views import generic
from formtools.wizard.views import SessionWizardView
from trapper.apps.accounts.models import UserTask
from trapper.apps.accounts.utils import create_external_media
from trapper.apps.common.tools import parse_pks, datetime_aware
from trapper.apps.common.views import BaseDeleteView, BaseUpdateView, BaseBulkUpdateView
from trapper.apps.common.views import LoginRequiredMixin
from trapper.apps.geomap.models import MapManagerUtils
from trapper.apps.messaging.models import Message, CollectionRequest
from trapper.apps.messaging.taxonomies import MessageType
from trapper.apps.research.models import ResearchProject
from trapper.apps.storage.forms import (
    CollectionForm,
    CollectionRequestForm,
    CollectionUploadConfigForm,
    CollectionUploadDataForm,
    BulkUpdateCollectionForm,
    CollectionResourceAppendForm,
)
from trapper.apps.storage.models import Collection
from trapper.apps.storage.tasks import celery_process_collection_upload
from trapper.apps.storage.views.resource import ResourceGridContextMixin

User = get_user_model()


class CollectionGridContextMixin:
    """"""

    def get_collection_url(self, **kwargs):
        """Return standard DRF API url for collections"""
        return reverse("storage:api-collection-list")

    def get_collection_delete_url(self, **kwargs):
        """Return standard url used for removing multiple collections"""
        return reverse("storage:collection_delete_multiple")

    def get_collection_context(self, **kwargs):
        """Build collection context"""
        context = {
            "research_projects": ResearchProject.objects.get_accessible(
                user=self.request.user
            ).values_list("pk", "name"),
            "owners": User.objects.filter(owned_collections__isnull=False).distinct(),
            "data_url": self.get_collection_url(**kwargs),
            "collection_delete_url": self.get_collection_delete_url(**kwargs),
            "maps": MapManagerUtils.get_accessible(user=self.request.user),
            "model_name": "collections",
            "update_redirect": False,
        }
        return context


class CollectionListView(
    LoginRequiredMixin, generic.TemplateView, CollectionGridContextMixin
):
    """View used for rendering template with collection grid.
    Context of view is updated with :class:`CollectionGridContextMixin`
    """

    model = Collection
    template_name = "storage/collections/collection_list.html"

    def get_context_data(self, **kwargs):
        """All we need to render base grid is:
        * Model name as title
        * Filter form instance
        * Hide classification project add action

        This view is not serving any data. Data is read using DRF API
        """
        context = super(CollectionListView, self).get_context_data(**kwargs)
        context["collection_context"] = self.get_collection_context(**kwargs)
        return context


view_collection_list = CollectionListView.as_view()


class CollectionOnDemandListView(CollectionListView):
    """View used for rendering template with ondemand collections grid."""

    template_name = "storage/collections/collection_ondemand_list.html"

    def get_context_data(self, **kwargs):
        context = super(CollectionOnDemandListView, self).get_context_data(**kwargs)
        context["collection_context"]["data_url"] = reverse(
            "storage:api-collection-ondemand-list"
        )
        return context


view_collection_ondemand_list = CollectionOnDemandListView.as_view()


class CollectionDetailView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    generic.DetailView,
    ResourceGridContextMixin,
):
    """View used for rendering details of specified collection.

    Before details are rendered, permissions are checked and if currently
    logged in user has not enough permissions to view details,
    proper message is displayed.

    This view uses
    :class:`apps.storage.views.resources.ResourceGridContextMixin`
    for altering behaviour resource grid rendered in details
    """

    template_name = "storage/collections/collection_detail.html"
    model = Collection
    raise_exception = True
    context_object_name = "collection"

    def test_func(self, user):
        """
        Collection details can be seen only if user has enough permissions
        """
        return self.get_object().can_view(user)

    def get_resource_url(self, **kwargs):
        """Alter url for resources DRF API, to get only resources that
        belongs to collection and are accessible for currently logged in
        user"""
        collection = kwargs.get("collection")
        return reverse(
            "storage:api-resource-collection-list",
            kwargs={"collection_pk": collection.pk},
        )

    def get_context_data(self, **kwargs):
        """
        Alter context data used for resources grid in collection details:

        * hide create collection action
        * hide filter by collections
        * hide resource delete action
        * hide resource update action
        """
        context = super(CollectionDetailView, self).get_context_data(**kwargs)

        collection = context["object"]
        context["resource_context"] = self.get_resource_context(collection=collection)
        context["resource_context"]["hide_create_collection"] = True
        context["resource_context"]["hide_filter_collection"] = True
        context["resource_context"]["collection_pk"] = collection.pk
        if not collection.can_update(user=self.request.user):
            context["resource_context"]["hide_change_actions"] = True
        return context


view_collection_detail = CollectionDetailView.as_view()


class CollectionCreateView(LoginRequiredMixin, generic.CreateView, JSONResponseMixin):
    """Collection's create view.
    Handle the creation of the :class:`apps.storage.models.Collection` objects.
    """

    template_name = "storage/collections/collection_create.html"
    model = Collection
    form_class = CollectionForm
    raise_exception = True

    def form_valid(self, form):
        """If form is valid then set `owner` as currently logged in user,
        and add message that collection has been created"""
        user = self.request.user
        form.instance.owner = user
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(f"New collection <strong>{form.instance.name}</strong> has been added"),
        )
        self.object = form.save()
        context = {
            "success": True,
            "msg": "",
            "url": reverse("storage:collection_detail", kwargs={"pk": self.object.pk}),
        }
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(
                self.template_name, {"form": form}, request=self.request
            ),
        }
        return self.render_json_response(context_dict=context)


view_collection_create = CollectionCreateView.as_view()


class CollectionUpdateView(BaseUpdateView):
    """Collection update view.
    It handles the update of the :class:`apps.storage.models.Collection` objects.
    """

    template_name = "storage/collections/collection_update.html"
    template_name_modal = "storage/collections/collection_update_modal.html"
    model = Collection
    raise_exception = True
    form_class = CollectionForm


view_collection_update = CollectionUpdateView.as_view()


class CollectionUploadWizardView(LoginRequiredMixin, SessionWizardView):
    """
    Wizard view used to handle an upload of a collection's definition
    (YAML) and data (ZIP archive) files. However, this view does not directly
    create any collection or resource objects; instead it runs a celery task
    If the `settings.CELERY_ENABLED` is set to True a task is run in the
    asynchronous mode.
    """

    form_list = [CollectionUploadConfigForm, CollectionUploadDataForm]
    template_names = [
        "storage/collections/collection_upload_config.html",
        "storage/collections/collection_upload_data.html",
    ]

    file_storage = FileSystemStorage()

    def get(self, request, *args, **kwargs):
        """Make sure that when entering this view, user's external media
        directory exists."""
        create_external_media(username=request.user.username)
        return super(CollectionUploadWizardView, self).get(request, *args, **kwargs)

    def get_template_names(self):
        """Each step of this wizard has its own template."""
        return self.template_names[self.get_step_index()]

    def done(self, form_list, **kwargs):
        """When all steps of this wizard are completed successfully a context
        data is passed to a celery task.
        """

        user = self.request.user
        params = {
            "owner": user,
        }

        form_list = list(form_list)

        definition_file = form_list[0].cleaned_data.get("definition_file")
        uploaded_yaml = form_list[0].cleaned_data.get("uploaded_yaml")

        if definition_file:
            params["definition_file"] = definition_file
        else:
            params["definition_file"] = uploaded_yaml

        archive_file = form_list[1].cleaned_data.get("archive_file")
        uploaded_media = form_list[1].cleaned_data.get("uploaded_media")

        remove_zip = form_list[1].cleaned_data.get("remove_zip", False)
        params["remove_zip"] = remove_zip

        if archive_file:
            params["archive_file"] = archive_file
        else:
            params["archive_file"] = uploaded_media

        if settings.CELERY_ENABLED:
            task = celery_process_collection_upload.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            msg = _(
                "You have successfully run the celery task. Your uploaded data "
                "package is being processed now. "
            )
            success_url = reverse("accounts:dashboard")
        else:
            msg = celery_process_collection_upload(**params)
            success_url = reverse("storage:collection_list")
        messages.success(request=self.request, message=msg)
        return redirect(success_url)


view_collection_upload = CollectionUploadWizardView.as_view()


class CollectionRequestView(LoginRequiredMixin, UserPassesTestMixin, generic.FormView):
    """"""

    success_url = reverse_lazy("storage:collection_list")
    template_name = "storage/collections/collection_request.html"
    form_class = CollectionRequestForm
    raise_exception = True

    # Template of the request message
    TEXT_TEMPLATE = _(
        "Dear {owner},\n\nI would like to ask you for the permission "
        "to use the following collection:\n"
        '<a href="{collection_url}">{collection_url}</a>.\n\n'
        "Best regards,\n{user}"
    )

    def get_collection(self):
        self.collection = get_object_or_404(Collection, pk=self.kwargs["pk"])

    def test_func(self, user):
        user = self.request.user
        self.get_collection()
        if self.collection.status != "OnDemand":
            return False
        if self.collection.owner == user or user in self.collection.managers.all():
            return False
        if user in self.collection.members.all():
            return False
        if self.collection.collection_request.filter(user_from=user).exists():
            messages.add_message(
                self.request,
                messages.ERROR,
                _("You have already sent a request for this collection."),
            )
            return False
        return True

    def get_context_data(self, *args, **kwargs):
        context = super(CollectionRequestView, self).get_context_data(**kwargs)
        # self.collection was set previously in the "get_initial" method
        context["collection"] = self.collection
        return context

    def get_initial(self, *args, **kwargs):
        """Initialize the form with the projects query, as well as the
        collection in question.
        """
        # check if user already sent a request for this collection
        collection_full_url = self.request.build_absolute_uri(
            reverse("storage:collection_detail", kwargs={"pk": self.collection.pk})
        )
        initial = {
            "object_pk": self.collection.pk,
            "text": self.TEXT_TEMPLATE.format(
                owner=self.collection.owner.username,
                collection_url=collection_full_url,
                user=self.request.user.username,
            ),
        }
        return initial

    def form_valid(self, form):
        """Create a :class:`apps.messaging.models.Message`
        and :class:`apps.messaging.models.CollectionRequest`
        objects directed to the owner of the collection.
        """
        collection = get_object_or_404(Collection, pk=form.cleaned_data["object_pk"])
        project = form.cleaned_data["project"]
        msg = Message.objects.create(
            subject=_("Request for collections"),
            text=form.cleaned_data["text"],
            user_from=self.request.user,
            user_to=collection.owner,
            date_sent=datetime_aware(),
            message_type=MessageType.COLLECTION_REQUEST,
        )
        coll_req = CollectionRequest(
            name=_("Request for collections"),
            user=collection.owner,
            user_from=self.request.user,
            message=msg,
            project=project,
        )
        coll_req.save()
        coll_req.collections.add(collection)

        # send email to the owner of the collection
        if settings.EMAIL_NOTIFICATIONS:
            if collection.owner.userprofile.system_notifications:
                send_mail(
                    subject=_("[TRAPPER] Request for one of your collections"),
                    message=form.cleaned_data["text"],
                    from_email=None,
                    recipient_list=[collection.owner.email],
                    fail_silently=True,
                )

        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "Your request for the collection <strong>{collection.name}</strong> has been "
                "successfully submitted."
            ),
        )
        return super(CollectionRequestView, self).form_valid(form)

    def form_invalid(self, form):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        messages.add_message(
            self.request, messages.ERROR, _("Error creating collection request")
        )
        return super(CollectionRequestView, self).form_invalid(form)


view_collection_request = CollectionRequestView.as_view()


class CollectionDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    colletions.

    Only collections that user has enough permissions for can be deleted
    """

    model = Collection
    redirect_url = "storage:collection_list"


view_collection_delete = CollectionDeleteView.as_view()


class CollectionResourceDeleteView(LoginRequiredMixin, generic.View, JSONResponseMixin):
    """This view is used to remove resources from collection.

    User is required to have at least access permissions for each resource that
    is removed from a collection.

    .. note::
        Only relation Resource <-> Collection is removed. Resources are not
        removed from a database.
    """

    raise_exception = True

    def post(self, request, *args, **kwargs):
        """
        `request.POST` method is used to remove multiple resources
        from collection in a single request using AJAX.
        """
        user = request.user
        data = request.POST.get("pks", None)

        try:
            collection = Collection.objects.get(pk=kwargs.get("pk", None))
        except Collection.DoesNotExist:
            collection = None
        if data and collection and collection.can_update(user=user):
            values = parse_pks(pks=data)
            status = True
            msg = ""
            collection.resources.through.objects.filter(
                resource__in=values, collection=collection.pk
            ).delete()
        else:
            status = False
            msg = _("Invalid request")

        context = {"status": status, "msg": msg}
        return self.render_json_response(context)


view_collection_resource_delete = CollectionResourceDeleteView.as_view()


class CollectionResourceAppendView(
    LoginRequiredMixin, generic.FormView, JSONResponseMixin
):
    """
    This view is used to append list of resources to collection. User is required
    to have at least access permissions for each resource that is added to collection.
    """

    template_name = "forms/simple_crispy_form.html"
    form_class = CollectionResourceAppendForm
    raise_exception = True

    def form_valid(self, form):
        """"""
        error = form.cleaned_data.get("error", None)
        if not error:
            new_resources = form.cleaned_data.get("new_resources", None)
            collection = form.cleaned_data.get("collection", None)
            collection.resources.add(*new_resources)
            msg = _(
                f"You have successfully added {new_resources.count()} "
                "new resources to selected collection."
            )
            context = {"success": True, "msg": msg}
        else:
            context = {
                "success": False,
                "msg": error,
            }
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """
        If form is not valid, form is re-rendered with error details and message about
        unsuccessfull operation is shown.
        """
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(
                self.template_name, {"form": form}, request=self.request
            ),
        }
        return self.render_json_response(context_dict=context)


view_collection_resource_append = CollectionResourceAppendView.as_view()


class CollectionBulkUpdateView(BaseBulkUpdateView):
    """Collection bulk update view."""

    template_name = "forms/simple_crispy_form.html"
    form_class = BulkUpdateCollectionForm
    raise_exception = True


view_collection_bulk_update = CollectionBulkUpdateView.as_view()
