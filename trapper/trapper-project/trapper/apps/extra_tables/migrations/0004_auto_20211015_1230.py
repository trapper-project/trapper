# Generated by Django 2.2.17 on 2021-10-15 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('extra_tables', '0003_auto_20210603_2054'),
    ]

    operations = [
        migrations.AddField(
            model_name='species',
            name='taxon_id',
            field=models.CharField(max_length=20, null=True, verbose_name='Taxon ID'),
        ),
        migrations.AddField(
            model_name='species',
            name='taxon_rank',
            field=models.CharField(choices=[('class', 'Class'), ('order', 'Order'), ('family', 'Family'), ('genus', 'Genus'), ('species', 'Species'), ('subspecies', 'Subspecies')], default='species', max_length=20),
        ),
        migrations.AlterField(
            model_name='species',
            name='english_name',
            field=models.CharField(default='eng name', max_length=100, verbose_name='English name'),
        ),
        migrations.AlterField(
            model_name='species',
            name='family',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Family'),
        ),
        migrations.AlterField(
            model_name='species',
            name='genus',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Genus'),
        ),
    ]
