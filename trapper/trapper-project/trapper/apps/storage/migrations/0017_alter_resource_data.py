# Generated by Django 3.2.4 on 2021-11-03 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0016_update_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resource',
            name='data',
            field=models.JSONField(blank=True, default=dict, null=True, verbose_name='Data'),
        ),
    ]
