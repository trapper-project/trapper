from django.contrib.auth import get_user_model
from django.test import TestCase, Client, override_settings
from django.urls import reverse

from trapper.apps.media_classification.models import Classification


User = get_user_model()


@override_settings(CELERY_ENABLED=False)
class TestCopyBboxes(TestCase):
    """
    Fixture contains all the setup necessary to test Classifications and AIClassifications.
    There are two users created, 1 added as a project admin, 2 added as expert.
    Five resources, with Classification, AIClassification and dynamic attrs are created:
    1: Classification with status=False (bboxes should never be copied)
    2: AIC and C matching observation type, but not count
    3: AIC and C with matching count, observation_type but C unambiguous
    4: AIC and C with all attrs matching
    5: C with one set of dyn_attrs, AIC with two, with all attrs matching.
    """

    multi_db = True
    fixtures = ["copy_bboxes_test_fixtures.yaml"]

    def _client(self, user_id: int):
        client = Client()
        user = User.objects.get(pk=user_id)
        client.force_login(user)
        return client

    def _url(self):
        return reverse(
            "media_classification:ai_classification_copy_bboxes",
            kwargs={"project_id": 1},
        )

    def test_copy_bboxes_non_admin(self):
        """Expert should not have access to copy bboxes action"""
        client = self._client(user_id=2)
        form_data = {"ai_classification_pks": "1,2,3,4,5"}
        response = client.post(self._url(), data=form_data)
        self.assertEqual(response.status_code, 403)

    def test_copy_bboxes_default_attrs(self):
        client = self._client(user_id=1)
        self.assertEqual(Classification.objects.filter(status_ai=True).count(), 0)
        form_data = {"ai_classification_pks": "1,2,3,4,5", "overwrite_bboxes": True}
        response = client.post(self._url(), data=form_data)
        self.assertEqual(response.status_code, 200)

        self.assertFalse(Classification.objects.get(id=1).status_ai)
        self.assertFalse(Classification.objects.get(id=2).status_ai)
        self.assertFalse(Classification.objects.get(id=3).status_ai)
        self.assertTrue(Classification.objects.get(id=4).status_ai)
        self.assertTrue(Classification.objects.get(id=5).status_ai)

    def test_copy_bboxes_chosen_attrs(self):
        client = self._client(user_id=1)
        user_1 = User.objects.get(pk=1)

        self.assertEqual(
            Classification.objects.filter(approved_ai_by__isnull=False).count(), 0
        )

        form_data = {"ai_classification_pks": "1,2,3,4,5", "sex": True}
        response = client.post(self._url(), data=form_data)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(Classification.objects.get(id=1).approved_ai_by, None)
        self.assertEqual(Classification.objects.get(id=2).approved_ai_by, None)
        self.assertEqual(Classification.objects.get(id=3).approved_ai_by, None)
        self.assertEqual(Classification.objects.get(id=4).approved_ai_by, user_1)
        self.assertEqual(Classification.objects.get(id=5).approved_ai_by, user_1)

        # make sure that only one ClassificationDynamicAttrs was edited for C5
        self.assertEqual(
            Classification.objects.get(id=5)
            .dynamic_attrs.filter(bboxes__isnull=False)
            .count(),
            1,
        )

    def test_copy_bboxes_all_attrs(self):
        client = self._client(user_id=1)

        self.assertEqual(
            Classification.objects.filter(approved_source_ai__isnull=False).count(), 0
        )

        form_data = {
            "ai_classification_pks": "1,2,3,4,5",
            "sex": True,
            "age": True,
        }
        response = client.post(self._url(), data=form_data)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(Classification.objects.get(id=1).approved_source_ai, None)
        self.assertEqual(Classification.objects.get(id=2).approved_source_ai, None)
        self.assertEqual(Classification.objects.get(id=3).approved_source_ai, None)
        self.assertEqual(Classification.objects.get(id=4).approved_source_ai_id, 4)
        self.assertEqual(Classification.objects.get(id=5).approved_source_ai_id, 5)

        # make sure that only one ClassificationDynamicAttrs was edited for C5
        self.assertEqual(
            Classification.objects.get(id=5)
            .dynamic_attrs.filter(bboxes__isnull=False)
            .count(),
            1,
        )
