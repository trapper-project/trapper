import datetime
from django.contrib.gis.geos import Point
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from trapper.apps.accounts.serializers import CSUserSerializer
from trapper.apps.citizen_science.mixins import BaseCSMixin
from trapper.apps.common.serializers import BasePKSerializer
from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.media_classification.models import (
    AIClassification,
    Classification,
    ClassificationProject,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus


class CSBBoxesSerializer(serializers.Serializer):
    left = serializers.SerializerMethodField()
    top = serializers.SerializerMethodField()
    width = serializers.SerializerMethodField()
    height = serializers.SerializerMethodField()

    bboxes = None

    class Meta:
        fields = ["left", "top", "width", "height"]

    def to_representation(self, instance):
        # We can get here bbox list directly or dynamic attrs as obj
        if isinstance(instance, list):
            obj = instance
        else:
            obj = instance.bboxes
        if not obj:
            return None
        return super().to_representation(obj)

    def get_left(self, obj):
        if not obj or not obj[0]:
            return None
        return obj[0][0]

    def get_top(self, obj):
        if not obj or not obj[0]:
            return None
        return obj[0][1]

    def get_width(self, obj):
        if not obj or not obj[0]:
            return None
        return obj[0][2]

    def get_height(self, obj):
        if not obj or not obj[0]:
            return None
        return obj[0][3]


class CSDeploymentMediaSerializer(serializers.ModelSerializer):
    thumbnail = serializers.SerializerMethodField()
    bboxes = serializers.SerializerMethodField()

    class Meta:
        model = Classification
        fields = ["pk", "thumbnail", "bboxes"]

    @staticmethod
    def get_thumbnail(obj):
        return obj.resource.get_cs_url(field="tfile")

    def get_bboxes(self, obj):
        bboxes = []
        for attr in obj.dynamic_attrs.all():
            if attr.bboxes and attr.bboxes[0]:
                bboxes.append(attr.bboxes)
        return CSBBoxesSerializer(
            bboxes,
            many=True,
            context=self.context,
        ).data


class CSProjectCreateLocationSerializer(serializers.ModelSerializer):
    latitude = serializers.FloatField(max_value=90.0, min_value=-90.0, source="get_y")
    longitude = serializers.FloatField(
        max_value=180.0, min_value=-180.0, source="get_x"
    )

    def to_representation(self, instance):
        cproject = self.context["classification_project"]
        user = self.context["request"].user
        if not (
            user == instance.owner
            # show coordinates if user can view all classifications in the project
            # i.e. it has a role included in `ClassificationProjectRoleLevels.VIEW_CLASSIFICATIONS`
            or cproject.can_view_classifications(user)
        ):
            instance.coordinates = None
        return super().to_representation(instance)

    class Meta:
        model = Location
        fields = ("name", "latitude", "longitude")
        extra_kwargs = {"name": {"allow_blank": False}}


class CSProjectCreateDeploymentSerializer(serializers.ModelSerializer):
    timestamp_issues = serializers.BooleanField(required=False)

    class Meta:
        model = Deployment
        fields = (
            "deployment_code",
            "start_date",
            "end_date",
            "bait_type",
            "camera_id",
            "camera_model",
            "camera_interval",
            "camera_height",
            "camera_tilt",
            "camera_heading",
            "detection_distance",
            "timestamp_issues",
            "feature_type",
            "habitat",
        )
        extra_kwargs = {
            "start_date": {"allow_null": False},
            "end_date": {"allow_null": False},
            "feature_type": {"allow_null": True},
        }

    def validate(self, attrs):
        now = timezone.now()
        errors = {}
        if "start_date" in attrs and attrs["start_date"] > now:
            errors["start_date"] = _("Start date can't be older than today.")
        if "end_date" in attrs and attrs["end_date"] > now:
            errors["end_date"] = _("End date can't be older than today.")
        if (
            all(k in attrs for k in ["start_date", "end_date"])
            and attrs["start_date"] > attrs["end_date"]
        ):
            errors["start_date"] = _("Start date can't be older than end date.")
        if errors:
            raise serializers.ValidationError(errors)
        return attrs


class CSDeploymentsSerializer(BaseCSMixin, BasePKSerializer):
    thumbnails = serializers.SerializerMethodField()
    location_name = serializers.ReadOnlyField(source="location.name")
    species = serializers.SerializerMethodField()
    sequences = serializers.SerializerMethodField()
    can_update = serializers.SerializerMethodField()
    can_delete = serializers.SerializerMethodField()
    owner = CSUserSerializer(read_only=True)

    classifications = None

    class Meta:
        model = Deployment
        fields = [
            "pk",
            "thumbnails",
            "deployment_id",
            "deployment_code",
            "location_name",
            "species",
            "start_date",
            "end_date",
            "sequences",
            "owner",
            "can_update",
            "can_delete",
        ]

    def get_thumbnails(self, obj):
        thumbnails = self.context.get("thumbnails", {}).get(obj.pk, [])
        return CSDeploymentMediaSerializer(
            thumbnails, many=True, context=self.context
        ).data

    def get_species(self, obj):
        species_stats = self.context.get("species_stats", {}).get(obj.pk, [])
        return species_stats

    def get_sequences(self, obj):
        sequences = self.context.get("sequences", {}).get(obj.pk, 0)
        return sequences

    def get_can_update(self, obj):
        """User can update deployment only when he is owner"""
        return obj.owner == self.context["request"].user

    def get_can_delete(self, obj):
        """User can delete deployment only when he is owner and deployment has no approved or user classifications"""
        deployments_with_user_classifications = self.context.get(
            "deployments_with_user_classifications", set()
        )
        return (
            obj.owner == self.context["request"].user
            and obj.pk not in deployments_with_user_classifications
        )


class CSLocationsDeploymentSerializer(CSProjectCreateLocationSerializer):
    class Meta:
        model = Deployment
        fields = [
            "pk",
            "deployment_id",
            "deployment_code",
            "start_date",
            "end_date",
        ]


class CSLocationsSerializer(CSProjectCreateLocationSerializer):
    deployments = CSLocationsDeploymentSerializer(many=True)

    class Meta:
        model = Location
        fields = [
            "pk",
            "name",
            "latitude",
            "longitude",
            "deployments",
        ]


class CSDeploymentMediaResourceSerializer(BasePKSerializer):
    location_name = serializers.ReadOnlyField(source="location.name")
    coordinates = serializers.SerializerMethodField()

    class Meta:
        model = Deployment
        fields = [
            "id",
            "deployment_id",
            "deployment_code",
            "location_name",
            "coordinates",
        ]

    def get_coordinates(self, obj):
        cproject = self.context["classification_project"]
        user = self.context["request"].user
        if (
            obj.owner == user
            # show coordinates if user can view all classifications in the project
            # i.e. it has a role included in `ClassificationProjectRoleLevels.VIEW_CLASSIFICATIONS`
            or cproject.can_view_classifications(user)
        ):
            return str(obj.location.coordinates)
        return ""


class CSSmallDeploymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deployment
        fields = [
            "pk",
            "deployment_id",
            "deployment_code",
        ]


class CSDeploymentSerializer(
    CSProjectCreateDeploymentSerializer, CSDeploymentsSerializer
):
    location_id = serializers.IntegerField(required=False)
    location = CSProjectCreateLocationSerializer(required=False)

    is_private = serializers.BooleanField(source="status")
    timestamp_issues = serializers.BooleanField(source="correct_tstamp")
    resources_count = serializers.SerializerMethodField()
    classified_ai_count = serializers.SerializerMethodField()
    classified_by_me_count = serializers.SerializerMethodField()
    classified_expert_count = serializers.SerializerMethodField()
    media = serializers.SerializerMethodField()

    class Meta:
        model = Deployment
        fields = [
            "pk",
            "deployment_id",
            "deployment_code",
            "location_id",
            "location",
            "start_date",
            "end_date",
            "owner",
            "camera_id",
            "camera_model",
            "camera_interval",
            "camera_height",
            "camera_tilt",
            "camera_heading",
            "detection_distance",
            "bait_type",
            "feature_type",
            "habitat",
            "timestamp_issues",
            "comments",
            "is_private",
            "is_incomplete",
            "location_id",
            "thumbnails",
            "species",
            "sequences",
            "resources_count",
            "classified_ai_count",
            "classified_by_me_count",
            "classified_expert_count",
            "media",
            "can_update",
            "can_delete",
        ]

    def validate(self, attrs):
        attrs = super().validate(attrs)
        user = self.context["request"].user
        # User can only edit when his deployment owner
        if self.instance.owner != user:
            raise serializers.ValidationError(_("Unable to edit this Deployment."))
        if location_id := attrs.get("location_id"):
            try:
                Location.objects.filter(Q(owner=user) | Q(is_public=True)).get(
                    pk=location_id
                )
            except Location.DoesNotExist:
                raise serializers.ValidationError(
                    {"location_id": _("Location does not exist.")}
                )
        return attrs

    def to_representation(self, instance):
        data = super().to_representation(instance)
        # Because in data model we have `correct_tstamp` field and frontend should get
        # timestamp_issues field we should negative value
        data["timestamp_issues"] = not data["timestamp_issues"]
        # Because in data model we have `status` field and frontend should get
        # is_private field we should parse value
        data["is_private"] = instance.status == Deployment.Status.PRIVATE
        return data

    def update(self, instance, validated_data):
        recreate_deployment_id = hasattr(validated_data, "deployment_code")
        classification_project = self.context["classification_project"]
        update_status = False

        if location_data := validated_data.pop("location", None):
            # if location has any other deployments we should create new location else
            # edit exists deployment
            if instance.location.deployments.exclude(id=instance.id).exists():
                location_data = {
                    "name": location_data["name"],
                    "location_id": f"{classification_project.id}-{instance.location.owner.id}-{location_data['name']}",
                    "coordinates": Point(
                        (
                            float(
                                location_data.get(
                                    "get_x", instance.location.coordinates.x
                                )
                            ),
                            float(
                                location_data.get(
                                    "get_y", instance.location.coordinates.y
                                )
                            ),
                        ),
                        srid=4326,
                    ),
                    "owner": instance.location.owner,
                    "is_public": False,
                }
                location = Location.objects.create(**location_data)
                instance.location_id = location.pk
            else:
                instance.location.name = location_data["name"]
                instance.location.coordinates = Point(
                    (
                        float(
                            location_data.get("get_x", instance.location.coordinates.x)
                        ),
                        float(
                            location_data.get("get_y", instance.location.coordinates.y)
                        ),
                    ),
                    srid=4326,
                )
                instance.location.location_id = (
                    f"{classification_project.id}-{instance.location.owner.id}-"
                    f"{location_data['name']}"
                )
                instance.location.save(
                    update_fields=["name", "coordinates", "location_id"]
                )
                recreate_deployment_id = True

        # Because in data model we have `correct_tstamp` field and from frontend we get
        # timestamp_issues field we should negative value
        if "correct_tstamp" in validated_data:
            validated_data["correct_tstamp"] = not validated_data["correct_tstamp"]
        # Because in data model we have `status` field and from frontend we get
        # is_private field we should correct parse value
        if "status" in validated_data:
            status = (
                Deployment.Status.PRIVATE
                if validated_data["status"]
                else Deployment.Status.PUBLIC
            )
            if status != instance.status:
                update_status = True
                validated_data["status"] = status
            else:
                # nothing changed remove element to prevent wrong update
                validated_data.pop("status")
        instance = super().update(instance, validated_data)
        if recreate_deployment_id:
            instance.update_deployment_id()
        if update_status:
            if instance.status == Deployment.Status.PUBLIC:
                instance.resources.update(status=ResourceStatus.PUBLIC)
            else:
                instance.resources.update(status=ResourceStatus.PRIVATE)
        return instance

    def get_thumbnails(self, obj):
        self.classifications = self.get_classifications_queryset(
            self.context["classification_project"], self.context["request"].user
        ).filter(resource__deployment=obj)
        return CSDeploymentMediaSerializer(
            self.classifications[:6], many=True, context=self.context
        ).data

    def get_timestamp_issues(self, obj):
        return not obj.correct_tstamp

    def get_resources_count(self, obj):
        return (
            Classification.objects.filter(
                Q(
                    collection__project=self.context["classification_project"],
                    resource__deployment=obj,
                )
                & (
                    Q(resource__status=ResourceStatus.PUBLIC)
                    | Q(resource__owner=self.context["request"].user)
                )
            )
            .distinct()
            .count()
        )

    def get_classified_ai_count(self, obj):
        return self.classifications.count()

    def get_classified_by_me_count(self, obj):
        return self.classifications.filter(
            user_classifications__owner=self.context["request"].user
        ).count()

    def get_classified_expert_count(self, obj):
        return self.classifications.filter(approved_source__isnull=False).count()

    def get_media(self, obj):
        return CSDeploymentMediaSerializer(
            self.classifications[:16], many=True, context=self.context
        ).data


class CSDeploymentProcessingProgressSerializer(serializers.ModelSerializer):
    progress = serializers.SerializerMethodField()
    finished = serializers.SerializerMethodField()

    class Meta:
        model = Deployment
        fields = ["pk", "deployment_id", "progress", "finished"]

    def get_progress(self, obj):
        project: ClassificationProject = self.context.get("classification_project")
        if project is None:
            return 0
        classifications = self.context.get("classifications")
        if classifications is None:
            return 0

        self.total = classifications.count()

        if self.total == 0:
            return 0

        self.with_thumbnail_and_preview = classifications.filter(
            resource__file_preview__isnull=False, resource__file_thumbnail__isnull=False
        ).count()

        self.with_approved_source_ai = classifications.filter(
            approved_source_ai__isnull=False
        ).count()

        if self.with_approved_source_ai > 0:
            ai_progress = 1
            blur_progress = 1
            self.with_ai_classification = None
            self.with_blurred_humans = None
        else:
            ai_classifications = AIClassification.objects.filter(
                classification__in=classifications
            ).values_list("classification", flat=True)

            self.with_ai_classification = classifications.filter(
                Q(approved_source_ai__isnull=False) | Q(pk__in=ai_classifications)
            ).count()

            ai_progress = self.with_ai_classification / self.total

            if project.copy_ai_classifications:
                ai_progress *= 0.5

            self.with_blurred_humans = classifications.filter(
                resource__humans_blurred=True
            ).count()

            blur_progress = self.with_blurred_humans / self.total

        result = 0

        result += self.with_thumbnail_and_preview / self.total * 0.1
        result += ai_progress * 0.8
        result += blur_progress * 0.1

        result = round(result, 2)

        return result

    def get_finished(self, obj):
        return self.with_approved_source_ai == self.total


class CSDeploymentUpdateTimestampsSerializer(serializers.ModelSerializer):
    time_offset = serializers.IntegerField(write_only=True)

    class Meta:
        model = Deployment
        fields = ["time_offset"]

    def update(self, instance, validated_data):
        resources = []
        for resource in instance.resources.all():
            resources.append(resource)
            resource.date_recorded += datetime.timedelta(
                seconds=validated_data["time_offset"]
            )
        Resource.objects.bulk_update(resources, ["date_recorded"])
        return instance
