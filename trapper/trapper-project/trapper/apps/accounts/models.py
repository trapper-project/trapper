# -*- coding: utf-8 -*-
"""
Module contains models related to user profile.
When User instance is created, then UserProfile instance is created by
default.

For registration django-allauth is used, but account is not activated.
Instad of mail is sent to admins, so they can activate account
(activation sends email to user).

"""
import os
import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils.timezone import get_default_timezone_name
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from timezone_field import TimeZoneField

from trapper.apps.accounts.taxonomy import (
    PackageType,
    UserRemoteTaskStatus,
)
from trapper.apps.accounts.utils import get_pretty_username, create_external_media
from trapper.apps.common.fields import ResizedImageField
from trapper.apps.common.fields import SafeTextField
from trapper.apps.common.utils.models import delete_old_file
from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels
from trapper.apps.messaging.taxonomies import MessageType


class User(AbstractUser):
    is_citizen_science = models.BooleanField(
        "Is Citizen Science user?",
        default=False,
    )

    def available_teams(self):
        """
        Return list of all available teams for user instance
        """

        from trapper.apps.teams.models import TeamMembership

        teams = self.members.filter(
            team_memberships__status=TeamMembership.MembershipStatus.APPROVED
        ).order_by("name")

        return teams

    def is_error_proof(self, classification_project):
        try:
            return (
                self.classification_project_roles.get(
                    classification_project=classification_project
                ).name
                == ClassificationProjectRoleLevels.ERROR_PROOF
            )
        except ObjectDoesNotExist:
            return False

    class Meta:
        db_table = "auth_user"
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def get_url(self):
        return reverse("geomap:map_list")


class UserProfile(models.Model):
    """
    Base profile model used to store additional details like avatar,
    some about me description and institution
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = ResizedImageField(
        upload_to="avatars", blank=True, null=True, max_width=250, max_height=250
    )
    about_me = SafeTextField(blank=True)
    institution = models.CharField(max_length=255, blank=True)
    system_notifications = models.BooleanField(
        default=True,
        help_text=_("Do you want to receive emails with system notifications?"),
    )
    has_ftp_account = models.BooleanField(default=False)
    timezone = TimeZoneField(
        default=get_default_timezone_name(), verbose_name=_("Timezone")
    )

    # CS Fields
    phone_number = models.CharField(max_length=25, blank=True)
    gdpr = models.BooleanField(default=False)
    gdpr_accepted_date = models.DateTimeField(
        null=True, blank=True, verbose_name=_("GDPR accepted date")
    )
    tos = models.BooleanField(default=False)
    tos_accepted_date = models.DateTimeField(
        null=True, blank=True, verbose_name=_("TOS accepted date")
    )
    default_cs_project = models.ForeignKey(
        "media_classification.ClassificationProject",
        related_name="default_projects",
        # CitizenScienceStatus.PUBLIC, CitizenScienceStatus.PRIVATE
        limit_choices_to={"citizen_science_status__in": [1, 2]},
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def __str__(self):
        """By default printing profile should display pretty version
        of username"""
        return get_pretty_username(user=self.user)

    def get_absolute_url(self):
        """Return absolute url to user profile"""
        return reverse("accounts:show_profile", kwargs={"username": self.user.username})

    @property
    def cs_avatar_url(self):
        if self.avatar:
            return reverse("cs_user_avatar", args=[self.id])
        return ""

    @property
    def avatar_url(self):
        """Return user avatar or default one"""
        if self.avatar and hasattr(self.avatar, "url"):
            return self.avatar.url
        else:
            return "{url}accounts/img/avatar.png".format(url=settings.STATIC_URL)

    def has_unread_messages(self):
        """Checks whether user has any unread messages
        (see :class:`trapper.apps.messaging.models.Message`).
        """

        return (
            self.user.received_messages.filter(date_received=None)
            .exclude(
                message_type__in=[
                    MessageType.COLLECTION_REQUEST,
                    MessageType.RESOURCE_REQUEST,
                ]
            )
            .exists()
        )

    def count_unread_messages(self):
        """Returns the number of unread messages.
        (see :class:`trapper.apps.messaging.models.Message`).
        """

        return (
            self.user.received_messages.filter(date_received=None)
            .exclude(
                message_type__in=[
                    MessageType.COLLECTION_REQUEST,
                    MessageType.RESOURCE_REQUEST,
                ]
            )
            .count()
        )

    def count_inbox_messages(self):
        """Returns total number of inbox messages.
        (see :class:`trapper.apps.messaging.models.Message`).
        """

        return self.user.received_messages.exclude(
            message_type=MessageType.COLLECTION_REQUEST
        ).count()

    def awaiting_collection_requests(self):
        """Returns the number of collections requests that has not been
        resolved yet.
        This means that some users asked for access to collection
        """

        return self.user.collection_requests.filter(resolved_at__isnull=True).count()

    def awaiting_resource_requests(self):
        """Returns the number of resource requests that has not been
        resolved yet.
        This means that some users asked for access to resources
        """

        return self.user.resource_requests.filter(resolved_at__isnull=True).count()

    def save(self, **kwargs):
        """Delete changed files before save new instance"""
        delete_old_file(self, "avatar")
        super(UserProfile, self).save(**kwargs)

    class Meta:
        verbose_name = _("User profile")
        verbose_name_plural = _("User profiles")


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """When new user is created - create default profile"""
    if created:
        UserProfile.objects.get_or_create(user=instance)
        # Ensure that the external media directory exists
        create_external_media(username=instance.username)


@receiver(pre_save, sender=User)
def active_notifiction(sender, instance, **kwargs):
    """When a user account becomes active a user is notified by mail"""
    if not settings.EMAIL_NOTIFICATIONS:
        return 1
    try:
        old_instance = User.objects.get(pk=instance.pk)
    except User.DoesNotExist:
        pass
    else:
        if (
            old_instance.is_active is False
            and instance.is_active is True
            and not instance.is_citizen_science
        ):
            # send an email notification to a user that account has been activated
            send_mail(
                subject="Your Trapper account has just been activated!",
                message=(
                    "Dear {username},\n\n"
                    "We activated your account. Now you can login to Trapper.\n\n"
                    "Best regards,\n"
                    "Trapper Team"
                ).format(username=instance.username.capitalize()),
                from_email=None,
                recipient_list=[instance.email],
                fail_silently=True,
            )


class UserTask(models.Model):
    """
    Helper model to save information about user's celery tasks
    """

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    task_id = models.CharField(max_length=765, unique=True)

    class Meta:
        verbose_name = _("User task")
        verbose_name_plural = _("User tasks")


class UserRemoteTask(models.Model):
    """
    Helper model to save information about user's remote tasks e.g.
    AI classifications at remote VPS
    """

    name = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    task_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    status = models.CharField(
        choices=UserRemoteTaskStatus.CHOICES, max_length=10, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    log = models.TextField(blank=True, null=True)

    def __str__(self):
        return "Remote task: %s: %s" % (self.name, self.pk)

    class Meta:
        verbose_name = _("User remote task")
        verbose_name_plural = _("User remote tasks")


UPLOAD_DIR = "protected/accounts/user_data_package/"


def user_data_package_upload_to(instance, filename):
    return "{0}/user_id_{1}/{2}".format(UPLOAD_DIR, instance.user.id, filename)


class UserDataPackage(models.Model):
    """"""

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    package = models.FileField(upload_to=user_data_package_upload_to)

    package_type = models.CharField(
        choices=PackageType.CHOICES, max_length=1, null=True, blank=True
    )
    date_created = models.DateTimeField(null=True, blank=True, default=now)
    uuid4 = models.UUIDField(default=uuid.uuid4)
    description = SafeTextField(blank=True, null=True)

    def filename(self):
        return os.path.basename(self.package.name)

    def can_delete(self, user):
        return self.user == user

    def get_size(self):
        try:
            size = self.package.size
        except FileNotFoundError:
            size = 0
        return size

    def get_download_url(self):
        return reverse("accounts:data_package_sendfile_media", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = _("User data package")
        verbose_name_plural = _("User data packages")


@receiver(post_delete, sender=UserDataPackage)
def submission_delete(sender, instance, **kwargs):
    instance.package.delete(False)


class UserFTPAccount(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    uid = models.SmallIntegerField(default=-1)
    gid = models.SmallIntegerField(default=-1)
    dir = models.CharField(max_length=255)
    quota_files = models.IntegerField(default=-1)
    quota_size = models.BigIntegerField(default=-1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("User FTP account")
        verbose_name_plural = _("User FTP accounts")
