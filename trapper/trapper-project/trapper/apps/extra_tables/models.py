# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext_lazy as _

from trapper.apps.extra_tables.taxonomy import TaxonRank, LicenceScope


class Species(models.Model):
    latin_name = models.CharField(
        max_length=100, blank=False, verbose_name=_("Latin name")
    )
    english_name = models.CharField(
        max_length=100, blank=False, verbose_name=_("English name"), default="eng name"
    )
    family = models.CharField(
        max_length=100, blank=True, null=True, verbose_name=_("Family")
    )
    genus = models.CharField(
        max_length=100, blank=True, null=True, verbose_name=_("Genus")
    )
    taxon_id = models.CharField(max_length=20, null=True, verbose_name=_("Taxon ID"))
    taxon_rank = models.CharField(
        max_length=20, default=TaxonRank.SPECIES, choices=TaxonRank.CHOICES
    )
    # alternative_name = models.CharField(max_length=100, blank=True, verbose_name=_("Alternative name"))

    class Meta:
        ordering = ["english_name"]
        verbose_name = _("Species")
        verbose_name_plural = _("Species")

    def __str__(self):
        return str("%s (%s)") % (self.english_name, self.latin_name)


class Licence(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()
    title = models.CharField(max_length=200)
    scope = models.CharField(
        max_length=5, default=LicenceScope.DATA, choices=LicenceScope.CHOICES
    )

    def __str__(self):
        return str("%s (%s)") % (self.name, self.scope)
