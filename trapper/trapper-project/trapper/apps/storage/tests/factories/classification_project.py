import datetime
import factory

from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import ClassificationProject, \
    create_slug_id
from trapper.apps.media_classification.taxonomy import ClassificationProjectStatus
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)


class ClassificationProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ClassificationProject
        exclude = ("now",)

    owner = factory.SubFactory(UserFactory)

    name = factory.Sequence(lambda n: f"classification_project_{n}")
    slug_id = factory.LazyFunction(create_slug_id)
    slug = factory.LazyAttribute(lambda o: f"{o.name}-{o.slug_id}")
    research_project = factory.SubFactory(ResearchProjectFactory, owner=owner)

    classificator = factory.SubFactory(ClassificatorFactory)
    default_ai_model = None
    now = factory.LazyFunction(timezone.now)
    date_created = factory.LazyAttribute(
        lambda o: o.now - datetime.timedelta(minutes=10)
    )
    status = ClassificationProjectStatus.ONGOING
    deployment_based_nav = True
    enable_sequencing = True
    enable_crowdsourcing = True

    disabled_at = None
    disabled_by = None
