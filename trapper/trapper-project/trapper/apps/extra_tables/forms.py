from django import forms

from trapper.apps.extra_tables.taxonomy import TaxonomyClasses
from trapper.apps.common.forms import BaseCrispyForm

CLASS_CHOICES = [(i, i) for i in TaxonomyClasses.CLASSES]


class SpeciesImportForm(BaseCrispyForm):
    chosen_class = forms.MultipleChoiceField(
        choices=CLASS_CHOICES,
        widget=forms.SelectMultiple(),
        label="",
        help_text="Hold down ctrl (PC) or command (Mac) key to select multiple.",
    )
