from unittest.mock import MagicMock, patch

from django.core.files import File
from django.test import TestCase

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.forms import (
    ClassificationImportForm,
    ClassificationPublishForm,
    SequenceBuildForm,
    ClassifyAIForm,
)
from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class MediaClassificationFormsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(MediaClassificationFormsTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

    @patch(
        "trapper.apps.media_classification.models.ClassificationProject.objects.get_accessible"
    )
    def test_classification_import_form(self, mock_get_accessible):
        mock_get_accessible.return_value = ClassificationProject.objects.all()
        mock_file = MagicMock(spec=File)
        mock_file.read.return_value = "name,value\ntest,2"
        classification_import_form = ClassificationImportForm(
            data={
                "observations_csv": mock_file,
                "project": self.classification_project.pk,
            }
        )
        classification_import_form.get_layout()

    @patch(
        "trapper.apps.media_classification.models.ClassificationProject.objects.get_accessible"
    )
    def test_classification_publish_form(self, mock_get_accessible):
        mock_get_accessible.return_value = ClassificationProject.objects.all()
        classification_publish_form = ClassificationPublishForm()
        classification_publish_form.get_layout()

    @patch(
        "trapper.apps.media_classification.models.ClassificationProject.objects.get_accessible"
    )
    def test_sequence_build_form(self, mock_get_accessible):
        mock_get_accessible.return_value = ClassificationProject.objects.all()
        sequence_build_form = SequenceBuildForm(
            data={"time_interval": 5, "deployments": True, "overwrite": True}
        )
        sequence_build_form.get_layout()
