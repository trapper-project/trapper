# -*- coding: utf-8 -*-
from trapper.apps.common.serializers import BasePKSerializer
from trapper.apps.extra_tables.models import Species


class SpeciesSerializer(BasePKSerializer):
    """"""

    class Meta:
        model = Species
        fields = [
            "pk",
            "__str__",
            "english_name",
            "latin_name",
            "genus",
            "family",
            "genus",
        ]


class CSSpeciesSerializer(BasePKSerializer):
    """"""

    class Meta:
        model = Species
        fields = [
            "id",
            "__str__",
            "english_name",
            "latin_name",
            "genus",
            "family",
            "genus",
        ]
