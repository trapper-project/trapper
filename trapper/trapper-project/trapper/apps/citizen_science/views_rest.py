from functools import lru_cache
import json
from django.utils.translation import get_language
from django.http import HttpResponse, Http404
from django.views.generic import View
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from trapper.apps.citizen_science.filters import CSClassificationFilter
from trapper.apps.citizen_science.mixins import BaseCSMixin
from trapper.apps.citizen_science.models import CitizenScienceConfiguration
from trapper.apps.citizen_science.serializers import (
    CSDashboardSerializer,
    CSProjectsSerializer,
    CSProjectSerializer,
    CSProjectMediaSerializer,
    CSProjectMediaResourceSerializer,
    CSProjectUploadMediaSerializer,
    CSProjectCreateUploadSerializer,
    CSProjectCompleteUploadSerializer,
    CSInboxSerializer,
    CSProjectMediaScrollSerializer,
)
from trapper.apps.common.models import Consent
from trapper.apps.common.serializers import ConsentSerializer, ProjectSettingsSerializer
from trapper.apps.common.views_api import CSListPagination, CSCursorPagination
from trapper.apps.extra_tables.models import Species
from trapper.apps.media_classification.models import (
    Classification,
    ClassificationDynamicAttrs,
    ClassificationProject,
    ClassificationProjectRole,
    FavoriteClassification,
    UserClassification,
    UserClassificationDynamicAttrs,
)
from trapper.apps.messaging.taxonomies import MessageType
from trapper.apps.sendfile.views import BaseServeFileView
from trapper.apps.storage.models import Resource


class CSGDPRView(generics.RetrieveAPIView):
    serializer_class = ConsentSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return Consent.objects.filter(
            project=Consent.Project.CS, type=Consent.Type.GDPR, language=get_language()
        ).first()


class CSTOSView(generics.RetrieveAPIView):
    serializer_class = ConsentSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return Consent.objects.filter(
            project=Consent.Project.CS, type=Consent.Type.TOS, language=get_language()
        ).first()


@lru_cache(maxsize=1)
def get_app_version():
    version_file_path = "/version.json"
    try:
        with open(version_file_path, "r") as file:
            version = file.read()
            version = json.loads(version)
            version = version.get("version", "unknown")
            if not isinstance(version, str):
                version = "unknown"
            return version
    except (FileNotFoundError, json.JSONDecodeError):
        return "unknown"


class CSAppVersionView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        version = get_app_version()
        return Response({"version": version})


class CSPublicStatsView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        classification_projects = ClassificationProject.objects.filter(
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC
        )

        classification_projects_count = classification_projects.count()

        users = (
            ClassificationProjectRole.objects.filter(
                classification_project__in=classification_projects
            )
            .values("user")
            .distinct()
            .count()
        )

        all_classifications = Classification.objects.filter(
            project__in=classification_projects
        )
        all_user_classifications = UserClassification.objects.filter(
            classification__in=all_classifications
        )

        dynamic_attrs = ClassificationDynamicAttrs.objects.filter(
            classification__in=all_classifications
        )

        user_classification_dynamic_attrs = (
            UserClassificationDynamicAttrs.objects.filter(
                userclassification__in=all_user_classifications
            )
        )

        species = (
            dynamic_attrs.filter(species__isnull=False).values("species_id").distinct()
        )

        species_user = (
            user_classification_dynamic_attrs.filter(species__isnull=False)
            .values("species_id")
            .distinct()
        )

        species_count = (
            Species.objects.filter(
                id__in=species.union(species_user).values("species_id")
            )
            .distinct()
            .count()
        )

        resource_ids = all_classifications.values("resource_id").distinct()

        resources = Resource.objects.filter(id__in=resource_ids)

        locations = resources.values("deployment__location").distinct().count()

        images = resources.count()

        return Response(
            {
                "classification_projects": classification_projects_count,
                "users": users,
                "species": species_count,
                "locations": locations,
                "images": images,
            }
        )


class CSProjectLogoView(BaseServeFileView):
    authenticated_only = False

    def get(self, request, *args, **kwargs):
        cs_config = CitizenScienceConfiguration.get_solo()
        if not cs_config.project_logo:
            # Return empty svg if logo is not set, allow browser to cache it for 5 minutes
            empty_svg = """<?xml version="1.0" encoding="UTF-8"?>
            <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0"></svg>
            """
            return HttpResponse(
                empty_svg,
                content_type="image/svg+xml",
                headers={
                    "Cache-Control": "max-age=300, public",
                },
            )

        return self.serve_file(cs_config.project_logo)


class CSProjectBackgroundView(BaseServeFileView):
    """Serve background image for Citizen Science Configuration."""

    authenticated_only = False

    def get(self, request, *args, **kwargs):
        cs_config = CitizenScienceConfiguration.get_solo()
        if not cs_config.background_image:
            empty_svg = """<?xml version="1.0" encoding="UTF-8"?>
            <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0"></svg>
            """
            return HttpResponse(
                empty_svg,
                content_type="image/svg+xml",
                headers={
                    "Cache-Control": "max-age=300, public",
                },
            )
        return self.serve_file(cs_config.background_image)


class CSProjectBackgroundImageView(BaseCSMixin, BaseServeFileView):
    """Serve background image for Classification Project."""

    authenticated_only = False

    def get(self, request, *args, **kwargs):
        queryset = ClassificationProject.objects.filter(
            citizen_science_status__in=[
                ClassificationProject.CitizenScienceStatus.PUBLIC,
                ClassificationProject.CitizenScienceStatus.PRIVATE,
            ]
        ).distinct()
        slug_id = self.kwargs["cs_project_slug"][-12:]

        instance = get_object_or_404(
            queryset,
            slug_id=slug_id,
        )

        if not instance.image_background:
            raise Http404
        return self.serve_file(instance.image_background)


class CSProjectSidebarBackgroundView(BaseServeFileView):
    authenticated_only = False

    def get(self, request, *args, **kwargs):
        cs_config = CitizenScienceConfiguration.get_solo()
        if not cs_config.sidebar_image:
            empty_svg = """<?xml version="1.0" encoding="UTF-8"?>
            <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0"></svg>
            """
            return HttpResponse(
                empty_svg,
                content_type="image/svg+xml",
                headers={
                    "Cache-Control": "max-age=300, public",
                },
            )
        return self.serve_file(cs_config.sidebar_image)


class CSAdditionalStylesView(View):
    def get(self, request, *args, **kwargs):
        cs_config = CitizenScienceConfiguration.get_solo()
        if not cs_config.additional_styles:
            return HttpResponse(
                "",
                content_type="text/css",
                headers={
                    "Cache-Control": "max-age=300, public",
                },
            )
        return HttpResponse(
            cs_config.additional_styles,
            content_type="text/css",
            headers={
                "Cache-Control": "max-age=300, public",
            },
        )


class CSProjectSettingsView(generics.RetrieveAPIView):
    serializer_class = ProjectSettingsSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return CitizenScienceConfiguration.get_solo()


class CSInboxView(generics.ListAPIView):
    serializer_class = CSInboxSerializer
    pagination_class = CSListPagination

    def get_queryset(self):
        return self.request.user.received_messages.filter(date_received=None).exclude(
            message_type__in=[
                MessageType.COLLECTION_REQUEST,
                MessageType.RESOURCE_REQUEST,
                MessageType.COLLECTION_DELETED,
                MessageType.RESEARCH_PROJECT_CREATED,
            ]
        )


class CSInboxMarkAsReceivedView(generics.UpdateAPIView):
    serializer_class = CSInboxSerializer
    http_method_names = ["patch"]
    lookup_url_kwarg = "message_id"

    def get_queryset(self):
        return self.request.user.received_messages.filter(date_received=None)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.mark_received(self.request.user)
        return Response(status=status.HTTP_204_NO_CONTENT)


class CSProjectsView(generics.ListAPIView):
    serializer_class = CSProjectsSerializer

    def get_queryset(self):
        return ClassificationProject.objects.only_cs(self.request.user)


class CSProjectView(BaseCSMixin, generics.RetrieveAPIView):
    serializer_class = CSProjectSerializer


class CSProjectMediaView(BaseCSMixin, generics.ListAPIView):
    serializer_class = CSProjectMediaSerializer
    pagination_class = CSListPagination
    filterset_class = CSClassificationFilter

    def get_own_classifications(self, classification_ids):
        user_classifications = (
            UserClassification.objects.filter(
                owner=self.request.user,
                classification_id__in=classification_ids,
            )
            .select_related("classification")
            .prefetch_related("classification__dynamic_attrs")
        )
        return {c.classification_id: c for c in user_classifications}

    def get_favorite_classifications(self, classification_ids):
        return FavoriteClassification.objects.filter(
            classification_id__in=classification_ids,
            user=self.request.user,
        ).values_list("classification_id", flat=True)

    def get_resources(self, resource_ids):
        resources = Resource.objects.filter(id__in=resource_ids).select_related(
            "deployment", "deployment__location", "owner", "owner__userprofile"
        )
        return {r.id: r for r in resources}

    def get_queryset(self):
        return (
            self.get_classifications_queryset()
            .select_related(
                "approved_source_ai",
                "approved_source",
                "sequence",
            )
            .prefetch_related(
                "dynamic_attrs",
                "dynamic_attrs__species",
                "approved_source_ai__dynamic_attrs",
                "approved_source_ai__dynamic_attrs__species",
                "approved_source__dynamic_attrs",
                "approved_source__dynamic_attrs__species",
            )
        )

    def list(self, request, *args, **kwargs):
        qs = self.get_queryset()
        qs_f = self.filter_queryset(qs)

        sort_by = self.request.GET.get("sort_by")
        if sort_by:
            reverse = self.request.GET.get("reverse") == "true"
            sort_by_str = "-" + sort_by if reverse else sort_by
            qs_f = qs_f.order_by(sort_by_str)

        page = self.paginate_queryset(qs_f)
        classification_ids = [c.id for c in page]
        resource_ids = [c.resource_id for c in page]
        ctx = self.get_serializer_context()
        ctx["own_classifications"] = self.get_own_classifications(classification_ids)
        ctx["favorite_classifications"] = self.get_favorite_classifications(
            classification_ids
        )
        ctx["resources"] = self.get_resources(resource_ids)

        serializer = self.get_serializer(page, many=True, context=ctx)
        return self.get_paginated_response(serializer.data)


class CSProjectMediaScrollView(CSProjectMediaView):
    pagination_class = CSCursorPagination
    serializer_class = CSProjectMediaScrollSerializer
    queryset = None

    # override list and get same queryset to create sequence map
    def list(self, request, *args, **kwargs):
        self.queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(self.queryset)

        classification_ids = [c.id for c in page]
        resource_ids = [c.resource_id for c in page]
        ctx = self.get_serializer_context()
        ctx["own_classifications"] = self.get_own_classifications(classification_ids)
        ctx["favorite_classifications"] = self.get_favorite_classifications(
            classification_ids
        )
        ctx["resources"] = self.get_resources(resource_ids)

        serializer = self.get_serializer(page, many=True, context=ctx)
        return self.get_paginated_response(serializer.data)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        sequences = []
        sequences_map = {}
        for cls in self.queryset.values("pk", "sequence_id"):
            if cls["sequence_id"] not in sequences_map:
                sequences.append(cls["sequence_id"])
            try:
                sequences_map[cls["sequence_id"]].append(cls["pk"])
            except KeyError:
                sequences_map[cls["sequence_id"]] = [cls["pk"]]

        context["sequences"] = sequences
        context["sequences_map"] = sequences_map
        return context


class CSProjectMediaResourceView(BaseCSMixin, generics.RetrieveAPIView):
    serializer_class = CSProjectMediaResourceSerializer
    pagination_class = CSListPagination
    filterset_class = CSClassificationFilter

    def get_serializer_context(self):
        ctx = super().get_serializer_context()
        ctx["own_classification"] = (
            self.get_object()
            .user_classifications.filter(owner=self.request.user)
            .first()
        )
        return ctx

    def get_object(self):
        qs = self.get_classifications_queryset()
        return get_object_or_404(qs, pk=self.kwargs["media_pk"])


class CSDashboardView(BaseCSMixin, generics.RetrieveAPIView):
    serializer_class = CSDashboardSerializer


class CSProjectCreateUploadView(BaseCSMixin, generics.CreateAPIView):
    serializer_class = CSProjectCreateUploadSerializer


class CSProjectUploadMediaView(BaseCSMixin, generics.CreateAPIView):
    serializer_class = CSProjectUploadMediaSerializer

    def get_queryset(self):
        return self.get_classifications_queryset()


class CSProjectCompleteUploadView(BaseCSMixin, generics.CreateAPIView):
    serializer_class = CSProjectCompleteUploadSerializer
