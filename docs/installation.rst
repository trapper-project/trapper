.. _installation:

============
Installation
============

TRAPPER is provided as `Docker <https://www.docker.com/>`_ container and can be deployed on both Linux- and Windows-based servers. However, we recommend a Linux distribution based on Debian e.g. `Ubuntu Server <http://www.ubuntu.com/server>`_. TRAPPER uses `Docker composer <https://docs.docker.com/compose/>`_, a tool for defining and running multi-container Docker applications. With this technology all basic services needed to run TRAPPER (including database and FTP server) can be automatically deployed and configured on a dedicated server with a possibility left open to configure particular services manually (e.g. a scenario with an external database server).

Installing Docker Community Edition (CE) & Docker Compose
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1) Follow the steps from the official Docker documentation:

   * Ubuntu: 
     https://docs.docker.com/install/linux/docker-ce/ubuntu/

   * Windows:
     https://hub.docker.com/editions/community/docker-ce-desktop-windows

2) Install Docker Compose: https://docs.docker.com/compose/

.. note::
   To learn more about Docker technology including the management of Docker images and
   containers please read the official `Docker documentation <https://docs.docker.com/get-started/>`_.  
   
Getting the source code of TRAPPER
++++++++++++++++++++++++++++++++++

1) First, clone the source code of TRAPPER to your local repository:
   
   .. code-block:: bash
                   
      $ git clone https://gitlab.com/trapper-project/trapper.git
    
2) To get the most up-to-date version of TRAPPER switch to a branch ``development``:

   .. code-block:: bash

      $ git checkout development

.. note::
   It is much easier to use Git Bash for cloning git repositories on Windows instead
   of using a standard Windows command line: https://git-scm.com/download/win

   
Running TRAPPER 
++++++++++++++++++++++++++++++++++++

.. note::
   If you need a custom configuration of TRAPPER (e.g. external database, ftp
   and email services) please check the :ref:`configuration` section for details.

1) Copy the ``trapper.env`` file to the ``.env`` file, you can do it using the command:

   .. code-block:: bash
      
      $ cp trapper.env .env
 
2) Adjust the variables in the ``.env`` file if you use a non-standard installation, e.g. the external postgresql database.


3) Run TRAPPER with the following command:

   * Production

     .. code-block:: bash
		     
	$ ./start.sh -pb prod

   * Development

     .. code-block:: bash

	$ ./start.sh -pb dev

.. note::
   Flags ``-p`` and ``-b`` tell the script to ``pull`` all required docker images and to ``build`` the ``trapper`` image locally.
   To learn more about all possible running options simply type ``./start.sh``. For example, to run TRAPPER with an external postgresql
   database you can use flag ``-d`` (but first check the :ref:`configuration-database` section). 

.. warning::
   The process of pulling, building & starting all docker containers can take a while!

4) Now your TRAPPER instance should be up!
              
Creating the admin (superuser) account
++++++++++++++++++++++++++++++++++++++

Enter the ``trapper`` docker container and create the ``superuser``:

   .. code-block:: bash
		   
      $ docker exec -it trapper bash
      $ python /app/trapper/trapper-project/manage.py createsuperuser

After providing a username, email and password you should be able to login
to TRAPPER.


SSL certificates
++++++++++++++++

If you already have the SSL certificates for your TRAPPER website you can use them by providing proper paths in
the `.env` configuration file. See the :ref:`configuration-ssl-certificates` section for details. These certificates
will be used to configure the HTTPS ``nginx`` proxy-server within a dedicated container. When there is no SSL certificates
provided they will be generated automatically using the `openssl <https://www.openssl.org/>`_ toolkit.  

.. note::        
   For example, you can get free SSL certificates from 
   `Let's Encrypt <https://letsencrypt.org>`_.
   
