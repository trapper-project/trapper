# -*- coding: utf-8 -*-
"""Views used by DRF to display json data used by storage applications"""
import os
from django.conf import settings

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.filters import FullTextSearchFilter
from trapper.apps.common.views_api import PaginatedReadOnlyModelViewSet
from trapper.apps.storage import serializers as storage_serializers
from trapper.apps.storage.collection_upload import CollectionProcessor
from trapper.apps.storage.filters import ResourceFilter, CollectionFilter
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.storage.tasks import celery_process_collection_upload
from trapper.apps.geomap.models import Location
from trapper.apps.accounts.utils import get_external_collections_path


class ResourceViewSet(PaginatedReadOnlyModelViewSet):
    """Return list of resources.

    * For anonymous users return only publicly available resources
    * For logged in users return resources that are available for user
    """

    serializer_class = storage_serializers.ResourceSerializer
    filter_backends = (DjangoFilterBackend, FullTextSearchFilter)
    filterset_class = ResourceFilter
    search_fields = [
        "search_data_vector",
        "search_name_vector",
    ]

    def get_queryset(self):
        return (
            Resource.objects.get_accessible(self.request.user)
            .select_related(
                "owner",
                "deployment__location",
            )
            .prefetch_related("managers", "tags")
        )


class ResourceLocationViewSet(PaginatedReadOnlyModelViewSet):
    """TODO: docstrings"""

    pagination_class = None
    serializer_class = storage_serializers.ResourceLocationSerializer
    filter_backends = (DjangoFilterBackend, FullTextSearchFilter)
    filterset_class = ResourceFilter
    search_fields = [
        "search_data_vector",
        "search_name_vector",
    ]

    def get_queryset(self):
        location_pk = self.kwargs["location_pk"]
        try:
            location = Location.objects.get(pk=location_pk)
        except Location.DoesNotExist:
            return Resource.objects.none()
        if location.can_view(user=self.request.user):
            qs = Resource.objects.filter(deployment__location_id=location_pk)
            return qs.prefetch_related("tags")
        else:
            return Resource.objects.none()


class ResourceCollectionViewSet(PaginatedReadOnlyModelViewSet):
    """Return list of resources for given collection."""

    serializer_class = storage_serializers.ResourceSerializer
    filter_backends = (DjangoFilterBackend, FullTextSearchFilter)
    filterset_class = ResourceFilter
    search_fields = ["search_data_vector", "search_name_vector"]

    def get_queryset(self):
        collection_pk = self.kwargs["collection_pk"]
        try:
            collection = Collection.objects.get(pk=collection_pk)
        except Collection.DoesNotExist:
            return Resource.objects.none()
        if collection.can_view(user=self.request.user):
            if collection.period_begin and collection.period_end:
                qs = collection.resources.filter(
                    # this is a performance workaround (faster query with ordering)
                    date_recorded__gte=collection.period_begin,
                    date_recorded__lte=collection.period_end,
                )
            else:
                qs = collection.resources.all()
            return qs.select_related(
                "owner",
                "deployment__location",
            ).prefetch_related("managers", "tags")
        else:
            return Resource.objects.none()


class CollectionViewSet(PaginatedReadOnlyModelViewSet):
    """Return list of collections.

    * For anonymous users return only publicly available resources
    * For logged in users return resources that are available for user
    """

    serializer_class = storage_serializers.CollectionSerializer
    filterset_class = CollectionFilter
    search_fields = ["name", "owner__username"]

    def get_queryset(self):
        """Limit collections depend on user login status"""
        return (
            Collection.objects.get_accessible(self.request.user)
            .select_related("owner")
            .prefetch_related("managers")
        )


class CollectionOnDemandViewSet(CollectionViewSet):
    def get_queryset(self):
        """Limit collections depend on user login status"""
        return Collection.objects.get_ondemand(user=self.request.user)


class CollectionMapViewSet(CollectionViewSet):
    pagination_class = None


class CollectionAppendViewSet(CollectionViewSet):
    pagination_class = None
    serializer_class = storage_serializers.CollectionAppendSerializer

    def get_queryset(self):
        user = self.request.user
        return Collection.objects.get_editable(user=user)


class ProcessUploadedCollection(APIView):
    """
    Trigger processing of a collection already uploaded to FTP server.
    ```
    data = {
        "yaml_file": "VENEZUELA_20220719_kbubnicki.yaml",
        "zip_file": "VENEZUELA_20220719_kbubnicki.zip",
        "remove_zip": False
    }
    ```
    """

    permission_classes = (permissions.IsAuthenticated,)

    def response_struct(self, message, errors, task_id):
        return {"data": {"message": message, "errors": errors, "task_id": task_id}}

    def post(self, request):
        user = request.user
        data = request.data
        yaml_file = os.path.join(
            get_external_collections_path(username=user.username),
            data.get("yaml_file", ""),
        )
        zip_file = os.path.join(
            get_external_collections_path(username=user.username),
            data.get("zip_file", ""),
        )
        remove_zip = data.get("remove_zip", False)

        if not (os.path.exists(yaml_file) and os.path.exists(zip_file)):
            return Response(
                self.response_struct(
                    "The requested files are not available on the FTP server.",
                    None,
                    None,
                ),
                status=status.HTTP_404_NOT_FOUND,
            )

        # validate YAML collection definition file
        processor = CollectionProcessor(
            definition_file=yaml_file,
            owner=user,
        )
        errors = processor.validate_definition()
        if errors:
            return Response(
                self.response_struct(
                    "Invalid YAML collection definition file.",
                    errors,
                    None,
                ),
                status=status.HTTP_400_BAD_REQUEST,
            )

        params = {
            "owner": user,
            "definition_file": yaml_file,
            "archive_file": zip_file,
            "remove_zip": remove_zip,
        }

        # everything is ok so lets try to start collection processing celery task
        if settings.CELERY_ENABLED:
            task = celery_process_collection_upload.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            return Response(
                self.response_struct(
                    (
                        "You have successfully run the celery task. Your uploaded data "
                        "package is being processed now. "
                    ),
                    None,
                    task.task_id,
                ),
                status=status.HTTP_200_OK,
            )
        else:
            msg = celery_process_collection_upload(**params)
            return Response(
                self.response_struct(
                    msg,
                    None,
                    None,
                ),
                status=status.HTTP_200_OK,
            )
