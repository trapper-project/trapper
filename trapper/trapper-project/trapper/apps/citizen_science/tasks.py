import logging
from datetime import timedelta

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import caches
from django.utils import timezone

from trapper import app
from trapper.apps.accounts.models import UserTask
from trapper.apps.accounts.taxonomy import UserRemoteTaskStatus
from trapper.apps.citizen_science.models import CitizenScienceConfiguration
from trapper.apps.media_classification.ai_providers.ai_provider_factory import (
    get_ai_provider_manager,
)

from trapper.apps.media_classification.ai_providers.ai_providers_base import (
    RemoteClassificationParams,
    AIProviderException,
)
from trapper.apps.media_classification.ai_providers.trapper_ai import (
    TrapperAIProviderManager,
)
from trapper.apps.media_classification.models import (
    Classification,
    AIClassificationJob,
    AIClassification,
    ClassificationProject,
    ClassificationProjectCollection,
)
from trapper.apps.media_classification.tasks import (
    celery_approve_ai_classifications,
    celery_build_sequences,
    celery_blur_humans,
    celery_blur_vehicles,
    celery_resource_feedback,
)
from trapper.apps.storage.models import Collection

User = get_user_model()

logger = logging.getLogger(__name__)


@app.task
def celery_check_if_cs_collection_finished(
    classification_project_pk, collection_pk, user_pk
):
    """
    All regular users whose does not log into the platform for more than two years have
    to be automatically marked as inactive.
    """
    now = timezone.now()

    try:
        collection = Collection.objects.get(pk=collection_pk, owner=user_pk)
    except Collection.DoesNotExist:
        return
    # If collection is already finished do nothing
    if collection.is_complete_cs_collection:
        return
    # if collection is not finished check date between last uploaded resource
    # and collection
    last_resource = collection.resources.order_by("-date_uploaded").first()
    if (
        collection.date_start_cs_collection_upload
        >= now - timedelta(seconds=settings.CS_CHECK_EVERY_COLLECTION_FINISHED)
        or last_resource
        and last_resource.date_uploaded
        > now - timedelta(seconds=settings.CS_CHECK_EVERY_COLLECTION_FINISHED)
    ):
        # if last resource is uploaded before decelerated time, recheck later
        celery_check_if_cs_collection_finished.apply_async(
            (classification_project_pk, collection_pk, user_pk),
            countdown=settings.CS_CHECK_EVERY_COLLECTION_FINISHED,
        )
    else:
        # finish collection if there is no resources or after decelerated time there is
        # no new resource
        if settings.CELERY_ENABLED:
            complete_collection_upload.delay(
                classification_project_pk, user_pk, collection.pk
            )
        else:
            complete_collection_upload(
                classification_project_pk, user_pk, collection.pk
            )


@app.task
def complete_collection_upload(
    classification_project_pk: int, user_pk: int, collection_pk: int
):
    # finish collection if there is no resources or after decelerated time there is no
    # new resource
    try:
        collection = Collection.objects.get(pk=collection_pk)
    except Collection.DoesNotExist:
        return
    collection.is_complete_cs_collection = True
    collection.date_complete_cs_collection_upload = timezone.now()
    collection.save(
        update_fields=[
            "is_complete_cs_collection",
            "date_complete_cs_collection_upload",
        ]
    )
    # Run TrapperAI
    classification_ids = list(
        Classification.objects.filter(
            approved_source_ai__isnull=True,
            resource__in=collection.resources.values_list("pk", flat=True),
        ).values_list("pk", flat=True)
    )
    conf = CitizenScienceConfiguration.get_solo()
    project = ClassificationProject.objects.get(pk=classification_project_pk)
    ai_model = project.object_detection_ai_model
    if not ai_model:
        logger.error("AI provider is not set for Citizen Science")
    elif classification_ids:
        manager: TrapperAIProviderManager = get_ai_provider_manager(ai_model)
        try:
            params = RemoteClassificationParams(
                classification_ids=classification_ids,
                classify_preview_files=False,
                project_id=classification_project_pk,
            )

            job_id = manager.request_classification(
                params=params,
                user_id=user_pk,
            )
            check_trapper_ai_classification_finished.apply_async(
                (job_id, classification_project_pk, collection_pk, user_pk),
                countdown=7,
            )
        except AIProviderException:
            pass

    # Run sequence builder
    user = User.objects.get(pk=user_pk)
    project_collections = ClassificationProjectCollection.objects.filter(
        project_id=classification_project_pk, collection__collection=collection
    )
    celery_build_sequences_params = {
        "data": {
            "time_interval": conf.build_sequence_time_interval,
            "deployments": False,
            "project_collections": project_collections,
        },
        "user": user,
    }
    if settings.CELERY_ENABLED:
        task = celery_build_sequences.delay(**celery_build_sequences_params)
        user_task = UserTask(user=user, task_id=task.task_id)
        user_task.save()
    else:
        celery_build_sequences(**celery_build_sequences_params)


@app.task
def check_trapper_ai_classification_finished(
    classification_job_pk: int,
    classification_project_pk: int,
    collection_pk: int,
    user_pk: int,
):
    try:
        classification_job = AIClassificationJob.objects.get(pk=classification_job_pk)
    except AIClassificationJob.DoesNotExist:
        return
    status = classification_job.user_remote_task.status
    if status == UserRemoteTaskStatus.SUCCESS:
        ai_classification_pks = AIClassification.objects.filter(
            classification__collection__project_id=classification_project_pk,
            classification__collection__collection__collection_id=collection_pk,
            classification__approved_source_ai__isnull=True,
            model_id=classification_job.ai_provider_id,
        ).values_list("pk", flat=True)
        project = ClassificationProject.objects.get(pk=classification_project_pk)
        user = User.objects.get(pk=user_pk)

        fields_to_copy = ["observation_type"]
        logger.info(
            "Running `celery_approve_ai_classifications` with mark_as_approved=False."
        )
        msg = celery_approve_ai_classifications(
            user=user,
            project_id=classification_project_pk,
            ai_classification_pks=ai_classification_pks,
            fields_to_copy=fields_to_copy,
            mark_as_approved=False,
            minimum_confidence=classification_job.ai_provider.minimum_confidence,
            overwrite_attrs=True,
            copy_bboxes=True,
        )
        logger.info(msg)
        project_collections = ClassificationProjectCollection.objects.filter(
            project_id=classification_project_pk,
            collection__collection_id=collection_pk,
        )

        celery_blur_params = {
            "pks": project_collections.values_list("pk", flat=True),
            "user": user,
            "exclude_blurred": False,
            "only_approved": False,
        }
        logger.info("Running celery_blur_humans.")
        msg = celery_blur_humans(**celery_blur_params)
        logger.info(msg)

        logger.info("Running celery_blur_vehicles.")
        msg = celery_blur_vehicles(**celery_blur_params)
        logger.info(msg)

        logger.info(
            "Running `celery_approve_ai_classifications` with mark_as_approved=True."
        )

        if not project.copy_ai_classifications or not project.species_ai_model:
            msg = celery_approve_ai_classifications(
                user=user,
                project_id=classification_project_pk,
                ai_classification_pks=ai_classification_pks,
                fields_to_copy=fields_to_copy,
                mark_as_approved=True,
                minimum_confidence=classification_job.ai_provider.minimum_confidence,
                overwrite_attrs=False,
            )
            logger.info(msg)
        # Clear cache because TrapperPaginator caches count result, and doesn't
        # refresh queryset,
        caches["default"].clear()

        # TrapperAI - species

        collection = Collection.objects.get(pk=collection_pk)
        classification_ids = list(
            Classification.objects.filter(
                resource__in=collection.resources.values_list("pk", flat=True),
            ).values_list("pk", flat=True)
        )
        if project.species_ai_model and classification_ids:
            logger.info(
                f"Running TrapperAI species classification for project {classification_project_pk}"
                + f" and collection {collection_pk}."
            )
            manager: TrapperAIProviderManager = get_ai_provider_manager(
                project.species_ai_model
            )
            try:
                params = RemoteClassificationParams(
                    classification_ids=classification_ids,
                    classify_preview_files=False,
                    project_id=classification_project_pk,
                )

                job_id = manager.request_classification(
                    params=params,
                    user_id=user_pk,
                )
                check_trapper_ai_species_classification_finished.apply_async(
                    (job_id, classification_project_pk, collection_pk, user_pk),
                    countdown=7,
                )
            except AIProviderException:
                pass
    elif status in [UserRemoteTaskStatus.REJECTED, UserRemoteTaskStatus.FAILURE]:
        logger.error(
            f"Classification job {classification_job_pk} failed or rejected. Unable to complete collection processing."
        )
        return
    else:
        # if job takes long more then 2h cancel
        # if classification_job.date_created >= timezone.now() - timedelta(hours=2):
        #     logger.error(f"Classification job {classification_job_pk} takes more then 2 hours. Cancel celery job.")
        #     return
        check_trapper_ai_classification_finished.apply_async(
            (classification_job_pk, classification_project_pk, collection_pk, user_pk),
            countdown=7,
        )


@app.task
def check_trapper_ai_species_classification_finished(
    classification_job_pk: int,
    classification_project_pk: int,
    collection_pk: int,
    user_pk: int,
):
    try:
        classification_job = AIClassificationJob.objects.get(pk=classification_job_pk)
    except AIClassificationJob.DoesNotExist:
        return
    status = classification_job.user_remote_task.status
    if status == UserRemoteTaskStatus.SUCCESS:
        project = ClassificationProject.objects.get(pk=classification_project_pk)
        if not project.copy_ai_classifications:
            return

        iou_threshold = project.species_matching_iou_threshold

        ai_classifications = AIClassification.objects.filter(
            classification__collection__project_id=classification_project_pk,
            classification__collection__collection__collection_id=collection_pk,
            model_id=classification_job.ai_provider_id,
        )
        user = User.objects.get(pk=user_pk)
        msg = celery_approve_ai_classifications(
            user=user,
            project_id=classification_project_pk,
            ai_classification_pks=ai_classifications.values_list("pk", flat=True),
            fields_to_copy=["species"],
            mark_as_approved=True,
            minimum_confidence=classification_job.ai_provider.minimum_confidence,
            overwrite_attrs=True,
            merge_bboxes=True,
            iou_threshold=iou_threshold,
        )
        logger.info(msg)

        # we need to approve object detections and not species classification because
        # other parts of the system are not ready for species classification
        approve_cs_trapper_ai_object_detections.delay(
            classification_project_pk, collection_pk, user_pk
        )

    elif status in [UserRemoteTaskStatus.REJECTED, UserRemoteTaskStatus.FAILURE]:
        logger.error(
            f"Classification job {classification_job_pk} failed or rejected. Unable to complete collection processing."
        )
        return
    else:
        # if job takes long more then 2h cancel
        # if classification_job.date_created >= timezone.now() - timedelta(hours=2):
        #     logger.error(f"Classification job {classification_job_pk} takes more then 2 hours. Cancel celery job.")
        #     return
        check_trapper_ai_species_classification_finished.apply_async(
            (classification_job_pk, classification_project_pk, collection_pk, user_pk),
            countdown=7,
        )


@app.task
def approve_cs_trapper_ai_object_detections(
    classification_project_pk: int,
    collection_pk: int,
    user_pk: int,
):
    project = ClassificationProject.objects.get(pk=classification_project_pk)
    ai_model = project.object_detection_ai_model
    ai_classifications = AIClassification.objects.filter(
        classification__collection__project_id=classification_project_pk,
        classification__collection__collection__collection_id=collection_pk,
        model_id=ai_model,
    )
    ai_classification_pks = ai_classifications.values_list("pk", flat=True)
    user = User.objects.get(pk=user_pk)
    logger.info(
        "Running `celery_approve_ai_classifications` with mark_as_approved=True."
    )
    msg = celery_approve_ai_classifications(
        user=user,
        project_id=classification_project_pk,
        ai_classification_pks=ai_classification_pks,
        fields_to_copy=[],
        mark_as_approved=True,
        minimum_confidence=ai_model.minimum_confidence,
        overwrite_attrs=False,
    )
    logger.info(msg)

    # Update search_data_vector field for related resources
    # This task copy approved AI classifications attrs to resource objects (vector searach space)
    params = {"user": user, "classifications": ai_classifications}
    celery_resource_feedback.delay(**params)
