'use strict';

(function (global, namespace, moduleName) {

    var module = {};
    var plugins = global[namespace].Plugins;
    var alert = global[namespace].Alert;
    var modal = global[namespace].Modal;
    var doc = global.document;

    var comments = function () {
        var replyBtns = doc.querySelectorAll('.btn-reply');
        var parentId = doc.querySelector('#id_parent');
        var message = doc.querySelector('#id_comment');
        var commentTab = doc.querySelector('#tab-comment-form');

        function reply(event) {
            parentId.value = event.target.parentNode.parentNode.parentNode.dataset.pk;
            commentTab.click();
            message.focus();
        }

        [].forEach.call(replyBtns, function (btn) {
            btn.addEventListener('click', reply);
        });
    };

    var classifications = function () {
        function getInfo(btn) {
            var td = btn.parentNode.parentNode.querySelectorAll('td');
            return 'Creator: ' + td[0].innerHTML + ', Date: ' + td[1].innerHTML;
        }

        var deleteBtns = doc.querySelectorAll('.btn-delete'),
            approveBtns = doc.querySelectorAll('.btn-approve'),
            approveAIBtns = doc.querySelectorAll('.btn-approve-ai');

        [].forEach.call(deleteBtns, function (btn) {
            btn.addEventListener('click', function (event) {
                event.preventDefault();
                var target = event.target;
                if (target.tagName.toLowerCase() === 'span') {
                    target = target.parentNode;
                }
                modal.confirm({
                    title: global['Translations']['GLOBAL']['delete_classification_question'],
                    content: getInfo(btn),
                    buttons: [{
                        type: 'danger',
                        label: global['Translations']['GLOBAL']['yes'],
                        onClick: function () {
                            global.location = target.href;
                        }
                    }, {
                        type: 'default',
                        label: global['Translations']['GLOBAL']['no']
                    }]
                });
            });
        });

        [].forEach.call(approveBtns, function (btn) {
            btn.addEventListener('click', function (event) {
                event.preventDefault();
                var target = event.target;
                if (target.tagName.toLowerCase() === 'span') {
                    target = target.parentNode;
                }
                modal.confirm({
                    title: global['Translations']['GLOBAL']['approve_classification_question'],
                    content: getInfo(btn),
                    buttons: [{
                        type: 'success',
                        label: global['Translations']['GLOBAL']['yes'],
                        onClick: function () {
                            send(target.href);
                        }
                    }, {
                        type: 'default',
                        label: global['Translations']['GLOBAL']['no']
                    }]
                });
            });
        });

        [].forEach.call(approveAIBtns, function (btn) {
            btn.addEventListener('click', function (event) {
                event.preventDefault();
                var target = event.target;
                if (target.tagName.toLowerCase() === 'span') {
                    target = target.parentNode;
                }
                console.log(target.href);
                modal.external({
                    title: global['Translations']['DIRECTIVES']['approve_selected_ai_classifications'],
                    url: target.href,
                    onShow: function ($modal) {
                        const $form = $modal.find('form');

                        $form.on('submit', function (e) {
                            e.preventDefault();
                            $.ajax({
                                method: 'POST',
                                url: target.href,
                                data: $form.serializeArray()
                            }).then(function (data) {
                                modal.hideModal();
                                if (data.success) {
                                    alert.success(data.msg);
                                    location.reload();
                                } else {
                                    alert.error(data.msg);
                                }
                            }).catch(function (error) {
                                modal.hideModal();
                                alert.error(error || msg_server_error);
                            });
                        });
                    }
                });
            });
        });

        function c(k) {
            return (doc.cookie.match('(^|; )' + k + '=([^;]*)') || 0)[2];
        }

        function send(url) {
            var sender = doc.createElement('form'),
                csrf = doc.createElement('input');
            sender.method = 'POST';
            sender.action = url;
            csrf.type = 'hidden';
            csrf.name = 'csrfmiddlewaretoken';
            csrf.value = c('csrftoken');
            sender.appendChild(csrf);
            doc.body.appendChild(sender);
            sender.submit();
        }
    };

    var sliderNav = function () {
        var slider = doc.querySelector('.panel-scroll');
        var btnFirst = doc.querySelector('.btn-first');
        var btnLast = doc.querySelector('.btn-last');
        var btnCurrent = doc.querySelector('.btn-current');
        btnFirst.addEventListener('click', function (e) {
            e.preventDefault();
            slider.scrollLeft = 0;
        });
        btnLast.addEventListener('click', function (e) {
            e.preventDefault();
            slider.scrollLeft = slider.scrollWidth;
        });
        btnCurrent.addEventListener('click', function (e) {
            e.preventDefault();
            slider.scrollLeft = slider.querySelector('.current').offsetLeft - 360;
        });
    };

    var showBboxes = function () {
        // button to show bboxes for the current user classification
        $("#show-bbox").click(function (event) {
            event.preventDefault();
            plugins.drawCanvas('#img-bbox', true, null);
        });
        // buttons to show bboxes for the selected ai classification
        $(".btn-show-bbox").each(function (index, btn) {
            $(btn).click(function (event) {
                event.preventDefault();
                plugins.drawCanvas('#img-bbox', true, $(btn).data('bbox'));
            });
        });
    };

    var initBboxAnnotator = function () {
        // [xmin, ymax, width, height] -> relative, Trapper
        // [xmin, ymax, width, height] -> absolute, bbox-annotator

        var annotator2trapper = function (bbox, widthImg, heightImg) {
            var xmin = bbox[0] / widthImg;
            var ymax = bbox[1] / heightImg;
            var w = bbox[2] / widthImg;
            var h = bbox[3] / heightImg;
            return [xmin, ymax, w, h];
        };

        var trapper2annotator = function (bbox, widthImg, heightImg, label) {
            var xmin = Math.round(bbox[0] * widthImg);
            var ymax = Math.round(bbox[1] * heightImg);
            var w = Math.round(bbox[2] * widthImg);
            var h = Math.round(bbox[3] * heightImg);
            return {
                "left": xmin, "top": ymax, "width": w, "height": h, "label": label
            };
        };

        var div = doc.getElementById("bbox_annotator");
        var labelsIds = JSON.parse(div.dataset.labels);

        // Initialize the bounding-box annotator.
        var annotator = new BBoxAnnotator({
            url: div.dataset.url,
            input_method: div.dataset.method,
            labels: Object.keys(labelsIds),
            onchange: function () {
                var bboxes = [];
                this.entries.forEach(function (item) {
                    var inputBbox = [item.left, item.top, item.width, item.height];
                    bboxes.push({
                        'pk': labelsIds[item.label].pk,
                        'bbox': annotator2trapper(inputBbox, width, height)
                    });
                });
                $("#id_bboxes").val(JSON.stringify(bboxes));
            }
        });
        // Initialize the reset button.
        $("#reset_button").click(function (e) {
            annotator.clear_all();
        });

        //var objects = JSON.parse(div.dataset.objects);
        var width = div.dataset.width;
        var height = div.dataset.height;
        [].forEach.call(Object.keys(labelsIds), function (key) {
            // trapper2annotator conversion
            var dattr = labelsIds[key];
            dattr.bboxes.forEach(function (bbox) {
                annotator.add_entry(
                    trapper2annotator(bbox, width, height, key)
                );
            });
            annotator.onchange();
        });
    };

    module.init = function () {
        console.log(moduleName + ' initialize');
        plugins.collapsable();
    };

    module.upload = function () {
        plugins.fileInputs();
        plugins.select2();
    };

    module.update = module.create = function () {
        plugins.dynamicTable('#table-formset');
        $('#table-formset .select2-formset').select2({
            minimumResultsForSearch: 5,
            minimumInputLength: 3,
        });
    };

    module.preview = function () {
        // comments();
        sliderNav();
        classifications();
        plugins.videoPlayer('trapper-video-player');
        plugins.select2();
        var gallery = plugins.gallery('#div-img-bbox', 'this');
        // add click event "Show original"
        $('#show-original').click(function (event) {
            event.preventDefault();
            gallery[0].dataset.src = this.href;
            gallery.trigger('click');
        });
        showBboxes('canvas-bbox');
	plugins.drawCanvas('#img-bbox', true, null);
    };

    module.classify = function () {
        // comments();
        sliderNav();
        classifications();
        plugins.videoPlayer('trapper-video-player');
        plugins.select2();
        var gallery = plugins.gallery('#div-img-bbox', 'this');
        // add click event "Show original"
        $('#show-original').click(function (event) {
            event.preventDefault();
            gallery[0].dataset.src = this.href;
            gallery.trigger('click');
        });
        showBboxes('canvas-bbox');
	plugins.drawCanvas('#img-bbox', true, null);
    };

    module.bulk = function () {
        plugins.select2();
        // bind some hotkeys
        $(doc).keydown(function (e) {
            if (e.key == 'd' && e.ctrlKey) {
                e.preventDefault();
                $('#btn-clear-selection').trigger('click');
            }
            ;
            if (e.key == 'f' && e.ctrlKey) {
                e.preventDefault();
                if (!$('.modal').length > 0) {
                    $('#btn-classify').trigger('click');
                }
                ;
            }
            ;
        });
    };

    module.bboxannotator = function () {
        initBboxAnnotator();
    };

    // if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

    // append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'Classification'));
