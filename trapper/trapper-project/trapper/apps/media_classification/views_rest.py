from django.db.models import QuerySet
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from trapper.apps.citizen_science.filters import CSClassificationFilter

from trapper.apps.citizen_science.mixins import BaseCSMixin
from trapper.apps.common.tools import parse_pks
from trapper.apps.media_classification.models import Sequence
from trapper.apps.media_classification.serializers_rest import (
    GroupClassifySerializer,
    SingleClassifySerializer,
    SequenceSerializer,
    MarkClassifyAsFavoriteSerializer,
    FeedbackClassifySerializer,
    FeedbackGroupClassifySerializer,
    RequestNewSpeciesForClassifySerializer,
    SingleClassifyVideoSerializer,
)


class SequencesView(BaseCSMixin, generics.ListAPIView):
    serializer_class = SequenceSerializer

    def get_serializer_context(self):
        ctx = super().get_serializer_context()
        classifications_for_sequence = {}
        for classification in self.get_classifications():
            sequence_id = classification.sequence_id
            if sequence_id not in classifications_for_sequence:
                classifications_for_sequence[sequence_id] = []
            classifications_for_sequence[sequence_id].append(classification.pk)

        ctx["classifications_for_sequence"] = classifications_for_sequence
        return ctx

    def get_classifications(self):
        all_sequences = self.get_queryset()

        classifications = self.get_classifications_queryset()
        classifications = classifications.filter(sequence__in=all_sequences)

        params = self.request.query_params.copy()
        params.pop("id", None)

        classifications = CSClassificationFilter(
            data=params,
            queryset=classifications,
            request=self.request,
        ).qs

        return classifications

    def get_queryset(self) -> QuerySet[Sequence]:
        query_pk = self.request.query_params.get("id", "").strip()
        ids = parse_pks(pks=query_pk)

        if not ids:
            return Sequence.objects.none()

        classification_project = self.get_classification_project()
        return Sequence.objects.filter(
            collection__project=classification_project, id__in=ids
        )


class SingleClassifyView(BaseCSMixin, generics.CreateAPIView):
    serializer_class = SingleClassifySerializer
    classification = None
    classificator = None

    def create(self, request, *args, **kwargs):
        self.get_classification_project()
        self.get_classification()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(status=status.HTTP_201_CREATED)

    def get_classification(self):
        self.classification = get_object_or_404(
            self.classification_project.classifications.all(),
            pk=self.kwargs.get("classification_pk"),
        )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        return {
            **context,
            "fields_defs": self.classification_project.classificator.prepare_form_fields(),
            "classification": self.classification,
        }


class SingleClassifyVideoView(SingleClassifyView):
    serializer_class = SingleClassifyVideoSerializer


class FeedbackClassifyView(SingleClassifyView):
    serializer_class = FeedbackClassifySerializer


class GroupClassifyView(SingleClassifyView):
    serializer_class = GroupClassifySerializer

    def create(self, request, *args, **kwargs):
        self.get_classification_project()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(status=status.HTTP_201_CREATED)


class FeedbackGroupClassifyView(GroupClassifyView):
    serializer_class = FeedbackGroupClassifySerializer


class MarkClassifyAsFavoriteView(BaseCSMixin, generics.CreateAPIView):
    serializer_class = MarkClassifyAsFavoriteSerializer
    classification = None
    classificator = None

    def create(self, request, *args, **kwargs):
        self.get_classification_project()
        self.get_classification()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(status=status.HTTP_201_CREATED)

    def get_classification(self):
        self.classification = get_object_or_404(
            self.classification_project.classifications.all(),
            pk=self.kwargs.get("classification_pk"),
        )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        return {
            **context,
            "classification": self.classification,
        }


class RequestNewSpeciesForClassifyView(MarkClassifyAsFavoriteView):
    serializer_class = RequestNewSpeciesForClassifySerializer
