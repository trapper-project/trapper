window.L = window.L || {};

(function(global, namespace, moduleName, $, L) {
    'use strict';

    var doc = global.document;

    var select2ajax = function(element, options) {
        options = options || {};
        var defaults = {
            minimumInputLength: 3,
            multiple: options.multiple || false,
            placeholder: options.placeholder || false,
            ajax: {
                cache: true,
                url: options.apiUrl,
                dataType: 'json',
                delay: 250,
                data: function(term, page) {
                    return {
                        search: term,
                        page: page
                    };
                },
                results: function(data, page) {
                    var more = (page * 10) < data.pagination.count;
                    return {
                        results: $.map(data.results, function(item) {
                            return {
                                text: item[options.textLabel],
                                id: item[options.idLabel]
                            }
                        }),
                        more: more
                    };
                }
            },
            initSelection: function(element, callback) {
                var ids = $(element).val();
                if (ids !== "" && ids.length !== 0) {
                    if (options.initLabel) {
                        callback({
                            text: options.initLabel,
                            id: ids
                        })
                        return
                    }
                    $.ajax(options.apiUrl, {
                        dataType: "json",
                        data: {
                            pk: ids,
			    page_size: 500
                        }
                    }).done(function(data) {
                        var initSel = [];
                        $(data.results).each(function() {
                            initSel.push({
                                text: this[options.textLabel],
                                id: this[options.idLabel],
                            });
                        });
                        if (options.multiple) {
                            callback(initSel);
                        } else {
                            callback(initSel[0]);
                        }
                    });
                }
            }
        };
        var settings = $.extend({}, defaults, options);
        return ($(element).select2(settings));
    };
    

    var videojsDefaultOptions = {
	responsive: true,
	inactivityTimeout : 0,
	playbackRates: [0.2, 0.5, 1, 2, 5, 10],
	userActions: {
	    hotkeys: function(event) {
 		if (event.which === 83) { // s
		    var current = this.playbackRate();
		    if (current + 1 <= 10) this.playbackRate(current + 1);
		};
		if (event.which === 88) { // x
		    var current = this.playbackRate();
		    if (current - 1 >= 0) this.playbackRate(current - 1);
		};
	    }
	},
	plugins: {
	    hotkeys: {
		seekStep: 5,
		alwaysCaptureHotkeys: true,
		volumeUpKey: function() {},
		volumeDownKey: function() {},
		forwardKey: function(event, player) {
		    return event.which === 67; // c
		},
		rewindKey: function(event, player) {
		    return event.which === 90; // z
		}
	    }
	}
    };
    
    var module = {
        tooltip: function() {
            $('[data-tooltip=tooltip]').tooltip();
        },
	
	videoPlayer: function(player_id, extraOptions) {
	    var video = doc.getElementById(player_id);
	    if (!video) return;
	    extraOptions = extraOptions || {};
	    var options = videojs.mergeOptions(videojsDefaultOptions, extraOptions)
	    videojs(video, options);
	},

	gallery: function(id, selector, extraOptions) {
	    var $container = $(id);
	    if (!$container) return;
	    extraOptions = extraOptions || {};
	    var defaultOptions = {
		selector: selector,
		thumbnail: true,
		videojs: true,
		videojsOptions: videojsDefaultOptions,
		speed: 100,
		showThumbByDefault: false
	    }
	    var options = $.extend({}, defaultOptions, extraOptions);
	    $container.lightGallery(options);
	    $container.on('onAfterSlide.lg',function(event, index, fromTouch, fromThumb){
	    	var video = $('.lg-current video');
	    	if (video.length > 0) {
		    setTimeout(function() {
			var player = video[0];
			player.focus();
                    }, 100);
		};
	    });
	    console.log('lightgallery initialized!');
	    return $container;
	},
	
        drawBboxes: function(canvas, data) {
            var ctx = canvas.getContext("2d");
            [].forEach.call(data, function(observation, index) {
		if (!observation.bboxes) return;
		// each bbox has this format: [ymin, xmin, ymax, xmax] -> OLD!!!
		// each bbox has this format: [xmin, ymin, width, height] -> NEW!!!
		[].forEach.call(observation.bboxes, function(bbox, index) {
                    // draw bboxes
                    ctx.beginPath();
                    ctx.lineWidth = 2;
                    ctx.strokeStyle = "lightgreen"; //
                    var xmin = Math.round(bbox[0] * ctx.canvas.width);
                    var ymax = Math.round(bbox[1] * ctx.canvas.height);
		    var w = Math.round(bbox[2] * ctx.canvas.width);
		    var h = Math.round(bbox[3] * ctx.canvas.height);
                    ctx.rect(xmin, ymax, w, h);
                    ctx.stroke();
                    // draw labels
		    if (observation.label) {
			ctx.font = "10pt arial";
			ctx.fillStyle = "lightgreen";
			ctx.fillText(observation.label, xmin + 2, ymax + 12);
		    };
		});
	    });
        },

        drawCanvas: function(id, imgInDiv, bboxData) {
	    var img = $(id);
	    if (!img) return;
	    var imgBbox = bboxData || img.data('bbox');
	    if (!imgBbox) return;
	    // create canvas & get context for drawing
	    var canvas = document.createElement('canvas');
            var ctx = canvas.getContext("2d");
            // keep url of the original image
	    var srcOriginal = img.parent().attr('data-src-original');
	    if (srcOriginal) img.attr('src', srcOriginal);
	    var imgTmp = new Image();
	    imgTmp.src = img.attr('src')
	    imgTmp.addEventListener('load', e => {
                canvas.width = imgTmp.width;
                canvas.height = imgTmp.height;
                ctx.drawImage(imgTmp, 0, 0, canvas.width, canvas.height);
                module.drawBboxes(canvas, imgBbox);
		var srcNew = canvas.toDataURL('image/jpeg', 1.0);
		img.attr('src', srcNew);
		if (imgInDiv) {
		    img.parent().attr('data-src', srcNew);
		    img.parent().attr('data-src-original', imgTmp.src);
		}
            });
        },
	
        enableToggler: function(container) {
            var checkboxes = container.querySelectorAll('input[type="checkbox"]');
            [].forEach.call(checkboxes, function(checkbox) {
                var field = checkbox.parentNode.querySelector('select');
                checkbox.addEventListener('change', function(e) {
                    field.disabled = !e.target.checked;
                });
            });
        },

        collapsable: function() {
            function preprocess(trigger) {
                var id = 'collapsable-' + (counter++);
                var target = trigger.nextSibling;
                var isActive = trigger.classList.contains('active');
                while (target.nodeType !== 1) {
                    target = target.nextSibling;
                }
                trigger.dataset.target = '#' + id;
                trigger.dataset.toggle = 'collapse';
                if (!isActive) {
                    trigger.classList.add('collapsed');
                }
                var wrapper = doc.createElement('div');
                wrapper.id = id;
                wrapper.className = 'collapse' + (isActive ? ' in' : '');
                wrapper.appendChild(target.cloneNode(true));
                trigger.parentNode.replaceChild(wrapper, target);
            }
            var counter = 0;
            var collapsable = doc.querySelectorAll('.collapsable');
            if (!collapsable.length) return;
            [].forEach.call(collapsable, preprocess);
        },

        dynamicTable: function(table_id) {
	    var table = $(table_id)[0];
	    if (!table) return;
            var namePrefix = table.dataset.name;
            var tbody = table.querySelector('tbody');
            var template = $('#dynamic-form-row-template')[0].innerHTML;
            var reg = new RegExp('(' + namePrefix + '-)(__prefix__)', 'g');
            var totalInput = doc.querySelector('input[name=' + namePrefix + '-TOTAL_FORMS]'),
		filledInput = doc.querySelector('input[name=' + namePrefix + '-INITIAL_FORMS]'),
		maxInput = doc.querySelector('input[name=' + namePrefix + '-MAX_NUM_FORMS]');
            var filled = parseInt(filledInput.value, 10),
		max = parseInt(maxInput.value, 10),
		total = parseInt(totalInput.value, 10);

	    function removeRow(row) {
		row.remove();
		total--;
		totalInput.value = total;
		// destroy select2
		$('#table-formset .select2-formset').select2('destroy');
		// update formset indices
		$(tbody).children('tr').each(function(k, row) {
		    var currentHtml = row.innerHTML;
		    row.innerHTML = currentHtml.replace(
			RegExp('(' + namePrefix + '-)([0-9]+)', 'g'), '$1' + k
		    );
		});
		// re-initialize select2
		$('#table-formset .select2-formset').select2({
		    minimumResultsForSearch: 5,
		    minimumInputLength: 3,
		});
		// 
		$('.btn-remove-row').unbind('click').click(function(){
		    removeRow(this.parentNode.parentNode);
		});
	    };

	    function createRow() {
		var row = doc.createElement('tr');
		row.innerHTML = template.replace(reg, '$1' + total);
		return row;
            }

	    $('.btn-remove-row').click(function(){
		removeRow(this.parentNode.parentNode);
	    });
	    
	    $('.btn-add-row').unbind('click').click(function(){
		if (filled >= max) return;
		var row = createRow();
		$(row).find('.btn-remove-row').click(function(){
		    removeRow(row);
		})
		tbody.appendChild(row);
		$(row).find('.select2-formset').each(function(j, cell) {
		    $(cell).select2({
			minimumResultsForSearch: 5,
			minimumInputLength: 3,
		    })
		});
		total++;
		totalInput.value = total;
	    });	    
	},

	dynamicTabs: function(tabs_id) {
	    var tabs = $(tabs_id)[0];
	    if (!tabs) return;
	    var namePrefix = tabs.dataset.name;
            var createBtn = tabs.querySelector('.tab-create');
            var tabsNav = tabs.querySelector('.nav-tabs');
            var tabsContainer = tabs.querySelector('.tab-content');
            var dynamicTabs = tabs.querySelectorAll('div[id^="tab-dynamic"]');
            if (!dynamicTabs) return;
            var template = doc.querySelector('#tab-dynamic-template').innerHTML;
            var reg = new RegExp('(' + namePrefix + '-)(__prefix__)', 'g');
            var totalInput = doc.querySelector('input[name=' + namePrefix + '-TOTAL_FORMS]'),
		filledInput = doc.querySelector('input[name=' + namePrefix + '-INITIAL_FORMS]'),
		maxInput = doc.querySelector('input[name=' + namePrefix + '-MAX_NUM_FORMS]');
            var filled = parseInt(filledInput.value, 10),
		max = parseInt(maxInput.value, 10),
		total = parseInt(totalInput.value, 10);
            var index = total;

            createBtn.addEventListener('click', function (e) {
		e.preventDefault();
		addTab();
            });

            tabsNav.addEventListener('click', function (event) {
		var target = event.target;
		if (target.tagName.toLowerCase() === 'span') {
                    target = target.parentNode;
		}
		if (target.tagName.toLowerCase() === 'button' && !target.classList.contains('btn-success')) {
                    removeTab(target);
		}
            });

            function updateInputs() {
		totalInput.value = total;
            }

            function addTab() {
		if (filled >= max) return;
		index++;
		total++;
		updateInputs();
		var tab = doc.createElement('li');
		tab.innerHTML = '<a href="#tab-dynamic-' +
		    index + '" data-toggle="tab"><strong>#' +
		    index + ' Dynamic</strong> <button class="btn btn-danger btn-xs">' +
		    '<span class="fa fa-remove"></span></button></a>';
		var tabContent = doc.createElement('div');
		tabContent.className = 'tab-pane';
		tabContent.id = 'tab-dynamic-' + index;
		tabContent.innerHTML = template.replace(reg, '$1' + (index - 1));
		tabsNav.insertBefore(tab, createBtn);
		tabsContainer.appendChild(tabContent);
		module.select2();
		tab.querySelector('a').click();
            }

            function removeTab(btn) {
		var tab = btn.parentNode.parentNode;
		var pane = doc.querySelector(btn.parentNode.hash);
		var checkbox = pane.querySelector('input[type=checkbox][hidden]');
		var prevTab = tab.previousSibling;
		while (prevTab.nodeType !== 1) {
                    prevTab = prevTab.previousSibling;
		}
		if (checkbox) {
                    checkbox.checked = true;
                    tab.remove();
                    pane.style.display = 'none';
		} else {
                    tab.remove();
                    pane.remove();
                    total--;
                    updateInputs();
		}
		prevTab.querySelector('a').click();
            };
	},
	
        fileInputs: function() {
            function helper(input) {
                var fakeInput = input.parentElement.querySelector('input[type=text]');
                input.addEventListener('change', function(e) {
                    e.preventDefault();
                    var value = '';
                    [].forEach.call(e.target.files, function(file) {
                        value = value + file.name + '; ';
                    });
                    fakeInput.value = value;
                });
            }
            var fileInputs = doc.querySelectorAll('.file-group input[type=file]');
            if (!fileInputs.length) return;
            [].forEach.call(fileInputs, helper);
        },

        datepicker: function() {
            $('.datepicker-control').datepicker({
                format: 'YYYY-MM-DD',
                clearBtn: true,
                autoclose: true,
                orientation: 'bottom right',
                todayHighlight: true
            });
            $('.timepicker-control').timepicker({
                show2400: true,
                timeFormat: 'HH:mm:ss'
            });
            $('.datetimepicker-control').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                showClear: true
            });
        },

        select2: function(options) {
            options = options || {};
            var defaults = {
                minimumResultsForSearch: 5,
                minimumInputLength: 3,
            };
            var settings = $.extend({}, defaults, options);
            $('.select2-default').select2(settings);
            // tags
            var $tokens = $('.select2-tags');
            if (!$tokens.length) return;
            $tokens.each(function(index, field) {
                $(field).select2({
	    	    tags: true,
	    	    multiple:true,
		    tokenSeparators: [',', ' '],
		    dropdownCss: {display:'none'}
                });
            });
        },
        select2ajax: select2ajax,

        select2ajaxForms: function() {
            // tokens
            var $tokens = $("input[class^='select2-ajax-']");
            if (!$tokens.length) return;
            $tokens.each(function(index, item) {
                var options = {
                    apiUrl: item.getAttribute('ajax-api-url'),
                    idLabel: item.getAttribute('ajax-id') || 'pk',
                    textLabel: item.getAttribute('ajax-label'),
                    multiple: item.getAttribute('ajax-multiple') || false
                }
                select2ajax($(item), options);
            });
        },

        lockerInputs: function() {
            var lockers = doc.querySelectorAll('.control-locker');
            [].forEach.call(lockers, function(locker) {
                locker.addEventListener('change', function(e) {
                    var control = locker.parentNode.parentNode.querySelector(
			'input.controlled, select.controlled'
		    );
                    if (e.target.checked) {
                        control.removeAttribute('disabled');
                    } else {
                        control.setAttribute('disabled', 'disabled');
                    }
                });
            });
        },

        smallMap: function() {
            var latlng = $('#location_small_map').attr("latlng").split(",")
            var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
            });
            var esri = L.tileLayer(
		'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
		{
                    maxZoom: 18,
                    attribution: 'Tiles &copy; Esri'
		}
	    )
            var map = L.map('location_small_map', { layers: [esri, osm] }).setView(latlng, 10);
            L.marker(latlng).addTo(map);
            var baseMaps = {"ESRI": esri, "OSM": osm};
            L.control.layers(baseMaps).addTo(map);
        }
	
    };
    
    // if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

    // append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'Plugins', jQuery, L));
