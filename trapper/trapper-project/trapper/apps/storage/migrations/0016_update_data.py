from django.db import migrations, models


def update_data(apps, schema_editor):
    obs_type_dict = {val: val.lower() for val in ["Human", "Animal", "Vehicle"]}
    age_dict = {
        "Undefined": "unknown",
        "Adult": "adult",
        "Subadult": "subadult",
        "Juvenile": "juvenile",
        "Offspring": "offspring"
    }
    sex_dict = {
        "Undefined": "unknown",
        "Female": "female",
        "Male": "male"
    }
    behaviour_dict = {val: val.lower() for val in ["Undefined", "Grazing", "Browsing", "Rooting", "Vigilance",
                                                   "Running", "Walking"]}
    behaviour_dict["Other"] = "undefined"

    Resource = apps.get_model("storage", "Resource")
    qs = Resource.objects.exclude(data={})
    for res in qs.iterator(chunk_size=1000):
        data = res.data
        for obs in data["observations"]:
            obs["age"] = age_dict[obs["age"]]
            obs["sex"] = sex_dict[obs["sex"]]
            obs["behaviour"] = behaviour_dict[obs["behaviour"]]
            obs["observation_type"] = obs_type_dict[obs["observation_type"]]
        res.data = data
    Resource.objects.bulk_update(qs, ["data", ], batch_size=10000)


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0015_auto_20211007_1756'),
    ]

    operations = [
        migrations.RunPython(update_data)
    ]
