import datetime

import factory

from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import (
    AIClassification,
    AIProvider,
    AIClassificationDynamicAttrs,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)


class AIProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AIProvider


class AIClassificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AIClassification
        exclude = ("now",)

    owner = factory.SubFactory(UserFactory)
    classification = factory.SubFactory(ClassificationFactory)
    model = factory.SubFactory(AIProviderFactory)
    now = factory.LazyFunction(timezone.now)
    created_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))
    updated_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))


class AIClassificationDynamicAttrsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AIClassificationDynamicAttrs

    ai_classification = factory.SubFactory(AIClassificationFactory)
