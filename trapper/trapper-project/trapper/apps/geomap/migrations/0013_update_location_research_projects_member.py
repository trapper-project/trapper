from django.db import migrations


def update_location_research_projects_member(apps, schema_editor):
    location_model = apps.get_model("geomap", "Location")
    rproject_model = apps.get_model("research", "ResearchProject")

    for rproject in rproject_model.objects.all():
        for collection in rproject.collections.all():
            qs = (
                location_model.objects.filter(
                    deployments__resources__collection__pk=collection.pk
                )
                .distinct()
                .exclude(research_project=rproject)
                .exclude(research_projects_member=rproject)
            )
            for location in qs:
                location.research_projects_member.add(rproject)


class Migration(migrations.Migration):

    dependencies = [
        ("geomap", "0012_location_research_projects_member"),
    ]

    operations = [
        migrations.RunPython(update_location_research_projects_member)
    ]
