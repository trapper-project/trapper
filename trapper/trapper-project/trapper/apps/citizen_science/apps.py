from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CitizenScienceConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "trapper.apps.citizen_science"
    verbose_name = _("Citizen Science")
