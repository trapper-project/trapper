=======================================
 Technology used - software components
=======================================

Backend
+++++++

The following is the list of the most important software components that have been used to 
design and implement Trapper: 

* `Ubuntu <http://www.ubuntu.com>`_
  A base operating system under which Trapper was developed and tested.
  
* `PostgreSQL <http://postgresql.org>`_
  The open source and industry standard relational database management system (RDBMS) 
  with `PostGIS <http://postgis.net/>`_  as its spatial extension.

* `nginx <http://nginx.org/>`_
  nginx [engine x] is an HTTP and reverse proxy server, a mail proxy server, and a generic TCP proxy server.

* `Gunicorn <http://gunicorn.org>`_
  This pure-python application is used to serve Trapper project.

* `Supervisor <http://supervisord.org/>`_
  This application is used to control all project related external applications
  like celery.

* `Celery <http://www.celeryproject.org/>`_
  This python application is used to run various tasks in asnynchronous mode
  allowing Trapper to work faster and more efficient. The celery is used to generate
  thumbnails from large files or videos, or to process uploaded collections
  by users.

* `Django <https://www.djangoproject.com/>`_
  The core component of Trapper, a high-level Python web framework maintained by the 
  Django Software Foundation. Django framework simplifies and significantly speeds up the 
  creation of complex, database-driven websites and emphasizes reusability and pluggability 
  of their components. Additionally, a variety of 3rd party, open source django applications 
  have been used to develop Trapper. For a complete list of these applications see 
  `this file <>`_

* `uMAP <https://github.com/umap-project/umap/>`_
  uMap lets you create maps with OpenStreetMap layers in a minute and embed them in your 
  site. It uses django-leaflet-storage and Leaflet.Storage, built on top of Django and Leaflet.

Frontend
++++++++

Trapper's front-end is developed based on three independent solutions:

1) HTML (version 5) templates and their CSS styles (powered by `SASS <http://sass-lang.com/>`_) 
2) set of scripts written in pure JavaScript (ECMAscript 5).
3) all the external libraries and frameworks included in the project:

* Twitter Bootstrap
  `Bootstrap Sass Official <https://github.com/twbs/bootstrap-sass>`_ which is
  official SASS version of `Twitter Bootstrap <http://getbootstrap.com/>`_.
  This library provides a set of HTML components and CSS styles used for
  Trapper scaffold creation.

* Font Awesome
  `Font Awesome <http://fortawesome.github.io/Font-Awesome/>`_ - webfont of
  vector icons used in the project.

* Angular JS
  `Angular JS <https://angularjs.org/>`_ all the grids/tables including their filters
  has been build on top of this Google's framework

* Angular Cookies
  Official `angular module <https://github.com/angular/bower-angular-cookies>`_
  for cookies management.

* Angular Sanitize
  Official `angular module <https://github.com/angular/bower-angular-sanitize>`_
  which improves angular templates data binding.

* Moment
  Extremaly powerful `library <http://momentjs.com/>`_ for date parsing & manipulation.

* Select2
  Complete `solution <https://select2.github.io/>`_ that extends default HTML select controls.

* Select2 Bootstrap CSS
  `CSS styles <https://github.com/t0m/select2-bootstrap-css>`_ for Select2 so it fits
  Twitter Bootstrap feel & look.

* Bootstrap WYSIHTML5
  `Javascript Plugin <https://github.com/Waxolunist/bootstrap3-wysihtml5-bower>`_ which
  brings WYSIWYG text editor to the table.

* Bootstrap Datepicker http://eternicode.github.io/bootstrap-datepicker/
  `Javascript Widget <http://eternicode.github.io/bootstrap-datepicker/>`_ - simple datepicker.

* Jquery Timepicker
  `Jquery Widget <http://jonthornton.github.io/jquery-timepicker/>`_ which is just a timepicker.

* Bootstrap Datetimepicker
  `Javascript Widget <http://eonasdan.github.io/bootstrap-datetimepicker/>`_ that
  combines both time and date picker.

* Video JS
  This `library <http://www.videojs.com/>`_ extends standard HTML5 video players.

* Video JS Rangeslider
  Video JS `plugin <https://github.com/danielcebrian/rangeslider-videojs>`_ that
  allows to set and get video sequences.
