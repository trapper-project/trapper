import datetime

import factory
from django.utils import timezone

from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.models import Deployment
from trapper.apps.geomap.taxonomy import DeploymentViewQuality
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)


class DeploymentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Deployment
        exclude = ("now",)

    deployment_id = factory.Sequence(lambda n: n)
    deployment_code = factory.Faker("word")

    owner = factory.SubFactory(UserFactory)
    comments = factory.Faker("paragraph", nb_sentences=3)

    research_project = factory.SubFactory(ResearchProjectFactory)
    location = factory.SubFactory(
        LocationFactory, research_project=research_project, owner=owner
    )

    now = factory.LazyFunction(timezone.now)
    date_created = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(days=11))
    start_date = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(days=10))
    end_date = factory.LazyAttribute(lambda o: o.now + datetime.timedelta(days=10))

    correct_setup = True
    correct_tstamp = True
    view_quality = DeploymentViewQuality.GOOD
