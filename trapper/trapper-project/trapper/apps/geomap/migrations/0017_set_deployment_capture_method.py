from django.db import migrations


def update_location_research_projects_member(apps, schema_editor):
    capture_method_dict = {
        1: 1,
        2: 2,
        3: 1,
    }
    deployment_model = apps.get_model("geomap", "Deployment")
    deployments = deployment_model.objects.all()
    for deployment in deployments:
        if deployment.research_project:
            deployment.capture_method = capture_method_dict.get(deployment.research_project.sensor_method, 1)
        else:
            deployment.capture_method = 1

    deployment_model.objects.bulk_update(deployments, ["capture_method", ], batch_size=1000)


class Migration(migrations.Migration):

    dependencies = [
        ("geomap", "0016_deployment_capture_method"),
    ]

    operations = [
        migrations.RunPython(update_location_research_projects_member)
    ]
