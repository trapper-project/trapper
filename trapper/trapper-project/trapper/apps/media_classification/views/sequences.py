# -*- coding: utf-8 -*-
"""
Views used to handle logic related to sequence management in media
classification application
"""
from braces.views import UserPassesTestMixin, JSONResponseMixin
from django.conf import settings
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from django.views import generic

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.tools import parse_pks, datetime_aware
from trapper.apps.common.views import BaseDeleteView, LoginRequiredMixin
from trapper.apps.media_classification.forms import SequenceBuildForm
from trapper.apps.media_classification.models import (
    Classification,
    ClassificationProjectCollection,
    Sequence,
    SequenceResourceM2M,
)
from trapper.apps.media_classification.serializers import SequenceReadSerializer
from trapper.apps.media_classification.tasks import celery_build_sequences


class SequenceChangeView(
    LoginRequiredMixin, UserPassesTestMixin, generic.View, JSONResponseMixin
):
    """
    Sequence's create or update view.
    Handle the creation or update of the
    :class:`apps.media_classification.models.Sequence` objects.

    Sequence modifications can be done only using POST method, and response
    is given as json
    """

    http_method_names = ["post"]
    raise_exception = True
    redirect_url = None

    def test_func(self, user):
        collection_pk = self.request.POST.get("collection_pk")
        self.collection = get_object_or_404(
            ClassificationProjectCollection, pk=collection_pk
        )
        if not self.collection.project.can_change_sequence(user=self.request.user):
            return False
        return True

    def get_resources(self, collection, resource_pks):
        return collection.collection.collection.resources.filter(
            pk__in=parse_pks(resource_pks)
        )

    def post(self, request, *args, **kwargs):
        """
        `request.POST` method is used to create sequences for given
        list of resources using AJAX.

        List of project pks is passed in `pks` key as list of integers
        separated by comma.

        Sequence can be created for multiple resources that belong to the
        same classification project collection within classification project.

        Response contains status of creation/update and serialized sequence
        object.
        """
        msg = _("Invalid request")
        status = False
        record = None
        sequence_original = None
        sequence_pk = request.POST.get("pk")
        resources = request.POST.get("resources")
        resources = self.get_resources(
            collection=self.collection, resource_pks=resources
        )
        if not resources:
            context = {"status": status, "record": record, "msg": msg}
            return self.render_json_response(context)

        description = request.POST.get("description")
        try:
            sequence = Sequence.objects.get(pk=sequence_pk)
            sequence_original = sequence
            sequence.description = description
        except (ValueError, Sequence.DoesNotExist):
            sequence = Sequence(
                collection=self.collection,
                created_by=request.user,
                created_at=datetime_aware(),
                description=description,
            )
        sequence.save()
        try:
            seq_resources = []
            for resource in resources:
                obj = SequenceResourceM2M(sequence=sequence, resource=resource)
                obj.full_clean()
                seq_resources.append(obj)
        except ValidationError as e:
            if sequence_original:
                sequence = sequence_original
                sequence.save()
            else:
                sequence.delete()
            msg = e.messages[0]
        else:
            sequence.resources.clear()
            for obj in seq_resources:
                obj.save()
            record = SequenceReadSerializer(
                instance=sequence, context={"request": self.request}
            ).data
            status = True
            # update classification objects with
            # sequence data
            sequence.classifications.clear()
            classifications = Classification.objects.filter(
                collection=self.collection, resource__in=resources
            )
            classifications.update(sequence=sequence)
        context = {"status": status, "record": record, "msg": msg}
        return self.render_json_response(context)


view_sequence_change = SequenceChangeView.as_view()


class SequenceDeleteView(BaseDeleteView):
    """
    View responsible for handling deletion of single or multiple
    sequences. Only sequences that user has enough permissions for
    can be deleted.
    """

    model = Sequence
    item_name_field = "sequence_id"
    redirect_url = "media_classification:project_list"

    def filter_editable(self, queryset, user):
        to_delete = []
        for obj in queryset:
            if obj.can_delete(user):
                to_delete.append(obj)
        return to_delete

    def bulk_delete(self, queryset):
        for obj in queryset:
            obj.delete()


view_sequence_delete = SequenceDeleteView.as_view()


class SequenceBuildView(LoginRequiredMixin, generic.FormView, JSONResponseMixin):
    """Sequences's (re)build view. Use this view to automatically build
    sequences for specified classification project collections.
    """

    template_name = "media_classification/projects/sequence_build_form.html"
    form_class = SequenceBuildForm
    raise_exception = True

    def form_valid(self, form):
        if not form.cleaned_data.get("project_collections"):
            msg = _(
                "Nothing to process (most probably you have no permission to run this action on "
                "the selected classification project collections)."
            )
            context = {"success": False, "msg": msg}
        else:
            user = self.request.user
            params = {"data": form.cleaned_data, "user": user}
            if settings.CELERY_ENABLED:
                task = celery_build_sequences.delay(**params)
                user_task = UserTask(user=user, task_id=task.task_id)
                user_task.save()
                msg = _(
                    "You have successfully run a celery task. The selected classification project "
                    "collections are being processed now."
                )
            else:
                msg = celery_build_sequences(**params)
            context = {"success": True, "msg": msg}
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_sequence_build = SequenceBuildView.as_view()
