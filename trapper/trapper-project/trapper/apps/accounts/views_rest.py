from django.contrib.auth import get_user_model
from django.http import Http404
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny

from .serializers import (
    CustomLoginSerializer,
    CustomLogoutSerializer,
    RegisterSerializer,
    ChangePasswordSerializer,
    ProfileSerializer,
    SetDefaultCSProjectSerializer,
    ActivateAccountSerializer,
    RequestPasswordResetSerializer,
    PasswordResetSerializer,
    UserProfileSerializer,
    ResendAccountActivationSerializer,
)
from ..sendfile.views import BaseServeFileView

User = get_user_model()


class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class ActivateAccountView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = ActivateAccountSerializer


class ResendAccountActivationView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = ResendAccountActivationSerializer


class RequestPasswordResetView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RequestPasswordResetSerializer


class PasswordResetView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = PasswordResetSerializer


class ProfileView(generics.RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    http_method_names = ["get", "patch"]

    def get_object(self):
        return self.request.user


class UserProfileView(generics.RetrieveAPIView):
    serializer_class = UserProfileSerializer
    http_method_names = ["get"]
    queryset = User.objects.filter(is_active=True)
    lookup_url_kwarg = "user_pk"


class UserAvatarView(BaseServeFileView):
    """
    View for serving user avatar through x-sendfile
    """

    def get(self, request, *args, **kwargs):
        user_pk = kwargs.get("user_pk", None)
        user = get_object_or_404(User, **{"pk": user_pk, "is_active": True})
        if not user.userprofile.avatar:
            raise Http404
        return self.serve_file(user.userprofile.avatar)


class ChangePasswordView(generics.UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    http_method_names = ["put"]

    def get_object(self):
        return self.request.user


class SetDefaultCSProjectView(generics.UpdateAPIView):
    """
    Set default project for Citizen Science
    """

    serializer_class = SetDefaultCSProjectSerializer
    http_method_names = ["put"]

    def get_object(self):
        return self.request.user


class CustomLoginView(generics.CreateAPIView):
    serializer_class = CustomLoginSerializer
    permission_classes = (AllowAny,)


class CustomLogoutView(generics.CreateAPIView):
    serializer_class = CustomLogoutSerializer
