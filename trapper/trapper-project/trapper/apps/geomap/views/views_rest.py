from django.db import transaction
from django.db.models import Q, F, Count
from django.db.models.expressions import Window
from django.db.models.functions import RowNumber
from django.utils.translation import gettext_lazy as _
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from trapper.apps.citizen_science.mixins import BaseCSDeploymentMixin, BaseCSMixin
from trapper.apps.common.tools import non_field_errors
from trapper.apps.common.views_api import CSListPagination
from trapper.apps.extra_tables.models import Species
from trapper.apps.geomap.filters import CSDeploymentFilter
from trapper.apps.geomap.models import Location
from trapper.apps.geomap.serializers import (
    CSDeploymentsSerializer,
    CSLocationsSerializer,
    CSDeploymentSerializer,
    CSDeploymentUpdateTimestampsSerializer,
)
from trapper.apps.geomap.serializers.citizen_science import (
    CSDeploymentProcessingProgressSerializer,
)
from trapper.apps.media_classification.models import (
    Classification,
    ClassificationProjectCollection,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.storage.models import Collection


class CSDeploymentsView(BaseCSDeploymentMixin, BaseCSMixin, generics.ListAPIView):
    serializer_class = CSDeploymentsSerializer
    pagination_class = CSListPagination
    filterset_class = CSDeploymentFilter

    def get_serializer_context(self):
        ctx = super().get_serializer_context()
        ctx["thumbnails"] = self.get_thumbnails_for_deployments()
        ctx["sequences"] = self.count_sequences_for_deployments()
        ctx["deployments_with_user_classifications"] = (
            self.get_deployments_with_user_classifications()
        )
        ctx["species_stats"] = self.get_species_stats_per_deployment()
        return ctx

    def get_thumbnails_for_deployments(self):
        deployments = self.paginator.page
        classifications = self.get_classifications_queryset()
        classifications = (
            classifications.annotate(
                row_number=Window(
                    expression=RowNumber(),
                    partition_by=[F("resource__deployment_id")],
                    order_by=["resource__date_recorded"],
                )
            )
            .filter(row_number__lte=4)
            .filter(resource__deployment__in=deployments)
            .select_related("resource")
            .prefetch_related("dynamic_attrs")
        )
        thumbnails = {}
        for classification in classifications:
            thumbnails.setdefault(classification.resource.deployment_id, []).append(
                classification
            )
        return thumbnails

    def count_sequences_for_deployments(self):
        deployments = self.paginator.page
        classifications = self.get_classifications_queryset()
        classifications = (
            classifications.filter(resource__deployment__in=deployments)
            .values("resource__deployment_id")
            .annotate(sequences=Count("sequence_id", distinct=True))
            .values_list("resource__deployment_id", "sequences")
            .order_by()
        )
        sequences = {}
        for deployment_id, count in classifications:
            sequences[deployment_id] = count
        return sequences

    def get_deployments_with_user_classifications(self):
        deployments = self.paginator.page
        classifications = self.get_classifications_queryset()
        deployments_with_user_classifications = (
            classifications.filter(resource__deployment__in=deployments)
            .filter(
                Q(approved_source__isnull=False) | Q(user_classifications__isnull=False)
            )
            .values_list("resource__deployment_id", flat=True)
            .distinct()
        )
        return set(deployments_with_user_classifications)

    def get_species_stats_per_deployment(self):
        deployments = self.paginator.page
        stats = (
            self.get_classifications_queryset()
            .filter(dynamic_attrs__species__isnull=False)
            .filter(resource__deployment__in=deployments)
            .values(
                deploymment_id=F("resource__deployment_id"),
                species_id=F("dynamic_attrs__species_id"),
            )
            .annotate(total=Count("species_id"))
            .annotate(
                row_number=Window(
                    expression=RowNumber(),
                    partition_by=["deploymment_id"],
                    order_by=["-total"],
                )
            )
            .filter(row_number__lte=5)
            .order_by("deploymment_id", "-total")
            .values_list("deploymment_id", "species_id", "total")
        )

        all_species_ids = [species_id for _, species_id, _ in stats]
        all_species = {
            species["id"]: species
            for species in Species.objects.filter(id__in=all_species_ids).values(
                "id", "english_name", "latin_name"
            )
        }

        species_stats = {}
        for deployment_id, species_id, total in stats:
            species = all_species[species_id]
            species_stats.setdefault(deployment_id, []).append(
                {
                    "total": total,
                    "latin_name": species["latin_name"],
                    "english_name": species["english_name"],
                }
            )

        return species_stats

    def get_queryset(self):
        return self.get_deployments(
            self.get_classification_project(), self.request.user
        ).select_related("owner", "owner__userprofile", "location")


class CSLocationsView(BaseCSDeploymentMixin, BaseCSMixin, generics.ListAPIView):
    serializer_class = CSLocationsSerializer
    filterset_class = CSDeploymentFilter

    def get_queryset(self):
        return self.get_deployments(
            self.get_classification_project(), self.request.user
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        # Because at this view we need same filters as in CSDeploymentsView we need
        # hack queryset, and first filter it, then get locations
        queryset = Location.objects.filter(deployments__in=queryset).distinct()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CSDeploymentView(
    BaseCSDeploymentMixin, BaseCSMixin, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = CSDeploymentSerializer
    http_method_names = ["get", "patch", "delete"]

    def get_object(self):
        qs = self.get_deployments(self.get_classification_project(), self.request.user)
        return get_object_or_404(qs, pk=self.kwargs["deployment_pk"])

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        classifications = self.get_classifications_queryset(
            self.classification_project, self.request.user
        ).filter(resource__deployment=instance)
        # User only can delete deployment if he is owner and there is no approved or user classifications
        can_delete = (
            instance.owner == self.request.user
            and not classifications.filter(
                Q(approved_source__isnull=False) | Q(user_classifications__isnull=False)
            ).exists()
        )
        if not can_delete:
            raise non_field_errors(_("You cannot delete this deployment."))
        location = instance.location
        collections = list(
            instance.resources.all().values_list("collection", flat=True)
        )
        instance.resources.all().delete()
        self.perform_destroy(instance)
        collection_without_resources = Collection.objects.filter(
            pk__in=collections, resources__isnull=True
        )
        ClassificationProjectCollection.objects.filter(
            collection__collection__in=collection_without_resources
        ).delete()
        ResearchProjectCollection.objects.filter(
            collection__in=collection_without_resources
        ).delete()
        collection_without_resources.delete()
        if not location.deployments.exclude(pk=instance.pk).exists():
            location.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CSDeploymentProcessingProgressView(CSDeploymentView):
    serializer_class = CSDeploymentProcessingProgressSerializer
    http_method_names = ["get"]

    def get_serializer_context(self):
        classifications = Classification.objects.filter(
            project=self.get_classification_project(),
            resource__deployment=self.get_object(),
        )
        ctx = super().get_serializer_context()
        ctx["classifications"] = classifications
        return ctx


class CSDeploymentUpdateTimestampsView(
    BaseCSDeploymentMixin, BaseCSMixin, generics.UpdateAPIView
):
    serializer_class = CSDeploymentUpdateTimestampsSerializer
    http_method_names = ["put"]

    def get_object(self):
        qs = self.get_deployments(self.get_classification_project(), self.request.user)
        return get_object_or_404(qs, pk=self.kwargs["deployment_pk"])
