from django.contrib.gis import admin

from .models import Team, TeamMembership


class TeamMembershipInline(admin.StackedInline):
    model = TeamMembership
    extra = 0


@admin.register(Team)
class TeamAdmin(admin.OSMGeoAdmin):
    default_zoom = 5

    autocomplete_fields = ("deployments",)

    list_display = [
        "name",
        "classification_project",
        "team_leader",
        "date_created",
    ]
    list_filter = [
        "classification_project"
    ]
    search_fields = [
        "name",
        "description"
    ]
    inlines = [TeamMembershipInline]


@admin.register(TeamMembership)
class TeamMembershipAdmin(admin.ModelAdmin):
    list_display = [
        "team",
        "user",
        "role",
        "status",
        "date_created",
    ]
    list_filter = [
        "team"
    ]
    search_fields = [
        "motivation"
    ]
