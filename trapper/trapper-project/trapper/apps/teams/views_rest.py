from django.http import Http404
from django.shortcuts import redirect
from django.utils.http import urlsafe_base64_decode
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from trapper.apps.accounts.models import User
from trapper.apps.accounts.serializers import CSUserSerializer
from trapper.apps.common.views_api import CSListPagination
from .filters import TeamFilters, NewMemberFilters
from .models import Team, TeamMembership
from .serializers import (
    TeamListingSerializer,
    TinyTeamSerializer,
    TeamDetailsSerializer,
    TeamLeaderInviteMemberSerializer,
    ShareDeploymentsSerializer,
    TeamMembershipSerializer,
    TeamManageSerializer,
)
from .tasks import (
    celery_send_team_invitation,
    celery_inform_ex_team_member_about_removal,
    celery_inform_team_leader_about_user_leave,
)


class TeamsListingView(APIView, CSListPagination):
    def get_queryset(self):
        """
        Return queryset with teams where user have access
        as a Team Leader or just a approved team member
        """

        cs_project_slug = self.kwargs["cs_project_slug"]

        return Team.objects.filter(
            classification_project__slug=cs_project_slug,
            team_memberships__user=self.request.user,
            team_memberships__status=TeamMembership.MembershipStatus.APPROVED,
        )

    def get(self, request, cs_project_slug: str):
        """
        Return list of all teams where user have an access
        """

        teams = self.get_queryset()

        filtered_teams = TeamFilters(request.GET, queryset=teams, request=request)

        teams = self.paginate_queryset(filtered_teams.qs, request, view=self)

        serializer = TeamListingSerializer(
            teams, many=True, context={"user": request.user}
        )

        return self.get_paginated_response(serializer.data)


class TeamsForSharingView(APIView):
    def get(self, request, cs_project_slug: str):
        """
        Return list of the teams where user belongs to
        """

        teams = Team.objects.filter(
            classification_project__slug=cs_project_slug,
            team_memberships__user=self.request.user,
            team_memberships__status=TeamMembership.MembershipStatus.APPROVED,
        )

        serializer = TinyTeamSerializer(teams, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class ShareDeploymentsView(APIView):
    def post(self, request, cs_project_slug: str, team_id: int):
        """
        Sharing deployments with Teams
        """

        try:
            # Get the team from specific classification project
            # where user belongs to with "Approved" membership status
            team = Team.objects.get(
                classification_project__slug=cs_project_slug,
                pk=team_id,
                team_memberships__user=self.request.user,
                team_memberships__status=TeamMembership.MembershipStatus.APPROVED,
            )

            serializer = ShareDeploymentsSerializer(
                data=request.data, instance=team, context={"user": request.user}
            )

            if serializer.is_valid():
                serializer.save()

                # Customized validated data as a dict are returned here
                # (it is not a mistake but FE requirement)
                return Response(
                    serializer.validated_data, status=status.HTTP_201_CREATED
                )
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except (Team.DoesNotExist, TeamMembership.DoesNotExist):
            raise Http404


class TeamCreateView(APIView):
    def post(self, request, cs_project_slug: str):
        """
        Create the team by the user
        """

        # Classification Project slug need to be passed
        # to join a newly created Team with it
        serializer = TeamManageSerializer(
            data=request.data,
            context={"user": request.user, "cs_project_slug": cs_project_slug},
        )

        if serializer.is_valid():
            team = serializer.save()

            new_team_serializer = TeamDetailsSerializer(
                team, context={"user": request.user}
            )

            return Response(new_team_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamManageView(APIView):
    def get(self, request, cs_project_slug: str, team_id: int):
        """
        Return details about the Team if user have an access
        (user have to be a approved member - Team Leader or just a Member)
        """
        teams = Team.objects.filter(
            classification_project__slug=cs_project_slug,
            team_memberships__user=request.user,
            team_memberships__status=TeamMembership.MembershipStatus.APPROVED,
        )

        try:
            team = teams.get(pk=team_id)
        except Team.DoesNotExist:
            raise Http404

        serializer = TeamDetailsSerializer(team, context={"user": request.user})

        return Response(serializer.data)

    def patch(self, request, cs_project_slug: str, team_id: int):
        """
        Update the team by Team Leader
        """

        try:
            team = Team.objects.get(
                classification_project__slug=cs_project_slug,
                pk=team_id,
                team_leader=request.user,
            )

            serializer = TeamManageSerializer(
                data=request.data,
                instance=team,
                partial=True,
                context={"user": request.user, "cs_project_slug": cs_project_slug},
            )

            if serializer.is_valid():
                updated_team = serializer.save()

                team_serializer = TeamDetailsSerializer(
                    updated_team, context={"user": request.user}
                )

                return Response(team_serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Team.DoesNotExist:
            raise Http404

    def delete(self, request, cs_project_slug: str, team_id: int):
        """
        Delete the team by Team Leader
        """
        try:
            team = Team.objects.get(
                classification_project__slug=cs_project_slug,
                pk=team_id,
                team_leader=request.user,
            )

            # Ex-active team members (users) IDs without Team Leader ID
            ex_user_ids = (
                team.team_memberships.exclude(user_id=request.user.pk)
                .filter(status=TeamMembership.MembershipStatus.APPROVED)
                .values_list("user_id", flat=True)
            )

            # Send mail message about Team membership removal
            for ex_user_id in ex_user_ids:
                if settings.CELERY_ENABLED:
                    celery_inform_ex_team_member_about_removal.delay(
                        team_name=team.name,
                        team_leader_id=request.user.pk,
                        ex_user_id=ex_user_id,
                    )
                else:
                    celery_inform_ex_team_member_about_removal(
                        team_name=team.name,
                        team_leader_id=request.user.pk,
                        ex_user_id=ex_user_id,
                    )

            team.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)
        except Team.DoesNotExist:
            raise Http404


class TeamLeaderSearchNewMemberView(APIView):
    def get(self, request, cs_project_slug: str, team_id: int):
        """
        Search active user database based on their
        username, last name and email
        """

        # All active users without user itself
        active_users = User.objects.filter(
            is_active=True,
        ).exclude(pk=request.user.pk)

        filtered_users = NewMemberFilters(
            request.GET, queryset=active_users, request=request
        )

        serializer = CSUserSerializer(filtered_users.qs, many=True)

        return Response(serializer.data)


class TeamLeaderInviteMemberView(APIView):
    def post(self, request, cs_project_slug: str, team_id: int):
        """
        Creating or updating existing team membership
        """

        team_leader_teams = Team.objects.filter(
            classification_project__slug=cs_project_slug, team_leader=request.user
        )

        try:
            team = team_leader_teams.get(pk=team_id)

            # Extend request data by Team ID
            request.data["team"] = team.pk
        except Team.DoesNotExist:
            raise Http404

        invitation_serializer = TeamLeaderInviteMemberSerializer(
            data=request.data,
        )

        if invitation_serializer.is_valid():
            team_membership = invitation_serializer.save()
            if settings.CELERY_ENABLED:
                # Send mail message with Team invitation
                celery_send_team_invitation.delay(
                    team_id=team.pk, new_user_member_id=team_membership.user.pk
                )
            else:
                # Send mail message with Team invitation
                celery_send_team_invitation(
                    team_id=team.pk, new_user_member_id=team_membership.user.pk
                )
            membership_serializer = TeamMembershipSerializer(team_membership)

            return Response(membership_serializer.data, status=status.HTTP_201_CREATED)

        return Response(
            invitation_serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )


class MemberManageInvitation(APIView):
    """
    Template class to manage the team membership invitation
    It can be handled by CHANGE_STATUS_TO value
    """

    authentication_classes = []
    permission_classes = []

    CHANGE_STATUS_TO = None
    REDIRECT_TO = f"https://{settings.FRONTEND_DOMAIN_NAME}/cs/projects"

    def _decode_id_from_token(self, token: str) -> int:
        """
        Decode ID from token string and return ID as integer
        """

        if token:
            raw_token = urlsafe_base64_decode(token)

            if raw_token.isdigit():
                return int(raw_token)

        raise Http404

    def get(self, request, cs_project_slug: str, team_id: int):
        """
        Change the team membership status from pending to approved or rejected
        """

        try:
            user_id_token = request.GET.get("token", None)
            user = User.objects.get(pk=self._decode_id_from_token(user_id_token))

            team = Team.objects.get(
                classification_project__slug=cs_project_slug, pk=team_id
            )

            team_membership_id_token = request.GET.get("key", None)
            pending_membership = team.team_memberships.get(
                pk=self._decode_id_from_token(team_membership_id_token),
                user=user,
                status=TeamMembership.MembershipStatus.PENDING,
            )
            pending_membership.status = self.CHANGE_STATUS_TO
            pending_membership.save(update_fields=["status"])

            # If user approved then see team details view
            if self.CHANGE_STATUS_TO == TeamMembership.MembershipStatus.APPROVED:
                return redirect(f"{self.REDIRECT_TO}/{cs_project_slug}/teams/{team.pk}")
            # If user rejected then see teams listing view
            else:
                return redirect(f"{self.REDIRECT_TO}/{cs_project_slug}/teams/")
        except (Team.DoesNotExist, TeamMembership.DoesNotExist, User.DoesNotExist):
            raise Http404


class MemberApproveInvitationView(MemberManageInvitation):
    """
    User approve team invitation so the team membership will
    change from "Pending" to "Approved"
    """

    CHANGE_STATUS_TO = TeamMembership.MembershipStatus.APPROVED


class MemberRejectInvitationView(MemberManageInvitation):
    """
    User reject team invitation so the team membership will
    change from "Pending" to "Rejected"
    """

    CHANGE_STATUS_TO = TeamMembership.MembershipStatus.REJECTED


class MemberLeaveTeamView(APIView):
    def post(self, request, cs_project_slug: str, team_id: int):
        """
        Member leave the team by himself
        """

        try:
            team = Team.objects.get(
                classification_project__slug=cs_project_slug,
                pk=team_id,
            )

            member = team.team_memberships.get(user=request.user)

            # If Member is a Team Leader then he can not leave the Team
            if team.team_leader != member.user:
                # Need to kept for control Celery task executed below
                ex_user_id = member.user.pk

                member.delete()
                if settings.CELERY_ENABLED:
                    # Send mail message to Team Leader that the team member left the team
                    celery_inform_team_leader_about_user_leave.delay(
                        team_id=team.pk, ex_user_id=ex_user_id
                    )
                else:
                    # Send mail message to Team Leader that the team member left the team
                    celery_inform_team_leader_about_user_leave(
                        team_id=team.pk, ex_user_id=ex_user_id
                    )

                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(
                    {"user": "Team Leader can not leave his own team"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        except (Team.DoesNotExist, TeamMembership.DoesNotExist):
            raise Http404


class TeamMemberView(APIView):
    def delete(
        self, request, cs_project_slug: str, team_id: int, team_membership_id: int
    ):
        """
        Removing existing member from the team by Team Leader
        """

        try:
            team = Team.objects.get(
                classification_project__slug=cs_project_slug,
                pk=team_id,
                team_leader=request.user,
            )

            # Team Leader request have to exclude membership instance itself
            member = team.team_memberships.exclude(user=request.user).get(
                pk=team_membership_id
            )

            # Need to kept for control Celery task executed below
            ex_membership_status = member.status
            ex_user_id = member.user.pk

            member.delete()

            # Send mail message about Team membership removal only
            # if ex-team membership was on "Approved" status
            if ex_membership_status == TeamMembership.MembershipStatus.APPROVED:
                if settings.CELERY_ENABLED:
                    celery_inform_ex_team_member_about_removal.delay(
                        team_name=team.name,
                        team_leader_id=team.team_leader.pk,
                        ex_user_id=ex_user_id,
                    )
                else:
                    celery_inform_ex_team_member_about_removal(
                        team_name=team.name,
                        team_leader_id=team.team_leader.pk,
                        ex_user_id=ex_user_id,
                    )

            return Response(status=status.HTTP_204_NO_CONTENT)
        except (Team.DoesNotExist, TeamMembership.DoesNotExist):
            raise Http404
