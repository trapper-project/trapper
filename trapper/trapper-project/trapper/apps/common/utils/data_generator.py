"""
Tools to generate and load fake data into the TRAPPER database.

Quick start:

from trapper.apps.common.utils import data_generator
gen = data_generator.TrapperFakeDataGenerator()

# Reset all data in a database (including or excluding users)
gen._reset_data(users=False)

# Generate basic data (map tile layers and "named" users)
gen.gen_basic_data()

# Generate users (params taken from a default config)
users = gen.gen_users()

# Generate research projects (params taken from a default config)
rps = gen.gen_research_projects()

# Generate random locations & deployments (params taken from a default config)
locs = gen.gen_locations()
deps = gen.gen_deployments()

# Generate 1000 locations owned by a specified user
my_locs = gen.gen_locations(number=1000, owner=1, prefix='MYlocs')

# Generate resources (random deployment, date_recorded & owner,
# no "physical" file assigned; params taken from a default config)
res = gen.gen_resources()

# Generate resources (with a random "physical" file downloaded
# from the "unsplash.com" and assigend to each resource; define owner)
res = gen.gen_resources(number=10, prefix='UNS', owner=1, random_img='unsplash')

# Same as above but create a named collection in the end; define owner, prefix
# and unsplash api search term
res = gen.gen_resources(
    number=10, prefix='UNS', random_img='unsplash', owner=1,
    create_coll=True, coll_name='UNSPLASH IMAGES',
    random_img_search='monkey'
)

# Generate a massive collection of random resources owned by a specified user
res = gen.gen_resources(
    number=100000, prefix='MYres', owner=4, create_coll=True,
    coll_name='LARGE_COL'
)

# Generate random collections (params taken from a default config)
colls = gen.gen_collections()

# Generate random collections but specify owner and number of resources in
# each collection
colls = gen.gen_collections(number=30, prefix='SHARED', owner=1, n_resources=2000)

# Generate collection members for specified collections and users
col_mems = gen._make_users_collection_members(colls, users=[3,4])

# Generate a "named" research projech
rp = gen.gen_research_projects(
    number=1, prefix='MY_GREAT_PROJECT', uuid4_names=False, owner=1
)

# Add previously generated collections to a research project generated above
rpcs = gen._add_collections2researchprojects(colls, projects=[rp[0].id,])
"""

import os
import sys
import json
import uuid
import logging
import datetime
import random
import tempfile
import requests
import numpy as np

from PIL import Image
from io import BytesIO

from umap.models import TileLayer

from django.apps import apps
from django.contrib.auth.models import Group
from django.core.files import File
from django.contrib.auth.hashers import make_password
from django.contrib.gis.geos import Point

from trapper.apps.accounts.models import User
from trapper.apps.storage.models import Resource, Collection, CollectionMember
from trapper.apps.common.tools import datetime_aware
from trapper.apps.storage.tasks import celery_update_thumbnails
from trapper.apps.messaging.models import Message
from trapper.apps.geomap.models import Location
from trapper.apps.geomap.models import Deployment
from trapper.apps.research.models import (
    ResearchProject,
    ResearchProjectRole,
    ResearchProjectCollection,
)
from trapper.apps.media_classification.models import (
    Classificator,
    ClassificationProject,
    ClassificationProjectRole,
    ClassificationProjectCollection,
    Sequence,
)

LOGGER = logging.getLogger("data_generator")
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler(sys.stdout))


GENERATOR_DEFAULT_CONFIG = {
    "accounts": {"user": {"number": 20, "prefix": "USER", "name_field": "username"}},
    "research": {
        "researchproject": {
            "number": 10,
            "prefix": "RP",
            "name_field": "name",
            "fk_fields": {
                "owner": {
                    "app": "accounts",
                    "model": "user",
                    "random": True,
                    "fixed": None,
                    "attrs": ["id"],
                }
            },
        },
    },
    "geomap": {
        "location": {
            "number": 1000,
            "prefix": "LOC",
            "name_field": "location_id",
            "fk_fields": {
                "owner": {
                    "app": "accounts",
                    "model": "user",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id"],
                },
                "research_project": {
                    "app": "research",
                    "model": "researchproject",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id"],
                },
            },
        },
        "deployment": {
            "number": 2000,
            "prefix": "DEP",
            "name_field": "deployment_code",
            "fk_fields": {
                "owner": {
                    "app": "accounts",
                    "model": "user",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id"],
                },
                "location": {
                    "app": "geomap",
                    "model": "location",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id", "research_project_id"],
                },
            },
        },
    },
    "storage": {
        "resource": {
            "number": 10000,
            "res_type": "I",
            "prefix": "RES",
            "name_field": "name",
            "fk_fields": {
                "owner": {
                    "app": "accounts",
                    "model": "user",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id"],
                },
                "deployment": {
                    "app": "geomap",
                    "model": "deployment",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id", "start_date", "end_date"],
                },
            },
        },
        "collection": {
            "number": 100,
            "prefix": "COL",
            "name_field": "name",
            "n_resources": 1000,
            "fk_fields": {
                "owner": {
                    "app": "accounts",
                    "model": "user",
                    "random": True,
                    "fixed_id": None,
                    "attrs": ["id"],
                }
            },
        },
    },
}


class TrapperFakeDataGenerator:
    """
    TODO: docstrings
    """

    def __init__(self, config=None, basic_data=False):
        if config:
            with open(config) as json_file:
                self.config = json.load(json_file)
        else:
            self.config = GENERATOR_DEFAULT_CONFIG
        self.basic_data = basic_data
        # number of objects created per model
        self.summary = {}

    # ---
    # --- Helper functions
    # ---

    @staticmethod
    def _reset_data(users=False):
        """Method responsible for deleting all data in a database"""
        LOGGER.info(u" Deleting all objects of the following models:")

        if users:
            LOGGER.info(u" > users...")
            User.objects.all().delete()
            LOGGER.info(u" > groups...")
            Group.objects.all().delete()

        LOGGER.info(u" > messages...")
        Message.objects.all().delete()
        LOGGER.info(u" > resources...")
        Resource.objects.all().delete()
        LOGGER.info(u" > collections...")
        Collection.objects.all().delete()
        LOGGER.info(u" > research projects...")
        ResearchProject.objects.all().delete()
        LOGGER.info(u" > research project roles...")
        ResearchProjectRole.objects.all().delete()
        LOGGER.info(u" > research project collections...")
        ResearchProjectCollection.objects.all().delete()
        LOGGER.info(u" > classification attributesets...")
        Classificator.objects.all().delete()
        LOGGER.info(u" > classification projects...")
        ClassificationProject.objects.all().delete()
        LOGGER.info(u" > classification project roles...")
        ClassificationProjectRole.objects.all().delete()
        LOGGER.info(u" > classification project collections...")
        ClassificationProjectCollection.objects.all().delete()
        LOGGER.info(u" > tile layers...")
        TileLayer.objects.all().delete()
        LOGGER.info(u" > locations...")
        Location.objects.all().delete()
        LOGGER.info(u" > deployments...")
        Deployment.objects.all().delete()
        LOGGER.info(u" > sequences...")
        Sequence.objects.all().delete()

    @staticmethod
    def _get_random_suffix(n):
        hex_uuid4 = uuid.uuid4().hex
        suffix = "".join([random.choice(hex_uuid4) for x in range(n)])
        return suffix

    # def _get_random_datetime(
    #     self, start_year=2009, end_year=datetime.datetime.now().year
    # ):
    #     try:
    #         dt = datetime_aware(
    #             data=datetime.datetime(
    #                 random.randint(start_year, end_year),
    #                 random.randint(1, 12),
    #                 random.randint(1, 28),
    #                 random.randint(0, 23),
    #                 random.randint(0, 59),
    #                 random.randint(0, 59)
    #             )
    #         )
    #         return dt
    #     except:
    #         self._get_random_datetime()

    @staticmethod
    def _get_random_datetime(
        start=datetime.datetime(2009, 1, 1), end=datetime.datetime.now(), aware=False
    ):
        """Return a random datetime between two datetime objects."""
        if not aware:
            start = datetime_aware(start)
            end = datetime_aware(end)
        delta = end - start
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        random_second = random.randrange(int_delta)
        return start + datetime.timedelta(seconds=random_second)

    @staticmethod
    def _create_random_image(width=160, height=120, file_ext=".jpg"):
        """Create a random image and save it in a temporary location"""
        tf = tempfile.NamedTemporaryFile(suffix=file_ext, delete=False)
        rgb_array = np.random.rand(height, width, 3) * 255
        image = Image.fromarray(rgb_array.astype("uint8")).convert("RGB")
        image.save(tf.name)
        return tf.name

    @staticmethod
    def _get_unsplash_image(width=640, height=480, file_ext=".jpg", search="wildlife"):
        """Get a random image from unsplash using provided API"""
        url = "https://source.unsplash.com/{width}x{height}/?{search}".format(
            width=width, height=height, search=search
        )
        response = requests.get(url)
        tf = tempfile.NamedTemporaryFile(suffix=file_ext, delete=False)
        image = Image.open(BytesIO(response.content))
        image.save(tf.name)
        return tf.name

    def _get_random_image(self, method, **kwargs):
        if method == "numpy":
            return self._create_random_image(**kwargs)
        if method == "unsplash":
            return self._get_unsplash_image(**kwargs)

    def _create_user(
        self, username, password=None, is_staff=False, is_superuser=False, save=False
    ):
        user = User(
            username=username,
            email="{username}@os-conservation.org".format(username=username),
            is_staff=is_staff,
            is_superuser=is_superuser,
            password=password or make_password(username),
        )
        if save:
            user.save()
        return user

    def _create_location(
        self, location_id, research_project, owner, srid=4326, save=False
    ):
        loc = Location(
            location_id=location_id,
            coordinates=Point(
                (random.uniform(0, 90), random.uniform(0, 180)), srid=4326
            ),
            research_project_id=research_project["id"],
            owner_id=owner["id"],
            date_created=self._get_random_datetime(),
        )
        if save:
            loc.save()
        return loc

    def _create_deployment(
        self, location, deployment_code, owner, duration=20, save=False
    ):
        date_created = self._get_random_datetime()
        days_delta = random.randint(duration + 10, 100)
        start_date = date_created - datetime.timedelta(days=days_delta)
        end_date = start_date + datetime.timedelta(days=duration)
        dep = Deployment(
            location_id=location["id"],
            deployment_code=deployment_code,
            research_project_id=location["research_project_id"],
            owner_id=owner["id"],
            start_date=start_date,
            end_date=end_date,
            date_created=date_created,
        )
        dep.deployment_id = "-".join([dep.deployment_code, dep.location.location_id])
        if save:
            dep.save()
        return dep

    def _create_resource(
        self,
        name,
        deployment,
        owner,
        res_type="I",
        save=False,
        random_image=None,
        random_image_kwargs=None,
    ):
        date_recorded = self._get_random_datetime(
            start=deployment["start_date"], end=deployment["end_date"], aware=True
        )
        res = Resource(
            name=name,
            deployment_id=deployment["id"],
            owner_id=owner["id"],
            date_recorded=date_recorded,
            resource_type=res_type,
        )
        if random_image:
            img_path = self._get_random_image(random_image, **random_image_kwargs)
            img_ext = os.path.splitext(img_path)[1]
            with open(img_path, "rb") as res_file:
                res.file.save(name + img_ext, File(res_file), save=False)
            res.update_metadata(commit=False)
            os.remove(img_path)
        if save:
            res.save()
        return res

    def _create_collection(self, name, owner, save=False):
        col = Collection(name=name, owner_id=owner["id"])
        if save:
            col.save()
        return col

    def _create_researchproject(self, name, owner, save=False):
        # acronym has to be unique; 10 chars max
        acronym = name[:2] + "".join(random.choice(uuid.uuid4().hex) for x in range(8))
        rproject = ResearchProject(
            name=name, acronym=acronym, owner_id=owner["id"], status=True
        )
        if save:
            rproject.save()
        return rproject

    @staticmethod
    def _add_resources2collections(collections, n_resources):
        resources = np.array(Resource.objects.values_list("pk", flat=True))
        N = len(collections)
        for i, collection in enumerate(collections):
            LOGGER.info(
                " > collection > adding resources to collection "
                "{i}/{N}...".format(i=i, N=N)
            )
            collection.resources.set(
                resources[np.random.randint(0, len(resources) - 1, n_resources)]
            )

    @staticmethod
    def _make_users_collection_members(collections, n_users=10, users=None, level=1):
        N = len(collections)
        if not users:
            users_qs = np.array(User.objects.values_list("pk", flat=True))
        objects = []
        for i, c in enumerate(collections):
            LOGGER.info(
                " > collection > making users collection members "
                "{i}/{N}...".format(i=i, N=N)
            )
            if not users:
                users = users_qs[np.random.randint(0, len(users_qs) - 1, n_users)]
            for u in users:
                obj = CollectionMember(user_id=u, collection=c, level=level)
                objects.append(obj)
        objects = CollectionMember.objects.bulk_create(objects)
        return objects

    @staticmethod
    def _add_collections2researchprojects(collections, n_projects=10, projects=None):
        N = len(collections)
        if not projects:
            projects_qs = np.array(ResearchProject.objects.values_list("pk", flat=True))
        objects = []
        for i, c in enumerate(collections):
            LOGGER.info(
                " > collection > adding collection to research projects "
                "{i}/{N}...".format(i=i, N=N)
            )
            if not projects:
                projects = projects_qs[
                    np.random.randint(0, len(projects_qs) - 1, n_projects)
                ]
            for p in projects:
                obj = ResearchProjectCollection(collection=c, project_id=p)
                objects.append(obj)
        objects = ResearchProjectCollection.objects.bulk_create(objects)
        return objects

    # ---
    # --- Data generation functions
    # ---

    def _gen_tile_layers(self):
        LOGGER.info(" > tile layers...")
        TileLayer.objects.get_or_create(
            name="OSM",
            url_template="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            minZoom=0,
            maxZoom=18,
            attribution="OSM",
        )

    def gen_named_users(self, password="1234"):
        """Generate named users"""
        LOGGER.info(" > named users...")
        self._create_user(
            username="admin",
            is_staff=True,
            is_superuser=True,
            password=password,
            save=True,
        )
        self._create_user(username="staff", is_staff=True, password=password, save=True)
        self._create_user(username="regular", password=password, save=True)

    def gen_objects(
        self,
        app,
        model,
        params,
        uuid4_names=True,
        number=None,
        prefix=None,
        config=None,
    ):
        """Bulk create objects of given model."""
        config = config or self.config[app][model]
        number = number or config["number"]
        prefix = prefix or config["prefix"]
        name_field = config["name_field"]
        fk_fields = config.get("fk_fields", {})
        method_name = "_create_{model}".format(model=model)
        method = getattr(self, method_name, None)
        _model = apps.get_model(app, model)
        fk_fields_data = {}
        for fk, v in fk_fields.items():
            fk_model = apps.get_model(v["app"], v["model"])
            if fk_model.objects.count() == 0:
                LOGGER.info(
                    " > {model} > No objects of {fk_model} FK model in a "
                    "database! Try to generate some of them first.".format(
                        model=model, fk_model=v["model"]
                    )
                )
                return
            if v["random"]:
                fk_fields_data[fk] = list(fk_model.objects.values(*v["attrs"]))
            else:
                fk_fields_data[fk] = list(
                    fk_model.objects.filter(pk=v["fixed_id"]).values(*v["attrs"])
                )
            if len(fk_fields_data[fk]) == 0:
                return

        objects = []
        for i in range(number):
            LOGGER.info(
                " > {model} > generating object {i}/{N} > prefix: {p}...".format(
                    model=model, i=i, N=number, p=prefix
                )
            )
            if uuid4_names:
                suffix = self._get_random_suffix(10)
                params[name_field] = "_".join([prefix, suffix])
            else:
                params[name_field] = prefix + str(i)
            for fk, v in fk_fields_data.items():
                if len(v) > 1:
                    params[fk] = random.choice(v)
                else:
                    params[fk] = v[0]
            obj = method(**params)
            objects.append(obj)

        LOGGER.info(
            " > {model} > Bulk creating {N} objects...".format(
                model=model, N=len(objects)
            )
        )
        objects = _model.objects.bulk_create(objects)
        LOGGER.info(" > {model} > Done!".format(model=model))
        self.summary["_".join([app, model])] = len(objects)
        return objects

    def gen_basic_data(self):
        # generate map-tile layers
        self._gen_tile_layers()
        # generate users
        self.gen_named_users()

    def gen_users(self, number=None, prefix=None, uuid4_names=True):
        # generate user
        app = "accounts"
        model = "user"
        objects = self.gen_objects(
            app, model, params={}, number=number, prefix=prefix, uuid4_names=uuid4_names
        )
        return objects

    def gen_research_projects(
        self, number=None, prefix=None, uuid4_names=True, owner=None
    ):
        # generate research projects
        app = "research"
        model = "researchproject"
        config = self.config[app][model]
        if owner:
            config["fk_fields"]["owner"]["random"] = False
            config["fk_fields"]["owner"]["fixed_id"] = owner
        objects = self.gen_objects(
            app,
            model,
            params={},
            number=number,
            prefix=prefix,
            uuid4_names=uuid4_names,
            config=config,
        )
        return objects

    def gen_locations(self, number=None, prefix=None, uuid4_names=True, owner=None):
        # generate locations
        app = "geomap"
        model = "location"
        config = self.config[app][model]
        if owner:
            config["fk_fields"]["owner"]["random"] = False
            config["fk_fields"]["owner"]["fixed_id"] = owner
        objects = self.gen_objects(
            app,
            model,
            params={},
            number=number,
            prefix=prefix,
            uuid4_names=uuid4_names,
            config=config,
        )
        return objects

    def gen_deployments(self, number=None, prefix=None, uuid4_names=True, owner=None):
        # generate deployments
        app = "geomap"
        model = "deployment"
        config = self.config[app][model]
        if owner:
            config["fk_fields"]["owner"]["random"] = False
            config["fk_fields"]["owner"]["fixed_id"] = owner
        objects = self.gen_objects(
            app,
            model,
            params={},
            number=number,
            prefix=prefix,
            uuid4_names=uuid4_names,
            config=config,
        )
        return objects

    def gen_resources(
        self,
        number=None,
        prefix=None,
        res_type=None,
        uuid4_names=True,
        owner=None,
        create_coll=False,
        coll_name=None,
        random_img=None,
        random_img_ext=".jpg",
        random_img_w=640,
        random_img_h=480,
        random_img_search="wildlife",
    ):
        # generate resources
        app = "storage"
        model = "resource"
        config = self.config[app][model]
        if owner:
            config["fk_fields"]["owner"]["random"] = False
            config["fk_fields"]["owner"]["fixed_id"] = owner
        params = {
            "res_type": res_type or config["res_type"],
            "random_image": random_img,
        }
        if random_img:
            params["random_image_kwargs"] = {
                "file_ext": random_img_ext,
                "width": random_img_w,
                "height": random_img_h,
                "search": random_img_search,
            }
        objects = self.gen_objects(
            app,
            model,
            params=params,
            number=number,
            prefix=prefix,
            uuid4_names=uuid4_names,
            config=config,
        )
        if random_img:
            celery_update_thumbnails.delay(objects)
        if create_coll and coll_name:
            suffix = self._get_random_suffix(10)
            coll_name = "_".join([coll_name, suffix])
            if not owner:
                owner = random.choice(User.objects.values("id"))
            else:
                owner = {"id": owner}
            coll = self._create_collection(coll_name, owner, save=True)
            coll.resources.set(objects)
        return objects

    def gen_collections(
        self, number=None, prefix=None, uuid4_names=True, owner=None, n_resources=None
    ):
        # generate random collections
        app = "storage"
        model = "collection"
        config = self.config[app][model]
        if owner:
            config["fk_fields"]["owner"]["random"] = False
            config["fk_fields"]["owner"]["fixed_id"] = owner
        collections = self.gen_objects(
            app,
            model,
            params={},
            number=number,
            prefix=prefix,
            uuid4_names=uuid4_names,
            config=config,
        )
        # add resources to generated collections
        n_resources = n_resources or config["n_resources"]
        self._add_resources2collections(collections, n_resources)
        return collections
