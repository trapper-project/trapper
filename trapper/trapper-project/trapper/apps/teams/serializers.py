from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from trapper.apps.accounts.serializers import CSUserSerializer
from trapper.apps.geomap.models import Deployment
from trapper.apps.media_classification.models import ClassificationProject
from .models import Team, TeamMembership


class TeamListingSerializer(serializers.ModelSerializer):
    """
    Serializer dedicated for Teams listing view
    """

    team_leader = CSUserSerializer()
    members_count = serializers.ReadOnlyField()
    can_manage = serializers.SerializerMethodField()

    def get_can_manage(self, obj) -> bool:
        """
        Return True or False depends on if the user is a Team Leader

        This flag can be used to determine if user is the owner and can
        display, edit and delete the team
        """

        user = self.context.get("user", None)

        if user:
            return obj.can_manage(user=user)

        return False

    class Meta:
        model = Team

        fields = (
            "id",
            "name",
            "description",
            "team_leader",
            "members_count",
            "can_manage",
            "date_created",
        )


class TinyTeamSerializer(serializers.ModelSerializer):
    """
    Tiny serializer for Teams
    """

    class Meta:
        model = Team

        fields = (
            "id",
            "name",
        )


class ShareDeploymentsSerializer(serializers.ModelSerializer):
    deployments = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Deployment.objects.all()
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Filter only deployments from specific
        # Classification Project defined in team instance
        if self.instance:
            self.fields["deployments"].queryset = Deployment.objects.filter(
                resources__classifications__project=self.instance.classification_project
            ).distinct()

    def validate(self, attr):
        """
        Deployments sharing validation
        """

        user = self.context.get("user", None)
        deployments = attr.get("deployments", None)

        if user and deployments:
            # Keep information about invalid deployment and rejection reason
            deployments_join_status = {"deployments": []}

            for deployment in deployments:
                deployment_data = {
                    "id": deployment.pk,
                    "deployment_code": deployment.deployment_code,
                    "rejection_reason": None,
                }

                # If team object have polygon then deployment (location) have to be extra filtered
                if self.instance.polygon:
                    if not self.instance.polygon.contains(
                        deployment.location.coordinates
                    ):
                        deployment_data["rejection_reason"] = _(
                            "Selected deployment is not in polygon"
                        )

                # Check if user have access to deployment. User can share only his own
                # deployments regardless of deployment status
                if deployment.owner != user:
                    deployment_data["rejection_reason"] = _(
                        "You have no rights to selected deployment"
                    )

                deployments_join_status["deployments"].append(deployment_data)

            return deployments_join_status

        raise serializers.ValidationError(
            {"deployments": _("Deployments list or user can not be empty")}
        )

    def update(self, instance, validated_data):
        """
        Connect Team with shared deployments
        """

        deployments = validated_data["deployments"]

        # Filter only valid deployments without rejection reason (have to be None)
        deployments = Deployment.objects.filter(
            pk__in=[
                dept["id"]
                for dept in filter(
                    lambda dept: dept["rejection_reason"] is None, deployments
                )
            ]
        )

        instance.deployments.add(*deployments)

        return instance

    class Meta:
        model = Team

        fields = ("deployments",)


class TeamMembershipSerializer(serializers.ModelSerializer):
    """
    Serializer dedicated for Team member (user) data part
    """

    # user = serializers.SerializerMethodField()
    user = CSUserSerializer()
    role = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    def get_role(self, obj):
        """
        Custom method for role key:value
        """
        return {obj.role: obj.get_role_display()}

    def get_status(self, obj):
        """
        Custom method for status key:value
        """
        return {obj.status: obj.get_status_display()}

    class Meta:
        model = TeamMembership

        fields = (
            "id",
            "user",
            "role",
            "status",
            "motivation",
            "date_created",
            "date_modified",
        )


class TeamDetailsSerializer(serializers.ModelSerializer):
    """
    Serializer dedicated for Team details view
    """

    team_leader = CSUserSerializer()
    members = TeamMembershipSerializer(source="team_memberships", many=True)
    can_manage = serializers.SerializerMethodField()

    def get_can_manage(self, obj) -> bool:
        """
        Return True or False depends on if the user is a Team Leader

        This flag can be used to determine if user is the owner and can
        display, edit and delete the team
        """

        user = self.context.get("user", None)

        if user:
            return obj.can_manage(user=user)

        return False

    class Meta:
        model = Team

        fields = (
            "id",
            "name",
            "description",
            "team_leader",
            "members",
            "can_manage",
            "date_created",
            "date_modified",
            "polygon",
        )


class TeamLeaderInviteMemberSerializer(serializers.ModelSerializer):
    """
    Serializer dedicated for invitation new team members
    """

    def validate(self, attrs):
        """
        Team membership validation before object creation
        """

        user = attrs.get("user", None)
        team = attrs.get("team", None)

        # User can be invited to the team more than once only if
        # user membership status is "Pending" or "Rejected". In the other hand
        # Team Leader can not invite the same user again.
        try:
            existing_member = team.team_memberships.get(user=user)

            if existing_member.status not in (
                TeamMembership.MembershipStatus.PENDING,
                TeamMembership.MembershipStatus.REJECTED,
            ):
                raise serializers.ValidationError(
                    {"user": "This user exists in the team"}
                )
            else:
                attrs["existing_member"] = existing_member
        except TeamMembership.DoesNotExist:
            pass

        return attrs

    def create(self, validated_data):
        """
        Creating a new team membership or updating an existing object
        """

        # Update team membership status for existing user from
        # "Rejected" to "Pending" (if Team Leader want to invite
        # the same user once again)
        existing_member = validated_data.get("existing_member", None)
        if existing_member:
            existing_member.status = TeamMembership.MembershipStatus.PENDING
            existing_member.save(update_fields=["status"])

            return existing_member
        # Create a new team membership object
        else:
            validated_data["role"] = TeamMembership.MembershipRole.MEMBER
            validated_data["status"] = TeamMembership.MembershipStatus.PENDING

            return TeamMembership.objects.create(**validated_data)

    class Meta:
        model = TeamMembership

        fields = ("user", "team", "motivation")


class TeamManageSerializer(serializers.ModelSerializer):
    """
    Serializer used for Team creation as well as for Team update
    """

    def validate(self, attrs):
        """
        Team creation or update require several criteria to be valid:
        - classification project have to exists and be a
          Citizen Science project (public or private)
        - Team name need to be unique in current workspace
          (includes teams create by the user and teams where the user is just a member)
        """

        try:
            # Get Classification Project object from current workspace
            cs_project_slug = self.context.get("cs_project_slug", None)

            classification_project = ClassificationProject.objects.get(
                slug=cs_project_slug,
                citizen_science_status__in=(
                    ClassificationProject.CitizenScienceStatus.PUBLIC,
                    ClassificationProject.CitizenScienceStatus.PRIVATE,
                ),
            )

            # Classification Project need to be passed for Team creation step
            attrs["classification_project"] = classification_project
        except ClassificationProject.DoesNotExist:
            raise serializers.ValidationError(
                {"classification_project": "Classification project does not exists"}
            )

        # Set user from request (session) as a Team Leader
        user = self.context.get("user", None)
        attrs["team_leader"] = user

        # All teams where user belongs to in current workspace
        user_teams = Team.objects.filter(
            classification_project=classification_project, team_memberships__user=user
        )
        # If instance exists it means the existing object will be updated
        # so it have to be excluded from Team name validation
        if self.instance:
            user_teams = user_teams.exclude(pk=self.instance.pk)

        team_name = attrs.get("name", None)
        if user_teams.filter(name=team_name).exists():
            raise serializers.ValidationError(
                {"name": "Team name exists in your teams in current workspace"}
            )

        return attrs

    def create(self, validated_data):
        """
        Creating a new team by the user
        """
        team = Team.objects.create(**validated_data)

        # Create user creator membership with Team Leader
        # role and "Approved" status
        TeamMembership.objects.create(
            team=team,
            user=team.team_leader,
            role=TeamMembership.MembershipRole.TEAM_LEADER,
            status=TeamMembership.MembershipStatus.APPROVED,
        )

        return team

    def update(self, instance, validated_data):
        """
        Updating existing team by Team Leader
        """

        instance.name = validated_data.get("name", instance.name)
        instance.description = validated_data.get("description", instance.description)

        if new_polygon := validated_data.get("polygon", None):
            instance.polygon = new_polygon

            # Exclude each deployment which is not in the polygon anymore
            for deployment in instance.deployments.all():
                if not new_polygon.contains(deployment.location.coordinates):
                    instance.deployments.remove(deployment)

        instance.save()

        return instance

    class Meta:
        model = Team

        fields = ("name", "description", "polygon")
