from celery.utils.log import get_task_logger
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from trapper import app
from trapper.apps.accounts.models import User
from trapper.apps.common.tools import TrapperCSMail, get_token
from .models import Team, TeamMembership

logger = get_task_logger(__name__)


@app.task(bind=True, max_retries=3, default_retry_delay=60)
def celery_send_team_invitation(self, team_id: int, new_user_member_id: int):
    """
    Send mail message in the context of Team invitation for the new user member
    """

    try:
        team = Team.objects.get(pk=team_id)
        team_membership = team.team_memberships.get(
            user_id=new_user_member_id, status=TeamMembership.MembershipStatus.PENDING
        )

        template_name = "send_team_invitation.html"
        title = _("You have been invited to the team")
        context = {
            "team_name": team.name,
            "team_leader": team.team_leader,
            "description": team.description,
            "motivation": team_membership.motivation,
            "project_name": settings.PROJECT_TITLE,
            "cp_slug": team.classification_project.slug,
            "team_id": team_id,
            "domain": settings.FRONTEND_DOMAIN_NAME,
            "user_id_token": get_token(team_membership.user.pk),
            "team_membership_id_token": get_token(team_membership.pk),
        }

        TrapperCSMail(
            template_name,
            title=title,
            context=context,
            user_mails=[team_membership.user.email],
        ).send()
    except Exception as e:
        logger.error(
            f'[DEBUG] Exception raised in "celery_send_team_invitation" task: {e}',
            exc_info=True,
            stack_info=True,
        )
        raise self.retry(exc=e, countdown=60)


@app.task(bind=True, max_retries=3, default_retry_delay=60)
def celery_inform_ex_team_member_about_removal(
    self, team_name: str, team_leader_id: int, ex_user_id: int
):
    """
    Send mail message to ex-team member that he was removed from the Team by Team Leader
    """

    try:
        team_leader = User.objects.get(pk=team_leader_id)

        template_name = "inform_ex_team_member.html"
        title = _("You have been removed from the team")
        context = {
            "team_name": team_name,
            "team_leader": team_leader,
            "project_name": settings.PROJECT_TITLE,
        }

        ex_user = User.objects.get(pk=ex_user_id)

        TrapperCSMail(
            template_name, title=title, context=context, user_mails=[ex_user.email]
        ).send()
    except Exception as e:
        logger.error(
            f'[DEBUG] Exception raised in "celery_inform_ex_team_member_about_removal" task: {e}',
            exc_info=True,
            stack_info=True,
        )
        raise self.retry(exc=e, countdown=60)


@app.task(bind=True, max_retries=3, default_retry_delay=60)
def celery_inform_team_leader_about_user_leave(self, team_id: int, ex_user_id: int):
    """
    Send mail message to Team Leader that the team member left the team
    """

    try:
        team = Team.objects.get(pk=team_id)

        ex_user = User.objects.get(pk=ex_user_id)

        template_name = "user_leave_the_team.html"
        title = _("Teammate has left the team")
        context = {
            "team_name": team.name,
            "ex_user": ex_user,
            "project_name": settings.PROJECT_TITLE,
        }

        TrapperCSMail(
            template_name, title=title, context=context, user_mails=[ex_user.email]
        ).send()
    except Exception as e:
        logger.error(
            f'[DEBUG] Exception raised in "celery_inform_team_leader_about_user_leave" task: {e}',
            exc_info=True,
            stack_info=True,
        )
        raise self.retry(exc=e, countdown=60)
