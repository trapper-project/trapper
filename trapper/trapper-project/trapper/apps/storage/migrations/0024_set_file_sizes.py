from django.conf import settings
from django.db import migrations


def set_resource_file_size(apps, schema_editor):
    Resource = apps.get_model("storage", "Resource")
    qs = Resource.objects.filter(file_size=0)
    resources_to_update = []

    for resource in qs:
        file_found = False
        try:
            resource.file_size = resource.file.size
            file_found = True
        except FileNotFoundError:
            pass
        if resource.file_preview.name:
            try:
                resource.file_preview_size = resource.file_preview.size
                file_found = True
            except FileNotFoundError:
                pass
        if resource.file_thumbnail.name:
            try:
                resource.file_thumbnail_size = resource.file_thumbnail.size
                file_found = True
            except FileNotFoundError:
                pass

        if file_found:
            resources_to_update.append(resource)

    Resource.objects.bulk_update(
        resources_to_update,
        ["file_size", "file_preview_size", "file_thumbnail_size"],
        batch_size=settings.BULK_BATCH_SIZE,
    )


class Migration(migrations.Migration):

    dependencies = [
        ("storage", "0023_auto_20230306_1558"),
    ]

    operations = [migrations.RunPython(set_resource_file_size)]
