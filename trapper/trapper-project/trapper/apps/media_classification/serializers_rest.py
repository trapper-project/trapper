from collections import defaultdict
from datetime import timedelta

from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.reverse import reverse

from trapper.apps.accounts.models import User, UserTask
from trapper.apps.citizen_science.filters import CSClassificationFilter
from trapper.apps.common.tools import non_field_errors, TrapperCSMail
from trapper.apps.extra_tables.models import Species
from trapper.apps.media_classification.models import (
    UserClassification,
    UserClassificationDynamicAttrs,
    Classification,
    Sequence,
    AIClassificationDynamicAttrs,
    FavoriteClassification,
    ClassificationProject,
)
from trapper.apps.media_classification.tasks import (
    celery_approve_user_classifications,
    send_request_new_species_for_classify,
)
from trapper.apps.media_classification.taxonomy import (
    ObservationType,
    ClassificationProjectRoleLevels,
)
from trapper.apps.messaging.models import Message


class SequenceSerializer(serializers.ModelSerializer):
    classifications = serializers.SerializerMethodField()

    class Meta:
        model = Sequence
        fields = [
            "id",
            "sequence_id",
            "classifications",
        ]

    def get_classifications(self, obj: Sequence):
        classifications_for_sequence = self.context["classifications_for_sequence"]
        return classifications_for_sequence.get(obj.id, [])


class ClassifyDynamicBboxesSerializer(serializers.Serializer):
    left = serializers.FloatField()
    top = serializers.FloatField()
    width = serializers.FloatField()
    height = serializers.FloatField()


class ClassifyDynamicSerializer(serializers.ModelSerializer):
    new_individual = serializers.BooleanField(required=False, allow_null=True)
    bboxes = serializers.JSONField(required=False, allow_null=True)

    class Meta:
        model = UserClassificationDynamicAttrs
        fields = None  # Declared in get_field_names

    def get_field_names(self, declared_fields, info):
        dynamic_fields_defs = self.context["fields_defs"]["D"]
        fields_list = list(dynamic_fields_defs.keys())
        fields = fields_list.copy()
        model_fields = [f.name for f in self.Meta.model._meta.get_fields()]
        for field_name in fields_list:
            if field_name not in model_fields:
                fields.remove(field_name)
        if "count_new" in fields:
            fields.remove("count_new")
            fields.append("new_individual")
        else:
            del declared_fields["new_individual"]
        # Currently unsupported fields
        if "individual_id" in fields:
            fields.remove("individual_id")
        if "classification_confidence" in fields:
            fields.remove("classification_confidence")

        self.Meta.fields = fields
        return super().get_field_names(declared_fields, info)

    # TODO: add validate species


class ClassifyDynamicVideoSerializer(ClassifyDynamicSerializer):
    bboxes = serializers.JSONField(required=False, allow_null=True)


class BaseClassifyMixin:
    """
    Add user to ClassificationProjectRole.

    Set bboxes for user classification.

    Send mail and create a Trapper Message to each active admins about
    detection of tracked species in specific classification project

    Tip: this class should be used by single and group classification
    """

    @staticmethod
    def set_bboxes_for_user_classification(user_classification):
        """
        Set has_bboxes attribute for user_classification
        """
        bboxes = user_classification.dynamic_attrs.values_list("bboxes", flat=True)
        if any(bboxes):
            user_classification.has_bboxes = True
            user_classification.save(update_fields=["has_bboxes"])

    @staticmethod
    def approve_if_user_is_error_proof(
        user: User,
        classification_project: ClassificationProject,
        user_classification_ids: list,
    ):
        """If user is error proof we should approve classifications"""
        if user.is_error_proof(classification_project):
            params = {
                "user": user,
                "project": classification_project,
                "user_classifications_pks": user_classification_ids,
            }
            if settings.CELERY_ENABLED:
                task = celery_approve_user_classifications.delay(**params)
                user_task = UserTask(user=user, task_id=task.task_id)
                user_task.save()
            else:
                celery_approve_user_classifications(**params)

    @staticmethod
    def add_user_to_classification_project(
        user: User, classification_project: ClassificationProject
    ):
        """
        Add if not exist user to ClassificationProjectRole on classify
        """
        if not classification_project.classification_project_roles.filter(
            user=user
        ).exists():
            # Create user if not exist
            classification_project.classification_project_roles.create(
                user=user,
                name=ClassificationProjectRoleLevels.CS_USER,
            )

    @staticmethod
    def send_message_and_mail_to_admins(
        species: Species, classification_project: ClassificationProject
    ):
        template_name = "media_classification/tracked_species.html"
        title = _(
            f"Tracked species ({species.english_name}) have been detected during classification process"
        )

        # URL to Classification Project details view
        url = reverse(
            "media_classification:project_detail", args=(classification_project.pk,)
        )
        context = {
            "species": species,
            "classification_project": classification_project.name,
            "domain": settings.DOMAIN_NAME,
            "url": url,
        }

        # Filter all active admin users
        admin_emails = []
        for user in User.objects.filter(is_superuser=True, is_active=True):
            admin_emails.append(user.email)

            # Create Trapper Message for each active admin user
            Message.objects.create(
                subject=title,
                text=_(
                    f"We would like to inform you that tracked species have been detected"
                    f' in the <a href="{url}">{classification_project.name}</a> classification project.'
                ),
                user_from=user,
                user_to=user,
            )

        # Send mail about detected tracked species to admins
        TrapperCSMail(
            template_name, title=title, context=context, user_mails=admin_emails
        ).send()


class SingleClassifySerializer(BaseClassifyMixin, serializers.ModelSerializer):
    # static = ClassifyStaticSerializer()
    dynamic = ClassifyDynamicSerializer(many=True)

    class Meta:
        model = UserClassification
        fields = ["dynamic"]

    def validate(self, attrs):
        dynamic_data = attrs.get("dynamic")
        if not dynamic_data:
            raise serializers.ValidationError({"dynamic": _("This field is required.")})

        if len(dynamic_data) > 1 and any(
            data["observation_type"] == ObservationType.BLANK for data in dynamic_data
        ):
            raise serializers.ValidationError(
                {
                    "dynamic": "If image is marked as empty, only one classification should be passed"
                }
            )

        # If we have Blank observation, only this data should pass validation, clean
        # other data
        if dynamic_data[0]["observation_type"] == ObservationType.BLANK:
            attrs["dynamic"][0] = {"observation_type": ObservationType.BLANK}
        self._check_classification()
        return attrs

    def _check_classification(self):
        classification = self.context["classification"]
        if classification.approved_source:
            raise non_field_errors(
                _("You can only pass classification only to not classified resources.")
            )
        return

    @staticmethod
    @transaction.atomic
    def _get_or_create_user_classification(classification, user):
        created = False
        user_classification = (
            UserClassification.objects.select_for_update()
            .filter(
                classification=classification,
                owner=user,
            )
            .first()
        )
        if not user_classification:
            user_classification = UserClassification.objects.create(
                classification=classification,
                owner=user,
            )
            created = True
        return user_classification, created

    def create(self, validated_data):
        classification = self.context["classification"]
        classification_project = self.context["classification_project"]
        user = self.context["request"].user
        now = timezone.now()
        classificator = classification_project.classificator
        # Get user classification
        user_classification, created = self._get_or_create_user_classification(
            classification, user
        )

        if not created:
            user_classification.updated_at = now

        classification.updated_at = now
        classification.updated_by = user
        classification.save(update_fields=["updated_at", "updated_by"])

        # Get static fields from classificator
        static_standard_fields = classificator.active_standard_attrs("STATIC")
        # Get dynamic fields from classificator
        dynamic_standard_fields = classificator.active_standard_attrs("DYNAMIC")

        # Static attrs
        if static_data := validated_data.get("static"):
            # First save all active standard attributes
            for attr_name in static_standard_fields:
                attr_value = static_data.pop(attr_name)
                setattr(user_classification, attr_name, attr_value)
        user_classification.save()

        # Dynamic attrs
        # bulk-delete of old rows
        user_classification.dynamic_attrs.all().delete()

        # Create new user dynamic attr
        for dynamic_validated_data in validated_data["dynamic"]:
            dynamic_attrs = UserClassificationDynamicAttrs(
                userclassification=user_classification,
            )

            # For all dynamic fields from classificator, get field name and set new
            # value if user set it
            for attr_name in dynamic_standard_fields:
                if attr_name == "count_new":
                    attr_value = dynamic_validated_data.get("new_individual", None)
                else:
                    attr_value = dynamic_validated_data.get(attr_name, None)
                if attr_value:
                    setattr(dynamic_attrs, attr_name, attr_value)

            # do not overwrite bboxes in a sequence, classification sequence is when
            # we have sequence_id or classification_ids in validated_data
            if bboxes := dynamic_validated_data.get("bboxes", None):
                if isinstance(bboxes, dict):
                    dynamic_attrs.bboxes = [
                        [
                            bboxes["left"],
                            bboxes["top"],
                            bboxes["width"],
                            bboxes["height"],
                        ]
                    ]
                else:
                    dynamic_attrs.bboxes = bboxes

            # Set default value
            dynamic_attrs.save()

            tracked_species = classification_project.classificator.tracked_species
            days_ago_7 = now - timedelta(days=7)
            species = dynamic_attrs.species
            if (
                species
                and tracked_species.filter(id=species.id).exists()
                and (
                    not classification.tracked_species_notifications_sent_at
                    or classification.tracked_species_notifications_sent_at < days_ago_7
                )
            ):
                self.send_message_and_mail_to_admins(
                    species=species, classification_project=classification_project
                )

                classification.tracked_species_notifications_sent_at = now
                classification.save(
                    update_fields=["tracked_species_notifications_sent_at"]
                )

        # set has_bboxes attribute for user_classification
        self.set_bboxes_for_user_classification(user_classification)

        self.approve_if_user_is_error_proof(
            user, classification_project, [user_classification.pk]
        )

        self.add_user_to_classification_project(user, classification_project)

        return user_classification


class SingleClassifyVideoSerializer(SingleClassifySerializer):
    dynamic = ClassifyDynamicVideoSerializer(many=True)


class FeedbackClassifySerializer(SingleClassifySerializer):
    def _check_classification(self):
        classification = self.context["classification"]
        if not classification.approved_source:
            raise non_field_errors(
                _("You can only pass feedback only to classified resources.")
            )
        return

    def create(self, validated_data):
        user_classification = super().create(validated_data)
        user_classification.is_feedback = True
        user_classification.save(update_fields=["is_feedback"])
        classification = self.context["classification"]
        url = f"https://{settings.DOMAIN_NAME}{reverse('media_classification:classify', args=(classification.pk,))}"
        body_template = _(
            "We would like to inform you that feedback has been submitted to the "
            "classification you approved.<br>"
            '<a href="{url}">Go to classification</a>'
        ).format(url=url)
        Message.objects.create(
            subject=_("Feedback to the approved classification has been submitted"),
            text=body_template,
            user_from=user_classification.owner,
            user_to=classification.approved_by,
        )
        return user_classification


class RequestNewSpeciesForClassifySerializer(serializers.Serializer):
    species = serializers.CharField()
    comment = serializers.CharField()

    def create(self, validated_data):
        classification = self.context["classification"]
        url = f"https://{settings.DOMAIN_NAME}{reverse('media_classification:classify', args=(classification.pk,))}"
        title = _(
            "Request for new species in the classification {classification_id}"
        ).format(classification_id=classification.pk)

        message = _(
            "We would like to inform you that {user} put new request for species"
            " in the classification.<br>"
            "Species: {species}<br>"
            "Comment: {comment}<br>"
            '<a href="{url}">Go to classification</a>'
        ).format(
            user=self.context["request"].user,
            species=validated_data["species"],
            comment=validated_data["comment"],
            url=url,
        )

        recipient_ids = list(
            self.context["classification_project"]
            .classification_project_roles.filter(
                name=ClassificationProjectRoleLevels.ADMIN
            )
            .values_list("user_id", flat=True)
        )
        params = {
            "title": title,
            "message": message,
            "recipient_ids": recipient_ids,
            "sender_id": self.context["request"].user.pk,
        }

        if settings.CELERY_ENABLED:
            send_request_new_species_for_classify.delay(**params)
        else:
            send_request_new_species_for_classify(**params)

        return {}


class GroupClassifySerializer(BaseClassifyMixin, serializers.ModelSerializer):
    classification_ids = serializers.ListSerializer(
        child=serializers.IntegerField(), required=False
    )
    dynamic = ClassifyDynamicSerializer(many=True)

    classifications = None
    user_classification_ids = []
    species = []

    class Meta:
        model = UserClassification
        fields = ["classification_ids", "dynamic"]

    def validate(self, attrs):
        dynamic_data = attrs.get("dynamic")
        if not dynamic_data:
            raise serializers.ValidationError({"dynamic": _("This field is required.")})
        else:
            if len(dynamic_data) != 1:
                raise serializers.ValidationError(
                    {"dynamic": _("Field should have only one classification.")}
                )

        # If we have Blank observation, only this data should pass validation, clean
        # other data
        if dynamic_data[0]["observation_type"] == ObservationType.BLANK:
            attrs["dynamic"][0] = {"observation_type": ObservationType.BLANK}

        request = self.context["request"]

        if classification_ids := attrs.get("classification_ids"):
            classifications = Classification.objects.filter(
                id__in=classification_ids,
                project=self.context["classification_project"],
            )
            # Filter classifications to get same queryset which is used in scroll view
            self.classifications = CSClassificationFilter(
                data=request.query_params, queryset=classifications, request=request
            ).qs
            if not self.classifications.exists():
                raise serializers.ValidationError(
                    {"classification_ids": _("Incorrect sequence ID.")}
                )
        else:
            raise serializers.ValidationError(
                _("Sequence ID or Classification IDs is required.")
            )

        self._check_classification()
        return super().validate(attrs)

    def _check_classification(self):
        self.classifications = self.classifications.filter(approved_source__isnull=True)
        if not self.classifications.exists():
            raise serializers.ValidationError(
                {
                    "classification_ids": _(
                        "Incorrect sequence ID. You can only pass classification only to not classified resources."
                    )
                }
            )
        return

    @staticmethod
    @transaction.atomic
    def _get_or_create_user_classification(classification, user):
        created = False
        user_classification = (
            UserClassification.objects.select_for_update()
            .filter(
                classification=classification,
                owner=user,
            )
            .first()
        )
        if not user_classification:
            user_classification = UserClassification.objects.create(
                classification=classification,
                owner=user,
            )
            created = True
        return user_classification, created

    def create(self, validated_data):
        now = timezone.now()
        user = self.context["request"].user
        classification_project = self.context["classification_project"]
        classificator = classification_project.classificator
        for classification in self.classifications:
            # Get user classification
            user_classification, created = self._get_or_create_user_classification(
                classification, user
            )
            if not created:
                user_classification.updated_at = now

            classification.updated_at = now
            classification.updated_by = user
            classification.save(update_fields=["updated_at", "updated_by"])

            # Get static fields from classificator
            static_standard_fields = classificator.active_standard_attrs("STATIC")
            # Get dynamic fields from classificator
            dynamic_standard_fields = classificator.active_standard_attrs("DYNAMIC")

            # Static attrs
            if static_data := validated_data.get("static"):
                # First save all active standard attributes
                for attr_name in static_standard_fields:
                    attr_value = static_data.pop(attr_name)
                    setattr(user_classification, attr_name, attr_value)
            user_classification.save()
            self.user_classification_ids.append(user_classification.pk)
            # Dynamic attrs
            dynamic_validated_data = validated_data["dynamic"][0]

            if dynamic_validated_data["observation_type"] == ObservationType.BLANK:
                user_classification.dynamic_attrs.all().delete()
                UserClassificationDynamicAttrs.objects.create(
                    userclassification=user_classification,
                )

            # Get dynamic initial/existing data from classification
            if user_classification.dynamic_attrs.exists():
                # Copying from User classification
                dynamic_existing_classification = (
                    user_classification.dynamic_attrs.all()
                )
            elif classification.ai_classifications.first():
                # Copying from AI Classification
                dynamic_existing_classification = (
                    classification.ai_classifications.first().dynamic_attrs.all()
                )
            else:
                raise non_field_errors(_("Unable to create classification."))

            # for all existing classifications
            for existing_classification in dynamic_existing_classification:
                # We only override classifications which have bboxes
                if isinstance(existing_classification, AIClassificationDynamicAttrs):
                    classification_dynamic = UserClassificationDynamicAttrs(
                        userclassification=user_classification,
                    )
                    if (
                        dynamic_validated_data["observation_type"]
                        != ObservationType.BLANK
                    ):
                        for field in classification_dynamic._meta.fields:
                            if field.primary_key or field.is_relation:
                                continue  # don't want to clone the PK or relations

                            setattr(
                                classification_dynamic,
                                field.name,
                                getattr(existing_classification, field.name),
                            )

                    classification_dynamic.save()
                else:
                    if (
                        existing_classification.observation_type
                        == ObservationType.BLANK
                    ):
                        continue
                    classification_dynamic = existing_classification
                # For all dynamic fields from classificator, get field name and set new
                # value if user set it
                for attr_name in dynamic_standard_fields:
                    if attr_name == "count_new":
                        attr_value = dynamic_validated_data.get("new_individual", None)
                    else:
                        attr_value = dynamic_validated_data.get(attr_name, None)
                    if attr_value:
                        setattr(classification_dynamic, attr_name, attr_value)

                classification_dynamic.save()

            # set has_bboxes attribute for user_classification
            self.set_bboxes_for_user_classification(user_classification)

        species = validated_data["dynamic"][0].get("species")

        tracked_species = classificator.tracked_species
        days_ago_7 = now - timedelta(days=7)

        if species and tracked_species.filter(id=species.id).exists():
            classifications = self.classifications.filter(
                Q(tracked_species_notifications_sent_at__isnull=True)
                | Q(tracked_species_notifications_sent_at__gte=days_ago_7)
            )

            self.send_message_and_mail_to_admins(
                species=species, classification_project=classification_project
            )

            classifications.update(tracked_species_notifications_sent_at=now)

        self.approve_if_user_is_error_proof(
            user, classification_project, self.user_classification_ids
        )

        self.add_user_to_classification_project(user, classification_project)

        return {}


class FeedbackGroupClassifySerializer(GroupClassifySerializer):
    def _check_classification(self):
        self.classifications = self.classifications.filter(
            approved_source__isnull=False
        )
        classifications_count = self.classifications.count()
        if classifications_count == 0:
            raise serializers.ValidationError(
                {
                    "classification_ids": _(
                        "Incorrect sequence IDs, no classifications found."
                    )
                }
            )
        elif classifications_count != len(
            self.initial_data.get("classification_ids", [])
        ):
            raise serializers.ValidationError(
                {
                    "classification_ids": _(
                        "You can pass feedback only to classified resources."
                    )
                }
            )
        return

    def create(self, validated_data):
        data = super().create(validated_data)
        user_classifications = UserClassification.objects.filter(
            id__in=self.user_classification_ids
        )
        user_classifications.update(is_feedback=True)
        approved_by = defaultdict(list)
        for cls in user_classifications:
            approved_by[cls.classification.approved_by].append(cls.pk)

        for user, classification_ids in approved_by.items():
            body_template = _(
                "We would like to inform you that feedback has been submitted to the "
                "classification you approved. Classification IDs:<br>"
                "{classification_ids}"
            ).format(classification_ids=", ".join(map(str, classification_ids)))
            Message.objects.create(
                subject=_(
                    "Feedback to the approved classifications has been submitted"
                ),
                text=body_template,
                user_from=user,
                user_to=user,
            )
        return data


class MarkClassifyAsFavoriteSerializer(serializers.Serializer):
    favorite = serializers.BooleanField()

    class Meta:
        fields = ["favorite"]

    def create(self, validated_data):
        classification = self.context["classification"]
        user = self.context["request"].user

        if validated_data["favorite"]:
            # Get or create favorite classification
            FavoriteClassification.objects.get_or_create(
                classification=classification,
                user=user,
            )
        else:
            try:
                favorite_classification = FavoriteClassification.objects.get(
                    classification=classification,
                    user=user,
                )
                favorite_classification.delete()
            except FavoriteClassification.DoesNotExist:
                pass
        return {}
