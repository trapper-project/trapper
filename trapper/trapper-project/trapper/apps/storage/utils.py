import base64
import hashlib


def calculate_file_checksum_hash(file) -> str:
    """
    Calculate checksum for file content and return it as a hex string.
    Accepts file as InMemoryUploadedFile or any file object that can be read using .read() method.
    """
    # first read the binary file content and convert it to base64 string
    file_content_str = base64.b64encode(file.read())

    image_checksum = hashlib.sha256(file_content_str).hexdigest()
    file.seek(0)

    return image_checksum
