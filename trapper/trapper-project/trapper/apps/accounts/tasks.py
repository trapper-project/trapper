import logging
from smtplib import SMTPException

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import mail_managers
from django.urls import reverse
from django.utils import timezone
from django.utils import translation
from django.utils.translation import gettext_lazy as _

from trapper import app
from trapper.apps.common.tools import get_token, TrapperCSMail

User = get_user_model()

logger = logging.getLogger(__name__)


@app.task
def celery_deactivate_user_after_2_years_tasks():
    """
    All regular users whose does not log into the platform for more than two years have
    to be automatically marked as inactive.
    """
    now = timezone.now()
    inactive_users = User.objects.filter(last_login__lte=now - relativedelta(years=2))
    count_inactive_users = inactive_users.count()
    for u in User.objects.filter(last_login__lte=now - relativedelta(years=2)):
        u.set_unusable_password()
        u.is_active = False
        u.save()

    logger.info(
        f"[deactivate_user_after_2_years_tasks] {count_inactive_users} users set as inactive."
    )
    return count_inactive_users


@app.task(
    autoretry_for=(SMTPException,), retry_kwargs={"max_retries": 5}, retry_backoff=True
)
def celery_send_registration_emails(user_pk, lang_code):
    """
    Send email to user with activate code and to managers with information
    """

    user = User.objects.get(pk=user_pk)

    with translation.override(lang_code):
        template_name = "accounts/active_email.html"
        title = _("Your account has just been created!")
        context = {
            "username": user.username,
            "domain": settings.FRONTEND_DOMAIN_NAME,
            "uid": get_token(user.pk),
            "token": default_token_generator.make_token(user),
        }
        user_mails = [user.email]

        mail_status = TrapperCSMail(
            template_name=template_name,
            title=title,
            context=context,
            user_mails=user_mails,
        ).send()

    admin_url = reverse("admin:accounts_user_change", args=(user.pk,))

    mail_managers(
        _("New request for account"),
        _(
            f"The following account was created: https://{settings.DOMAIN_NAME}{admin_url}"
        ),
    )

    return mail_status


@app.task(
    autoretry_for=(SMTPException,), retry_kwargs={"max_retries": 5}, retry_backoff=True
)
def celery_resend_activation_email(user_pk, lang_code):
    """
    Send email to user with activate code and to managers with information
    """

    user = User.objects.get(pk=user_pk)

    with translation.override(lang_code):
        template_name = "accounts/active_email.html"
        title = _("Your activation link has just been resent")
        context = {
            "username": user.username,
            "domain": settings.FRONTEND_DOMAIN_NAME,
            "uid": get_token(user.pk),
            "token": default_token_generator.make_token(user),
        }
        user_mails = [user.email]

        mail_status = TrapperCSMail(
            template_name=template_name,
            title=title,
            context=context,
            user_mails=user_mails,
        ).send()

    return mail_status


@app.task(
    autoretry_for=(SMTPException,), retry_kwargs={"max_retries": 5}, retry_backoff=True
)
def celery_send_password_reset_emails(user_pk, lang_code):
    """
    Send password reset email to user
    """
    user = User.objects.get(pk=user_pk)

    with translation.override(lang_code):
        template_name = "accounts/reset_password_email.html"
        title = _("Password reset for your account")
        context = {
            "username": user.username,
            "domain": settings.FRONTEND_DOMAIN_NAME,
            "uid": get_token(user.pk),
            "token": default_token_generator.make_token(user),
        }
        user_mails = [user.email]

        mail_status = TrapperCSMail(
            template_name=template_name,
            title=title,
            context=context,
            user_mails=user_mails,
        ).send()

    return mail_status
