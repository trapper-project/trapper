# Generated by Django 2.2.17 on 2021-10-06 12:02

from django.db import migrations, models
import trapper.apps.common.fields


class Migration(migrations.Migration):

    dependencies = [
        ('geomap', '0007_auto_20211005_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deployment',
            name='array',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='deployment',
            name='camera_id',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='deployment',
            name='camera_model',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='deployment',
            name='habitat',
            field=trapper.apps.common.fields.SafeTextField(blank=True, default='', verbose_name='Habitat'),
        ),
        migrations.AlterField(
            model_name='deployment',
            name='session',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='deployment',
            name='setup_by',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
    ]
