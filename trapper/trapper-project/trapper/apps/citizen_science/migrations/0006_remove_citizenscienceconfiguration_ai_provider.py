# Generated by Django 4.2.4 on 2024-05-31 06:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        (
            "citizen_science",
            "0005_alter_citizenscienceconfiguration_background_image_and_more",
        ),
    ]

    operations = [
        migrations.RemoveField(
            model_name="citizenscienceconfiguration",
            name="ai_provider",
        ),
    ]
