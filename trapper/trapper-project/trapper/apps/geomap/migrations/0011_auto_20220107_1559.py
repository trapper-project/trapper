# Generated by Django 3.2.5 on 2022-01-07 14:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geomap', '0010_deployment_camera_interval'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='deployment',
            name='unique_deployment_id',
        ),
        migrations.AlterField(
            model_name='deployment',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='deployments', to='geomap.location', verbose_name='Location'),
        ),
        migrations.AddConstraint(
            model_name='deployment',
            constraint=models.UniqueConstraint(fields=('deployment_id', 'research_project'), name='unique_deployment_id'),
        ),
    ]
