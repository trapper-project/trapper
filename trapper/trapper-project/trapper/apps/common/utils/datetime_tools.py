import datetime

import pytz


def localize_datetime_dst(dt, timezone, ignore_DST=False):
    """
    Function used to localize naive datetime objects in the given timezone, taking into account whether or not to
    ignore DST offset.
    Returns dt converted to UTC for saving in the database.
    Based on https://stackoverflow.com/a/48321914

    :param dt: naive datetime object
    :param timezone: pytz timezone instance
    :param ignore_DST: bool, whether to ignore Daytime Savings Time when converting datetime
    """
    if ignore_DST:
        utc_offset = timezone.utcoffset(dt)
        dst_offset = timezone.dst(dt)
        standard_offset = utc_offset - dst_offset
        d = dt.replace(tzinfo=datetime.timezone(standard_offset))
    else:
        d = timezone.localize(dt)
    return d.astimezone(pytz.UTC)


def set_correct_offset_dst(dt, timezone, ignore_DST=False):
    """
    Function used to localize UTC datetime objects (from the database) in the given timezone, taking into account
    whether or not to consider DST offset.
    Returns dt converted to desired timezone, with offset calculated considering ignore_DST.

    :param dt: UTC datetime object
    :param timezone: pytz timezone instance
    :param ignore_DST: bool, whether to ignore Daytime Savings Time when converting datetime
    """
    if ignore_DST:
        d = dt.astimezone(timezone)
        utc_offset = d.utcoffset()
        dst_offset = d.dst()
        standard_offset = utc_offset - dst_offset
        d = d.astimezone(datetime.timezone(standard_offset))
    else:
        d = dt.astimezone(timezone)
    return d
