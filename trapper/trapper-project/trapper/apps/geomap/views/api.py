# -*- coding: utf-8 -*-
import pandas

from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.serializers import ValidationError as rest_verror
from rest_framework_gis.filters import InBBoxFilter

from trapper.apps.common.views_api import PaginatedReadOnlyModelViewSet
from trapper.apps.geomap import serializers as geomap_serializers
from trapper.apps.geomap.filters import (
    LocationFilter,
    LocationGeoFilter,
    DeploymentFilter,
    MapFilter,
)
from trapper.apps.geomap.models import Location, Deployment, MapManagerUtils
from umap.models import Map


class LocationViewSet(PaginatedReadOnlyModelViewSet):
    """
    TODO: docstrings
    """

    queryset = Location.objects.all()
    serializer_class = geomap_serializers.LocationSerializer
    filterset_class = LocationFilter
    search_fields = [
        "location_id",
        "name",
        "description",
        "county",
        "city",
        "owner__username",
    ]
    select_related = [
        "research_project",
        "owner",
    ]

    def get_queryset(self):
        queryset = (
            Location.objects.get_available(user=self.request.user)
            .select_related(*self.select_related)
            .prefetch_related("managers")
        )
        return queryset


class LocationGeoViewSet(ListAPIView):
    """
    TODO: docstrings
    """

    serializer_class = geomap_serializers.LocationGeoSerializer
    filterset_class = LocationGeoFilter
    search_fields = [
        "location_id",
        "name",
        "description",
        "county",
        "city",
        "owner__username",
        "deployments__deployment_id",
    ]
    bbox_filter_field = "coordinates"
    filter_backends = (
        SearchFilter,
        DjangoFilterBackend,
        InBBoxFilter,
    )
    select_related = [
        "research_project",
        "owner",
    ]

    def get_queryset(self):
        queryset = (
            Location.objects.get_available(user=self.request.user)
            .select_related(*self.select_related)
            .prefetch_related("managers")
        )
        return queryset


class DeploymentViewSet(PaginatedReadOnlyModelViewSet):
    """
    TODO: docstrings
    """

    queryset = Deployment.objects.all()
    serializer_class = geomap_serializers.DeploymentSerializer
    filterset_class = DeploymentFilter
    search_fields = ["deployment_id", "owner__username"]
    select_related = [
        "location",
        "owner",
        "research_project",
    ]
    prefetch_related = ["managers", "tags"]

    def get_queryset(self):
        base_queryset = super(DeploymentViewSet, self).get_queryset()
        queryset = (
            Deployment.objects.get_accessible(
                base_queryset=base_queryset, user=self.request.user
            )
            .prefetch_related(*self.prefetch_related)
            .select_related(*self.select_related)
        )
        return queryset


class MapViewSet(PaginatedReadOnlyModelViewSet):
    """
    TODO: docstrings
    """

    queryset = Map.objects.all()
    serializer_class = geomap_serializers.MapSerializer
    filterset_class = MapFilter
    search_fields = ["name", "description", "owner__username"]
    select_related = [
        "owner",
    ]

    def get_queryset(self):
        queryset = MapManagerUtils.get_accessible(user=self.request.user)
        qs = queryset.select_related(*self.select_related)
        return qs


class LocationTableView(ListAPIView):
    """
    Returns a table with locations.
    """

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = geomap_serializers.LocationTableSerializer
    pagination_class = None
    filterset_class = LocationFilter
    search_fields = [
        "location_id",
        "name",
        "description",
        "country",
        "state",
        "county",
        "city",
    ]  # fields from db
    sort_by = ["researchProject", "locationID"]  # fields from serializer

    def get_queryset(self):
        queryset = Location.objects.get_available(
            user=self.request.user
        ).select_related("research_project")
        return queryset

    def get_columns_order(self):
        return self.serializer_class.Meta.fields

    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        if queryset.count() == 0:
            raise rest_verror(
                "Empty queryset or you have no permission to access the requested objects."
            )
        serializer = self.get_serializer(queryset, many=True)
        df = pandas.DataFrame.from_records(
            serializer.data, columns=self.get_columns_order()
        )
        df = df.sort_values(self.sort_by)
        df = df.reset_index(drop=True)
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=locations.csv"
        df.to_csv(response, encoding="utf-8", index=False)
        return response


class DeploymentTableView(ListAPIView):
    """
    Returns a table with deployments.
    """

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = geomap_serializers.DeploymentTableSerializer
    pagination_class = None
    filterset_class = DeploymentFilter
    search_fields = ["deployment_id", "owner__username"]
    sort_by = ["deploymentID"]  # field from serializer

    def get_queryset(self):
        queryset = (
            Deployment.objects.get_accessible(user=self.request.user)
            .select_related("location", "research_project")
            .prefetch_related("tags")
        )
        return queryset

    def get_columns_order(self):
        return self.serializer_class.Meta.fields

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if queryset.count() == 0:
            raise rest_verror(
                "Empty queryset or you have no permission to access the requested objects."
            )
        serializer = self.get_serializer(queryset, many=True)
        df = pandas.DataFrame.from_records(
            serializer.data, columns=self.get_columns_order()
        )
        df = df.sort_values(self.sort_by)
        df = df.reset_index(drop=True)
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=deployments.csv"
        df.to_csv(response, encoding="utf-8", index=False)
        return response
