from trapper.apps.media_classification.models import AIClassificationJob


def update_remote_classification_task_status(classification_job_id):
    from .ai_provider_factory import get_ai_provider_manager

    job = AIClassificationJob.objects.get(pk=classification_job_id)

    manager = get_ai_provider_manager(job.ai_provider)

    manager.update_remote_classification_task_status(job)


def schedule_status_update(classification_job_id, delay=0):
    from ..tasks import celery_update_remote_classification_task_status

    celery_update_remote_classification_task_status.apply_async(
        (classification_job_id,), countdown=delay
    )
