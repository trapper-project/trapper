# -*- coding: utf-8 -*-
"""Forms related to Messaging application"""
from __future__ import unicode_literals

from crispy_forms.layout import Layout, Fieldset
from django.contrib.auth import get_user_model
from trapper.apps.common.forms import BaseCrispyModelForm
from trapper.apps.messaging.models import Message

User = get_user_model()


class MessageForm(BaseCrispyModelForm):
    """Message form that is used for creating messages to other users.
    This form exclude currently logged in user from list of
    recipients.
    Also sending messages to inactive users is not allowed
    """

    select2_fields = ("user_to",)

    class Meta:
        model = Message
        fields = ["subject", "text", "user_to"]

    def __init__(self, *args, **kwargs):
        request = kwargs.pop("request", None)
        super(MessageForm, self).__init__(*args, **kwargs)
        # exclude inactive users and current user
        user_to_field = self.fields["user_to"]
        user_to_field.queryset = User.objects.filter(is_active=True).exclude(
            pk=request.user.pk
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(Fieldset("", "subject", "user_to", "text"))
