from allauth.account.auth_backends import AuthenticationBackend
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from rest_framework.request import Request


class CustomAuthenticationBackend(AuthenticationBackend):
    def authenticate(self, request, **credentials):
        user = super().authenticate(request, **credentials)
        # Citizen science cannot login to Expert
        if user and not isinstance(request, Request) and user.is_citizen_science:
            raise ValidationError(
                _("Citizen science user cannot login to Expert Portal.")
            )
        return user
