from django.urls import path
from django.views.decorators.csrf import ensure_csrf_cookie

from trapper.apps.citizen_science.views_rest import (
    CSGDPRView,
    CSPublicStatsView,
    CSTOSView,
    CSDashboardView,
    CSProjectsView,
    CSProjectView,
    CSProjectMediaView,
    CSProjectMediaResourceView,
    CSProjectUploadMediaView,
    CSProjectCreateUploadView,
    CSProjectCompleteUploadView,
    CSProjectMediaScrollView,
    CSInboxView,
    CSInboxMarkAsReceivedView,
    CSProjectSettingsView,
    CSProjectLogoView,
    CSProjectBackgroundView,
    CSProjectSidebarBackgroundView,
    CSAdditionalStylesView,
    CSProjectBackgroundImageView,
    CSAppVersionView,
)

urlpatterns = [
    path("gdpr/", CSGDPRView.as_view(), name="cs_gdpr"),
    path("tos/", CSTOSView.as_view(), name="cs_tos"),
    path("logo/", CSProjectLogoView.as_view(), name="cs_logo"),
    path("version/", CSAppVersionView.as_view(), name="cs_version"),
    path("public_stats/", CSPublicStatsView.as_view(), name="cs_public_stats"),
    path(
        "background_image/",
        CSProjectBackgroundView.as_view(),
        name="cs_background_image",
    ),
    path(
        "sidebar_image/",
        CSProjectSidebarBackgroundView.as_view(),
        name="cs_sidebar_image",
    ),
    path(
        "additional_styles/",
        CSAdditionalStylesView.as_view(),
        name="cs_additional_styles",
    ),
    path(
        "settings/",
        ensure_csrf_cookie(CSProjectSettingsView.as_view()),
        name="cs_settings",
    ),
    path("projects/", CSProjectsView.as_view(), name="cs_projects"),
    path("inbox/", CSInboxView.as_view(), name="cs_inbox"),
    path(
        "inbox/<int:message_id>/mark-as-received/",
        CSInboxMarkAsReceivedView.as_view(),
        name="cs_inbox_mark_as_received",
    ),
    path(
        "<slug:cs_project_slug>/background-image/",
        CSProjectBackgroundImageView.as_view(),
        name="cs_project_background_image",
    ),
    path(
        "<slug:cs_project_slug>/dashboard/",
        CSDashboardView.as_view(),
        name="cs_dashboard",
    ),
    path(
        "<slug:cs_project_slug>/media/", CSProjectMediaView.as_view(), name="cs_media"
    ),
    path(
        "<slug:cs_project_slug>/media/scroll/",
        CSProjectMediaScrollView.as_view(),
        name="cs_media_scroll",
    ),
    path(
        "<slug:cs_project_slug>/media/<int:media_pk>/",
        CSProjectMediaResourceView.as_view(),
        name="cs_media_resource",
    ),
    path(
        "<slug:cs_project_slug>/create-upload/",
        CSProjectCreateUploadView.as_view(),
        name="cs_create_upload",
    ),
    path(
        "<slug:cs_project_slug>/upload-media/",
        CSProjectUploadMediaView.as_view(),
        name="cs_upload_media",
    ),
    path(
        "<slug:cs_project_slug>/complete-upload/",
        CSProjectCompleteUploadView.as_view(),
        name="cs_complete_upload",
    ),
    path("<slug:cs_project_slug>/", CSProjectView.as_view(), name="cs_project"),
]
