from oauth2_provider.oauth2_validators import OAuth2Validator


class TrapperOAuthValidator(OAuth2Validator):
    def get_userinfo_claims(self, request):
        claims = super().get_userinfo_claims(request)
        user_id = request.user.id

        # Name of the user in the JupyterHub
        hub_username = f"trapper_user_{user_id}"
        claims["hub_username"] = hub_username
        return claims
