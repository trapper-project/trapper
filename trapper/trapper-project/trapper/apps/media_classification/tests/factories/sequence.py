import datetime

import factory
from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import Sequence
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)


class SequenceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Sequence
        exclude = ("now",)

    sequence_id = factory.Sequence(lambda n: f"sequence_{n}")
    description = factory.Faker("paragraph", nb_sentences=3)
    collection = factory.SubFactory(ClassificationProjectCollectionFactory)
    now = factory.LazyFunction(timezone.now)
    created_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))
    created_by = factory.SubFactory(UserFactory)
