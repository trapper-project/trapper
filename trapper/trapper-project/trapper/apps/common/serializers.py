# -*- coding: utf-8 -*-
"""Base serializer classes used in other applications where REST API is
defined"""
from django.urls import reverse
from rest_framework import serializers

from trapper.apps.accounts.utils import get_pretty_username
from trapper.apps.citizen_science.models import CitizenScienceConfiguration
from trapper.apps.common.models import Consent


class BaseListSerializer(serializers.ListSerializer):
    """Base list serializer class."""

    def __init__(self, *args, **kwargs):
        super(BaseListSerializer, self).__init__(*args, **kwargs)
        self.user = self.context["request"].user


class BasePKSerializer(serializers.ModelSerializer):
    """Base serializer class. Contains read-only `pk` field."""

    pk = serializers.ReadOnlyField()  # Read-only value of primary key


class PrettyUserField(serializers.ReadOnlyField):
    """This field takes user instance as value and return
    pretty formatted name"""

    def to_representation(self, obj):
        return get_pretty_username(obj)


class MixinErrorSerializer(serializers.Serializer):
    """Allow you to return all errors in same time."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._errors = {}

    def _add_error(self, field, message):
        if self._errors.get(field):
            self._errors[field].append(message)
        else:
            self._errors[field] = [message]

    def run_validation(self, *args, **kwargs):
        data = super().run_validation(*args, **kwargs)
        if self._errors:
            raise serializers.ValidationError(self._errors)
        return data


class ConsentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consent
        fields = (
            "date_modified",
            "content",
            "version",
        )


class ProjectSettingsSerializer(serializers.ModelSerializer):
    project_logo = serializers.SerializerMethodField()
    default_latitude = serializers.SerializerMethodField()
    default_longitude = serializers.SerializerMethodField()

    class Meta:
        model = CitizenScienceConfiguration
        fields = (
            "project_name",
            "project_logo",
            "linkedin_url",
            "facebook_url",
            "twitter_url",
            "default_latitude",
            "default_longitude",
            "additional_styles",
        )

    @staticmethod
    def get_project_logo(obj):
        return reverse("cs_logo")

    @staticmethod
    def get_default_latitude(obj):
        if obj.default_coordinates:
            return obj.default_coordinates.y

    @staticmethod
    def get_default_longitude(obj):
        if obj.default_coordinates:
            return obj.default_coordinates.x
