# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2019-08-21 22:45
from __future__ import unicode_literals

from django.db import migrations, models
import trapper.apps.storage.models


class Migration(migrations.Migration):

    dependencies = [
        ("storage", "0003_create_table_index_storage_date_recorded"),
    ]

    operations = [
        migrations.AlterField(
            model_name="resource",
            name="date_recorded",
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name="resource",
            name="file",
            field=models.FileField(upload_to=trapper.apps.storage.models.UPLOAD_DIR_F),
        ),
        migrations.AlterField(
            model_name="resource",
            name="file_preview",
            field=models.ImageField(
                blank=True,
                editable=False,
                null=True,
                upload_to=trapper.apps.storage.models.PREVIEW_DIR_F,
            ),
        ),
        migrations.AlterField(
            model_name="resource",
            name="file_thumbnail",
            field=models.ImageField(
                blank=True,
                editable=False,
                null=True,
                upload_to=trapper.apps.storage.models.THUMBNAIL_DIR_F,
            ),
        ),
    ]
