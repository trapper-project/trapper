# Generated by Django 2.2.11 on 2020-06-18 19:05

import django.contrib.postgres.indexes
import django.contrib.postgres.search
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("storage", "0011_auto_20200618_1520"),
    ]

    operations = [
        migrations.AddField(
            model_name="resource",
            name="search_data_vector",
            field=django.contrib.postgres.search.SearchVectorField(null=True),
        ),
        migrations.AddIndex(
            model_name="resource",
            index=django.contrib.postgres.indexes.GinIndex(
                fields=["search_data_vector"], name="storage_res_search__097127_gin"
            ),
        ),
    ]
