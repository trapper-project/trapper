from datetime import datetime, timezone
from io import BytesIO

from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from mock.mock import patch
from rest_framework import status
from rest_framework.test import APITestCase

from trapper.apps.accounts.models import User
from trapper.apps.common.tests.commons import BaseAPITestCase
from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)


class RegisterTests(APITestCase):
    @patch("django.utils.timezone.now")
    def test_create_account_correct(self, mocked_timezone):
        """
        Ensure we can create a new account object.
        """
        now = datetime(2010, 1, 1, tzinfo=timezone.utc)
        mocked_timezone.return_value = now
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "+02 123 456-789",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {
                "email": "test@email.com",
                "username": "test",
                "first_name": "First",
                "last_name": "Last",
                "phone_number": "+02 123 456-789",
                "institution": "Test Company",
                "about_me": "It's me",
                "gdpr": True,
                "tos": True,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        user = User.objects.get(email="test@email.com")
        self.assertTrue(user.is_citizen_science)
        self.assertEqual(user.userprofile.gdpr_accepted_date, now)
        self.assertEqual(user.userprofile.tos_accepted_date, now)

    def test_create_account_obligatory_fields_correct(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "",
            "institution": "",
            "about_me": "",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {
                "email": "test@email.com",
                "username": "test",
                "first_name": "First",
                "last_name": "Last",
                "phone_number": "",
                "institution": "",
                "about_me": "",
                "gdpr": True,
                "tos": True,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_account_incorrect_second_password(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@# ",
            "password2": "1234",
            "phone_number": "1234567",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(), {"password": ["Password fields didn't match."]}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_password_too_weak(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123",
            "password2": "123",
            "phone_number": "1234567",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {
                "password": [
                    "This password is too short. It must contain at least 8 characters.",
                    "This password is too common.",
                    "This password is entirely numeric.",
                ]
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_username_too_long(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test_test_test_test_test_test_test_",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "",
            "institution": "",
            "about_me": "",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {"username": ["Ensure this field has no more than 30 characters."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_incorrect_phone_number(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "telephone",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {
                "phone_number": [
                    "Phone number format is invalid. Min. 7, max 24 digits and only `+- ()` chars are allowed."
                ]
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_gdpr_false(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "1234567",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": False,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(response.json(), {"gdpr": ["Consent is required."]})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_tos_false(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "1234567",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": False,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(response.json(), {"tos": ["Consent is required."]})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_email_duplicate(self):
        """
        Ensure we can create a new account object.
        """
        User.objects.create(email="test@email.com", username="testuser")
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "1234567",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {"email": ["This field must be unique."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_account_username_duplicate(self):
        """
        Ensure we can create a new account object.
        """
        User.objects.create(email="testuser@email.com", username="test")
        data = {
            "email": "test@email.com",
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "password": "123ASD!@#",
            "password2": "123ASD!@#",
            "phone_number": "1234567",
            "institution": "Test Company",
            "about_me": "It's me",
            "gdpr": True,
            "tos": True,
        }
        response = self.client.post(reverse("auth_register"), data=data)
        self.assertEqual(
            response.json(),
            {"username": ["A user with that username already exists."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class AccountsTests(BaseAPITestCase):
    def test_get_profile_correct(self):
        image = BytesIO()
        Image.new("RGB", (100, 100)).save(image, "JPEG")
        image.seek(0)
        self.user.username = "Username"
        self.user.email = "test@email.com"
        self.user.first_name = "First name"
        self.user.last_name = "Last name"
        self.user.userprofile.phone_number = "123456789"
        self.user.userprofile.about_me = "About me"
        self.user.userprofile.institution = "Institution"
        self.user.userprofile.avatar = SimpleUploadedFile("image.jpg", image.getvalue())
        self.user.save()

        response = self.client.get(reverse("profile"))

        self.assertEqual(
            response.json(),
            {
                "username": "Username",
                "email": "test@email.com",
                "first_name": "First name",
                "last_name": "Last name",
                "phone_number": "123456789",
                "about_me": "About me",
                "avatar": response.json()["avatar"],
                "institution": "Institution",
                "gdpr": False,
                "gdpr_accepted_date": None,
                "tos": False,
                "tos_accepted_date": None,
                "default_cs_project": None,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_profile_correct(self):
        image = BytesIO()
        Image.new("RGB", (250, 250)).save(image, "JPEG")
        image.seek(0)
        data = {
            "username": "TestUsername",
            "first_name": "My First",
            "last_name": "Your Last",
            "phone_number": "111111111",
            "avatar": SimpleUploadedFile("image.jpg", image.getvalue()),
            "about_me": "About Me Poem",
            "institution": "Corpo",
        }
        response = self.client.patch(reverse("profile"), data=data)
        self.user.refresh_from_db()
        self.assertEqual(
            response.json(),
            {
                "about_me": "About Me Poem",
                "avatar": response.json()["avatar"],
                "first_name": "My First",
                "institution": "Corpo",
                "last_name": "Your Last",
                "phone_number": "111111111",
                "username": "TestUsername",
                "email": self.user.email,
                "gdpr": False,
                "gdpr_accepted_date": None,
                "tos": False,
                "tos_accepted_date": None,
                "default_cs_project": None,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user.username, data["username"])
        self.assertEqual(self.user.first_name, data["first_name"])
        self.assertEqual(self.user.last_name, data["last_name"])
        self.assertTrue(
            self.user.userprofile.avatar.storage.exists(
                self.user.userprofile.avatar.name
            )
        )
        self.assertEqual(self.user.userprofile.phone_number, data["phone_number"])
        self.assertEqual(self.user.userprofile.about_me, data["about_me"])
        self.assertEqual(self.user.userprofile.institution, data["institution"])

    def test_edit_profile_edit_only_name_correct(self):
        data = {
            "first_name": "My First",
            "last_name": "Your Last",
        }
        response = self.client.patch(reverse("profile"), data=data)
        self.user.refresh_from_db()
        self.assertEqual(
            response.json(),
            {
                "about_me": "",
                "avatar": "",
                "first_name": "My First",
                "institution": "",
                "last_name": "Your Last",
                "phone_number": "",
                "username": self.user.username,
                "email": self.user.email,
                "gdpr": False,
                "gdpr_accepted_date": None,
                "tos": False,
                "tos_accepted_date": None,
                "default_cs_project": None,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user.first_name, data["first_name"])
        self.assertEqual(self.user.last_name, data["last_name"])

    def test_edit_profile_try_edit_read_only(self):
        cp = ClassificationProjectFactory.create(
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC
        )

        data = {
            "email": "test@email.com",
            # "gdpr": True,
            # "tos": True,
            "default_cs_project": cp.slug,
        }

        self.user.username = "Username"
        self.user.email = "Username"
        self.user.first_name = "First name"
        self.user.last_name = "Last name"
        self.user.userprofile.phone_number = "123456789"
        self.user.userprofile.about_me = "Me"
        self.user.userprofile.institution = "Here"
        self.user.userprofile.avatar = None
        self.user.save()

        response = self.client.patch(reverse("profile"), data=data)
        self.user.refresh_from_db()
        self.assertEqual(
            response.json(),
            {
                "about_me": "Me",
                "avatar": "",
                "first_name": self.user.first_name,
                "institution": "Here",
                "last_name": self.user.last_name,
                "phone_number": "123456789",
                "username": self.user.username,
                "email": self.user.username,
                "gdpr": False,
                "gdpr_accepted_date": None,
                "tos": False,
                "tos_accepted_date": None,
                "default_cs_project": None,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user.email, self.user.username)
        self.assertEqual(self.user.userprofile.gdpr, False)
        self.assertEqual(self.user.userprofile.tos, False)
        self.assertEqual(self.user.userprofile.default_cs_project, None)

    def test_edit_profile_username_too_long(self):
        data = {"username": "test_test_test_test_test_test_test_"}

        response = self.client.patch(reverse("profile"), data=data)
        self.user.refresh_from_db()
        self.assertEqual(
            response.json(),
            {"username": ["Ensure this field has no more than 30 characters."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_edit_profile_remove_avatar_correct(self):
        image = BytesIO()
        Image.new("RGB", (100, 100)).save(image, "JPEG")
        image.seek(0)
        self.user.userprofile.avatar = SimpleUploadedFile("image.jpg", image.getvalue())
        self.user.save()
        data = {
            "avatar": "",
        }
        response = self.client.patch(reverse("profile"), data=data)

        self.assertEqual(
            response.json(),
            {
                "username": self.user.username,
                "email": self.user.email,
                "first_name": self.user.first_name,
                "last_name": self.user.last_name,
                "phone_number": "",
                "about_me": "",
                "avatar": "",
                "institution": "",
                "gdpr": False,
                "gdpr_accepted_date": None,
                "tos": False,
                "tos_accepted_date": None,
                "default_cs_project": None,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_profile_avatar_incorrect_size(self):
        image = BytesIO()
        Image.new("RGB", (10000, 10000)).save(image, "JPEG")
        image.seek(0)
        data = {
            "username": "TestUsername",
            "first_name": "My First",
            "last_name": "Your Last",
            "phone_number": "111111111",
            "avatar": SimpleUploadedFile("image.jpg", image.getvalue()),
            "about_me": "About Me Poem",
            "institution": "Corpo",
        }
        response = self.client.patch(reverse("profile"), data=data)
        self.user.refresh_from_db()
        self.assertEqual(
            response.json(),
            {"avatar": ["File is larger than > 1.0MB."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_edit_profile_avatar_incorrect_dimensions(self):
        image = BytesIO()
        Image.new("RGB", (100, 100)).save(image, "JPEG")
        image.seek(0)
        data = {
            "username": "TestUsername",
            "first_name": "My First",
            "last_name": "Your Last",
            "phone_number": "111111111",
            "avatar": SimpleUploadedFile("image.jpg", image.getvalue()),
            "about_me": "About Me Poem",
            "institution": "Corpo",
        }
        response = self.client.patch(reverse("profile"), data=data)
        self.user.refresh_from_db()
        self.assertEqual(
            response.json(),
            {"avatar": ["Minimum dimensions are: 250 x 250 px."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_correct(self):
        data = {
            "new_password": "zaq!@#EDC",
            "new_password2": "zaq!@#EDC",
            "old_password": "P477W0rD",
        }
        self.user.set_password("P477W0rD")
        self.user.save()
        response = self.client.put(reverse("change_password"), data=data)
        self.assertEqual(response.json(), {})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_password_too_weak(self):
        data = {"new_password": "123", "new_password2": "123", "old_password": "123"}
        response = self.client.put(reverse("change_password"), data=data)
        self.assertEqual(
            response.json(),
            {
                "new_password": [
                    "This password is too short. It must contain at least 8 "
                    "characters.",
                    "This password is too common.",
                    "This password is entirely numeric.",
                ]
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_not_same(self):
        self.user.set_password("1234")
        self.user.save()
        data = {
            "new_password": "123asd#@!",
            "new_password2": "123",
            "old_password": "1234",
        }
        response = self.client.put(reverse("change_password"), data=data)
        self.assertEqual(
            response.json(),
            {"new_password": ["Password fields didn't match."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password_incorrect_old_password(self):
        data = {
            "new_password": "123asd#@!",
            "new_password2": "123asd#@!",
            "old_password": "1234",
        }
        response = self.client.put(reverse("change_password"), data=data)
        self.assertEqual(
            response.json(),
            {
                "old_password": [
                    "Your old password was entered incorrectly. Please enter it again."
                ],
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_set_default_cs_project_correct(self):
        classification_project = ClassificationProjectFactory.create(
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC
        )
        data = {"default_cs_project": classification_project.slug}
        response = self.client.put(reverse("set_default_cs_project"), data=data)
        self.assertEqual(response.json(), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.user.userprofile.default_cs_project, classification_project
        )

    def test_set_default_cs_project_project_not_cs(self):
        classification_project = ClassificationProjectFactory.create()
        data = {"default_cs_project": classification_project.slug}
        response = self.client.put(reverse("set_default_cs_project"), data=data)
        self.assertEqual(
            response.json(),
            {"default_cs_project": ["The selected project is invalid."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(self.user.userprofile.default_cs_project, None)

    def test_set_default_cs_project_incorrect_project_type(self):
        data = {"default_cs_project": "Not PK"}
        response = self.client.put(reverse("set_default_cs_project"), data=data)
        self.assertEqual(
            response.json(),
            {"default_cs_project": ["The selected project is invalid."]},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(self.user.userprofile.default_cs_project, None)
