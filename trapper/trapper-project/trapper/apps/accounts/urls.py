# -*- coding: utf-8 -*-
"""
Urls related to users management
"""
from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter
from trapper.apps.accounts.views import accounts as accounts_views
from trapper.apps.accounts.views import api as api_views
from trapper.apps.common import views_api as common_api_views

app_name = "accounts"

router = DefaultRouter(trailing_slash=False)
router.register(r"users", api_views.UserViewSet, basename="api-user")

urlpatterns = [
    re_path(r"^api/", include(router.urls)),
    re_path(
        r"^api/users/login/$", api_views.TestUserLogin.as_view(), name="api_user_data"
    ),
    re_path(r"^profile/$", accounts_views.view_mine_profile, name="mine_profile"),
    re_path(
        r"^profile/(?P<username>[\w_\.\+\-@]+)/$",
        accounts_views.view_user_profile,
        name="show_profile",
    ),
    re_path(
        r"^new-token/$",
        accounts_views.new_token_view,
        name="new_auth_token",
    ),
    re_path(r"^dashboard/$", accounts_views.view_dashbord, name="dashboard"),
    re_path(
        r"^celery/cancel/$",
        accounts_views.view_celery_task_cancel,
        name="celery_task_cancel",
    ),
    re_path(
        r"^data-package/delete/(?P<pk>\d+)/$",
        accounts_views.view_data_package_delete,
        name="data_package_delete",
    ),
    re_path(
        r"^data-package/(?P<pk>\d+)/$",
        accounts_views.view_data_package_sendfile_media,
        name="data_package_sendfile_media",
    ),
    re_path(r"^api/test/$", common_api_views.test_api_view, name="test_api_view"),
]
