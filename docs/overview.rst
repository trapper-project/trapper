==========
 Overview
==========

Trapper is an open source, `django <https://www.djangoproject.com/>`_ based web application to manage `camera trapping <https://en.wikipedia.org/wiki/Camera_trap>`_ projects. Motion-triggered camera trapping is increasingly becoming an important tool in ecological research. Because of the nature of collected data (multimedia files) even relatively small camera-trapping projects can generate large and complex datasets. The organization of these large collections of multimedia files and efficient querying for a particular subsets of data, especially in a spatio-temporal context, is often a challenging task. Without an appropriate software solution this can become a serious data management problem, leading to delays and inaccessibility of data in the long run. We propose a new approach which, in contrary to available software solutions, is a fully open-source web application using spatially enabled data that can handle arbitrary media types (both pictures and videos), supports collaborative work on a project and data sharing between system users. We used state of the art and well-recognized open-source software components and modern, general purpose programming language Python to design a flexible software framework for data management in camera trapping studies.

TRAPPER in a nuthsell
---------------------
- it is open-source
- provides a spatially-enabled database backend
- can handle both pictures & videos
- provides a flexible model of classifications
- promotes data re-use
- supports collaborative work on a project
- provides API

Demo
----
`<https://demo.trapper-project.org>`_

Read more
---------

Bubnicki, J. W., Churski, M. and Kuijper, D. P. (2016), TRAPPER: an open source web‐based application to manage camera trapping projects. Methods Ecol Evol, 7: 1209-1216. doi:10.1111/2041-210X.12571

`<https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12571>`_

For more news about TRAPPER please visit the Open Science Conservation Fund (OSCF) website:

`<https://os-conservation.org>`_
