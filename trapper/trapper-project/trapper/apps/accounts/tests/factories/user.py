import factory

from django.contrib.auth import get_user_model

User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        exclude = ("username_tmp",)

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = factory.Sequence(lambda n: f"user_{n}")
    email = factory.lazy_attribute(lambda a: a.username)
    password = factory.Faker("password")

    is_staff = False
    is_superuser = False
