from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible

phone_regex = RegexValidator(
    regex=r"^[\+?\(?\)?\- \d]{7,24}$",
    message=_(
        "Phone number format is invalid. Min. 7, max 24 digits and only `+- ()` chars are allowed."
    ),
)


@deconstructible
class ImageValidator(object):
    messages = {
        "min_dimensions": _(
            'Minimum dimensions are: %(width)s x %(height)s px.'
        ),
        "size": _(
            "File is larger than > %(size)sMB."
        )
    }

    def __init__(self, size: int = None, min_width: int = None, min_height: int = None):
        self.size = size
        self.min_width = min_width
        self.min_height = min_height

    def __call__(self, value):
        if self.size is not None and value.size > self.size:
            raise ValidationError(
                self.messages['size'],
                code='invalid_size',
                params={
                    'size': self.size/1024/1024,
                    'value': value,
                }
            )

        if (self.min_width is not None and self.min_height is not None and
                (value.image.width < self.min_width or value.image.height < self.min_height)):
            raise ValidationError(
                self.messages['min_dimensions'],
                code='invalid_dimensions',
                params={
                    'width': self.min_width,
                    'height': self.min_height,
                    'value': value,
                }
            )

    def __eq__(self, other):
        return (
                isinstance(other, self.__class__) and
                self.size == other.size and
                self.min_width == other.image.width and
                self.min_height == other.image.height
        )
