import os
import json
import uuid
from datetime import datetime, timedelta
from urllib.parse import urljoin

import requests
from azure.storage.blob import BlockBlobService, BlobPermissions, ContainerPermissions
from django.urls import reverse
from trapper.apps.accounts.models import UserRemoteTask
from trapper.apps.media_classification.ai_providers.ai_providers_base import (
    BaseAIProviderManager,
    RemoteClassificationParams,
    DetectionInfo,
)
from trapper.apps.media_classification.ai_providers.async_tasks import (
    schedule_status_update,
)
from trapper.apps.media_classification.models import (
    MegadetectorBatchProcessingProvider,
    AIClassificationJob,
    AIClassificationJobAdditionalResource,
    Classification,
    AIClassificationDynamicAttrs,
)


class MegadetectorService:
    def __init__(self, api_url: str):
        self.api_url = api_url

    def request_detections(self, request_params):
        print(f"megadetector_service::request_detections {self.api_url}")
        request_detections_url = urljoin(self.api_url, "request_detections")
        response = requests.post(
            request_detections_url,
            json=request_params,
            verify=True,
        )

        return response

    def fetch_detections(self, message):
        print(f"megadetector_service::fetch_detections {self.api_url}")
        output_file_urls = message["output_file_urls"]
        detections = output_file_urls["detections"]

        response = requests.get(detections)

        return response.json()

    def fetch_results(self, request_id: str):
        print(f"megadetector_service::fetch_results {self.api_url}")
        response = requests.get(
            url=urljoin(self.api_url, f"task/{request_id}"),
            verify=True,
        )

        return response


class MegadetectorBatchProcessingProviderManager(BaseAIProviderManager):
    config: MegadetectorBatchProcessingProvider

    def __init__(self, config: MegadetectorBatchProcessingProvider):
        super().__init__()
        self.config = config
        self.megadetector_service = MegadetectorService(self.config.api_url)

        self.stages = {
            "prepare_classification_run": self.prepare_classification_run,
            "start_classification_process": self.start_classification_process,
            "fetch_results": self.fetch_results,
            "parse_results": self.parse_results,
        }

    def request_classification(self, params: RemoteClassificationParams, user_id):
        job = self.init_job(user_id)
        self.save_remote_classification_params(params, job.id)
        job.stage = "prepare_classification_run"
        job.save()

        schedule_status_update(job.id)

    def azure_service(self):
        return BlockBlobService(
            account_name=self.config.azure_storage_account_name,
            account_key=self.config.azure_storage_account_access_key,
        )

    def prepare_resources_azure_storage(
        self, params: RemoteClassificationParams, job: AIClassificationJob
    ):
        data = {}
        classifications = Classification.objects.filter(
            pk__in=params.classification_ids
        ).select_related("resource")

        for classification in classifications:
            resource = classification.resource
            _file = getattr(resource, "file")
            if params.classify_preview_files:
                _file = getattr(resource, "file_preview")
            if _file:
                blob_name = f"trapper-{job.id}/resources/{str(uuid.uuid4())}__{os.path.basename(_file.name)}"
                with _file.open("rb") as f:
                    self.azure_service().create_blob_from_stream(
                        self.config.azure_storage_container,
                        blob_name=blob_name,
                        stream=f,
                    )
                data[classification.pk] = blob_name

        images_requested = [
            [resource_url, json.dumps({"object_id": object_id})]
            for object_id, resource_url in data.items()
        ]

        return images_requested

    def prepare_resources_public_links(self, params: RemoteClassificationParams):
        data = {}
        classifications = Classification.objects.filter(
            pk__in=params.classification_ids
        ).select_related("resource")
        for classification in classifications:
            r = classification.resource
            field = "file"
            if params.classify_preview_files:
                field = "pfile"
            _file = getattr(r, field)
            url = classification.resource.get_url_with_filename(
                field=field,
                extra_file_name=f"{str(uuid.uuid4())}__{os.path.basename(_file.name)}",
            )
            data[classification.pk] = self.get_absolute_url(url)

        images_requested = [
            [resource_url, json.dumps({"object_id": object_id})]
            for object_id, resource_url in data.items()
        ]

        return images_requested

    def store_resource_index_in_azure_storage(
        self, images_requested, job: AIClassificationJob
    ):
        data = json.dumps(images_requested, indent=4)
        blob_name = f"trapper-{job.id}/index-{str(uuid.uuid4())}.json"
        self.azure_service().create_blob_from_text(
            container_name=self.config.azure_storage_container,
            blob_name=blob_name,
            text=data,
        )

        sas_token = self.azure_service().generate_blob_shared_access_signature(
            container_name=self.config.azure_storage_container,
            blob_name=blob_name,
            expiry=(datetime.utcnow() + timedelta(days=1)).isoformat(),
            permission=BlobPermissions(read=True),
        )

        return self.azure_service().make_blob_url(
            container_name=self.config.azure_storage_container,
            blob_name=blob_name,
            sas_token=sas_token,
        )

    def store_resource_index_in_db(self, images_requested, job: AIClassificationJob):
        images_requested_resource = AIClassificationJobAdditionalResource(
            resource_name="images_requested",
            ai_provider=self.config,
            classification_job=job,
            data=images_requested,
        )
        images_requested_resource.save()

        images_requested_url = self.get_absolute_url(
            reverse(
                "media_classification:api-classify-ai-extra",
                kwargs={
                    "ai_provider_token": self.config.token,
                    "classification_job_id": job.id,
                    "resource_name": "images_requested",
                },
            ),
        )

        return images_requested_url

    def get_container_sas(self):
        sas_token = self.azure_service().generate_container_shared_access_signature(
            self.config.azure_storage_container,
            expiry=(datetime.utcnow() + timedelta(days=1)).isoformat(),
            permission=ContainerPermissions(list=True, read=True),
        )

        return self.azure_service().make_container_url(
            container_name=self.config.azure_storage_container, sas_token=sas_token
        )

    def prepare_classification_run(self, job: AIClassificationJob):
        params = self.load_remote_classification_params(job.id)

        if self.config.use_azure_storage_for_images:
            images_requested = self.prepare_resources_azure_storage(params, job)
        else:
            images_requested = self.prepare_resources_public_links(params)

        if self.config.use_azure_storage_for_image_list:
            images_requested_json_sas = self.store_resource_index_in_azure_storage(
                images_requested, job
            )
        else:
            images_requested_json_sas = self.store_resource_index_in_db(
                images_requested, job
            )

        request_params = {
            "images_requested_json_sas": images_requested_json_sas,
            "request_name": f"trapper-{job.id}",
            "caller": self.config.caller,
            "country": "PL",
            "organization_name": "",
            "use_url": not self.config.use_azure_storage_for_images,
        }

        if self.config.use_azure_storage_for_images:
            request_params["input_container_sas"] = self.get_container_sas()

        if self.config.model_version:
            request_params["model_version"] = self.config.model_version

        print(request_params)
        AIClassificationJobAdditionalResource(
            resource_name="request_params",
            ai_provider=self.config,
            classification_job=job,
            data=request_params,
        ).save()

        job.stage = "start_classification_process"
        job.save()

        schedule_status_update(job.id, 2)

    def start_classification_process(self, job: AIClassificationJob):
        print(f"megadetector::start_classification_process {job.id}")
        remote_task: UserRemoteTask = job.user_remote_task
        request_params_resource = AIClassificationJobAdditionalResource.objects.get(
            classification_job=job,
            resource_name="request_params",
        )
        request_params = request_params_resource.data

        response = self.megadetector_service.request_detections(request_params)

        print(response.text, response.status_code)
        if response.status_code == 503:
            print("Queue if full, retrying in 30 seconds")
            schedule_status_update(job.id, 30)
        elif not response.ok:
            try:
                msg = response.json()["error"]
            except (KeyError, ValueError):
                msg = str(response.status_code)
            remote_task.status = "REJECTED"
            remote_task.log = "Response status_code: {code}. {msg}".format(
                code=response.status_code, msg=msg
            )
            remote_task.save()
            self.set_stage(job, "rejected")
        else:
            megadetector_job_data = AIClassificationJobAdditionalResource(
                resource_name="megadetector_job_data",
                ai_provider=self.config,
                classification_job=job,
                data=response.json(),
            )
            megadetector_job_data.save()
            print(megadetector_job_data.data)
            job.stage = "fetch_results"
            job.save()
            schedule_status_update(job.id, 10)

    def fetch_results(self, job: AIClassificationJob):
        megadetector_job_data_resource = (
            AIClassificationJobAdditionalResource.objects.get(
                resource_name="megadetector_job_data",
                classification_job=job,
            )
        )
        megadetector_job_data = megadetector_job_data_resource.data
        request_id = megadetector_job_data["request_id"]
        response = self.megadetector_service.fetch_results(request_id)
        print(response.json(), response.status_code)
        resp = response.json()
        status = resp["Status"]
        request_status = status["request_status"]
        if request_status == "running":
            print(
                "Classification is still running, rescheduling next check in 30 seconds"
            )
            schedule_status_update(job.id, 30)
        elif request_status == "completed":
            message = status["message"]
            if isinstance(message, dict):
                detections = self.megadetector_service.fetch_detections(message)
                print(detections)

                (
                    megadetector_results_resource,
                    _,
                ) = AIClassificationJobAdditionalResource.objects.get_or_create(
                    resource_name="megadetector_results",
                    ai_provider=self.config,
                    classification_job=job,
                )
                megadetector_results_resource.data = detections
                megadetector_results_resource.save()

                self.set_stage(job, "parse_results")

                schedule_status_update(job.id)
            else:
                self.fail_job(job, f"Classification failed: {message}")
        else:
            print(f"Request status: {request_status}")

    def parse_results(self, job: AIClassificationJob):
        megadetector_results_resource = (
            AIClassificationJobAdditionalResource.objects.get(
                resource_name="megadetector_results",
                classification_job=job,
            )
        )
        megadetector_results = megadetector_results_resource.data

        detection_categories = megadetector_results["detection_categories"]
        images = megadetector_results["images"]

        owner = job.user_remote_task.user

        print(megadetector_results)

        def bbox_megadetector_to_trapper(bounding_box):
            x, y, w, h = bounding_box
            return [x, y, w, h]

        ai_classifications = []

        detection_category_mapping = {
            ot.value: ot.observation_type
            for ot in self.config.observation_type_mappings.all()
        }
        print("Detection category mapping", detection_category_mapping)

        all_detected_objects = {}
        for image_classification_data in images:
            metadata = json.loads(image_classification_data["meta"])
            classification_id = metadata["object_id"]

            detected_objects = []

            for detection in image_classification_data["detections"]:
                category = detection["category"]
                category_name = detection_categories[category]
                observation_type = detection_category_mapping.get(category_name)

                if not observation_type:
                    self.fail_job(
                        job,
                        f"Detection category label mapping from '{category_name}' not found. "
                        f"Check your MegaDetector provider config.",
                    )
                    return

                confidence = detection["conf"]
                # confidence = round(confidence, 2)
                if confidence < self.config.minimum_confidence:
                    continue
                bounding_box = detection["bbox"]
                bounding_box = bbox_megadetector_to_trapper(bounding_box)
                detection_info = DetectionInfo(
                    observation_type=observation_type,
                    confidence=confidence,
                    bounding_box=bounding_box,
                )

                detected_objects.append(detection_info)

            ai_classification_object = self.prepare_ai_classification_object(
                classification_id, owner
            )
            ai_classifications.append(ai_classification_object)
            all_detected_objects[classification_id] = detected_objects

        ai_classifications = self.create_ai_classification_objects(ai_classifications)

        ai_classification_dynamic_objects = []
        for ai_classification in ai_classifications:
            ai_classification_dynamic_objects += (
                self.prepare_ai_classification_dynamic_objects(
                    ai_classification.pk,
                    all_detected_objects[ai_classification.classification_id],
                    empty_score=self.config.minimum_confidence,
                )
            )

        AIClassificationDynamicAttrs.objects.bulk_create(
            ai_classification_dynamic_objects
        )

        job.stage = "finished"
        job.save()
