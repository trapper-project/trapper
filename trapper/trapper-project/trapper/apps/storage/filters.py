# -*- coding: utf-8 -*-
"""Filters used in storage application when backend is used to
limit data"""
from functools import reduce
from operator import or_

import django_filters

from django.contrib.auth import get_user_model
from django.contrib.postgres.search import SearchQuery
from django.db.models import Q, ExpressionWrapper, F, BooleanField

from trapper.apps.common.filters import (
    BaseFilterSet,
    BaseOwnBooleanFilter,
    BaseDateFilter,
    BaseTimeFilter,
    BaseLocationsMapFilter,
)
from trapper.apps.research.models import ResearchProject
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.storage.taxonomy import ResourceType, ResourceStatus, CollectionStatus

User = get_user_model()


class OwnResourceBooleanFilter(BaseOwnBooleanFilter):
    """Filter for owned :class:`apps.storage.models.Resource` model"""

    status_class = ResourceStatus


class OwnCollectionBooleanFilter(BaseOwnBooleanFilter):
    """Filter for owned :class:`apps.storage.models.Collection` model"""

    status_class = CollectionStatus


# Filtersets
class ResourceFilter(BaseFilterSet):
    """Filter used in resource list view"""

    resource_type = django_filters.ChoiceFilter(
        choices=ResourceType.get_all_choices(), label="Type"
    )
    status = django_filters.ChoiceFilter(choices=ResourceStatus.get_all_choices())
    rdate_from = BaseDateFilter(
        field_name="date_recorded__date",
        lookup_expr=("gte"),
    )
    rdate_to = BaseDateFilter(
        field_name="date_recorded__date",
        lookup_expr=("lte"),
    )
    udate_from = BaseDateFilter(
        field_name="date_uploaded__date",
        lookup_expr=("gte"),
    )
    udate_to = BaseDateFilter(
        field_name="date_uploaded__date",
        lookup_expr=("lte"),
    )
    rtime_from = BaseTimeFilter(
        field_name="date_recorded", lookup_expr="gte", time_format="%H:%M"
    )
    rtime_to = BaseTimeFilter(
        field_name="date_recorded", lookup_expr="lte", time_format="%H:%M"
    )
    owner = OwnResourceBooleanFilter(label="My Resources")
    locations_map = BaseLocationsMapFilter(field_name="deployment__location")
    collections = django_filters.MultipleChoiceFilter(
        field_name="collection", distinct=False
    )
    deployments = django_filters.CharFilter(method="get_deployments")
    deployment__isnull = django_filters.BooleanFilter()

    tags = django_filters.MultipleChoiceFilter(
        choices=Resource.tags.values_list("pk", "name")
    )
    observation_type = django_filters.CharFilter(method="get_observation_type")
    species = django_filters.CharFilter(method="get_species")
    timestamp_error = django_filters.BooleanFilter(
        label="Timestamp error", method="get_timestamp_error"
    )

    class Meta:
        model = Resource
        exclude = [
            "extras_mime_type",
            "date_uploaded",
            "date_recorded",
            "file",
            "extra_file",
            "file_thumbnail",
            "file_preview",
            "data",
            "search_data_vector",
            "search_name_vector",
        ]

    def __init__(self, *args, **kwargs):
        super(ResourceFilter, self).__init__(*args, **kwargs)
        # breakpoint()
        if self.data.get("collections", None):
            self.filters["collections"].field.choices = Collection.objects.values_list(
                "pk", "name"
            )

    def get_deployments(self, qs, name, value):
        values = [
            int(k) for k in self.request.GET.getlist("deployments") if k and k.isdigit()
        ]
        if not values:
            return qs
        query = reduce(or_, (Q(deployment_id=v) for v in values))
        return qs.filter(query)

    def get_observation_type(self, qs, name, value):
        if not value:
            return qs
        return qs.filter(
            search_data_vector=SearchQuery(
                f"observation_type:{value}", search_type="phrase"
            )
        )

    def get_species(self, qs, name, value):
        values = [
            int(k) for k in self.request.GET.getlist("species") if k and k.isdigit()
        ]
        if not values:
            return qs
        query = reduce(
            or_,
            (
                Q(
                    search_data_vector=SearchQuery(
                        f"species_id:{v}", search_type="phrase"
                    )
                )
                for v in values
            ),
        )
        return qs.filter(query)

    def get_timestamp_error(self, qs, name, value):
        qs = qs.annotate(
            timestamp_error=ExpressionWrapper(
                Q(date_recorded__lt=F("deployment__start_date"))
                | Q(date_recorded__gt=F("deployment__end_date")),
                output_field=BooleanField(),
            )
        )
        return qs.filter(timestamp_error=value)


class CollectionFilter(BaseFilterSet):
    """Filter used in collection list view"""

    status = django_filters.Filter()
    owner = OwnCollectionBooleanFilter(label="My Collections")
    research_projects = django_filters.MultipleChoiceFilter(
        field_name="research_projects",
        choices=ResearchProject.objects.values_list("pk", "name"),
    )
    owners = django_filters.MultipleChoiceFilter(
        field_name="owner", choices=User.objects.values_list("pk", "username")
    )
    locations_map = BaseLocationsMapFilter(field_name="resources__deployment__location")

    class Meta:
        model = Collection
        exclude = ["date_created", "description", "bbox", "resources"]
