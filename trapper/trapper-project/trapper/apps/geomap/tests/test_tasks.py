import datetime
from unittest.mock import Mock, patch

import frictionless
import pandas as pd
import pytz
from django.test import TestCase

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.models import Deployment, Location
from trapper.apps.geomap.tasks import (
    LocationImporter,
    DeploymentImporter,
    celery_import_locations,
)
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)


class GeomapTasksTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(GeomapTasksTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)

        date_now = datetime.datetime.now()
        data = Mock()
        data.columns = ["locationID", "longitude", "latitude"]
        waypoint = Mock()
        waypoint.name = "TestName"
        waypoint.longitude = 9.123213
        waypoint.latitude = 2.123124
        data.waypoints = [waypoint]
        cls.location_importer1 = LocationImporter(
            cls.user1, cls.research_project, pytz.UTC, gpx_data=data
        )
        cls.location_importer2 = LocationImporter(
            cls.user1, cls.research_project, pytz.UTC, gpx_data=data
        )
        frame_data = [
            ["123", 9.123213, 2.123124],
            ["234", 8.564654, 4.654456],
        ]
        frame_columns = [
            "locationID",
            "longitude",
            "latitude",
        ]
        data_frame_location = pd.DataFrame(frame_data, columns=frame_columns)
        cls.data_frame_location = data_frame_location

        cls.location_importer3 = LocationImporter(
            cls.user1,
            cls.research_project,
            pytz.UTC,
            csv_data=data_frame_location,
        )

        cls.location1 = LocationFactory(
            owner=cls.user1, timezone=pytz.UTC, research_project=cls.research_project
        )
        cls.location2 = LocationFactory(
            owner=cls.user1, timezone=pytz.UTC, research_project=cls.research_project
        )
        frame_data = [
            [
                "123",
                cls.location1.location_id,
                9.123213,
                2.123124,
                (date_now - datetime.timedelta(minutes=10)).strftime(
                    "%Y-%m-%dT%H:%M:%S"
                ),
                date_now.strftime("%Y-%m-%dT%H:%M:%S"),
                123,
            ],
            [
                "234",
                cls.location2.location_id,
                8.564654,
                4.654456,
                (date_now - datetime.timedelta(minutes=60)).strftime(
                    "%Y-%m-%dT%H:%M:%S"
                ),
                date_now.strftime("%Y-%m-%dT%H:%M:%S"),
                123,
            ],
        ]
        frame_data_new_location = [
            [
                "789",
                "new_location",
                9.123218,
                2.123128,
                (date_now - datetime.timedelta(minutes=10)).strftime(
                    "%Y-%m-%dT%H:%M:%S"
                ),
                date_now.strftime("%Y-%m-%dT%H:%M:%S"),
            ]
        ]
        frame_columns = [
            "deploymentID",
            "locationID",
            "longitude",
            "latitude",
            "deploymentStart",
            "deploymentEnd",
            "_id",
        ]
        data_frame_deployments = pd.DataFrame(frame_data, columns=frame_columns)
        cls.data_frame_deployments_new_location = pd.DataFrame(
            frame_data_new_location, columns=frame_columns[:-1]
        )
        cls.data_frame_deployments = data_frame_deployments

        cls.deployment_importer1 = DeploymentImporter(
            cls.user1, cls.research_project, pytz.UTC, data=data_frame_deployments
        )
        cls.deployment_importer2 = DeploymentImporter(
            cls.user1,
            cls.research_project,
            pytz.UTC,
            data=data_frame_deployments,
            update=True,
        )

    def test_location_importer(self):
        LocationImporter(
            self.user1,
            self.research_project,
            pytz.UTC,
            csv_data=self.data_frame_location,
        )

    def test_location_importer_get_schema(self):
        self.location_importer1.get_schema()
        self.location_importer2.get_schema()

    @patch("trapper.apps.geomap.tasks.frictionless")
    def test_location_importer_validate_table(self, mock_frictionless):
        mock_frictionless.validate = Mock(
            return_value=frictionless.Report(valid=True, stats=None)
        )
        self.location_importer1.validate_table()
        self.location_importer2.validate_table()
        mock_frictionless.validate.return_value = frictionless.Report(
            valid=True, stats=None
        )
        self.location_importer2.validate_table()

    def test_location_importer_import_locations(self):
        self.location_importer1.import_locations()
        self.location_importer3.import_locations()

    def test_celery_import_locations(self):
        celery_import_locations(
            user=self.user1,
            research_project=self.research_project,
            timezone=pytz.UTC,
            csv_data=self.data_frame_location,
        )

    def test_deployment_importer(self):
        DeploymentImporter(
            self.user1,
            self.research_project,
            pytz.UTC,
            data=self.data_frame_deployments,
        )

    def test_deployment_importer_get_schema(self):
        self.deployment_importer1.get_schema()
        self.deployment_importer2.get_schema()

    def test_deployment_importer_validate_table(self):
        self.deployment_importer1.validate_table()

        self.deployment_importer2.validate_table()

    @patch("trapper.apps.geomap.tasks.dict")
    def test_deployment_importer_import_locations(self, mock_dict):
        mock_dict.return_value = {
            self.location1.location_id: self.location1.pk,
            self.location2.location_id: self.location2.pk,
        }
        self.deployment_importer1.import_deployments()

    def test_import_deployments_create_locations(self):
        self.assertFalse(Deployment.objects.filter(deployment_id="123").exists())
        self.assertFalse(Location.objects.filter(location_id="new_location").exists())

        deployment_importer3 = DeploymentImporter(
            self.user1,
            self.research_project,
            pytz.UTC,
            data=self.data_frame_deployments_new_location,
            create_locations=True,
        )
        deployment_importer3.validate_table()
        self.assertTrue(deployment_importer3.report.valid)

        deployment_importer3.import_deployments()
        deployment_qs = Deployment.objects.filter(deployment_id="789")
        location_qs = Location.objects.filter(location_id="new_location")
        self.assertTrue(deployment_qs.exists())
        self.assertTrue(location_qs.exists())
        self.assertEqual(deployment_qs.first().location, location_qs.first())

    def test_location_tz_validation_in_deployment_import(self):
        tz = pytz.timezone("America/Los_Angeles")
        date_now = datetime.datetime.now()

        # locations in self.data_frame_deployments are in UTC - exception should be raised
        # when trying to import using another timezone
        with self.assertRaises(ValueError):
            DeploymentImporter(
                self.user1, self.research_project, tz, data=self.data_frame_deployments
            )

        location3 = LocationFactory(owner=self.user1, timezone=tz)
        new_deployment_row = {
            "deploymentID": "345",
            "locationID": location3.location_id,
            "longitude": 9.87654,
            "latitude": 7.65432,
            "deploymentStart": (date_now - datetime.timedelta(minutes=60)).strftime(
                "%Y-%m-%dT%H:%M:%S"
            ),
            "deploymentEnd": date_now.strftime("%Y-%m-%dT%H:%M:%S"),
            "_id": 123,
        }
        df_deployments = self.data_frame_deployments
        df_deployments.loc[2] = new_deployment_row

        # exception should also be raised if provided data references locations from
        # more that one timezone
        with self.assertRaises(ValueError):
            DeploymentImporter(
                self.user1, self.research_project, pytz.UTC, data=df_deployments
            )


class DeploymentImportTimezoneTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Class of tests to check that deployments' start and end dates are converted, saved and serialized correctly,
        using all possible values for ignore_DST flag in summer and winter.
        """
        super().setUpTestData()
        cls.user1 = UserFactory()
        cls.research_project = ResearchProjectFactory(owner=cls.user1)

        cls.timezone = pytz.timezone("America/Los_Angeles")
        # offset for the given tz is - 8 hours, -7 in dst
        start_date_str = "2022-07-01T10:00:00"
        end_date_str = "2022-07-20T18:00:00"

        cls.deployment_data = {
            "deploymentID": ["dep1"],
            "locationID": ["loc1"],
            "longitude": [1.23456],
            "latitude": [9.87654],
            "deploymentStart": [start_date_str],
            "deploymentEnd": [end_date_str],
        }

    def test_deployment_dates_saving_summer_dst(self):
        """
        test behaviour observing DST, with summer dates
        """
        deployments_df = pd.DataFrame.from_dict(self.deployment_data)
        importer = DeploymentImporter(
            self.user1,
            self.research_project,
            self.timezone,
            data=deployments_df,
            create_locations=True,
            ignore_DST=False,
        )
        importer.import_deployments()
        dep1 = Deployment.objects.get(deployment_id="dep1")
        # assert timezone info was saved correctly
        self.assertEqual(dep1.timezone, self.timezone)
        self.assertFalse(dep1.ignore_DST)

        # assert that deployment's start and end dates were correctly converted to UTC before saving
        self.assertEqual(dep1.start_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.start_date.hour, 17)
        self.assertEqual(dep1.start_date.day, 1)
        self.assertEqual(dep1.end_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.end_date.hour, 1)
        self.assertEqual(dep1.end_date.day, 21)

        # assert that the dates serialize with correct offset
        self.assertEqual(
            dep1.start_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-07-01 10:00 -0700"
        )
        self.assertEqual(
            dep1.end_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-07-20 18:00 -0700"
        )

    def test_deployment_dates_saving_winter_dst(self):
        """
        test behaviour observing DST, with winter dates
        """
        data = self.deployment_data
        data["deploymentStart"] = ["2022-01-01T10:00:00"]
        data["deploymentEnd"] = ["2022-01-20T18:00:00"]
        deployments_df = pd.DataFrame.from_dict(data)

        importer = DeploymentImporter(
            self.user1,
            self.research_project,
            self.timezone,
            data=deployments_df,
            create_locations=True,
            ignore_DST=False,
        )
        importer.import_deployments()
        dep1 = Deployment.objects.get(deployment_id="dep1")
        # assert timezone info was saved correctly
        self.assertEqual(dep1.timezone, self.timezone)
        self.assertFalse(dep1.ignore_DST)

        # assert that deployment's start and end dates were correctly converted to UTC before saving
        self.assertEqual(dep1.start_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.start_date.hour, 18)
        self.assertEqual(dep1.start_date.day, 1)
        self.assertEqual(dep1.end_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.end_date.hour, 2)
        self.assertEqual(dep1.end_date.day, 21)

        # assert that the dates serialize with correct offset
        self.assertEqual(
            dep1.start_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-01-01 10:00 -0800"
        )
        self.assertEqual(
            dep1.end_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-01-20 18:00 -0800"
        )

    def test_deployment_dates_saving_summer_ignore_dst(self):
        """
        test behaviour ignoring DST, with summer dates
        """
        deployments_df = pd.DataFrame.from_dict(self.deployment_data)
        importer = DeploymentImporter(
            self.user1,
            self.research_project,
            self.timezone,
            data=deployments_df,
            create_locations=True,
            ignore_DST=True,
        )
        importer.import_deployments()
        dep1 = Deployment.objects.get(deployment_id="dep1")
        # assert timezone info was saved correctly
        self.assertEqual(dep1.timezone, self.timezone)
        self.assertTrue(dep1.ignore_DST)

        # assert that deployment's start and end dates were correctly converted to UTC before saving
        self.assertEqual(dep1.start_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.start_date.hour, 18)
        self.assertEqual(dep1.start_date.day, 1)
        self.assertEqual(dep1.end_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.end_date.hour, 2)
        self.assertEqual(dep1.end_date.day, 21)

        # assert that the dates serialize with correct offset
        self.assertEqual(
            dep1.start_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-07-01 10:00 -0800"
        )
        self.assertEqual(
            dep1.end_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-07-20 18:00 -0800"
        )

    def test_deployment_dates_saving_winter_ignore_dst(self):
        """
        test behaviour ignoring DST, with winter dates
        """
        data = self.deployment_data
        data["deploymentStart"] = ["2022-01-01T10:00:00"]
        data["deploymentEnd"] = ["2022-01-20T18:00:00"]
        deployments_df = pd.DataFrame.from_dict(data)

        importer = DeploymentImporter(
            self.user1,
            self.research_project,
            self.timezone,
            data=deployments_df,
            create_locations=True,
            ignore_DST=True,
        )
        importer.import_deployments()
        dep1 = Deployment.objects.get(deployment_id="dep1")
        # assert timezone info was saved correctly
        self.assertEqual(dep1.timezone, self.timezone)
        self.assertTrue(dep1.ignore_DST)

        # assert that deployment's start and end dates were correctly converted to UTC before saving
        self.assertEqual(dep1.start_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.start_date.hour, 18)
        self.assertEqual(dep1.start_date.day, 1)
        self.assertEqual(dep1.end_date.tzinfo, datetime.timezone.utc)
        self.assertEqual(dep1.end_date.hour, 2)
        self.assertEqual(dep1.end_date.day, 21)

        # assert that the dates serialize with correct offset
        self.assertEqual(
            dep1.start_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-01-01 10:00 -0800"
        )
        self.assertEqual(
            dep1.end_date_tz.strftime("%Y-%m-%d %H:%M %z"), "2022-01-20 18:00 -0800"
        )
