from django.utils.translation import gettext_lazy as _

from trapper.apps.common.taxonomy import BaseTaxonomy


class TaxonRank(BaseTaxonomy):
    CLASS = "class"
    ORDER = "order"
    FAMILY = "family"
    GENUS = "genus"
    SPECIES = "species"
    SUBSPECIES = "subspecies"

    CHOICES = (
        (CLASS, _("Class")),
        (ORDER, _("Order")),
        (FAMILY, _("Family")),
        (GENUS, _("Genus")),
        (SPECIES, _("Species")),
        (SUBSPECIES, _("Subspecies")),
    )


class TaxonomyClasses(BaseTaxonomy):
    CLASSES = [
        "Actinopterygii",
        "Amphibia",
        "Aves",
        "Cephalaspidomorphi",
        "Leptocardii",
        "Elasmobranchii",
        "Holocephali",
        "Mammalia",
        "Myxini",
        "Reptilia",
        "Sarcopterygii",
        "Appendicularia",
        "Ascidiacea",
        "Thaliacea",
    ]


class LicenceScope(BaseTaxonomy):
    DATA = "data"
    MEDIA = "media"

    CHOICES = [
        (DATA, _("Data")),
        (MEDIA, _("Media")),
    ]
