from django.contrib import admin

from trapper.apps.accounts.models import UserProfile
from trapper.apps.common.models import Consent


@admin.register(Consent)
class ConsentAdmin(admin.ModelAdmin):
    list_display = ("project", "type", "language")
    actions = ["clear_gdpr", "clear_tos", "clear_both_consents"]

    @admin.action(description="Clear user gdpr consents.")
    def clear_gdpr(self, request, queryset):
        UserProfile.objects.all().update(gdpr=False, gdpr_accepted_date=None)

    @admin.action(description="Clear user tos consents.")
    def clear_tos(self, request, queryset):
        UserProfile.objects.all().update(tos=False, tos_accepted_date=None)

    @admin.action(description="Clear both (gdpr and tos) user consents.")
    def clear_both_consents(self, request, queryset):
        UserProfile.objects.all().update(
            gdpr=False, gdpr_accepted_date=None, tos=False, tos_accepted_date=None
        )
