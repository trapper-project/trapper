from django.urls import reverse
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.common.utils.test_tools import ExtendedTestCase
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)


class GeomapApiTest(ExtendedTestCase):
    @classmethod
    def setUpTestData(cls):
        super(GeomapApiTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)

        cls.location1 = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )

        cls.deployment1 = DeploymentFactory(
            owner=cls.user1,
            research_project=cls.research_project,
            location=cls.location1,
        )

    def test_api_location_export(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:api-location-export"))
        self.assertEqual(response.status_code, 200)

    def test_api_deployment_export(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:api-deployment-export"))
        self.assertEqual(response.status_code, 200)

    def test_api_location_geojson(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:api-location-geojson"))
        self.assertEqual(response.status_code, 200)
