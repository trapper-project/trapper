# -*- coding: utf-8 -*-
"""
Module that holds logic for generting thumbnails from either images
or videos.

For videos by default thumbnail is generated from the first second.
Thumbnailer construction supports using it as celery task.
"""

import os
from io import BytesIO as StringIO

import ffmpeg
from PIL import Image
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from trapper.apps.storage.taxonomy import ResourceType

__all__ = ["Thumbnailer", "ThumbnailerException"]


class ThumbnailerException(Exception):
    """Default exception used by thumbnailer that could be
    easily catch at other modules"""

    pass


class Thumbnailer(object):
    """Thumbnailer class for converting images/videos"""

    def __init__(self, resource, raw_data=None, commit=True):
        """Thumbnailer requires :class:`storage.Resource` instance

        @:param resource: :class:`storage.Resource` model instance
        @:param raw_data: file-like object
        """
        self.resource = resource
        self.raw_data = raw_data
        self.commit = commit

    def create(self):
        """Detect what processor should be used to generate thumbnails and
        run it. If there is no processor that could handle resource,
        throw exception"""
        resource_processor = self.processor
        if callable(resource_processor):
            resource_processor()
        else:
            raise ThumbnailerException(u"This resource type is not handled")

    def prepare_thumbnail(self, raw_image, extension, commit=True):
        """Convert raw thumbnail image into :class:`models.ImageField`
        For that process image is converted using PIL and written into StringIO

        @:param raw_image - image file content
        @:param extension - extension of file
        """
        try:
            buff = StringIO(raw_image)
            img = Image.open(buff)
            img.thumbnail(settings.DEFAULT_THUMBNAIL_SIZE, Image.Resampling.LANCZOS)
            temp_handle = StringIO()
            img.save(temp_handle, extension, quality=60)
            img.close()
            buff.close()
        except IOError:
            return 1

        temp_handle.seek(0)

        suf = SimpleUploadedFile(
            os.path.split(self.resource.file.name)[-1],
            temp_handle.read(),
            content_type=extension,
        )
        temp_handle.close()
        self.resource.file_thumbnail_size = suf.size
        self.resource.file_thumbnail.save(
            "{path}_thumbnail.{ext}".format(
                path=os.path.splitext(suf.name)[0], ext=extension
            ),
            suf,
            save=commit,
        )
        self.resource.file_thumbnail.close()
        suf.close()

    def prepare_preview(self, raw_image, extension, commit=True):
        """Convert raw thumbnail image into :class:`models.ImageField`
        For that process image is converted using PIL and written into StringIO

        @:param raw_image - image file content
        @:param extension - extension of file
        """
        try:
            buff = StringIO(raw_image)
            img = Image.open(buff)
            img.thumbnail(settings.DEFAULT_PREVIEW_SIZE, Image.Resampling.LANCZOS)
            temp_handle = StringIO()
            img.save(temp_handle, extension, quality=60)
            img.close()
            buff.close()
        except IOError:
            return 1
        temp_handle.seek(0)
        suf = SimpleUploadedFile(
            os.path.split(self.resource.file.name)[-1],
            temp_handle.read(),
            content_type=extension,
        )
        temp_handle.close()
        self.resource.file_preview_size = suf.size
        self.resource.file_preview.save(
            "{path}_preview.{ext}".format(
                path=os.path.splitext(suf.name)[0], ext=extension
            ),
            suf,
            save=commit,
        )
        self.resource.file_preview.close()
        suf.close()

    def process_image(self):
        """Processor used to prepare thumbnail from images."""
        mime_type = self.resource.mime_type
        extension = mime_type.split("/")[-1]
        if not self.raw_data:
            try:
                self.resource.file.seek(0)
            except FileNotFoundError as e:
                raise ThumbnailerException(e)
        raw_image = self.raw_data or self.resource.file.read()
        self.prepare_thumbnail(raw_image=raw_image, extension=extension, commit=False)
        self.prepare_preview(
            raw_image=raw_image, extension=extension, commit=self.commit
        )
        if not self.raw_data:
            self.resource.file.close()

    def process_video(self):
        """Processor used to prepare thumbnail from videos.
        This processor uses ffmpeg binaries
        """
        if not settings.VIDEO_THUMBNAIL_ENABLED:
            return

        if not self.raw_data:
            stdin_data = None
            try:
                input_cmd = self.resource.file.path
            except NotImplementedError:
                # use .url instead of .path to support remote storages
                input_cmd = self.resource.file.url
        else:
            stdin_data = self.raw_data
            input_cmd = "pipe:"

        try:
            raw_image, _ = (
                ffmpeg.input(input_cmd)
                .filter("select", "gte(n,{})".format(1))
                .output("pipe:", vframes=1, format="image2", vcodec="mjpeg")
                .run(capture_stdout=True, input=stdin_data)
            )
        except ffmpeg.Error as e:
            raise ThumbnailerException(e)

        extension = "jpeg"
        self.prepare_thumbnail(
            raw_image=raw_image, extension=extension, commit=self.commit
        )

    @property
    def processor(self):
        """Property that return processor that should be used to work with
        given resource"""
        processor_map = {
            ResourceType.TYPE_IMAGE: self.process_image,
            ResourceType.TYPE_VIDEO: self.process_video,
        }
        return processor_map.get(self.resource.resource_type, None)
