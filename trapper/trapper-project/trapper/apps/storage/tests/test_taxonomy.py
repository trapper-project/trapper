from unittest.mock import patch, Mock, MagicMock

from django.test import TestCase
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.storage.taxonomy import CollectionManagers


class StorageTaxonomyTest(TestCase):
    """Tests related to user django admin page"""

    @classmethod
    def setUpTestData(cls):
        super(StorageTaxonomyTest, cls).setUpTestData()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

    @patch("trapper.apps.storage.taxonomy.get_current_user")
    def test_collection_managers(self, mock_get_user):
        mock_get_user.return_value = self.user1
        collection_managers = CollectionManagers()

        collection_managers.get_all_choices()

        mock_base_choices = Mock(return_value=True)
        mock_base_choices.exclude = Mock(return_value=mock_base_choices)
        mock_base_choices.values_list = MagicMock(return_value=["element1", "element2"])

        mock_user = Mock(return_value=True)
        mock_user.pk = 1
        mock_user.is_authenticated = False
        mock_get_user.return_value = mock_user
        collection_managers.get_all_choices(base_choices=mock_base_choices)

        mock_user = Mock(return_value=True)
        mock_user.pk = 1
        mock_user.is_authenticated = True
        mock_get_user.return_value = mock_user
        collection_managers.get_all_choices(base_choices=mock_base_choices)
