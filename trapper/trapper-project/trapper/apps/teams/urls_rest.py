from django.conf.urls import include
from django.urls import path

from .views_rest import (
    TeamsListingView,
    TeamsForSharingView,
    ShareDeploymentsView,
    TeamCreateView,
    TeamManageView,
    TeamLeaderInviteMemberView,
    TeamLeaderSearchNewMemberView,
    TeamMemberView,
    MemberApproveInvitationView,
    MemberRejectInvitationView,
    MemberLeaveTeamView,
)

teams_urlpatterns = [
    path("", TeamsListingView.as_view(), name="team_listing"),
    path(
        "available_for_sharing/",
        TeamsForSharingView.as_view(),
        name="teams_available_for_sharing",
    ),
    path("create/", TeamCreateView.as_view(), name="team_create"),
    path("<int:team_id>/", TeamManageView.as_view(), name="team_manage"),
    path(
        "<int:team_id>/members/search/",
        TeamLeaderSearchNewMemberView.as_view(),
        name="search_new_member",
    ),
    path(
        "<int:team_id>/members/invite/",
        TeamLeaderInviteMemberView.as_view(),
        name="team_leader_invite_member",
    ),
    path(
        "<int:team_id>/members/approve/",
        MemberApproveInvitationView.as_view(),
        name="member_approve_invitation",
    ),
    path(
        "<int:team_id>/members/reject/",
        MemberRejectInvitationView.as_view(),
        name="member_reject_invitation",
    ),
    path(
        "<int:team_id>/members/leave/",
        MemberLeaveTeamView.as_view(),
        name="team_leader_delete_member",
    ),
    path(
        "<int:team_id>/members/<int:team_membership_id>/",
        TeamMemberView.as_view(),
        name="team_member",
    ),
    path(
        "<int:team_id>/share_deployments/",
        ShareDeploymentsView.as_view(),
        name="share_deployments",
    ),
]

urlpatterns = [
    path("<slug:cs_project_slug>/", include(teams_urlpatterns)),
]
