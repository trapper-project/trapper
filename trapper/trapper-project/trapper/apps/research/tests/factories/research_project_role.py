import datetime
import factory

from django.utils import timezone

from trapper.apps.research.models import ResearchProjectRole
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.accounts.tests.factories.user import UserFactory


class ResearchProjectRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ResearchProjectRole
        exclude = ("now",)

    user = factory.SubFactory(UserFactory)
    project = factory.SubFactory(ResearchProjectFactory)
    name = ResearchProjectRoleType.COLLABORATOR

    now = factory.LazyFunction(timezone.now)
    date_created = factory.LazyAttribute(
        lambda o: o.now - datetime.timedelta(minutes=10)
    )
