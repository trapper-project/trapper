import pytz
from django.urls import reverse
from rest_framework.fields import DateTimeField
from slugify import slugify

from trapper.apps.common.tests.commons import BaseAPITestCase
from trapper.apps.extra_tables.tests.factories import SpeciesFactory
from trapper.apps.geomap.models import Deployment
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class DeploymentTests(BaseAPITestCase):
    def setUp(self):
        super().setUp()

        self.research_project = ResearchProjectFactory(owner=self.user)

        self.timezone = pytz.timezone("Europe/Warsaw")
        self.location = LocationFactory(
            owner=self.user,
            research_project=self.research_project,
            timezone="Europe/Warsaw",
        )

        self.deployment = DeploymentFactory(
            owner=self.user,
            research_project=self.research_project,
            location=self.location,
            status=Deployment.Status.PRIVATE,
        )
        self.resource1 = ResourceFactory(owner=self.user, deployment=self.deployment)
        self.resource2 = ResourceFactory(owner=self.user, deployment=self.deployment)
        self.collection1 = CollectionFactory(owner=self.user)
        self.collection1.resources.set((self.resource1, self.resource2))
        self.species1 = SpeciesFactory()
        self.species2 = SpeciesFactory()
        self.classificator = ClassificatorFactory(owner=self.user)
        self.classificator.species.add(self.species1, self.species2)
        self.classificator.tracked_species.add(self.species2)
        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user,
            classificator=self.classificator,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )
        self.research_project.collections.set([self.collection1])
        collection = ResearchProjectCollection.objects.filter(
            project=self.classification_project.research_project.pk
        )[0]
        self.classification_project_collection = ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=collection
        )

    def test_get_valid_deployment(self):
        url = reverse(
            "cs_deployment", args=(self.classification_project.slug, self.deployment.pk)
        )
        expected_response = {
            "bait_type": 1,
            "camera_heading": None,
            "camera_height": None,
            "camera_id": "",
            "camera_interval": None,
            "camera_model": "",
            "camera_tilt": None,
            "can_delete": True,
            "can_update": True,
            "classified_ai_count": 0,
            "classified_by_me_count": 0,
            "classified_expert_count": 0,
            "comments": self.deployment.comments,
            "deployment_code": self.deployment.deployment_code,
            "deployment_id": self.deployment.deployment_id,
            "detection_distance": None,
            "end_date": DateTimeField().to_representation(self.deployment.end_date),
            "feature_type": "none",
            "habitat": "",
            "is_incomplete": False,
            "is_private": True,
            "location": {
                "latitude": self.location.get_y,
                "longitude": self.location.get_x,
                "name": self.location.name,
            },
            "location_id": self.location.pk,
            "media": [],
            "owner": {"avatar": "", "id": self.user.pk, "name": self.user.username},
            "pk": self.deployment.pk,
            "resources_count": 2,
            "sequences": 0,
            "species": [],
            "start_date": DateTimeField().to_representation(self.deployment.start_date),
            "thumbnails": [],
            "timestamp_issues": False,
        }
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected_response)

    def test_update_valid_deployment(self):
        url = reverse(
            "cs_deployment", args=(self.classification_project.slug, self.deployment.pk)
        )
        data = {
            "comments": "new comments",
            "deployment_code": "new deployment code",
            "end_date": "2021-01-01T01:00:00+01:00",
            "location": {
                "latitude": 10.0,
                "longitude": 10.0,
                "name": "new location",
            },
            "start_date": "2020-01-01T01:00:00+01:00",
            "is_private": True,
        }
        expected_response = {
            "bait_type": 1,
            "camera_heading": None,
            "camera_height": None,
            "camera_id": "",
            "camera_interval": None,
            "camera_model": "",
            "camera_tilt": None,
            "can_delete": True,
            "can_update": True,
            "classified_ai_count": 0,
            "classified_by_me_count": 0,
            "classified_expert_count": 0,
            "comments": data["comments"],
            "deployment_code": data["deployment_code"],
            "deployment_id": f"new-deployment-code-{self.location.location_id}",
            "detection_distance": None,
            "end_date": data["end_date"],
            "feature_type": "none",
            "habitat": "",
            "is_incomplete": False,
            "is_private": True,
            "location": data["location"],
            "location_id": self.location.pk,
            "media": [],
            "owner": {"avatar": "", "id": self.user.pk, "name": self.user.username},
            "pk": self.deployment.pk,
            "resources_count": 2,
            "sequences": 0,
            "species": [],
            "start_date": data["start_date"],
            "thumbnails": [],
            "timestamp_issues": False,
        }
        response = self.client.patch(url, data=data, format="json")
        self.deployment.refresh_from_db()
        expected_response["deployment_id"] = (
            f"new-deployment-code-{slugify(self.deployment.location.location_id)}"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected_response)
