import logging
from urllib.parse import urljoin
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.conf import settings

from trapper.apps.media_classification.models import ClassificationProject

from rest_framework import generics
from rest_framework.response import Response

import requests

logger = logging.getLogger(__name__)


class JupyterHubClient:
    token = settings.JUPYTER_HUB_API_TOKEN
    jupyter_hub_url = settings.JUPYTER_HUB_URL

    def api_url(self):
        return urljoin(self.jupyter_hub_url, "/hub/api")

    def spawn_url(self):
        return urljoin(self.jupyter_hub_url, "/hub/spawn")

    def __init__(self, user_id: int):
        self.user_id = user_id

    def _get_hub_username(self):
        return f"trapper_user_{self.user_id}"

    def _get_headers(self):
        return {
            "Authorization": f"token {self.token}",
            "Content-Type": "application/json",
        }

    def _get_user_url(self):
        return f"{self.api_url()}/users/{self._get_hub_username()}"

    def _get_server_url(self):
        return f"{self._get_user_url()}/server"

    def create_user(self):
        r = requests.post(
            url=self._get_user_url(),
            headers=self._get_headers(),
            json={},
        )
        if r.status_code != 201:
            raise Exception(f"Error while creating user: {r.text}")

    def ensure_user_exists(self):
        r = requests.get(self._get_user_url(), headers=self._get_headers())
        if r.status_code == 404:
            self.create_user()
        elif r.status_code != 200:
            raise Exception(f"Error while checking user: {r.text}")

    def open_notebook(self):
        self.ensure_user_exists()

        return self.spawn_url()


@method_decorator(ensure_csrf_cookie, name="dispatch")
class OpenNotebookView(generics.CreateAPIView):
    """
    Open a Jupyter notebook for a classification project
    """

    def check_is_user_allowed(self, user, project: ClassificationProject):
        return user.is_staff or project.is_project_admin(user)

    def post(self, request, *args, **kwargs):
        project = get_object_or_404(ClassificationProject, pk=self.kwargs["pk"])
        if not self.check_is_user_allowed(request.user, project):
            return Response(
                status=403, data={"detail": "User is not allowed to open this notebook"}
            )

        try:
            server_url = JupyterHubClient(request.user.id).open_notebook()
        except Exception as e:
            print(e)
            logger.error(f"Error while opening notebook: {e}", exc_info=True)
            return Response(status=503, data={"detail": "Error while opening notebook"})

        return Response(status=200, data={"server_url": server_url})
