import logging

from django.core.management.base import BaseCommand
from trapper.apps.storage.models import Resource
from trapper.apps.storage.thumbnailer import Thumbnailer

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger("main_stdout")


class Command(BaseCommand):
    """Base command class to handle all this stuff"""

    def add_arguments(self, parser):
        parser.add_argument(
            "--all",
            action="store_true",
            dest="all",
            default=False,
            help=(
                "(Re)generate thumbnails for all resources. By default only "
                "resources with missing thumbnails are selected."
            ),
        )

    def handle(self, *args, **options):
        all_res = options["all"]
        if not all_res:
            LOGGER.info("Generating missing thumbnails only.")
            resources = Resource.objects.filter(file_thumbnail="")
        else:
            LOGGER.info("(Re)generating thumbnails for all resources.")
            resources = Resource.objects.all()
        N = len(resources)
        LOGGER.info(f"Found {N} resources.")
        i = 1
        for r in resources:
            LOGGER.info(f"RESOURCE {i}/{N}: {r.name}")
            try:
                Thumbnailer(r).create()
            except Exception:
                i += 1
                continue
            i += 1
