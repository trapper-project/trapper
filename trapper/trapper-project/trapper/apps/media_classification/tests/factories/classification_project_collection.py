import factory

from trapper.apps.media_classification.models import ClassificationProjectCollection
from trapper.apps.research.tests.factories.research_project_collection import (
    ResearchProjectCollectionFactory,
)
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)


class ClassificationProjectCollectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ClassificationProjectCollection
        exclude = ("now",)

    project = factory.SubFactory(ClassificationProjectFactory)
    collection = factory.SubFactory(ResearchProjectCollectionFactory)
    is_active = True
    enable_sequencing_experts = True
    enable_crowdsourcing = True
