import datetime
import factory

from django.utils import timezone

from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import ClassificationProjectRole


class ClassificationProjectRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ClassificationProjectRole
        exclude = ("now",)

    user = factory.SubFactory(UserFactory)
    classification_project = factory.SubFactory(ClassificationProjectFactory)
    name = ClassificationProjectRoleLevels.COLLABORATOR

    now = factory.LazyFunction(timezone.now)
    date_created = factory.LazyAttribute(
        lambda o: o.now - datetime.timedelta(minutes=10)
    )
