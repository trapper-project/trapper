from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ExtraTablesConfig(AppConfig):
    name = "trapper.apps.extra_tables"
    verbose_name = _("Extra tables")
