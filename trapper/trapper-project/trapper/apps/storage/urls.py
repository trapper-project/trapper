# -*- coding: utf-8 -*-

from django.conf.urls import include
from django.urls import reverse_lazy, path, re_path
from django.views.generic import RedirectView
from rest_framework.routers import DefaultRouter
from trapper.apps.storage.views import api as api_views
from trapper.apps.storage.views import collection as collection_views
from trapper.apps.storage.views import resource as resource_views

app_name = "storage"

router = DefaultRouter(trailing_slash=False)
router.register(r"resources", api_views.ResourceViewSet, basename="api-resource")
router.register(
    r"resources/collection/(?P<collection_pk>\d+)",
    api_views.ResourceCollectionViewSet,
    basename="api-resource-collection",
)
router.register(
    r"resources/location/(?P<location_pk>\d+)",
    api_views.ResourceLocationViewSet,
    basename="api-resource-location",
)
router.register(r"collections", api_views.CollectionViewSet, basename="api-collection")
router.register(
    r"collections_ondemand",
    api_views.CollectionOnDemandViewSet,
    basename="api-collection-ondemand",
)
router.register(
    r"collections_map", api_views.CollectionMapViewSet, basename="api-collection-map"
)
router.register(
    r"collections_append",
    api_views.CollectionAppendViewSet,
    basename="api-collection-append",
)

urlpatterns = [
    re_path(r"^api/", include(router.urls), name="api"),
    re_path(
        r"^$",
        RedirectView.as_view(url=reverse_lazy("storage:resource_list")),
        name="storage_index",
    ),
    re_path(
        r"^api/collection/process/",
        api_views.ProcessUploadedCollection.as_view(),
        name="api-collection-process",
    ),
]

urlpatterns += [
    # Resources
    re_path(
        r"^resource/$",
        RedirectView.as_view(url=reverse_lazy("storage:resource_list")),
        name="resource_index",
    ),
    re_path(
        r"^resource/list/$", resource_views.view_resource_list, name="resource_list"
    ),
    re_path(
        r"^resource/detail/(?P<pk>\d+)/$",
        resource_views.view_resource_detail,
        name="resource_detail",
    ),
    re_path(
        r"^resource/delete/(?P<pk>\d+)/$",
        resource_views.view_resource_delete,
        name="resource_delete",
    ),
    re_path(
        r"^resource/delete/$",
        resource_views.view_resource_delete,
        name="resource_delete_multiple",
    ),
    re_path(
        r"^resource/define-prefix/$",
        resource_views.view_resource_define_prefix,
        name="resource_define_prefix",
    ),
    re_path(
        r"^resource/create/$",
        resource_views.view_resource_create,
        name="resource_create",
    ),
    re_path(
        r"^resource/update/(?P<pk>\d+)/$",
        resource_views.view_resource_update,
        name="resource_update",
    ),
    re_path(
        r"^resource/bulk-update/$",
        resource_views.view_resource_bulk_update,
        name="resource_bulk_update",
    ),
    re_path(
        r"^resource/data-package/",
        resource_views.view_resource_data_package,
        name="resource_data_package",
    ),
    re_path(
        r"^resource/regenerate-tokens/",
        resource_views.view_resource_regenerate_tokens,
        name="resource_regenerate_tokens",
    ),
    re_path(
        r"^resource/media/(?P<pk>\d+)/(?P<field>(p|t|e)?(file))/$",
        resource_views.view_resource_sendfile_media,
        name="resource_sendfile_media",
    ),
    path(
        "resource/media/<int:resource_pk>/<str:resource_field>/<str:token>/<str:filename>",
        resource_views.view_resource_sendfile_media_with_file_name,
        name="resource_sendfile_media_with_filename",
    ),
    # Collections
    re_path(
        r"^collection/$",
        RedirectView.as_view(url=reverse_lazy("storage:collection_list")),
        name="collection_index",
    ),
    re_path(
        r"^collection/list/$",
        collection_views.view_collection_list,
        name="collection_list",
    ),
    re_path(
        r"^collection/list/ondemand/$",
        collection_views.view_collection_ondemand_list,
        name="collection_ondemand_list",
    ),
    re_path(
        r"^collection/detail/(?P<pk>\d+)/$",
        collection_views.view_collection_detail,
        name="collection_detail",
    ),
    re_path(
        r"^collection/append/$",
        collection_views.view_collection_resource_append,
        name="collection_append",
    ),
    re_path(
        r"^collection/delete/$",
        collection_views.view_collection_delete,
        name="collection_delete_multiple",
    ),
    re_path(
        r"^collection/delete/(?P<pk>\d+)/$",
        collection_views.view_collection_delete,
        name="collection_delete",
    ),
    re_path(
        r"^collection/upload/(?P<pk>\d+)/$",
        collection_views.view_collection_upload,
        name="collection_upload",
    ),
    re_path(
        r"^collection/update/(?P<pk>\d+)/$",
        collection_views.view_collection_update,
        name="collection_update",
    ),
    re_path(
        r"^collection/resource-delete/(?P<pk>\d+)/$",
        collection_views.view_collection_resource_delete,
        name="collection_resource_delete",
    ),
    re_path(
        r"^collection/create/$",
        collection_views.view_collection_create,
        name="collection_create",
    ),
    re_path(
        r"^collection/upload/$",
        collection_views.view_collection_upload,
        name="collection_upload",
    ),
    re_path(
        r"^collection/delete/(?P<pk>\d+)$",
        collection_views.view_collection_delete,
        name="collection_delete",
    ),
    re_path(
        r"^collection/request/(?P<pk>\d+)/$",
        collection_views.view_collection_request,
        name="collection_request",
    ),
    re_path(
        r"^collection/bulk-update/$",
        collection_views.view_collection_bulk_update,
        name="collection_bulk_update",
    ),
]
