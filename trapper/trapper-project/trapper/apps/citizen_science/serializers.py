import datetime
import logging

import dateutil.parser as parser
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.core.cache import caches
from django.core.validators import ValidationError as DjangoValidationError
from django.db import transaction, IntegrityError
from django.db.models import Q, F, Count, Sum, Case, When
from django.db.models.functions import Cast, ExtractDay, TruncDate
from django.forms.fields import (
    BooleanField,
    ChoiceField,
    DecimalField,
    FloatField,
    TypedChoiceField,
    IntegerField,
    CharField,
)
from django.utils import timezone
from django.utils.timezone import utc
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from trapper.apps.accounts.serializers import CSUserSerializer
from trapper.apps.accounts.validators import ImageValidator
from trapper.apps.citizen_science.mixins import BaseCSDeploymentMixin, BaseCSMixin
from trapper.apps.citizen_science.tasks import (
    celery_check_if_cs_collection_finished,
    complete_collection_upload,
)
from trapper.apps.common.tools import get_image_modification_date_from_exif
from trapper.apps.extra_tables.serializers import CSSpeciesSerializer
from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.geomap.serializers import (
    CSProjectCreateLocationSerializer,
    CSProjectCreateDeploymentSerializer,
    CSSmallDeploymentSerializer,
    CSBBoxesSerializer,
    CSDeploymentMediaResourceSerializer,
)
from trapper.apps.geomap.taxonomy import DeploymentFeatureType
from trapper.apps.media_classification.models import (
    ClassificationProject,
    ClassificationProjectCollection,
    Classification,
    Sequence,
)
from trapper.apps.media_classification.serializers import (
    CSMediaClassificationSerializer,
)
from trapper.apps.media_classification.taxonomy import (
    ObservationType,
    SpeciesSex,
    SpeciesAge,
    ClassificationStatus,
    SpeciesBehaviour,
    ClassificationProjectRoleLevels,
)
from trapper.apps.messaging.models import Message
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.taxonomy import ResearchProjectBaitUse
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.storage.taxonomy import (
    CollectionStatus,
    ResourceMimeType,
    ResourceStatus,
)
from trapper.apps.storage.utils import calculate_file_checksum_hash
from trapper.apps.teams.serializers import TinyTeamSerializer

User = get_user_model()
logger = logging.getLogger(__name__)


class CSDashboardSerializer(BaseCSMixin, serializers.Serializer):
    start_date = serializers.SerializerMethodField()
    joined_date = serializers.SerializerMethodField()
    project_statistics = serializers.SerializerMethodField()
    own_statistics = serializers.SerializerMethodField()

    class Meta:
        fields = (
            "start_date",
            "joined_date",
            "project_statistics",
            "own_statistics",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.classifications_queryset = self.get_classifications_queryset(
            self.context["classification_project"], self.context["request"].user
        )

    def get_start_date(self, obj):
        classification = self.classifications_queryset.order_by(
            "resource__date_recorded"
        ).first()
        if classification:
            return serializers.DateTimeField().to_representation(
                classification.created_at
            )
        return

    def get_joined_date(self, obj):
        classifications_queryset = self._get_base_classifications_queryset(
            self.context["classification_project"]
        )
        user = self.context["request"].user
        classification = (
            classifications_queryset.filter(resource__owner=user)
            .order_by("resource__date_recorded")
            .first()
        )
        if classification:
            return serializers.DateTimeField().to_representation(
                classification.created_at
            )
        return

    @staticmethod
    def _get_by_species_stats(queryset):
        count_species_qs = (
            queryset.filter(dynamic_attrs__species__isnull=False)
            .values(
                "dynamic_attrs__species",
                "dynamic_attrs__species__english_name",
            )
            .annotate(
                total=Count("dynamic_attrs__species"),
                total_classifications=Count("pk", distinct=True),
                sequences=Count(
                    "sequence_id",
                    distinct=True,
                ),
            )
            .order_by("-total", "dynamic_attrs__species")
            .values(
                "total",
                "sequences",
                english_name=F("dynamic_attrs__species__english_name"),
            )
        )

        return list(count_species_qs)

    @staticmethod
    def _generate_pie_chart_data(by_species_stats):
        pie_chart_item_without_other = 10
        other_species_sum = sum(
            item["total"] for item in by_species_stats[pie_chart_item_without_other:]
        )

        species_list = []
        for data in by_species_stats[:pie_chart_item_without_other]:
            species_list.append(
                {
                    "total": data["total"],
                    "english_name": data["english_name"],
                }
            )

        if other_species_sum:
            species_list.append(
                {
                    "total": other_species_sum,
                    "english_name": "Other",
                }
            )
        return species_list

    @staticmethod
    def _generate_bar_chart_data(by_species_stats):
        bar_char_data_amount: int = 5
        species = by_species_stats[:bar_char_data_amount]
        bar_char_data = []
        for data in species:
            bar_char_data.append(
                {
                    "english_name": data["english_name"],
                    "images": data["total"],
                    "sequences": data["sequences"],
                }
            )
        return bar_char_data

    def get_project_statistics(self, obj):
        from django.db.models import IntegerField

        classifications_queryset = self.get_classifications_queryset(
            self.context["classification_project"], self.context["request"].user
        )

        deployment_ids = classifications_queryset.values_list(
            "resource__deployment_id", flat=True
        ).distinct()

        deployments = Deployment.objects.filter(id__in=deployment_ids).annotate(
            trap_days=Cast(
                ExtractDay(TruncDate(F("end_date")) - TruncDate(F("start_date"))),
                IntegerField(),
            )
        )

        deployments_stats = deployments.aggregate(
            total=Count("pk", distinct=True),
            sum_trap_days=Sum("trap_days"),
            locations=Count("location", distinct=True),
        )

        sum_trap_days = deployments_stats["sum_trap_days"] or 0
        total = deployments_stats["total"] or 0
        locations = deployments_stats["locations"] or 0

        by_species_stats = self._get_by_species_stats(classifications_queryset)

        return {
            "active_users": self.context[
                "classification_project"
            ].classification_project_roles.count(),
            "locations": locations,
            "deployments": total,
            "images": classifications_queryset.aggregate(total=Count("resource"))[
                "total"
            ],
            "recordings": classifications_queryset.filter(
                dynamic_attrs__observation_type=ObservationType.ANIMAL
            ).aggregate(total=Count("dynamic_attrs__species"))["total"],
            "camera_trap_days": sum_trap_days or 0,
            # "total_species": total_species,
            "pie_chart": self._generate_pie_chart_data(by_species_stats),
            "bar_chart": self._generate_bar_chart_data(by_species_stats),
        }

    def get_own_statistics(self, obj):
        from django.db.models import IntegerField

        classifications_queryset = self._get_base_classifications_queryset(
            self.context["classification_project"]
        )
        user = self.context["request"].user
        classifications_queryset = classifications_queryset.filter(resource__owner=user)

        deployment_ids = classifications_queryset.values_list(
            "resource__deployment_id", flat=True
        ).distinct()

        deployments = Deployment.objects.filter(id__in=deployment_ids).annotate(
            trap_days=Cast(
                ExtractDay(TruncDate(F("end_date")) - TruncDate(F("start_date"))),
                IntegerField(),
            ),
            location_id_if_own=Case(
                When(location__owner=user, then=F("location_id")),
                default=None,
                output_field=IntegerField(),
            ),
        )

        deployments_stats = deployments.aggregate(
            total=Count("pk", distinct=True, filter=Q(owner=user)),
            sum_trap_days=Sum("trap_days", filter=Q(owner=user)),
            locations=Count("location_id_if_own", distinct=True),
            resources=Count("resources", distinct=True),
        )

        sum_trap_days = deployments_stats["sum_trap_days"] or 0
        total = deployments_stats["total"] or 0
        locations = deployments_stats["locations"] or 0
        resources = deployments_stats["resources"] or 0

        by_species_stats = self._get_by_species_stats(classifications_queryset)

        return {
            "locations": locations,
            "deployments": total,
            "images": resources,
            "recordings": classifications_queryset.filter(
                dynamic_attrs__observation_type=ObservationType.ANIMAL
            ).aggregate(total=Count("dynamic_attrs__species"))["total"],
            "camera_trap_days": sum_trap_days or 0,
            "pie_chart": self._generate_pie_chart_data(by_species_stats),
            "bar_chart": self._generate_bar_chart_data(by_species_stats),
        }


class CSInboxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("pk", "subject", "text", "date_sent")


class CSTaxonomySerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField(source="value")


class CSProjectSerializer(BaseCSDeploymentMixin, serializers.ModelSerializer):
    available_filters = serializers.SerializerMethodField()
    classification_spec = serializers.SerializerMethodField()

    class Meta:
        model = ClassificationProject
        fields = (
            "name",
            "slug",
            "available_filters",
            "classification_spec",
            "video_support_enabled",
        )

    def get_available_filters(self, obj: ClassificationProject):
        available_filters = {}
        user = self.context["request"].user
        if obj.classificator.species.exists():
            available_filters["species"] = CSSpeciesSerializer(
                obj.classificator.species.all(), many=True
            ).data
        taxonomy_filters = {
            "classification_status": ClassificationStatus,
            "observation_type": ObservationType,
            "sex": SpeciesSex,
            "age": SpeciesAge,
            "bait_type": ResearchProjectBaitUse,
            "feature_type": DeploymentFeatureType,
        }

        for filter_name in taxonomy_filters:
            data = taxonomy_filters[filter_name].choices_as_list()
            available_filters[filter_name] = CSTaxonomySerializer(data, many=True).data

        user_ids = obj.classifications.values_list(
            "resource__owner_id", flat=True
        ).distinct()
        available_filters["owner"] = CSUserSerializer(
            User.objects.filter(pk__in=user_ids).order_by(
                "first_name", "last_name", "username"
            ),
            many=True,
        ).data
        available_filters["date_range"] = True

        deployments_qs = self.get_deployments(obj, user).order_by("deployment_code")
        deployments_pk = deployments_qs.values_list("pk", flat=True)
        deployments_list = []

        for d in deployments_qs:
            deployments_list.append(
                {
                    "id": d.pk,
                    "deployment_id": d.deployment_id,
                    "deployment_code": d.deployment_code,
                    "my_deployment": d.owner_id == user.pk
                }
            )
        available_filters["deployments"] = deployments_list
        available_filters["locations"] = (
            Location.objects.filter(
                deployments__in=deployments_pk,
                # TODO: owner is not expected here if other user Locations were shared
                owner=user,
            )
            .order_by("name", "-id")
            .distinct()
            .values("id", "name")
        )
        available_filters["default_bait_type"] = str(ResearchProjectBaitUse.NONE)

        # Set of IDs and names of available teams for user instance
        # only for the specific classification project (active workspace)
        available_teams = TinyTeamSerializer(
            user.available_teams().filter(classification_project=obj), many=True
        ).data
        available_filters["available_teams"] = available_teams

        return available_filters

    def get_classification_spec(self, obj: ClassificationProject):
        classificator = obj.classificator
        fields_defs = classificator.prepare_form_fields()
        spec = {"static": [], "dynamic": []}
        field_type_mapping = {
            BooleanField: "boolean",
            CharField: "string",
            ChoiceField: "choices",
            TypedChoiceField: "choices",
            IntegerField: "integer",
            DecimalField: "float",
            FloatField: "float",
        }

        observation_types = [
            t for t in ObservationType.choices_as_list() if t.get("id") != "blank"
        ]

        field_type_values = {
            "is_setup": {
                "label": _("Is setup"),
            },
            "count": {
                "label": _("Count"),
            },
            "count_new": {
                "label": _(
                    "New Individual"
                ),  # Inside CS count new is mapped to new individual
            },
            "individual_id": {
                "label": _("Individual id"),
            },
            "classification_confidence": {
                "label": _("Classification confidence"),
            },
            "observation_type": {
                "label": _("Observation type"),
                "values": observation_types,
            },
            "sex": {
                "label": _("Sex"),
                "values": SpeciesSex.choices_as_list(),
            },
            "age": {
                "label": _("Age"),
                "values": SpeciesAge.choices_as_list(),
            },
            "behaviour": {
                "label": _("Behaviour"),
                "values": SpeciesBehaviour.choices_as_list(),
            },
            "species": {
                "label": _("Species"),
                "values": obj.classificator.species.values(
                    "id", value=F("english_name")
                ),
            },
            "bboxes": {
                "label": _("Bboxes"),
            },
        }
        # Prepare static fields
        for field_name, field_type in fields_defs["S"].items():
            try:
                label = field_type_values[field_name]["label"]
            except KeyError:
                label = " ".join(field_name.split("_")).title()
            spec["static"].append(
                {
                    "name": field_name,
                    "label": label,
                    "type": field_type_mapping[field_type.__class__],
                }
            )
        # Prepare dynamic fields
        for field_name, field_type in fields_defs["D"].items():
            try:
                label = field_type_values[field_name]["label"]
            except KeyError:
                label = " ".join(field_name.split("_")).title()
            field_def = {
                "name": field_name,
                "label": label,
                "type": field_type_mapping[field_type.__class__],
            }
            # In CS count_new field is mapped for new_individual field which is bool
            if field_name == "count_new":
                field_def["name"] = "new_individual"
                field_def["type"] = "boolean"
            if field_def["type"] == "choices":
                field_def["choices"] = CSTaxonomySerializer(
                    field_type_values[field_def["name"]]["values"], many=True
                ).data
            spec["dynamic"].append(field_def)
        return spec


class CSSequenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sequence
        fields = ("id", "sequence_id")


class CSProjectMediaSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    file_name = serializers.SerializerMethodField()
    thumbnail_url = serializers.SerializerMethodField()
    original_url = serializers.SerializerMethodField()
    deployment = serializers.SerializerMethodField()
    location_name = serializers.SerializerMethodField()
    sequence = CSSequenceSerializer()
    date_recorded = serializers.SerializerMethodField()
    observation_type = serializers.SerializerMethodField()
    species = serializers.SerializerMethodField()
    is_classified_by_me = serializers.SerializerMethodField()
    is_approved = serializers.SerializerMethodField()
    has_feedback = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()
    is_favorite = serializers.SerializerMethodField()
    bboxes = serializers.SerializerMethodField()

    classification = None
    dynamic_attrs = None

    class Meta:
        model = Classification
        fields = [
            "pk",
            "name",
            "file_name",
            "thumbnail_url",
            "original_url",
            "deployment",
            "location_name",
            "sequence",
            "date_recorded",
            "observation_type",
            "species",
            "is_classified_by_me",
            "is_approved",
            "has_feedback",
            "owner",
            "is_favorite",
            "bboxes",
        ]

    def _get_user_classification(self, obj):
        own_classification = self.context["own_classifications"].get(obj.pk)
        return own_classification

    def _get_resource(self, obj):
        resource = self.context.get("resources", {}).get(obj.resource_id)
        return resource or obj.resource

    def get_name(self, obj):
        return self._get_resource(obj).name

    def get_file_name(self, obj):
        return self._get_resource(obj).get_file_name()

    def get_deployment(self, obj):
        return CSSmallDeploymentSerializer(self._get_resource(obj).deployment).data

    def get_location_name(self, obj):
        return self._get_resource(obj).deployment.location.name

    def get_date_recorded(self, obj):
        return self._get_resource(obj).date_recorded

    def get_thumbnail_url(self, obj):
        return self._get_resource(obj).get_cs_url(field="tfile")

    def get_original_url(self, obj):
        return self._get_resource(obj).get_cs_url()

    def get_observation_type(self, obj):
        # Getting classification base of requirements
        # 1. If classification is approved, use it
        # 2. If classification is not approved, use user classification if exists use it
        # 3. If classification is not approved and user classification does not exist,
        #    use approved_source_ai
        classification = obj
        own_classification = self._get_user_classification(obj)
        if not obj.approved_source:
            if own_classification:
                classification = own_classification
            # elif getattr(obj, "approved_source_ai"):
            #     classification = obj.approved_source_ai
        self.dynamic_attrs = classification.dynamic_attrs.all()
        return (
            [attrs.get_observation_type_display() for attrs in self.dynamic_attrs]
            if self.dynamic_attrs
            else []
        )

    def get_species(self, obj):
        species = (
            [attrs.species for attrs in self.dynamic_attrs if attrs.species]
            if self.dynamic_attrs
            else []
        )
        return CSSpeciesSerializer(species, many=True).data

    def get_is_classified_by_me(self, obj) -> bool:
        return self._get_user_classification(obj) is not None

    def get_is_approved(self, obj) -> bool:
        return obj.approved_source is not None

    def get_has_feedback(self, obj) -> bool:
        c = self._get_user_classification(obj)
        return c and c.is_feedback

    def get_owner(self, obj):
        return CSUserSerializer(self._get_resource(obj).owner).data

    def get_is_favorite(self, obj):
        fav = self.context["favorite_classifications"]
        return obj.pk in fav

    def get_bboxes(self, obj):
        return CSBBoxesSerializer(self.dynamic_attrs, many=True).data


class CSProjectMediaScrollSerializer(CSProjectMediaSerializer):
    next_classification_sequence = serializers.SerializerMethodField()
    previous_classification_sequence = serializers.SerializerMethodField()

    queryset = None

    class Meta:
        model = Classification
        fields = CSProjectMediaSerializer.Meta.fields + [
            "next_classification_sequence",
            "previous_classification_sequence",
        ]

    def get_next_classification_sequence(self, obj):
        if seq_id := obj.sequence_id:
            current_index = self.context["sequences"].index(seq_id)
            try:
                next_sequence = self.context["sequences"][current_index + 1]
            except IndexError:
                # If it is last sequence, don't return anything
                return None
            # get first classification from next sequence
            return self.context["sequences_map"][next_sequence][0]

    def get_previous_classification_sequence(self, obj):
        if seq_id := obj.sequence_id:
            current_index = self.context["sequences"].index(seq_id)
            if current_index == 0:
                # If it is first sequence, it doesn't have previous element
                return None
            previous_sequence = self.context["sequences"][current_index - 1]
            # get first classification from previous sequence
            return self.context["sequences_map"][previous_sequence][0]


class CSProjectMediaResourceSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source="resource.name")
    deployment = CSDeploymentMediaResourceSerializer(source="resource.deployment")
    sequence = CSSequenceSerializer()
    file_name = serializers.ReadOnlyField(source="resource.get_file_name")
    resource_type = serializers.ReadOnlyField(source="resource.resource_type")
    date_recorded = serializers.ReadOnlyField(source="resource.date_recorded")
    date_uploaded = serializers.ReadOnlyField(source="resource.date_uploaded")
    owner = CSUserSerializer(source="resource.owner")
    classified_in = serializers.SerializerMethodField()
    classification = serializers.SerializerMethodField()
    user_classification = serializers.SerializerMethodField()
    is_classified_by_me = serializers.SerializerMethodField()
    is_approved = serializers.SerializerMethodField()
    has_feedback = serializers.SerializerMethodField()
    thumbnail_url = serializers.SerializerMethodField()
    original_url = serializers.SerializerMethodField()
    is_favorite = serializers.SerializerMethodField()

    class Meta:
        model = Classification
        fields = (
            "pk",
            "name",
            "deployment",
            "sequence",
            "file_name",
            "resource_type",
            "date_recorded",
            "date_uploaded",
            "owner",
            "classified_in",
            "classification",
            "user_classification",
            "is_classified_by_me",
            "is_approved",
            "has_feedback",
            "thumbnail_url",
            "original_url",
            "is_favorite",
        )

    def _user_classification(self):
        own_classification = self.context["own_classification"]
        return own_classification

    def get_classified_in(self, obj):
        return ClassificationProject.objects.filter(
            status=ClassificationProject.CitizenScienceStatus.PUBLIC,
            collections__collection__resources=obj.resource,
        ).values("name", "slug")

    def get_is_classified_by_me(self, obj) -> bool:
        c = self._user_classification()
        return c is not None

    def get_is_approved(self, obj) -> bool:
        return obj.approved_source is not None

    def get_has_feedback(self, obj) -> bool:
        c = self._user_classification()
        return c and c.is_feedback

    def get_classification(self, obj):
        return CSMediaClassificationSerializer(obj, context=self.context).data

    def get_user_classification(self, obj):
        c = self._user_classification()
        if not c:
            return None
        return CSMediaClassificationSerializer(c, context=self.context).data

    @staticmethod
    def get_thumbnail_url(obj):
        return obj.resource.get_cs_url(field="tfile")

    @staticmethod
    def get_original_url(obj):
        return obj.resource.get_cs_url()

    def get_is_favorite(self, obj):
        return obj.is_user_favorite(self.context["request"].user)


class CSProjectsSerializer(serializers.ModelSerializer):
    tags = serializers.SerializerMethodField()
    image_background = serializers.ReadOnlyField(source="cs_image_background_url")
    description = serializers.CharField(source="research_project.description")

    class Meta:
        model = ClassificationProject
        fields = (
            "name",
            "slug",
            "description",
            "tags",
            "image_background",
        )

    @staticmethod
    def get_tags(obj):
        """Custom method for retrieving research project tags"""
        return [k.name for k in obj.research_project.keywords.all()]


class CSProjectCreateUploadSerializer(serializers.Serializer):
    location_id = serializers.IntegerField(required=False, write_only=True)
    location = CSProjectCreateLocationSerializer(required=False, write_only=True)
    deployment_id = serializers.IntegerField(required=False)
    deployment = CSProjectCreateDeploymentSerializer(required=False, write_only=True)
    collection_id = serializers.IntegerField(read_only=True)
    comments = serializers.CharField(default="", allow_blank=True, write_only=True)
    is_private = serializers.BooleanField(default=False, write_only=True)
    is_incomplete = serializers.BooleanField(default=False, write_only=True)

    class Meta:
        fields = (
            "location_id",
            "location",
            "deployment_id",
            "deployment",
            "collection",
            "comments",
            "is_private",
            "is_incomplete",
        )

    def validate(self, attrs):
        errors = {}
        user = self.context["request"].user
        if location_id := attrs.get("location_id"):
            try:
                attrs["location_id"] = Location.objects.filter(
                    Q(owner=user) | Q(is_public=True)
                ).get(pk=location_id)
            except Location.DoesNotExist:
                errors["location_id"] = _("Location does not exist.")
        elif not attrs.get("location") and not attrs.get("deployment_id"):
            errors["location_id"] = _("Location is required.")
        elif attrs.get("location") and attrs.get("deployment_id"):
            errors["location_id"] = _(
                "You can't create new Location to exist deployment."
            )
        if deployment_id := attrs.get("deployment_id"):
            try:
                attrs["deployment_id"] = Deployment.objects.filter(
                    Q(owner=user) | Q(location__is_public=True)
                ).get(pk=deployment_id)
            except Deployment.DoesNotExist:
                errors["deployment_id"] = _("Deployment does not exist.")
        elif not attrs.get("deployment"):
            errors["deployment_id"] = _("Deployment is required.")
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    @transaction.atomic
    def create(self, validated_data):
        owner = self.context["request"].user
        now = timezone.now()
        classification_project = self.context["classification_project"]
        research_project = classification_project.research_project
        location = None

        # Get or create new private Location
        if location_id := validated_data.get("location_id"):
            location = location_id
        elif location_data := validated_data.get("location"):
            location_data["location_id"] = (
                f"{classification_project.id}-{owner.id}-{location_data['name']}"
            )
            location_data["coordinates"] = Point(
                (
                    float(location_data.pop("get_x")),
                    float(location_data.pop("get_y")),
                ),
                srid=4326,
            )
            location_data["owner"] = owner
            location_data["is_public"] = False
            # Assign Research project for Location
            location_data["research_project"] = research_project

            location = Location.objects.create(**location_data)

        # Get or create new Deployment
        if deployment_id := validated_data.get("deployment_id"):
            deployment = deployment_id
            if deployment.comments:
                deployment.comments = (
                    f"{deployment.comments} \n [{now.strftime('%d.%m.%Y %H:%M:%S')}]:"
                    f" {validated_data['comments']}"
                )
            else:
                deployment.comments = validated_data["comments"]
            deployment.is_incomplete = validated_data["is_incomplete"]
            # Assign Research project for Deployment
            deployment.research_project = research_project
            deployment.save(
                update_fields=["comments", "is_incomplete", "research_project"]
            )
        else:
            deployment_data = validated_data["deployment"]
            deployment_data["correct_tstamp"] = not deployment_data.pop(
                "timestamp_issues", False
            )
            deployment_data["location"] = location
            deployment_data["owner"] = owner
            deployment_data["comments"] = validated_data["comments"]
            deployment_data["is_incomplete"] = validated_data["is_incomplete"]
            deployment_data["feature_type"] = (
                deployment_data.get("feature_type") or DeploymentFeatureType.NONE
            )
            deployment_data["deployment_id"] = (
                f'{deployment_data["deployment_code"]}-{location.id}'
            )
            deployment_data["status"] = Deployment.Status.PUBLIC
            if validated_data["is_private"]:
                deployment_data["status"] = Deployment.Status.PRIVATE

            deployment_data["research_project"] = research_project

            deployment = Deployment.objects.create(**validated_data["deployment"])

        # Get or create new Collection and link it with Research and Classification projects
        status = CollectionStatus.PUBLIC
        if validated_data["is_private"]:
            status = CollectionStatus.PRIVATE
        try:
            collection = Collection.objects.get(
                cs_deployment=deployment,
                owner=owner,
            )
            collection.is_complete_cs_collection = False
            collection.date_start_cs_collection_upload = now
            collection.date_complete_cs_collection_upload = None
            collection.status = status
            collection.save(
                update_fields=[
                    "is_complete_cs_collection",
                    "date_start_cs_collection_upload",
                    "date_complete_cs_collection_upload",
                    "status",
                ]
            )
        except Collection.DoesNotExist:
            collection = Collection.objects.create(
                cs_deployment=deployment,
                status=status,
                owner=owner,
                name=f"{classification_project.slug}_{deployment.id}_{owner.id}_{now.strftime('%Y-%m-%d_%H:%m')}",
                date_start_cs_collection_upload=now,
                is_complete_cs_collection=False,
            )
            research_project_collection = ResearchProjectCollection.objects.create(
                project=research_project, collection=collection
            )
            ClassificationProjectCollection.objects.create(
                project=classification_project, collection=research_project_collection
            )

        # Check if user exists in classification project
        if not classification_project.classification_project_roles.filter(
            user=owner
        ).exists():
            # Create user if not exist
            classification_project.classification_project_roles.create(
                user=owner,
                name=ClassificationProjectRoleLevels.CS_USER,
            )

        celery_check_if_cs_collection_finished.apply_async(
            (classification_project.pk, collection.pk, owner.pk),
            countdown=settings.CS_CHECK_EVERY_COLLECTION_FINISHED,
        )

        # Clear cache because TrapperPaginator caches count result, and doesn't
        # refresh queryset,
        caches["default"].clear()
        return {"deployment_id": deployment.id, "collection_id": collection.id}


class CSProjectUploadMediaSerializer(serializers.ModelSerializer):
    collection = serializers.IntegerField(write_only=True)
    metadata = serializers.JSONField(write_only=True)

    class Meta:
        model = Resource
        fields = (
            "file",
            "deployment",
            "collection",
            "metadata",
        )
        extra_kwargs = {
            "file": {"write_only": True},
            "deployment": {"write_only": True},
        }

    def validate_collection(self, value):
        try:
            value = Collection.objects.get(pk=value, owner=self.context["request"].user)
        except Collection.DoesNotExist:
            raise serializers.ValidationError(_("Collection does not exist."))
        if value.is_complete_cs_collection:
            logger.error(
                f"Unable to upload images to collection {value}({value.pk}). Collection is set as completed."
            )
            raise serializers.ValidationError(_("Collection completed."))
        return value

    def _parse_file(self, file, metadata):
        # frontend change filename to format <uuid>.<ext>
        file_uuid = file.name.split(".")[0]
        file_metadata = None
        try:
            # Try to get file metadata
            file_metadata = metadata[file_uuid]
            file_name = file_metadata.get("name") or file.name
            file.name = file_name
        except KeyError:
            logger.error("There is no file metadata", exc_info=True, stack_info=True)
            file_name = file.name

        # calculate file_content_hash
        file_checksum_hash = calculate_file_checksum_hash(file)

        date_recorded = get_image_modification_date_from_exif(file)
        if not date_recorded:
            if file_metadata:
                # if we are unable to get modification_date from exif try to get from metadata
                date_recorded = file_metadata.get("last_modified_date")
                if not date_recorded:
                    date_recorded = file_metadata.get("last_modified")
                    if date_recorded:
                        # fix to correct timestamp form frontend
                        date_recorded = date_recorded / 1000
            else:
                date_recorded = datetime.datetime.now().timestamp()
        if isinstance(date_recorded, str):
            if date_recorded.isnumeric():
                date_recorded = int(date_recorded)
            else:
                try:
                    date_recorded = datetime.datetime.strptime(
                        date_recorded, "%Y:%m:%d %H:%M:%S"
                    ).replace(tzinfo=utc)
                except ValueError:
                    date_recorded = parser.parse(date_recorded)
        if isinstance(date_recorded, int) or isinstance(date_recorded, float):
            try:
                date_recorded = datetime.datetime.fromtimestamp(date_recorded)
            except TypeError:
                logger.error(
                    "Incorrect date_recorded format", exc_info=True, stack_info=True
                )
                date_recorded = datetime.datetime.now()
        return file, file_name, date_recorded, file_checksum_hash, file_metadata

    def create(self, validated_data):
        files = self.context["request"].FILES.getlist("file")
        resources = []
        mime_type_choices_str = [choice for _, choice in ResourceMimeType.CHOICES]
        for file in files:
            # try:
            #     validate_image_file_extension(file)
            #     ImageValidator(size=settings.MAX_UPLOAD_SIZE)(file)
            # except DjangoValidationError:
            #     continue
            try:
                status = ResourceStatus.PUBLIC
                if validated_data["collection"].status == CollectionStatus.PRIVATE:
                    status = ResourceStatus.PRIVATE

                file, file_name, date_recorded, file_checksum_hash, file_metadata = (
                    self._parse_file(file, validated_data["metadata"])
                )

                mime_type = file_metadata.get("type", "")

                if not mime_type.startswith("video"):
                    # validate_image_file_extension(file)
                    ImageValidator(size=settings.MAX_UPLOAD_SIZE)(file)

                resource = Resource.objects.create(
                    name=file_name,
                    file=file,
                    date_recorded=date_recorded,
                    owner=self.context["request"].user,
                    deployment=validated_data["deployment"],
                    status=status,
                    file_checksum_hash=file_checksum_hash,
                    mime_type=(
                        mime_type
                        if mime_type and mime_type in mime_type_choices_str
                        else None
                    ),
                )
            except IntegrityError as e:
                logger.warning(e)
                continue
            except DjangoValidationError as e:
                logger.warning(e)
                continue
            if not resource.mime_type:
                resource.update_mimetype(commit=False)
            resource.update_resource_type(commit=False)
            resource.update_file_size(commit=False)
            resource.save()
            resource.generate_thumbnails()
            resources.append(resource)
        # Add all object to collection
        validated_data["collection"].resources.add(*[k.pk for k in resources])
        return {}


class CSProjectCompleteUploadSerializer(serializers.Serializer):
    deployment = serializers.IntegerField(write_only=True)
    collection = serializers.IntegerField(write_only=True)

    class Meta:
        fields = (
            "deployment",
            "collection",
        )

    def validate_collection(self, value):
        try:
            value = Collection.objects.get(pk=value, owner=self.context["request"].user)
        except Collection.DoesNotExist:
            raise serializers.ValidationError(_("Collection does not exist."))
        return value

    def create(self, validated_data):
        if settings.CELERY_ENABLED:
            complete_collection_upload.delay(
                self.context["classification_project"].pk,
                self.context["request"].user.pk,
                validated_data["collection"].pk,
            )
        else:
            complete_collection_upload(
                self.context["classification_project"].pk,
                self.context["request"].user.pk,
                validated_data["collection"].pk,
            )
        return {}
