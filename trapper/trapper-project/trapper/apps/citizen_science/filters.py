import django_filters
from django.db.models import Q

from conf.settings.base import str2bool
from trapper.apps.media_classification.filters import ClassificationFilter
from trapper.apps.media_classification.models import (
    Classification,
    ClassificationProject,
)
from trapper.apps.media_classification.taxonomy import ObservationType


class CSClassificationFilter(ClassificationFilter):
    owner = django_filters.CharFilter(method="get_owner")
    search = django_filters.CharFilter(method="get_search")
    species = django_filters.CharFilter(method="get_species")
    is_classified_by_me = django_filters.CharFilter(method="get_is_classified_by_me")
    is_approved = django_filters.CharFilter(method="get_is_approved")
    has_feedback = django_filters.CharFilter(method="get_has_feedback")
    by_me = django_filters.CharFilter(method="get_by_me")
    by_team = django_filters.CharFilter(method="get_by_team")
    favorite = django_filters.CharFilter(method="get_favorite")
    observation_type = django_filters.ChoiceFilter(
        method="get_observation_type",
        choices=ObservationType.get_all_choices(),
        distinct=True,
    )

    class Meta:
        model = Classification
        fields = [
            "search",
            "species",
            "observation_type",
            "sex",
            "age",
            "is_classified_by_me",
            "is_approved",
            "has_feedback",
            "owner",
            "deployment",
            "locations_map",
            "by_team",
            "favorite",
            "rdate_from",
            "rdate_to",
        ]

    def __init__(self, *args, **kwargs):
        classification_project_slug = kwargs["request"].parser_context["kwargs"][
            "cs_project_slug"
        ]
        kwargs["data"] = kwargs["data"].copy()
        kwargs["data"]["project"] = ClassificationProject.objects.get(
            slug=classification_project_slug
        ).pk
        super().__init__(*args, **kwargs)

    def get_owner(self, qs, name, value):
        if not value.isnumeric():
            return qs
        return qs.filter(resource__owner_id=value)

    def get_search(self, qs, name, value):
        if value:
            return qs.filter(resource__file__icontains=value)
        return qs

    def get_species(self, qs, name, value):
        if value:
            if str2bool(self.data.get("by_me", None)):
                return qs.filter(
                    Q(
                        user_classifications__owner=self.request.user,
                        user_classifications__dynamic_attrs__species=value,
                    )
                    | Q(
                        approved_source__owner=self.request.user,
                        dynamic_attrs__species=value,
                    )
                )
            return qs.filter(dynamic_attrs__species=value)
        return qs

    def get_is_classified_by_me(self, qs, name, value):
        if str2bool(value):
            return qs.filter(user_classifications__owner=self.request.user)
        else:
            return qs.exclude(user_classifications__owner=self.request.user)

    def get_is_approved(self, qs, name, value):
        if str2bool(value):
            return qs.filter(approved_source__isnull=False)
        else:
            return qs.exclude(approved_source__isnull=False)

    def get_has_feedback(self, qs, name, value):
        if str2bool(value):
            return qs.filter(
                user_classifications__owner=self.request.user,
                user_classifications__is_feedback=True,
            )
        else:
            return qs.exclude(
                user_classifications__owner=self.request.user,
                user_classifications__is_feedback=True,
            )

    def get_by_me(self, qs, name, value):
        if str2bool(value):
            return qs.filter(resource__owner=self.request.user)
        else:
            return qs.exclude(resource__owner=self.request.user)

    def get_by_team(self, qs, name, value):
        """
        Return classification from co-related Teams via resources -> deployments
        """
        # Value is team id
        if value:
            return qs.filter(resource__deployment__team_deployments=value)
        return qs

    def get_favorite(self, qs, name, value):
        if str2bool(value):
            return qs.filter(favorite_classifications__user=self.request.user)
        else:
            return qs.exclude(favorite_classifications__user=self.request.user)

    def get_observation_type(self, qs, name, value):
        if value:
            return qs.filter(
                (
                    Q(
                        approved_source_ai__dynamic_attrs__observation_type=value,
                        approved_source__isnull=True,
                    )
                    & ~Q(user_classifications__owner=self.request.user)
                )
                | Q(
                    user_classifications__owner=self.request.user,
                    user_classifications__dynamic_attrs__observation_type=value,
                    approved_source__isnull=True,
                )
                | Q(
                    approved_source__isnull=False, dynamic_attrs__observation_type=value
                )
            )
        return qs
