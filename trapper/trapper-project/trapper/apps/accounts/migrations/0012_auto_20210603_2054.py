# Generated by Django 2.2.17 on 2021-06-03 18:54

from django.db import migrations, models
import trapper.apps.accounts.models
import trapper.apps.storage.cloud_storages
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0011_auto_20200717_1640"),
    ]

    operations = [
        # MODIFIED TO REMOVE OLD CLASS REFERENCE
        # migrations.AlterField(
        #     model_name='userdatapackage',
        #     name='package',
        #     field=models.FileField(storage=trapper.apps.storage.cloud_storages.S3ExternalMediaStorage(), upload_to=trapper.apps.accounts.models.user_data_package_upload_to),
        # ),
        migrations.AlterField(
            model_name="userdatapackage",
            name="uuid4",
            field=models.UUIDField(default=uuid.uuid4),
        ),
    ]
