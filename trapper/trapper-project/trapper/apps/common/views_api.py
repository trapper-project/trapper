# -*- coding: utf-8 -*-
"""Views related to **Django Rest Framework** application that are used
in other applications to define REST API"""
import hashlib

from django.conf import settings
from django.core.cache import caches
from django.core.exceptions import EmptyResultSet
from django.core.paginator import Paginator
from django.utils.functional import cached_property
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q
from rest_framework import pagination, permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.filters import SearchFilter
from rest_framework.pagination import CursorPagination, _reverse_ordering
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from trapper.apps.media_classification.models import Classification
from trapper.middleware import get_current_user


class TrapperPaginator(Paginator):
    """
    TODO: docstrings
    """

    cache = caches["default"]
    timeout = getattr(settings, "CACHE_COUNT_TIMEOUT", 60 * 60)

    @cached_property
    def count(self):
        """
        Returns the total number of objects, across all pages.
        """
        try:
            user = get_current_user()
            cache_key = (
                    "api-count:"
                    + user.username
                    + ":"
                    + hashlib.md5(str(self.object_list.query).encode("utf8")).hexdigest()
            )
            # return existing value, if any
            value = self.cache.get(cache_key)
            if value is not None and value != 0:
                return value
            # cache new value
            value = self.object_list.select_related(None).count()
            self.cache.set(cache_key, value, self.timeout)
            return value
        except (AttributeError, TypeError, EmptyResultSet):
            # AttributeError if object_list has no count() method.
            # TypeError if object_list.count() requires arguments
            # (i.e. is of type list).
            return len(self.object_list)


class ListPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 500
    django_paginator_class = TrapperPaginator

    def get_paginated_response(self, data):
        return Response(
            {
                "pagination": {
                    "page": self.page.number,
                    "page_size": self.page.paginator.per_page,
                    "pages": self.page.paginator.num_pages,
                    "count": self.page.paginator.count,
                },
                "results": data,
            }
        )


class CSListPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 500

    total = 0

    def set_total(self, view):
        """
        Set class attributes with total number of objects in base view queryset.
        """
        queryset = view.get_queryset()
        self.total = queryset.only("pk").count()

    def paginate_queryset(self, queryset, request, view=None):
        self.set_total(view)
        return super().paginate_queryset(queryset, request, view)

    def get_paginated_response(self, data):
        return Response(
            {
                "pagination": {
                    "page": self.page.number,
                    "page_size": self.page.paginator.per_page,
                    "pages": self.page.paginator.num_pages,
                    "count": self.page.paginator.count,
                    "total": self.total,
                },
                "results": data,
            }
        )


# Pagination used to infinity scroll
class CSCursorPagination(CursorPagination):
    page_size = 10
    page_size_query_param = "page_size"
    ordering = "pk"
    order_by = ("resource__date_recorded", "pk")

    def paginate_queryset(self, queryset, request, view=None):
        self.page_size = self.get_page_size(request)
        if not self.page_size:
            return None

        self.base_url = request.build_absolute_uri()
        self.ordering = self.get_ordering(request, queryset, view)

        self.cursor = self.decode_cursor(request)
        if self.cursor is None:
            (offset, reverse, current_position) = (0, False, None)
        else:
            (offset, reverse, current_position) = self.cursor
        # Cursor pagination always enforces an ordering.
        if reverse:
            queryset = queryset.order_by(*_reverse_ordering(self.order_by))
        else:
            queryset = queryset.order_by(*self.order_by)

        # If we have a cursor with a fixed position then filter by that.
        if current_position is not None:
            try:
                classification = Classification.objects.get(pk=current_position)
                date_recorded = classification.resource.date_recorded

                order = self.order_by[0]
                is_reversed = order.startswith("-")

                if self.cursor.reverse != is_reversed:
                    q = Q(
                        resource__date_recorded=date_recorded,
                        pk__lte=classification.pk,
                    ) | Q(resource__date_recorded__lt=date_recorded)
                else:
                    q = Q(
                        resource__date_recorded=date_recorded,
                        pk__gte=classification.pk,
                    ) | Q(resource__date_recorded__gt=date_recorded)

                queryset = queryset.filter(q)
            except Classification.DoesNotExist:
                queryset = queryset.none()
        # If we have an offset cursor then offset the entire page by that amount.
        # We also always fetch an extra item in order to determine if there is a
        # page following on from this one.
        results = list(queryset[offset: offset + self.page_size + 1])
        self.page = list(results[: self.page_size])

        # Determine the position of the final item following the page.
        if len(results) > len(self.page):
            has_following_position = True
            following_position = self._get_position_from_instance(
                results[-1], self.ordering
            )
        else:
            has_following_position = False
            following_position = None

        if reverse:
            # If we have a reverse queryset, then the query ordering was in reverse
            # so we need to reverse the items again before returning them to the user.
            self.page = list(reversed(self.page))

            # Determine next and previous positions for reverse cursors.
            self.has_next = (current_position is not None) or (offset > 0)
            self.has_previous = has_following_position
            if self.has_next:
                self.next_position = current_position
            if self.has_previous:
                self.previous_position = following_position
        else:
            # Determine next and previous positions for forward cursors.
            self.has_next = has_following_position
            self.has_previous = (current_position is not None) or (offset > 0)
            if self.has_next:
                self.next_position = following_position
            if self.has_previous:
                self.previous_position = current_position

        # Display page controls in the browsable API if there is more
        # than one page.
        if (self.has_previous or self.has_next) and self.template is not None:
            self.display_page_controls = True

        return self.page


class PaginatedReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Base class for readonly ModelViewSets.
    """

    # Pagination
    pagination_class = ListPagination
    # filter backends
    filter_backends = (DjangoFilterBackend, SearchFilter)

    def list(self, request, *args, **kwargs):
        """
        TODO: docstrings
        """
        qs = self.get_queryset()
        qs_f = self.filter_queryset(qs)

        if self.request.GET.get("all_filtered", None):
            if qs_f.count() > 100000:
                return Response([])
            return Response(qs_f.values_list("pk", flat=True))

        # clear user's cache if requested
        if self.request.GET.get("clear_cache", None):
            cache = caches["default"]
            cache_count_key = (
                    "api-count:"
                    + request.user.username
                    + ":"
                    + hashlib.md5(str(qs_f.query).encode("utf8")).hexdigest()
            )
            cache.delete(cache_count_key)

        sort_by = self.request.GET.get("sort_by")
        if sort_by:
            reverse = self.request.GET.get("reverse") == "true"
            sort_by_str = "-" + sort_by if reverse else sort_by
            qs_f = qs_f.order_by(sort_by_str)

        page = self.paginate_queryset(qs_f)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(qs_f, many=True)
        return Response(serializer.data)


class PlainTextRenderer(renderers.BaseRenderer):
    media_type = "text/plain"
    format = "txt"

    def render(self, data, media_type=None, renderer_context=None):
        return data


class TestAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        return Response(status=HTTP_200_OK)


test_api_view = TestAPIView.as_view()
