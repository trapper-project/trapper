# Generated by Django 2.2.17 on 2021-08-09 13:34

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0037_auto_20210805_1601"),
    ]

    operations = [
        migrations.AddField(
            model_name="aiclassification",
            name="has_bboxes",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="aiclassification",
            name="is_empty",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="aiclassification",
            name="static_attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Static attrs"
            ),
        ),
    ]
