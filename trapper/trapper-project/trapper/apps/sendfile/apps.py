from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class SendfileConfig(AppConfig):
    name = "trapper.apps.sendfile"
    verbose_name = _("Sendfile")
