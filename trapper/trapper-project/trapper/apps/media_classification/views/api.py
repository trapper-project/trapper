# -*- coding: utf-8 -*-
"""
Media classification DRF API.
"""
import json
import os
import zipfile

from django.conf import settings
from django.core.files.temp import NamedTemporaryFile
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.serializers import ValidationError as rest_verror
from rest_framework.views import APIView

from trapper.apps.accounts.models import UserTask
from trapper.apps.accounts.utils import get_external_data_packages_path
from trapper.apps.common.tools import df_to_geojson, aggregate_results
from trapper.apps.common.views_api import PaginatedReadOnlyModelViewSet
from trapper.apps.geomap.models import Deployment
from trapper.apps.media_classification import serializers as classification_serializers
from trapper.apps.media_classification.filters import (
    UserClassificationFilter,
    ClassificationProjectFilter,
    ClassificationProjectCollectionFilter,
    ClassificationFilter,
    SequenceFilter,
    ClassificatorFilter,
    AIClassificationFilter,
)
from trapper.apps.media_classification.forms import ClassificationImportForm
from trapper.apps.media_classification.models import (
    UserClassification,
    ClassificationProject,
    Classificator,
    ClassificationProjectCollection,
    Classification,
    Sequence,
    AIProvider,
    AIClassification,
)

from trapper.apps.media_classification.tasks import (
    ClassificationImporter,
    celery_import_classifications,
    ResultsDataPackageGenerator,
)
from trapper.apps.storage.models import Resource

from trapper.apps.media_classification.ai_providers.ai_provider_factory import (
    get_ai_provider_manager,
)


class UserClassificationViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of user classifications."""

    permission_classes = (permissions.IsAuthenticated,)
    queryset = UserClassification.objects.all()
    filterset_class = UserClassificationFilter
    serializer_class = classification_serializers.UserClassificationSerializer
    search_fields = [
        "dynamic_attrs__species__latin_name",
        "dynamic_attrs__species__english_name",
        "=dynamic_attrs__attrs",
        "=static_attrs",
        "classification__resource__name",
    ]
    prefetch_related = [
        "owner__userprofile",
        "dynamic_attrs",
        "dynamic_attrs__species",
        "classification__resource__deployment__location",
        "classification__collection",
        "classification__project__owner",
    ]

    def get_queryset(self):
        queryset = UserClassification.objects.get_accessible(
            user=self.request.user
        ).prefetch_related(*self.prefetch_related)
        return queryset


class ClassificationViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of classifications."""

    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = ClassificationFilter
    serializer_class = classification_serializers.ClassificationSerializer
    search_fields = [
        "dynamic_attrs__species__latin_name",
        "dynamic_attrs__species__english_name",
        "=dynamic_attrs__attrs",
        "=static_attrs",
        "resource__name",
    ]
    prefetch_related = [
        "resource__deployment__location",
        "resource__managers",
        "dynamic_attrs",
        "dynamic_attrs__species",
        "ai_classifications",
        "project__classificator",
        "user_classifications",
        "approved_source_ai__dynamic_attrs",
    ]

    def get_queryset(self):
        queryset = Classification.objects.get_accessible(
            user=self.request.user
        ).prefetch_related(*self.prefetch_related)
        return queryset


class AIClassificationViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of AI classifications."""

    permission_classes = (permissions.IsAuthenticated,)
    filter_class = AIClassificationFilter
    serializer_class = classification_serializers.AIClassificationSerializer
    search_fields = [
        "dynamic_attrs__species__latin_name",
        "dynamic_attrs__species__english_name",
        "=dynamic_attrs__attrs",
        "=static_attrs",
        "classification__resource__name",
    ]
    prefetch_related = [
        "classification",
        "classification__resource__deployment__location",
        "classification__resource__managers",
        "dynamic_attrs",
        "dynamic_attrs__species",
        "classification__project__classificator",
    ]

    def get_queryset(self):
        queryset = AIClassification.objects.get_accessible(
            user=self.request.user
        ).prefetch_related(*self.prefetch_related)
        return queryset


class ClassificationResultsView(ListAPIView):
    """
    Returns a table with classification results.
    """

    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = ClassificationFilter
    search_fields = ["resource__name", "=dynamic_attrs__attrs", "=static_attrs"]
    prefetch_related = ["resource__deployment__location", "dynamic_attrs", "sequence"]
    project = None

    def get_serializer_class(self):
        pass

    def get_extra_params(self):
        query_params = self.request.query_params
        params = {
            "include_trapper_id": query_params.get("include_trapper_id", "True")
            == "True",
            "include_events": query_params.get("include_events", "True") == "True",
            "events_agg_age": query_params.get("events_agg_age", "False") == "True",
            "events_agg_sex": query_params.get("events_agg_sex", "False") == "True",
            "trapper_format": query_params.get("trapper_format", "True") == "True",
        }
        return params

    def get_project(self):
        project_pk = self.kwargs.get("project_pk")
        self.project = get_object_or_404(ClassificationProject, pk=project_pk)

    def get_queryset(self):
        self.get_project()
        queryset = (
            Classification.objects.get_accessible(user=self.request.user)
            .prefetch_related(*self.prefetch_related)
            .filter(project=self.project)
        )
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if queryset.count() == 0:
            raise rest_verror(
                "Empty queryset or you have no permission to access the requested objects."
            )
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=observations.csv"
        if self.project:
            classificator = self.project.classificator
            if classificator:
                # get extra query parameters if available
                params = self.get_extra_params()
                df = classification_serializers.prepare_results_table(
                    queryset,
                    include_trapper_id=params["include_trapper_id"],
                    include_events=params["include_events"],
                    events_agg_age=params["events_agg_age"],
                    events_agg_sex=params["events_agg_sex"],
                    trapper_format=params["trapper_format"],
                )
                df.to_csv(response, encoding="utf-8", index=False)
        return response


class ClassificationProjectMediaTableView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = ClassificationFilter
    pagination_class = None
    project = None

    def get_serializer_class(self):
        pass

    def get_extra_params(self):
        query_params = self.request.query_params
        params = {
            "trapper_url": query_params.get("trapper_url", "True") == "True",
            "url_token": query_params.get("url_token", "True") == "True",
            "private_human": query_params.get("private_human", "True") == "True",
            "private_vehicle": query_params.get("private_vehicle", "True") == "True",
            "private_species": query_params.getlist("private_species", []),
        }
        return params

    def get_project(self):
        project_pk = self.kwargs.get("project_pk")
        self.project = get_object_or_404(ClassificationProject, pk=project_pk)

    def get_queryset(self):
        self.get_project()
        classifications = (
            Classification.objects.get_accessible(user=self.request.user)
            .prefetch_related("sequence")
            .filter(project=self.project)
        )
        return classifications

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if queryset.count() == 0:
            raise rest_verror(
                "Empty queryset or you have no permission to access the requested objects."
            )
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=media.csv"
        host = request.build_absolute_uri("/")
        params = self.get_extra_params()
        df = classification_serializers.prepare_media_table(
            queryset,
            trapper_url=params["trapper_url"],
            trapper_url_token=params["url_token"],
            host=host,
            private_human=params["private_human"],
            private_vehicle=params["private_vehicle"],
            private_species=params["private_species"],
        )
        df.to_csv(response, encoding="utf-8", index=False)
        return response


class AIClassificationResultsView(ListAPIView):
    """
    Returns a table with AI classification results.
    """

    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = AIClassificationFilter
    search_fields = [
        "clasification__resource__name",
        "=dynamic_attrs__attrs",
        "=static_attrs",
    ]
    prefetch_related = [
        "clasification__resource__deployment__location",
        "dynamic_attrs",
        "clasification__sequence",
    ]
    project = None

    def get_serializer_class(self):
        pass

    def get_extra_params(self):
        query_params = self.request.query_params
        params = {
            "trapper_format": query_params.get("trapper_format", "False") == "True",
        }
        return params

    def get_project(self):
        project_pk = self.kwargs.get("project_pk")
        self.project = get_object_or_404(ClassificationProject, pk=project_pk)

    def get_queryset(self):
        self.get_project()
        queryset = (
            AIClassification.objects.get_accessible(user=self.request.user)
            .prefetch_related(*self.prefetch_related)
            .filter(classification__project=self.project)
        )
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if queryset.count() == 0:
            raise rest_verror(
                "Empty queryset or you have no permission to access the requested objects."
            )
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=ai_observations.csv"
        if self.project:
            classificator = self.project.classificator
            if classificator:
                params = self.get_extra_params()
                df = classification_serializers.prepare_ai_results_table(
                    queryset, trapper_format=params["trapper_format"]
                )
                df.to_csv(response, encoding="utf-8", index=False)
        return response


class ClassificationResultsAggView(ListAPIView):
    """
    Returns CSV or geojson with aggregated classification results.

    extra params for aggregation:
    :agg_target_level: str, "deployment" or "location", "deployment" by default
    :event_fun: str, function name to be used when aggregating data into events, max by default
    :count_fun: str, function name to be used when aggregating, sum by default
    :count_var: str, name of classificator's attribute to be aggregated
    :all_dep: bool, whether to use all deployments from the associated research project
    :geojson: bool, whether to format output as geojson (csv is returned otherwise)
    """

    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = ClassificationFilter
    search_fields = ["resource__name", "=dynamic_attrs__attrs", "=static_attrs"]
    prefetch_related = ["resource__deployment__location", "dynamic_attrs", "sequence"]
    agg_functions = ["sum", "min", "max", "mean"]
    agg_target_levels = ["deployment", "location"]
    project = None

    def get_serializer_class(self):
        pass

    def get_extra_params(self):
        agg_params = self.request.query_params

        agg_target_level = agg_params.get("agg_target_level", "deployment")
        if agg_target_level not in self.agg_target_levels:
            raise rest_verror("agg_target_level: wrong value")

        event_fun = agg_params.get("event_fun", "max")
        if event_fun not in self.agg_functions:
            raise rest_verror("event_fun: wrong value")

        count_fun = agg_params.get("count_fun", "sum")
        if count_fun not in self.agg_functions:
            raise rest_verror("count_fun: wrong value")

        count_var = agg_params.get("count_var", "count")

        count_var_map = {
            "count": "count",
            "countNew": "count_new",
        }
        c = self.project.classificator
        attrs = c.get_numerical_attrs()
        if count_var_map[count_var] not in attrs:
            raise rest_verror("count_var: wrong value")

        all_dep = agg_params.get("all_dep") in ["True", "true"]
        filter_dep = agg_params.get("filter_dep")
        geojson = agg_params.get("geojson") in ["True", "true"]

        params = {
            "agg_target_level": agg_target_level,
            "event_fun": event_fun,
            "count_fun": count_fun,
            "count_var": count_var,
            "all_dep": all_dep,
            "filter_dep": filter_dep,
            "geojson": geojson,
        }

        return params

    def get_project(self):
        project_pk = self.kwargs.get("project_pk")
        self.project = get_object_or_404(ClassificationProject, pk=project_pk)

    def get_queryset(self):
        self.get_project()
        queryset = (
            Classification.objects.get_accessible(user=self.request.user)
            .prefetch_related(*self.prefetch_related)
            .filter(project=self.project)
        )
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if queryset.count() == 0:
            raise rest_verror(
                "Empty queryset or you have no permission to access the requested objects."
            )

        params = self.get_extra_params()
        all_deployments = params.pop("all_dep")
        filter_deployments = params.pop("filter_dep")

        deployment_pks = set(
            queryset.values_list("resource__deployment__pk", flat=True)
        )

        # include all deployments from the project, some of them may not have any records
        if all_deployments:
            project_deployments_pks = Deployment.objects.filter(
                research_project=self.project.research_project
            ).values_list("pk", flat=True)
            deployment_pks = set(list(deployment_pks) + list(project_deployments_pks))

        deployments = Deployment.objects.filter(pk__in=deployment_pks)

        if filter_deployments:
            deployments = deployments.filter(
                deployment_id__icontains=filter_deployments
            )
            queryset = queryset.filter(resource__deployment__in=deployments)

        # exclude deployments without start and end dates provided
        deployments = deployments.exclude(start_date__isnull=True).exclude(
            end_date__isnull=True
        )

        df_obs = classification_serializers.prepare_results_table(
            queryset, trapper_format=True
        )
        df_dep = classification_serializers.prepare_deployments_table(deployments)

        params.update({"df_obs": df_obs, "df_dep": df_dep})
        geo = params.pop("geojson")
        out = aggregate_results(**params)

        if geo:
            properties = out.drop(["latitude", "longitude"], axis=1).columns
            out_geojson = df_to_geojson(
                out, properties, lat="latitude", lon="longitude"
            )
            response = Response(out_geojson, content_type="application/json")
        else:
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = "attachment; filename=trap_rates.csv"
            out.to_csv(response, encoding="utf-8", index=False)

        return response


class ResultsDataPackageAPIView(APIView):
    """
    Generates a data package (or loads one from cache) and returns it as a zip file.

    :clear_cache: if True, the cached data package will be cleared and a new one will be generated
    :get_metadata: if True, only the metadata will be included in the response
    :exclude_blank: if True, the blank observations will be excluded
    :all_deployments: if True, all deployments from the ResearchProject will be included
    :filter_deployments: filter deployments by the given string in their deployment_id
    :include_events: if True, the event-based observations will be included (Camtrap DP specific)
    :export_format: the format of the data package: camtrapdp (Camtrap DP) or trapper (internal format)
    :include_ids: whether to include the '_id' column in exported tables
    :trapper_url_token: if True, the media table will include the access token for each media file
    :private_human: if True, the media table will not include the access token for human observations
    :private_vehicle: if True, the media table will not include the access token for vehicle observations
    """

    permission_classes = (permissions.IsAuthenticated,)

    def response_struct(self, message, errors, package):
        return {"data": {"message": message, "errors": errors, "package": package}}

    def get_extra_params(self):
        query_params = self.request.query_params
        params = {
            "clear_cache": query_params.get("clear_cache", "False") == "True",
            "get_metadata": query_params.get("get_metadata", "False") == "True",
            # other params
            "exclude_blank": query_params.get("exclude_blank", "False") == "True",
            "all_dep": query_params.get("all_deployments", "True") == "True",
            # filter deployments by text
            "filter_dep": query_params.get("filter_deployments", None),
            "include_events": query_params.get("include_events", "False") == "True",
            # two formats available: camtrapdp and trapper
            "export_format": query_params.get("export_format", "camtrapdp"),
            # include Trapper-specific db ids
            "include_ids": query_params.get("include_ids", "True") == "True",
            "trapper_url_token": query_params.get("trapper_url_token", "True")
            == "True",
            "private_human": query_params.get("private_human", "True") == "True",
            "private_vehicle": query_params.get("private_vehicle", "True") == "True",
        }
        return params

    def get(self, request, *args, **kwargs):
        user = request.user
        user_cache_dir = get_external_data_packages_path(user.username)
        host = self.request.build_absolute_uri("/")

        # retrieve ClassificationProject
        project_pk = self.kwargs.get("project_pk")
        try:
            self.project = ClassificationProject.objects.get(pk=project_pk)
        except ClassificationProject.DoesNotExist:
            return Response(
                self.response_struct(
                    "The requested classification project does not exist.", None, None
                ),
                status=status.HTTP_404_NOT_FOUND,
            )

        # check permissions
        if not self.project.can_view_classifications(user):
            return Response(
                self.response_struct("Permission denied.", None, None),
                status=status.HTTP_403_FORBIDDEN,
            )

        # parse request parameters
        params = self.get_extra_params()
        clear_cache = params.pop("clear_cache")
        get_metadata = params.pop("get_metadata")
        export_format = params.get("export_format")

        # generate cache parameters
        cache_name_package = (
            f"_cache_package_{user}_{self.project.pk}_{export_format}.zip"
        )
        cache_path_package = os.path.join(user_cache_dir, cache_name_package)
        cached_package_exists = os.path.exists(cache_path_package)

        # generate the data package
        try:
            if cached_package_exists and not clear_cache:
                msg = "Package succesfully loaded from cache. Set clear_cache=True to re-build it."

            else:
                package = ResultsDataPackageGenerator(
                    params, user, self.project, host=host
                ).run(return_package=True)

                # save package files as temporary files
                descriptor = package.get_descriptor()
                meta_temp_file = NamedTemporaryFile()
                with open(meta_temp_file.name, "w") as _file:
                    json.dump(descriptor, _file)

                dep_table = package.tables["deployments"]
                dep_temp_file = NamedTemporaryFile()
                dep_table.to_csv(dep_temp_file.name, index=False)

                med_table = package.tables["media"]
                med_temp_file = NamedTemporaryFile()
                med_table.to_csv(med_temp_file.name, index=False)

                obs_table = package.tables["observations"]
                obs_temp_file = NamedTemporaryFile()
                obs_table.to_csv(obs_temp_file.name, index=False)

                # compress files to a single zip
                files_to_zip = {
                    "metadata.json": meta_temp_file,
                    "deployments.csv": dep_temp_file,
                    "media.csv": med_temp_file,
                    "observations.csv": obs_temp_file,
                }
                with zipfile.ZipFile(cache_path_package, mode="w") as zf:
                    for name, file in files_to_zip.items():
                        zf.write(
                            file.name, arcname=name, compress_type=zipfile.ZIP_DEFLATED
                        )

                msg = "Package successfully generated and saved in cache. Set clear_cache=True to re-build it."

            # return metadata only
            if get_metadata:
                # Frictionless descriptor of Camtrap DP package
                with zipfile.ZipFile(cache_path_package) as zf:
                    with zf.open("metadata.json") as json_file:
                        descriptor = json.load(json_file)

                return Response(
                    self.response_struct(msg, None, {"descriptor": descriptor}),
                    status=status.HTTP_200_OK,
                )

            # return the full data package
            with open(cache_path_package, "rb") as zip_file:
                zip_data = zip_file.read()

            response = HttpResponse(zip_data, content_type="application/zip")
            return response

        except Exception as e:
            return Response(
                self.response_struct("Bad request", str(e), None),
                status=status.HTTP_400_BAD_REQUEST,
            )


class ClassificationMapViewSet(ClassificationViewSet):
    pagination_class = None
    serializer_class = classification_serializers.ClassificationMapSerializer


class ClassificatorViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of classificators."""

    permission_classes = (permissions.IsAuthenticated,)
    queryset = Classificator.objects.all()
    filterset_class = ClassificatorFilter
    serializer_class = classification_serializers.ClassificatorSerializer
    search_fields = ["name", "owner__username"]

    def get_queryset(self):
        return Classificator.objects.get_accessible(user=self.request.user)


class ClassificationProjectViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of classification projects.
    List of projects is limited only to those that are not marked as
    disabled (removed)
    """

    permission_classes = (permissions.IsAuthenticated,)
    queryset = ClassificationProject.objects.all()
    filterset_class = ClassificationProjectFilter
    serializer_class = classification_serializers.ClassificationProjectSerializer
    search_fields = ["name", "owner__username", "research_project__name"]
    select_related = ["owner", "classificator", "research_project"]

    def get_queryset(self):
        return (
            ClassificationProject.objects.get_accessible(user=self.request.user)
            .filter(disabled_at__isnull=True)
            .select_related(*self.select_related)
            .prefetch_related("classification_project_roles__user")
        )


class ClassificationProjectCollectionViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of classification project collections."""

    permission_classes = (permissions.IsAuthenticated,)
    queryset = ClassificationProjectCollection.objects.all()
    filterset_class = ClassificationProjectCollectionFilter
    serializer_class = (
        classification_serializers.ClassificationProjectCollectionSerializer
    )
    search_fields = [
        "collection__collection__name",
        "collection__collection__owner__username",
    ]

    def get_queryset(self):
        project_pk = self.kwargs["project_pk"]
        try:
            project = ClassificationProject.objects.get(pk=project_pk)
        except ClassificationProject.DoesNotExist:
            return ClassificationProject.objects.none()
        if project.can_view(user=self.request.user):
            return ClassificationProjectCollection.objects.filter(
                project=project
            ).prefetch_related(
                "collection__collection__managers",
                "collection__collection__owner",
                "project__owner",
                "project__classificator",
            )
        else:
            return ClassificationProjectCollection.objects.none()


class SequenceViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of sequences"""

    pagination_class = None
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = SequenceFilter
    serializer_class = classification_serializers.SequenceReadSerializer
    search_fields = ["name", "created_by__username"]
    prefetch_related = [
        "resources",
    ]

    def get_queryset(self):
        queryset = Sequence.objects.all().prefetch_related(*self.prefetch_related)
        return queryset


class ClassificationResourcesViewSet(PaginatedReadOnlyModelViewSet):
    """Returns a list of classified resources within classification project
    for given classification project collection. It uses a custom
    pagination mechanism to limit a number of resources in a sequence
    returned to a user.

    Unauthenticated users get empty queryset
    """

    default_size = 5
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = ClassificationFilter
    serializer_class = classification_serializers.ClassificationResourceSerializer
    select_related = ["resource", "resource__deployment__location", "sequence"]

    def get_queryset(self):
        collection_pk = self.kwargs["collection_pk"]
        collection = get_object_or_404(
            ClassificationProjectCollection, pk=collection_pk
        )
        user = self.request.user

        if user.is_authenticated:
            queryset = (
                collection.classifications.all()
                .select_related(*self.select_related)
                .prefetch_related(
                    "user_classifications",
                )
            )
        else:
            queryset = Classification.objects.none()

        return queryset

    def list(self, request, *args, **kwargs):
        resource_pk = self.request.GET.get("current")
        if not resource_pk:
            return super().list(request, *args, **kwargs)
        base_queryset = self.get_queryset()
        queryset = self.filter_queryset(base_queryset)
        pagination_data = {
            "total": base_queryset.count(),
            "filtered": queryset.count(),
        }
        resource_obj = get_object_or_404(Resource, pk=resource_pk)
        # custom pagination based on current object
        try:
            size = int(self.request.GET.get("size"))
        except (ValueError, TypeError):
            size = self.default_size
        qs_lte = (
            queryset.filter(resource__date_recorded__lte=resource_obj.date_recorded)
            .values_list("pk", flat=True)
            .order_by("resource")[: size + 1]
        )
        qs_gt = (
            queryset.filter(resource__date_recorded__gt=resource_obj.date_recorded)
            .values_list("pk", flat=True)
            .order_by("-resource")[:size]
        )
        pks = list(qs_lte)
        pks.extend(list(qs_gt))
        queryset = queryset.filter(pk__in=pks)
        serializer = self.get_serializer(queryset, many=True)
        response = {"pagination": pagination_data, "results": serializer.data}
        return Response(response)


view_classification_resources = ClassificationResourcesViewSet.as_view({"get": "list"})


class ClassificationImport(APIView):
    """
    Import expert and AI classifications to the existing classification project. Example data:
    data = {
        "file": observations csv file,
        "project_id": "1",
        "approve": True,
        "import_bboxes": True,
        "import_expert_classifications": True,
        "import_ai_classifications": True,
        "overwrite_attrs": True,
        "ai_provider_id: "1"
    }
    """

    permission_classes = (permissions.IsAuthenticated,)

    def response_struct(self, message, errors, task_id):
        return {"data": {"message": message, "errors": errors, "task_id": task_id}}

    def post(self, request):
        user = request.user
        data = request.data

        # retrieve ClassificationProject
        try:
            project = ClassificationProject.objects.get(pk=data.get("project_id"))
        except ClassificationProject.DoesNotExist:
            return Response(
                self.response_struct(
                    "The requested classification project does not exist.", None, None
                ),
                status=status.HTTP_404_NOT_FOUND,
            )

        # retrieve AIProvider if necessary
        import_ai_classifications = data.get("import_ai_classifications", False)
        if import_ai_classifications:
            try:
                ai_provider = AIProvider.objects.get(pk=data.get("ai_provider_id"))
            except AIProvider.DoesNotExist:
                return Response(
                    self.response_struct(
                        "The requested AI provider does not exist.", None, None
                    ),
                    status=status.HTTP_404_NOT_FOUND,
                )
        else:
            ai_provider = None

        form_data = {
            "project": project,
            "approve": data.get("approve", True),
            "import_bboxes": data.get("import_bboxes", True),
            "import_expert_classifications": data.get(
                "import_expert_classifications", True
            ),
            "import_ai_classifications": import_ai_classifications,
            "overwrite_attrs": data.get("overwrite_attrs", False),
            "ai_provider": ai_provider,
        }
        file_data = {
            "observations_csv": data.get("file"),
        }
        # form handles flags validation, user project admin permission check and csv data parsing
        form = ClassificationImportForm(form_data, file_data)
        if form.is_valid():
            params = {
                "data": form.cleaned_data.get("observations_df"),
                "project": project,
                "user": user,
                "approve": form.cleaned_data.get("approve"),
                "import_bboxes": form.cleaned_data.get("import_bboxes"),
                "import_expert_classifications": form.cleaned_data.get(
                    "import_expert_classifications"
                ),
                "import_ai_classifications": form.cleaned_data.get(
                    "import_ai_classifications"
                ),
                "overwrite_attrs": form.cleaned_data.get("overwrite_attrs"),
                "ai_provider": form.cleaned_data.get("ai_provider"),
            }
            importer = ClassificationImporter(**params)
            importer.validate_table()
            if not importer.report.valid:
                return Response(
                    self.response_struct(
                        "Your table could not be imported; correct all the errors and try again.",
                        importer.report,
                        None,
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
            params["data"] = importer.data
            if settings.CELERY_ENABLED:
                task = celery_import_classifications.delay(**params)
                user_task = UserTask(user=self.request.user, task_id=task.task_id)
                user_task.save()
                msg = "You have successfully run a celery task. Classifications are being imported now."
                task_id = task.task_id
            else:
                msg = celery_import_classifications(**params)
                task_id = None
            return Response(
                self.response_struct(msg, None, task_id), status=status.HTTP_201_CREATED
            )

        else:
            return Response(
                self.response_struct("Bad request", form.errors, None),
                status=status.HTTP_400_BAD_REQUEST,
            )


@api_view(["PUT"])
def classify_ai_callback(request, ai_provider_token):
    """
    Handles callback from external AI Provider
    """

    ai_provider = get_object_or_404(AIProvider, token=ai_provider_token)

    manager = get_ai_provider_manager(ai_provider)
    return manager.handle_callback(request)


@api_view(["GET"])
def classify_ai_extra_resource(
    request, ai_provider_token, classification_job_id, resource_name
):
    """
    Serves extra resources accessible by remote AI providers, eg. list of images to process
    """

    ai_provider = get_object_or_404(AIProvider, token=ai_provider_token)

    manager = get_ai_provider_manager(ai_provider)
    return manager.get_extra_resource(request, classification_job_id, resource_name)
