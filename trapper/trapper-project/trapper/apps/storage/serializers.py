# -*- coding: utf-8 -*-
"""Serializers used with storage application for DRF"""

from django.urls import reverse
from rest_framework import serializers

from trapper.apps.common.serializers import BasePKSerializer, PrettyUserField
from trapper.apps.storage.models import Resource, Collection


class ResourceNestedSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Resource` used within other
    serializers that are connected (mostly through fk) to
    :class:`apps.storage.models.Resource` model instances"""

    class Meta:
        model = Resource
        fields = ("name", "owner", "url")

    owner = PrettyUserField()
    url = serializers.ReadOnlyField(source="get_absolute_url")


class ResourceSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Resource`
    Serializer contains urls for details/delete/update resource if user
    has enough permissions
    """

    class Meta:
        model = Resource
        fields = (
            "pk",
            "name",
            "owner",
            "owner_profile",
            "resource_type",
            "date_recorded",
            "observation_type",
            "species",
            "tags",
            "url",
            "url_original",
            "mime",
            "thumbnail_url",
            # data for action columns
            "update_data",
            "detail_data",
            "delete_data",
            "date_recorded_correct",
        )

    name = serializers.ReadOnlyField(source="prefixed_name")
    date_recorded = serializers.ReadOnlyField(source="date_recorded_tz")
    owner = PrettyUserField()
    owner_profile = serializers.SerializerMethodField("get_profile")
    url = serializers.ReadOnlyField(source="get_url")
    url_original = serializers.ReadOnlyField(source="get_url_original")
    mime = serializers.ReadOnlyField(source="mime_type")
    thumbnail_url = serializers.ReadOnlyField(source="get_thumbnail_url")

    observation_type = serializers.SerializerMethodField()
    species = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()

    update_data = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()
    date_recorded_correct = serializers.ReadOnlyField(source="check_date_recorded")

    def get_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_update_data(self, item):
        """Custom method for retrieving update url"""
        return Resource.objects.api_update_context(
            item=item, user=self.context["request"].user
        )

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return Resource.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return Resource.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )

    def get_tags(self, obj):
        """Custom method for retrieving custom tags"""
        return [k.name for k in obj.tags.all()]

    def get_observation_type(self, obj):
        """Custom method for retrieving observation type tags"""
        tags = obj.data.get("observations")
        if tags:
            return set(k["observation_type"] for k in tags if k["observation_type"])
        else:
            return []

    def get_species(self, obj):
        """Custom method for retrieving species tags"""
        tags = obj.data.get("observations")
        if tags:
            return set(k["scientific_name"] for k in tags if k.get("scientific_name"))
        else:
            return []


class ResourceLocationSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Resource` used
    by the map view.
    """

    class Meta:
        model = Resource
        fields = (
            "pk",
            "name",
            "resource_type",
            "deployment",
            "date_recorded",
            "tags",
            "thumbnail_url",
            "detail_data",
        )

    name = serializers.ReadOnlyField(source="prefixed_name")
    date_recorded = serializers.ReadOnlyField(source="date_recorded_tz")
    deployment = serializers.ReadOnlyField(source="deployment.deployment_id")
    thumbnail_url = serializers.ReadOnlyField(source="get_thumbnail_url")
    tags = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return Resource.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_tags(self, obj):
        """Custom method for retrieving resource tags"""
        return [k.name for k in obj.tags.all()]


class ResourceTableSerializer(serializers.ModelSerializer):
    """"""

    class Meta:
        model = Resource
        fields = (
            "pk",
            "name",
            "date_recorded",
            "resource_type",
            "mime_type",
            "file_url",
            "loc_id",
            "loc_X",
            "loc_Y",
            "dep_id",
            "dep_start",
            "dep_end",
            "tags",
            "owner",
        )

    date_recorded = serializers.ReadOnlyField(source="date_recorded_tz")
    owner = serializers.ReadOnlyField(source="owner.username")
    file_url = serializers.ReadOnlyField(source="get_url")
    loc_id = serializers.SerializerMethodField()
    loc_X = serializers.SerializerMethodField()
    loc_Y = serializers.SerializerMethodField()
    dep_id = serializers.SerializerMethodField()
    dep_start = serializers.SerializerMethodField()
    dep_end = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()

    def get_loc_id(self, item):
        if item.deployment:
            return item.deployment.location.location_id
        return None

    def get_loc_X(self, item):
        if item.deployment:
            return round(item.deployment.location.get_x, 5)
        return None

    def get_loc_Y(self, item):
        if item.deployment:
            return round(item.deployment.location.get_y, 5)
        return None

    def get_dep_id(self, item):
        if item.deployment:
            return item.deployment.deployment_id
        return None

    def get_dep_start(self, item):
        if item.deployment:
            return item.deployment.start_date
        return None

    def get_dep_end(self, item):
        if item.deployment:
            return item.deployment.end_date
        return None

    def get_tags(self, item):
        tags_values = item.tags.values_list("name", flat=True)
        return ",".join(tags_values)


class CollectionSerializer(BasePKSerializer):
    """Serializer for :class:`storage.Collection`

    Serializer contains urls for details/delete/update collection if user
    has enough permissions
    """

    class Meta:
        model = Collection
        fields = (
            "pk",
            "name",
            "owner",
            "owner_profile",
            "status",
            "description",
            "update_data",
            "detail_data",
            "delete_data",
        )

    owner = PrettyUserField()
    owner_profile = serializers.SerializerMethodField()

    update_data = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()

    def get_owner_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_update_data(self, item):
        """Custom method for retrieving update url"""
        return Collection.objects.api_update_context(
            item=item, user=self.context["request"].user
        )

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return Collection.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return Collection.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )


class CollectionAppendSerializer(BasePKSerializer):
    class Meta:
        model = Collection
        fields = ("pk", "name")
