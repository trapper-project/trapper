# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2019-08-21 22:45
from __future__ import unicode_literals

import django.core.files.storage
from django.db import migrations, models
import trapper.apps.accounts.models


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="userprofile",
            name="has_ftp_account",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="userdatapackage",
            name="package",
            field=models.FileField(
                storage=django.core.files.storage.FileSystemStorage(
                    base_url=b"/external_media/",
                    location=b"/opt/trapper/trapper/external_media",
                ),
                upload_to=trapper.apps.accounts.models.user_data_package_upload_to,
            ),
        ),
    ]
