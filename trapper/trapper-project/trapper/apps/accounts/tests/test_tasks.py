import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from trapper.apps.accounts.tasks import celery_deactivate_user_after_2_years_tasks
from trapper.apps.accounts.tests.factories.user import UserFactory


User = get_user_model()


class TasksTest(TestCase):
    def test_deactivate_user_after_2_years_tasks(self):
        UserFactory(last_login=timezone.now(), is_active=True)
        UserFactory(last_login=datetime.datetime(2020, 1, 1), is_active=True)

        self.assertEqual(User.objects.filter(is_active=True).count(), 2)

        celery_deactivate_user_after_2_years_tasks()

        self.assertEqual(User.objects.filter(is_active=True).count(), 1)
        self.assertEqual(User.objects.filter(is_active=False).count(), 1)
