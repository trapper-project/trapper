# -*- coding: utf-8 -*-
from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter

from trapper.apps.extra_tables.views import api as api_views

app_name = "extra_tables"

router = DefaultRouter(trailing_slash=False)
router.register(r"species", api_views.SpeciesViewSet, basename="api-species")

urlpatterns = [
    re_path(r"^api/", include(router.urls)),
]
