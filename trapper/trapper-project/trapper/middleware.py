import threading
import pytz
from django.conf import settings
from django.contrib.auth.models import AnonymousUser

from django.utils import timezone

_thread_locals = threading.local()


def get_current_request():
    """Function used to retrieve request associated with current thread"""
    return getattr(_thread_locals, "request", None)


def get_current_user():
    """Function used to retrieve user stored in request associated with
    current thread"""
    request = get_current_request()
    return getattr(request, "user", None)


class ThreadLocals:
    """Middleware that gets various objects from the
    request object and saves them in thread local storage."""

    def __init__(self, get_response):
        self.get_response = get_response

    def _clear_request(self):
        """Remove request data from local thread."""
        try:
            del _thread_locals.request
        except AttributeError:
            pass

    def __call__(self, request):
        """Store request in local thread.

        WARNING!: Do not set any other data in _thread_locals
        since most probably it won't be updated in other places and
        you will spend weeks to find why software doesn't work.

        Instead of store data in request and write method that
        recovers it from current request.
        """
        _thread_locals.request = request

        response = self.get_response(request)

        # Clear this from thread after usage
        self._clear_request()
        return response

    def process_exception(self, request, exception):
        """We want to remove request from thread locals even for broken
        views. Since we don't return anything - default exception handler
        will work"""
        self._clear_request()


class TimezoneMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user
        if user is not None and not isinstance(user, AnonymousUser):
            tz = user.userprofile.timezone
            timezone.activate(tz)
        else:
            tzname = settings.TIME_ZONE
            timezone.activate(pytz.timezone(tzname))

        return self.get_response(request)
