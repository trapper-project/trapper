from django.db import migrations


def save_is_empty_as_observation_type(apps, schema_editor):
    Classification = apps.get_model("media_classification", "Classification")
    cls = Classification.objects.filter(is_empty=True)
    ClassificationDynamicAttrs = apps.get_model(
        "media_classification", "ClassificationDynamicAttrs"
    )
    c_dyn_attrs_to_create = []
    c_dyn_attrs_to_update = []
    for c in cls:
        if c.dynamic_attrs.exists():
            for c_dyn_attrs in c.dynamic_attrs.all():
                c_dyn_attrs.observation_type = "blank"
                c_dyn_attrs_to_update.append(c_dyn_attrs)
        else:
            c_dyn_attrs = ClassificationDynamicAttrs(classification=c, observation_type="blank")
            c_dyn_attrs_to_create.append(c_dyn_attrs)

    ClassificationDynamicAttrs.objects.bulk_create(
        c_dyn_attrs_to_create, batch_size=1000
    )
    ClassificationDynamicAttrs.objects.bulk_update(
        c_dyn_attrs_to_update, ["observation_type"], batch_size=1000
    )

    UserClassification = apps.get_model("media_classification", "UserClassification")
    user_cls = UserClassification.objects.filter(is_empty=True)
    UserClassificationDynamicAttrs = apps.get_model(
        "media_classification", "UserClassificationDynamicAttrs"
    )
    user_c_dyn_attrs_to_create = []
    user_c_dyn_attrs_to_update = []
    for c in user_cls:
        if c.dynamic_attrs.exists():
            for c_dyn_attrs in c.dynamic_attrs.all():
                c_dyn_attrs.observation_type = "blank"
                user_c_dyn_attrs_to_update.append(c_dyn_attrs)
        else:
            c_dyn_attrs = UserClassificationDynamicAttrs(userclassification=c, observation_type="blank")
            user_c_dyn_attrs_to_create.append(c_dyn_attrs)

    UserClassificationDynamicAttrs.objects.bulk_create(
        user_c_dyn_attrs_to_create, batch_size=1000
    )
    UserClassificationDynamicAttrs.objects.bulk_update(
        user_c_dyn_attrs_to_update, ["observation_type"], batch_size=1000
    )


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0031_add_is_setup"),
    ]

    operations = [migrations.RunPython(save_is_empty_as_observation_type)]