from cairosvg import svg2png
from django.contrib.gis.db import models
from solo.models import SingletonModel
import io
from django.core.files.images import ImageFile
from trapper.apps.common.models import SVGImageField
from django.utils.translation import gettext_lazy as _


class CitizenScienceConfiguration(SingletonModel):
    project_name = models.CharField(max_length=20)
    project_logo = SVGImageField(
        blank=True,
        null=True,
    )
    email_logo = models.ImageField(
        blank=True,
        null=True,
        editable=False,
        help_text="If project_logo is '.svg' file, app will generate png file which will be attach to emails.",
    )
    background_image = SVGImageField(
        blank=True,
        null=True,
        help_text="Background image for the web app",
    )
    sidebar_image = SVGImageField(
        blank=True,
        null=True,
        help_text="Sidebar image for the web app",
    )
    additional_styles = models.TextField(
        blank=True,
        null=True,
        help_text="Additional styles for the web app. Should be in CSS format, mainly :root section with variables",
    )
    linkedin_url = models.URLField(blank=True)
    facebook_url = models.URLField(blank=True)
    twitter_url = models.URLField(blank=True)
    build_sequence_time_interval = models.IntegerField(default=5)
    default_coordinates = models.PointField(
        srid=4326,
        verbose_name=_("Default coordinates"),
        help_text=_("Default area in Upload and Team creation form"),
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = "Citizen Science Configuration"

    def save(self, *args, **kwargs):
        if self.project_logo:
            if self.project_logo.name.endswith(".svg"):
                email_logo_name = f"{self.project_logo.name[:-3]}.png"
                bytes_png = svg2png(
                    file_obj=self.project_logo,
                    output_width=600,
                    output_height=150,
                )
                self.email_logo = ImageFile(io.BytesIO(bytes_png), name=email_logo_name)
            else:
                self.email_logo = None
        super().save(*args, **kwargs)
