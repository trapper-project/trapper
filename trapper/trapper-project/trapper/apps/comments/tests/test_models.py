from django.test import TestCase
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.comments.models import UserComment


class UserCommentModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(UserCommentModelTest, cls).setUpTestData()

        cls.user1 = UserFactory()

    def test_user_comment_model_save(self):
        user_comment = UserComment()
