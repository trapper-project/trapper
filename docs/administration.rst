.. _administration:

==============
Administration
==============

The Admin site is available in the top menu and it is only visible for users with admin permissions.

.. image:: images/administration/top_bar.png
    :alt: top menu with Admin site button

Admin actions for User model
++++++++++++++++++++++++++++

Registered users will be listed on the Admin site. They need to be activated in order to log in, and will need
project roles to be able to work on projects. All that and more can be achieved using admin actions, listed at the bottom
left corner of User changelist admin view. Before running any action, select *Users* by marking checkboxes by usernames.

.. image:: images/administration/user_admin.png
    :alt: User changelist with actions select visible

Set roles for selected users action
===================================

After selecting *Users*, action and clicking the blue *Go* button next to the action select, you will access the form.

.. image:: images/administration/set_roles_for_users.png
    :alt: set roles for selected users form

Here you can specify to which *Research projects* and *Classification projects* to add *Users*, and as which roles.
By marking the checkmark, you can also activate all selected users.

After filling the form, hit the *Submit* button in the bottom right corner.

Create/delete FTP accounts
==========================

Users will need FTP accounts in order to upload data to the Trapper server. FTP accounts for *Users* can be created
using admin actions.

After selecting *Users* and running the action, you will see a short form.

.. image:: images/administration/ftp_accounts_form.png
    :alt: create FTP accounts form

Fill the form and submit.

Creating FTP accounts both allows *Users* to log in to FTP server using Trapper account credentials, and creates
the necessary directory structure in your external media directory.

Download Species from Catalogue of Life
+++++++++++++++++++++++++++++++++++++++

*Species* table needs to be filled by the admin, so that selected *Species* can be added to *Classificators*.

You can add them manually one species at a time, use a csv file to import them, or you can run an admin action
to download taxa for chosen classes of animals from Catalogue of Life. The action is available in the *Species*
changelist admin view (*Import from COL* button in top right corner).

.. image:: images/administration/species_admin.png
    :alt: species changelist

In the form, choose classes to import taxa from and submit.

.. image:: images/administration/import_from_col.png
    :alt: import from col form

.. note::
    Species import is run asynchronously, so the imported species may not be visible right away after running the action.

Add and configure AIProvider
++++++++++++++++++++++++++++

To be able to use AI methods for classification, you need to add an *AIProvider* (for example Megadetector)
to your *Research project*.

*AIProvider* needs to be manually added and configured via admin panel first.

Go to admin panel -> *Media Classification* -> *AIProvider* -> *+ Add ai provider*.

.. image:: images/administration/ai_provider_type.png
    :alt: ai provider form part one

In the first part if the form, choose type of *AIProvider* to add.
Regardless of the chosen type, several fields are required to fill:

- name for your *AIProvider* instance
- minimum confidence: default value is 0.9, meaning that predictions from AI with confidence lower than that will not be saved
- trapper instance url: url of your Trapper server
- API url: url of AI API

Last required part of *AIProvider* configuration is adding mappings.

.. image:: images/administration/ai_provider_mappings.png
    :alt: ai provider form part two

The mappings are used to translate AI classification results to Trapper classification attributes - add the values
returned by your chosen AI API and map them to attribute values.
