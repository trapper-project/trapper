'use strict';

(function(global, namespace, moduleName) {
    
    var module = {};
    var plugins = global[namespace].Plugins;
    var alert = global[namespace].Alert;
    var modal = global[namespace].Modal;
    var uploader = global[namespace].Uploader;
    var doc = global.document;

    module.init = function() {
        console.log(moduleName + ' initialize');
    };

    module.edit = module.create = function() {
        plugins.select2();
        plugins.wysiwyg();
    };

    module.request = function() {
        plugins.wysiwyg();
    };

    module.upload = function() {
        plugins.fileInputs();
    };

    var deleteConfirm = function() {
        var deleteBtn = doc.querySelector('.btn-delete');
        if (!deleteBtn) return;
        deleteBtn.addEventListener('click', showModal);
        function showModal(e) {
            e.preventDefault();
            modal.confirm({
                title: global['Translations']['GLOBAL']['delete_collection'],
                content: global['Translations']['GLOBAL']['delete_collection_question'],
                buttons: [{
                    type: 'success',
                    label: global['Translations']['GLOBAL']['yes'],
                    onClick: function() {
                        window.location = e.target.href;
                    }
                }, {
                    type: 'danger',
                    label: global['Translations']['GLOBAL']['no']
                }]
            });
        }
    };

    module.preview = function() {
        deleteConfirm();
    };

    // if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

    // append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'Collection'));
