from django.db import migrations


def mark_homo_sapiens_observations_as_obs_type_human(apps, schema_editor):
    ClassificationDynamicAttrs = apps.get_model(
        "media_classification", "ClassificationDynamicAttrs"
    )
    cda = ClassificationDynamicAttrs.objects.filter(
        species__latin_name__icontains="homo sapiens"
    ).exclude(observation_type="human")
    cda.update(observation_type="human")

    UserClassificationDynamicAttrs = apps.get_model(
        "media_classification", "UserClassificationDynamicAttrs"
    )
    ucda = UserClassificationDynamicAttrs.objects.filter(
        species__latin_name__icontains="homo sapiens"
    ).exclude(observation_type="human")
    ucda.update(observation_type="human")


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0034_remove_is_empty_from_static_attrs_order"),
    ]

    operations = [
        migrations.RunPython(mark_homo_sapiens_observations_as_obs_type_human)
    ]
