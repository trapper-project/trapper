from django.contrib.postgres.search import SearchVector
from django.db import migrations
from django.db.models import F


def update_name_search_vectors(apps, schema_editor):
    Resource = apps.get_model("storage", "Resource")
    qs = Resource.objects.all()
    qs.annotate(vector=SearchVector("name")).update(
        search_name_vector=F("vector")
    )


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0020_auto_20220318_2106'),
    ]

    operations = [
        migrations.RunPython(update_name_search_vectors)
    ]
