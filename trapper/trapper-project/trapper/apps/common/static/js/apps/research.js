'use strict';

(function(global, namespace, moduleName) {

    var module = {};

    var plugins = global[namespace].Plugins;
    var alert = global[namespace].Alert;
    var modal = global[namespace].Modal;
    var uploader = global[namespace].Uploader;

    var doc = global.document;

    module.init = function() {
        console.log(moduleName + ' initialize');
    };

    var deleteConfirm = function() {
        var deleteBtn = doc.querySelector('.btn-delete');

        if (!deleteBtn) {
            return;
        }

        deleteBtn.addEventListener('click', showModal);

        function showModal(e) {
            e.preventDefault();

            modal.confirm({
                title: global['Translations']['GLOBAL']['delete_research_project'],
                content: global['Translations']['GLOBAL']['delete_research_project_question'],
                buttons: [{
                    type: 'success',
                    label: global['Translations']['GLOBAL']['yes'],
                    onClick: function() {
                        window.location = e.target.href;
                    }
                }, {
                    type: 'danger',
                    label: global['Translations']['GLOBAL']['no']
                }]
            });
        }
    };

    module.update = module.create = function() {
	plugins.select2();
	plugins.dynamicTable('#table-formset');
        plugins.collapsable();
	$('#table-formset .select2-formset').select2({
	    minimumResultsForSearch: 5,
	    minimumInputLength: 3,
	});
    };

    module.preview = function() {
        deleteConfirm();
        plugins.collapsable();
    };

    // if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

    // append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'Research'));
