from trapper.apps.media_classification.ai_providers.megadetector import (
    MegadetectorBatchProcessingProviderManager,
)
from trapper.apps.media_classification.ai_providers.trapper_ai import (
    TrapperAIProviderManager,
)
from trapper.apps.media_classification.models import (
    TrapperAIProvider,
    MegadetectorBatchProcessingProvider,
    AIProvider,
)


def get_ai_provider_manager(ai_provider: AIProvider):
    managers = {
        TrapperAIProvider: TrapperAIProviderManager,
        MegadetectorBatchProcessingProvider: MegadetectorBatchProcessingProviderManager,
    }

    manager = managers[type(ai_provider)](ai_provider)

    return manager
