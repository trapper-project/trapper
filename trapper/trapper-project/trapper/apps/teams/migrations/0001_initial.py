# Generated by Django 4.2.4 on 2023-10-11 22:36

from django.conf import settings
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('media_classification', '0061_classification_tracked_species_notifications_sent_at_and_more'),
        ('geomap', '0018_deployment_bait_type_deployment_is_incomplete_and_more'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=150, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description')),
                ('polygon', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=4326, verbose_name='Polygon')),
                ('classification_project', models.ForeignKey(help_text='Active workspace in Citizen Science', on_delete=django.db.models.deletion.CASCADE, related_name='classification_project_teams', to='media_classification.classificationproject', verbose_name='Classification project')),
                ('deployments', models.ManyToManyField(blank=True, related_name='team_deployments', to='geomap.deployment', verbose_name='Deployments')),
            ],
            options={
                'verbose_name': 'Team',
                'ordering': ['-date_created'],
            },
        ),
        migrations.CreateModel(
            name='TeamMembership',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('role', models.SmallIntegerField(choices=[(0, 'Team Leader'), (1, 'Member')], default=0, verbose_name='Role')),
                ('status', models.SmallIntegerField(choices=[(0, 'Pending'), (1, 'Approved'), (2, 'Rejected')], default=0, verbose_name='Status')),
                ('motivation', models.TextField(blank=True, default='', verbose_name='Motivation')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='team_memberships', to='teams.team', verbose_name='Team')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_memberships', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'Team membership',
                'ordering': ['-date_created'],
            },
        ),
        migrations.AddField(
            model_name='team',
            name='members',
            field=models.ManyToManyField(related_name='members', through='teams.TeamMembership', to=settings.AUTH_USER_MODEL, verbose_name='Members'),
        ),
        migrations.AddField(
            model_name='team',
            name='team_leader',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='team_leaders', to=settings.AUTH_USER_MODEL, verbose_name='Team Leader'),
        ),
    ]
