from django.db import migrations, models


def assign_new_values(apps, schema_editor):
    obs_type_dict = {val: val.lower() for val in ["Human", "Animal", "Vehicle"]}
    age_dict = {
        "Undefined": "unknown",
        "Adult": "adult",
        "Subadult": "subadult",
        "Juvenile": "juvenile",
        "Offspring": "offspring"
    }
    sex_dict = {
        "Undefined": "unknown",
        "Female": "female",
        "Male": "male"
    }
    behaviour_dict = {val: val.lower() for val in ["Undefined", "Grazing", "Browsing", "Rooting", "Vigilance",
                                                   "Running", "Walking"]}

    ClassificationDynamicAttrs = apps.get_model("media_classification", "ClassificationDynamicAttrs")
    UserClassificationDynamicAttrs = apps.get_model("media_classification", "UserClassificationDynamicAttrs")

    class_dyn_attrs = ClassificationDynamicAttrs.objects.all()
    user_class_dyn_attrs = UserClassificationDynamicAttrs.objects.all()

    for qs in [class_dyn_attrs, user_class_dyn_attrs]:
        for old, new in obs_type_dict.items():
            qs.filter(observation_type=old).update(observation_type=new)
        for old, new in age_dict.items():
            qs.filter(age=old).update(age=new)
        for old, new in sex_dict.items():
            qs.filter(sex=old).update(sex=new)
        for old, new in behaviour_dict.items():
            qs.filter(behaviour=old).update(behaviour=new)


class Migration(migrations.Migration):

    dependencies = [
        ('media_classification', '0019_auto_20211011_1715'),
    ]

    operations = [
        migrations.RunPython(assign_new_values)
    ]
