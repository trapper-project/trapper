# -*- coding: utf-8 -*-
"""
Helper functions to decode structured data stored by django-hstore fields
"""
import datetime
import json
from email.mime.image import MIMEImage
from typing import List, Union

import exifread
import logging
import lxml.html.clean as clean
import numpy as np
from django.conf import settings
from django.core.files.uploadedfile import TemporaryUploadedFile, InMemoryUploadedFile
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework.exceptions import ValidationError
from rest_framework.settings import api_settings

logger = logging.getLogger(__name__)


def json_loads_helper(obj):
    """Try to convert given object into json"""
    try:
        return json.loads(obj)
    except (TypeError, ValueError):
        return obj


def parse_hstore_field(data):
    """Convert hstore value stored in database into python dictionary"""
    return dict(map(lambda kv: (kv[0], json_loads_helper(kv[1])), data.iteritems()))


def datetime_aware(data=None):
    """
    Make datetime object timezone aware.
    By default return timezone aware datatime.datetime.now()
    """
    if data is None:
        data = datetime.datetime.now()
    return timezone.make_aware(data, timezone.get_current_timezone())


def parse_pks(pks):
    """Method used to parse string with comma separated numbers"""
    output = []
    if isinstance(pks, str):
        for value in pks.split(","):
            try:
                value = int(value.strip())
            except ValueError:
                pass
            else:
                output.append(value)
    return output


def clean_html(value):
    """
    Clean html value and strip potentially dangerous code using
    :class:`lxml.html.clean.Cleaner`
    """
    cleaned = ""
    if value and type(value) is str and value.strip():
        cleaner = clean.Cleaner(safe_attrs_only=True, safe_attrs=frozenset(["href"]))
        cleaned = cleaner.clean_html(value)
        # Cleaner wraps with p tag, it should be removed
        if cleaned.startswith("<p>") and cleaned.endswith("</p>"):
            cleaned = cleaned[3:-4]
    return cleaned


def df_to_geojson(df, properties, lat="latitude", lon="longitude"):
    geojson = {"type": "FeatureCollection", "features": []}
    for _, row in df.iterrows():
        feature = {
            "type": "Feature",
            "properties": {},
            "geometry": {"type": "Point", "coordinates": []},
        }
        feature["geometry"]["coordinates"] = [row[lon], row[lat]]
        for prop in properties:
            feature["properties"][prop] = row[prop]
        geojson["features"].append(feature)
    return geojson


def aggregate_results(
    df_obs,
    df_dep,
    agg_source_level="media",
    agg_target_level="deployment",
    count_var="count",
    count_fun="sum",
    event_fun="max",
    min_days=1,
):
    # OBSERVATIONS AGGREGATION

    # remove blank and unclassified observations if present
    df_obs = df_obs[~df_obs.observationType.isin(["blank", "unclassified"])]

    # filter by observation level
    df_obs = df_obs[df_obs.observationLevel == agg_source_level]

    # replace empty strings in count_var column with NaN and fill NaN with 0
    df_obs[count_var].replace("", np.nan, inplace=True)
    df_obs[count_var].fillna(0.0, inplace=True)
    df_obs[count_var] = df_obs[count_var].astype(float)

    group_by_columns = [
        "deploymentID",
        "eventID",
        "observationType",
        "scientificName",
    ]

    if agg_source_level == "media":
        # first aggregate at a single-media event level (always sum)
        g0 = (
            df_obs.groupby(["mediaID", "eventStart", "eventEnd"] + group_by_columns)
            .agg(
                {
                    count_var: "sum",
                }
            )
            .reset_index()
        )

        g1 = (
            g0.groupby(group_by_columns)
            .agg(
                {
                    count_var: event_fun,
                    "eventStart": "min",
                    "eventEnd": "max",
                }
            )
            .reset_index()
        )
    else:
        g1 = df_obs[group_by_columns + [count_var, "eventStart", "eventEnd"]]

    g1.rename(columns={count_var: "count"}, inplace=True)

    # DEPLOYMENTS/LOCATIONS AGGREGATION

    # remove deployments with days < min_days
    df_dep = df_dep[df_dep.days > min_days]

    if agg_target_level == "location":
        # estimate correct number of days per location and add as a new column "location_days" to df_dep
        df_dep["location_days"] = df_dep.groupby("locationID")["days"].transform("sum")

    # remove eventID from group_by_columns
    group_by_columns.remove("eventID")

    g2 = (
        g1.groupby(group_by_columns)
        .agg(
            {
                "count": count_fun,
            }
        )
        .reset_index()
    )

    deployments_agg = df_dep.merge(g2, how="left", on="deploymentID")

    if agg_target_level == "location":
        group_by_columns[0] = "locationID"
        deployments_agg = (
            deployments_agg.groupby(group_by_columns)
            .agg(
                {
                    "count": count_fun,
                    "location_days": "first",
                    "longitude": "first",
                    "latitude": "first",
                }
            )
            .reset_index()
        )
        # rename location_days to days
        deployments_agg.rename(columns={"location_days": "days"}, inplace=True)

    deployments_agg.drop_duplicates(inplace=True)
    deployments_agg["count"].fillna(0, inplace=True)
    deployments_agg["trapRate"] = deployments_agg["count"] / deployments_agg["days"]
    deployments_agg.trapRate.fillna(0, inplace=True)

    # replace all remaining NaNs with ""
    deployments_agg.replace(np.nan, "", inplace=True)

    # round trapRate to 5 decimal places
    deployments_agg.trapRate = deployments_agg.trapRate.round(5)

    return deployments_agg


def is_request_ajax(request):
    return request.META.get("HTTP_X_REQUESTED_WITH") == "XMLHttpRequest"


def get_image_modification_date_from_exif(
    file: Union[TemporaryUploadedFile, InMemoryUploadedFile],
):
    # extracted from EXIF, DateTimeOriginal or DateCreated, if exists
    image = file.file
    try:
        exif_data = exifread.process_file(
            image, details=False, stop_tag="DateTimeOriginal"
        )
    except KeyError:
        # https://github.com/ianare/exif-py/issues/188
        exif_data = {}
    created_date_timestamp = exif_data.get("EXIF DateTimeOriginal") or exif_data.get(
        "Image DateTimeOriginal"
    )
    if created_date_timestamp:
        created_date_timestamp = created_date_timestamp.values
    return created_date_timestamp


def non_field_errors(message: str) -> ValidationError:
    """
    Return default Rest Framework ValidationError when no field is specified
    """
    return ValidationError({api_settings.NON_FIELD_ERRORS_KEY: [message]})


class TrapperCSMail:
    """
    Universal class for sending HTML mails with attachments
    """

    def __init__(
        self, template_name: str, title: str, context: dict, user_mails: List[str]
    ):
        from trapper.apps.citizen_science.models import CitizenScienceConfiguration

        self.cs_config = CitizenScienceConfiguration.get_solo()

        if self.cs_config.email_logo:
            self.email_logo = self.cs_config.email_logo
        elif self.cs_config.project_logo:
            self.email_logo = self.cs_config.project_logo
        else:
            logger.error(
                "No email_logo or project_logo found in CitizenScienceConfiguration",
                exc_info=True,
                stack_info=True,
            )
            self.email_logo = None

        self.template_name = template_name

        self.title = self._create_title(title)

        # Extend message dict by CS project name and logo
        context["project_name"] = self.cs_config.project_name
        context["email_logo_name"] = (
            self.email_logo.name if self.email_logo else "blank"
        )
        self.html_message = render_to_string(template_name, context)

        self.user_mails = user_mails

    def _create_title(self, title):
        """Create title with prefix for email"""
        prefix = ""
        if settings.DOMAIN_NAME == "localhost":
            prefix = "[LOCALHOST]"
        elif "dev." in settings.DOMAIN_NAME:
            prefix = "[DEV]"
        elif "uat." in settings.DOMAIN_NAME:
            prefix = "[UAT]"

        if self.cs_config.project_name:
            prefix = f"[{self.cs_config.project_name}]{prefix}"

        if prefix:
            return f"{prefix} {title}"
        else:
            return title

    def send(self):
        msg = EmailMultiAlternatives(
            self.title, self.html_message, settings.DEFAULT_FROM_EMAIL
        )
        # Send via "to" if single mail is the list
        # Otherwise use "bcc" for more than one
        if len(self.user_mails) > 1:
            msg.bcc = self.user_mails
        else:
            msg.to = self.user_mails

        msg.content_subtype = "html"
        msg.mixed_subtype = "related"
        if self.email_logo:
            try:
                image = MIMEImage(self.email_logo.read())
            except TypeError:
                image = MIMEImage(self.cs_config.project_logo.read(), _subtype="jpeg")
            msg.attach(image)
            image.add_header("Content-ID", f"<{self.email_logo.name}>")

        return msg.send()


def get_token(value: int):
    """
    Return base64 encoded value
    """

    return urlsafe_base64_encode(force_bytes(str(value)))
