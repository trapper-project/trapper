import itertools
from typing import List
from attr import dataclass
from django.contrib.auth import get_user_model
from django.test import TestCase
from trapper.apps.extra_tables.tests.factories import SpeciesFactory
from trapper.apps.media_classification.models import (
    AIClassification,
    AIClassificationDynamicAttrs,
    Classification,
    ClassificationDynamicAttrs,
)

from trapper.apps.media_classification.tasks.classification_approvals import (
    ApproveAIClassifications,
)
from trapper.apps.media_classification.tests.factories.ai_classification import (
    AIClassificationDynamicAttrsFactory,
    AIClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationDynamicAttrsFactory,
    ClassificationFactory,
)

User = get_user_model()


@dataclass
class BBox:
    x: int
    y: int
    w: int
    h: int

    def to_trapper_format(self):
        return [[self.x, self.y, self.w, self.h]]


class AIClassificationTestCaseBuilder:
    def __init__(self, classification: Classification):
        self.classification = AIClassificationFactory(classification=classification)

    def dattr(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, BBox):
                kwargs["bboxes"] = arg.to_trapper_format()
            elif arg in ["animal", "human", "vehicle"]:
                kwargs["observation_type"] = arg
            elif isinstance(arg, float):
                kwargs["classification_confidence"] = arg
            else:
                raise ValueError(f"Unknown argument: {arg}")
        AIClassificationDynamicAttrsFactory(
            ai_classification=self.classification, **kwargs
        )
        return self

    def build(self):
        return self.classification


class ClassificationTestCaseBuilder:
    def __init__(self, project_id: int, user_id: int):
        self.project_id = project_id
        self.user_id = user_id
        self.classification = ClassificationFactory(project_id=project_id)

    def dattr(self, *args, **kwargs):
        for arg in args:
            if isinstance(arg, BBox):
                kwargs["bboxes"] = arg.to_trapper_format()
            elif arg in ["animal", "human", "vehicle"]:
                kwargs["observation_type"] = arg
            elif isinstance(arg, float):
                kwargs["classification_confidence"] = arg
            else:
                raise ValueError(f"Unknown argument: {arg}")
        ClassificationDynamicAttrsFactory(classification=self.classification, **kwargs)
        return self

    def ai(self):
        return AIClassificationTestCaseBuilder(self.classification)

    def build(self) -> Classification:
        return self.classification


class TestApproveAIClassifications(TestCase):
    fixtures = [
        "ai_classification_tests_fixtures.yaml",
        "ai_classification_approval_fixtures.yaml",
    ]

    def setUp(self) -> None:
        super().setUp()
        self.project_owner = User.objects.get(pk=1)
        self.some_user = User.objects.get(pk=2)

    def _run_ai_approval_process(
        self,
        fields_to_copy,
        mark_as_approved=False,
        minimum_confidence=0,
        overwrite_attrs=True,
        copy_bboxes=True,
    ):
        ai_classification = AIClassification.objects.get(pk=123)
        appr = ApproveAIClassifications(
            user=self.project_owner,
            project_id=4,
            ai_classification_pks=[123],
            fields_to_copy=fields_to_copy,
            minimum_confidence=minimum_confidence,
            mark_as_approved=mark_as_approved,
            overwrite_attrs=overwrite_attrs,
            copy_bboxes=copy_bboxes,
        )

        appr.run()

        classification: Classification = Classification.objects.get(pk=72)
        dynamic_attrs = list(
            ClassificationDynamicAttrs.objects.filter(classification=classification)
        )
        ai_dynamic_attrs = list(
            AIClassificationDynamicAttrs.objects.filter(
                ai_classification=ai_classification
            )
        )

        return classification, dynamic_attrs, ai_dynamic_attrs

    def test_ai_classification_copy_parameters(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
            ],
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ai.observation_type
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

        assert classification.approved_by is None
        assert classification.status_ai == False

    def test_ai_classification_approve(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
            ],
            mark_as_approved=True,
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ai.observation_type
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

        assert classification.approved_ai_by == self.project_owner
        assert classification.status_ai == True
        assert classification.approved_source_ai.id == 123

    def test_ai_classification_copy_parameters_skip_some(self):
        _, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "species",
                "count",
            ],
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ""  # skipped, so should not be copied
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

    def test_ai_classification_copy_bounding_boxes(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[],
        )

        assert classification.has_bboxes == True

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.bboxes == ai.bboxes

    def test_ai_classification_skip_bounding_boxes(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[],
            copy_bboxes=False,
        )

        assert classification.has_bboxes == False

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.bboxes == []
            assert ai.bboxes != []

    def test_ai_classification_copy_minimum_confidence(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
            ],
            minimum_confidence=0.90,
        )

        assert classification.has_bboxes == True

        assert len(dynamic_attrs) == 1
        assert len(ai_dynamic_attrs) == 2

    def test_ai_classification_overwrite_attrs(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
            ],
            overwrite_attrs=True,
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ai.observation_type
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

        assert classification.approved_ai_by is None
        assert classification.status_ai == False

    def test_ai_classification_skip_overwriting_args(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
            ],
            overwrite_attrs=False,
            mark_as_approved=True,
            copy_bboxes=False,
        )
        assert len(dynamic_attrs) == 0
        assert classification.approved_ai_by is not None
        assert classification.status_ai == True

    def test_ai_classification_merge_bboxes(self):
        base_confidence = 0.80
        classification = (
            ClassificationTestCaseBuilder(4, 1)
            .dattr(BBox(0.1, 0.1, 0.2, 0.3), "animal", base_confidence)
            .dattr(BBox(0.6, 0.5, 0.3, 0.3), "animal", base_confidence)
            .build()
        )

        species1 = SpeciesFactory()
        species2 = SpeciesFactory()

        def assert_approval_status(approve, ai_cls):
            if approve:
                self.assertEqual(classification.approved_source_ai_id, ai_cls.pk)
            else:
                self.assertNotEqual(classification.approved_source_ai_id, ai_cls.pk)

        def assert_classification_confidence(overwrite_confidence, confidences):
            dynamic_attrs = list(
                ClassificationDynamicAttrs.objects.filter(classification=classification)
            )
            for dynamic_attr, confidence in zip(dynamic_attrs, confidences):
                if overwrite_confidence:
                    self.assertAlmostEqual(
                        float(dynamic_attr.classification_confidence), confidence
                    )
                else:
                    self.assertAlmostEqual(
                        float(dynamic_attr.classification_confidence), base_confidence
                    )

                species_confidence = dynamic_attr.attr_confidence.get("species", None)
                self.assertAlmostEqual(species_confidence, confidence)

        def assert_species_id(species_ids):
            dynamic_attrs = list(
                ClassificationDynamicAttrs.objects.filter(classification=classification)
            )
            for dynamic_attr, species_id in zip(dynamic_attrs, species_ids):
                self.assertEqual(dynamic_attr.species_id, species_id)

        # each combination
        for mark_as_approved, overwrite_confidence, copy_bboxes in itertools.product(
            [True, False], repeat=3
        ):
            with self.subTest(
                mark_as_approved=mark_as_approved,
                overwrite_confidence=overwrite_confidence,
                copy_bboxes=copy_bboxes,
            ):
                with self.subTest("merge overlapping in order"):
                    b = ClassificationTestCaseBuilder(4, 1)
                    classification = (
                        b.dattr(BBox(0.1, 0.1, 0.2, 0.3), "animal", base_confidence)
                        .dattr(BBox(0.6, 0.5, 0.3, 0.3), "animal", base_confidence)
                        .build()
                    )
                    ai_classification_species1 = (
                        b.ai()
                        .dattr(BBox(0.1, 0.1, 0.2, 0.3), 0.85, species=species1)
                        .dattr(BBox(0.61, 0.51, 0.3, 0.3), 0.95, species=species2)
                        .build()
                    )
                    classification.approve_ai_classification(
                        ai_classification=ai_classification_species1,
                        fields_to_copy=["species"],
                        minimum_confidence=0.1,
                        mark_as_approved=mark_as_approved,
                        overwrite_attrs=True,
                        commit=True,
                        merge_bboxes=True,
                        overwrite_confidence=overwrite_confidence,
                        copy_bboxes=copy_bboxes,
                    )

                    assert_species_id([species1.id, species2.id])
                    assert_classification_confidence(overwrite_confidence, [0.85, 0.95])
                    assert_approval_status(mark_as_approved, ai_classification_species1)

                with self.subTest("merge overlapping not in order"):
                    b = ClassificationTestCaseBuilder(4, 1)
                    classification = (
                        b.dattr(BBox(0.1, 0.1, 0.2, 0.3), "animal", base_confidence)
                        .dattr(BBox(0.6, 0.5, 0.3, 0.3), "animal", base_confidence)
                        .build()
                    )
                    ai_classification_species2 = (
                        b.ai()
                        .dattr(BBox(0.61, 0.51, 0.3, 0.3), 0.95, species=species1)
                        .dattr(BBox(0.1, 0.1, 0.2, 0.3), 0.85, species=species2)
                        .build()
                    )
                    classification.approve_ai_classification(
                        ai_classification=ai_classification_species2,
                        fields_to_copy=["species"],
                        minimum_confidence=0.1,
                        mark_as_approved=mark_as_approved,
                        overwrite_attrs=True,
                        commit=True,
                        merge_bboxes=True,
                        overwrite_confidence=overwrite_confidence,
                        copy_bboxes=copy_bboxes,
                    )
                    assert_species_id([species2.id, species1.id])
                    assert_approval_status(mark_as_approved, ai_classification_species2)
                    assert_classification_confidence(overwrite_confidence, [0.85, 0.95])
