from django.contrib.postgres.search import SearchQuery, SearchVector
from django.db import migrations, models


def update_data_scientific_name(apps, schema_editor):
    Resource = apps.get_model("storage", "Resource")
    qs = Resource.objects.filter(search_data_vector=SearchQuery("species_latin"))
    for res in qs:
        data = res.data
        for obs in data["observations"]:
            if "species_latin" in obs:
                scientific_name = obs.pop("species_latin")
                obs["scientific_name"] = scientific_name
        res.data = data
        res.search_data_vector = SearchVector("data")
    Resource.objects.bulk_update(qs, ["data", "search_data_vector"], batch_size=1000)


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0019_resource_humans_blurred'),
    ]

    operations = [
        migrations.RunPython(update_data_scientific_name)
    ]
