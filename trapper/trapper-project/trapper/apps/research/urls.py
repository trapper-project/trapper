# -*- coding: utf-8 -*-

from django.conf.urls import include
from django.urls import reverse_lazy, re_path
from django.views.generic import RedirectView

from rest_framework.routers import DefaultRouter

from trapper.apps.research.views import api as api_views
from trapper.apps.research.views import research as research_views

app_name = "research"

router = DefaultRouter(trailing_slash=False)
router.register(
    r"projects", api_views.ResearchProjectViewSet, basename="api-research-project"
)
router.register(
    r"project/(?P<project_pk>\d+)/collections",
    api_views.ResearchProjectCollectionViewSet,
    basename="api-research-project-collection",
)

urlpatterns = [
    re_path(r"^api/", include(router.urls)),
    re_path(
        r"^$",
        RedirectView.as_view(url=reverse_lazy("research:project_list")),
        name="project_index",
    ),
]

urlpatterns += [
    re_path(
        r"project/list/$",
        research_views.view_research_project_list,
        name="project_list",
    ),
    re_path(
        r"project/detail/(?P<pk>\d+)/$",
        research_views.view_research_project_detail,
        name="project_detail",
    ),
    re_path(
        r"project/create/$",
        research_views.view_research_project_create,
        name="project_create",
    ),
    re_path(
        r"project/update/(?P<pk>\d+)/$",
        research_views.view_research_project_update,
        name="project_update",
    ),
    re_path(
        r"project/delete/(?P<pk>\d+)/$",
        research_views.view_research_project_delete,
        name="project_delete",
    ),
    re_path(
        r"^project/delete/$",
        research_views.view_research_project_delete,
        name="project_delete_multiple",
    ),
    re_path(
        r"project/collection/add/$",
        research_views.view_research_project_collection_add,
        name="project_collection_add",
    ),
    re_path(
        r"^project/collection/delete/$",
        research_views.view_research_project_collection_delete,
        name="project_collection_delete_multiple",
    ),
    re_path(
        r"^project/collection/delete/(?P<pk>\d+)/$",
        research_views.view_research_project_collection_delete,
        name="project_collection_delete",
    ),
]
