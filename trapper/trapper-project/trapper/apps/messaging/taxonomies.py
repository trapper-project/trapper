# -*- coding: utf-8 -*-
from django.utils.translation import gettext_lazy as _


class MessageType(object):
    """
    Contain all variables used to define message type.
    """

    STANDARD = 1
    RESOURCE_REQUEST = 2
    COLLECTION_REQUEST = 3
    RESOURCE_DELETED = 4
    COLLECTION_DELETED = 5
    RESEARCH_PROJECT_CREATED = 6

    CHOICES = (
        (STANDARD, _("Standard message")),
        (RESOURCE_REQUEST, _("Resource request")),
        (COLLECTION_REQUEST, _("Collection request")),
        (RESOURCE_DELETED, _("Resource deleted")),
        (COLLECTION_DELETED, _("Collection deleted")),
        (RESEARCH_PROJECT_CREATED, _("Research project created")),
    )

    DICT_CHOICES = dict(CHOICES)


class MessageApproveStatus(object):
    APPROVED = True
    DECLINED = False
    UNKNOWN = None

    CHOICES = (
        (APPROVED, _("Approved")),
        (DECLINED, _("Declined")),
        (UNKNOWN, _("Not processed yet")),
    )
