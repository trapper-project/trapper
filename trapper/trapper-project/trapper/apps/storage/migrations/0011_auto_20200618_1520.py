# Generated by Django 2.2.11 on 2020-06-18 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("storage", "0010_auto_20200616_1248"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="resource",
            options={"ordering": ("-date_recorded",)},
        ),
        migrations.AddIndex(
            model_name="resource",
            index=models.Index(
                fields=["deployment", "-date_recorded"],
                name="storage_res_deploym_e0e820_idx",
            ),
        ),
    ]
