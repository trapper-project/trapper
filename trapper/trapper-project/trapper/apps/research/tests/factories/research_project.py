import datetime

import factory

from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.research.models import ResearchProject


class ResearchProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ResearchProject
        exclude = ("now",)

    owner = factory.SubFactory(UserFactory)

    name = factory.Sequence(lambda n: f"research_project_{n}")
    acronym = factory.Sequence(lambda n: f"rp_{n}")
    description = factory.Faker("paragraph", nb_sentences=3)
    abstract = factory.Faker("paragraph", nb_sentences=3)
    methods = factory.Faker("paragraph", nb_sentences=3)

    sampling_design = 1
    sensor_method = 1
    animal_types = 1
    bait_use = 1
    event_interval = 0

    now = factory.LazyFunction(timezone.now)
    date_created = factory.LazyAttribute(
        lambda o: o.now - datetime.timedelta(minutes=10)
    )

    status = True
    status_date = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=5))
