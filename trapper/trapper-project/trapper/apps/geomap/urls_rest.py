from django.urls import path

from trapper.apps.geomap.views.views_rest import (
    CSDeploymentProcessingProgressView,
    CSDeploymentsView,
    CSLocationsView,
    CSDeploymentView,
    CSDeploymentUpdateTimestampsView,
)


urlpatterns = [
    path("<slug:cs_project_slug>/", CSDeploymentsView.as_view(), name="cs_deployments"),
    path(
        "<slug:cs_project_slug>/locations/",
        CSLocationsView.as_view(),
        name="cs_locations",
    ),
    path(
        "<slug:cs_project_slug>/<int:deployment_pk>/progress/",
        CSDeploymentProcessingProgressView.as_view(),
        name="cs_deployment",
    ),
    path(
        "<slug:cs_project_slug>/<int:deployment_pk>/",
        CSDeploymentView.as_view(),
        name="cs_deployment",
    ),
    path(
        "<slug:cs_project_slug>/<int:deployment_pk>/update_timestamps/",
        CSDeploymentUpdateTimestampsView.as_view(),
        name="cs_deployment_update_timestamp",
    ),
]
