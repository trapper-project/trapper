from django.db import migrations, models


def add_is_setup_to_classificators(apps, schema_editor):
    Classificator = apps.get_model("media_classification", "Classificator")
    classificators = Classificator.objects.exclude(
        static_attrs_order__contains="is_setup"
    )
    for cls in classificators:
        cls.static_attrs_order += ",is_setup"
    Classificator.objects.bulk_update(
        classificators, fields=["static_attrs_order"], batch_size=1000
    )


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0030_auto_20211201_1543"),
    ]

    operations = [migrations.RunPython(add_is_setup_to_classificators)]
