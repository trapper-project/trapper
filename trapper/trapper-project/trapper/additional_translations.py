# Helper file for generating translation for dynamic values and missing values of installed packages
# during `makemessages` django command.

from django.utils.translation import gettext_lazy as _

_("Celery Results")
_("Periodic Tasks")
_("Process collection upload")
_("Taggit")
_("Task results")
_("Upload thumbnails")
_("Observation")
